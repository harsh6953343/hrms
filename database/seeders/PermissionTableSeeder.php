<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $permissions = [
            [
                'id' => 1,
                'name' => 'dashboard.view',
                'group' => 'dashboard',
            ],
            [
                'id' => 2,
                'name' => 'users.view',
                'group' => 'users',
            ],
            [
                'id' => 3,
                'name' => 'users.create',
                'group' => 'users',
            ],
            [
                'id' => 4,
                'name' => 'users.edit',
                'group' => 'users',
            ],
            [
                'id' => 5,
                'name' => 'users.delete',
                'group' => 'users',
            ],
            [
                'id' => 6,
                'name' => 'users.toggle_status',
                'group' => 'users',
            ],
            [
                'id' => 7,
                'name' => 'users.change_password',
                'group' => 'users',
            ],
            [
                'id' => 8,
                'name' => 'roles.view',
                'group' => 'roles',
            ],
            [
                'id' => 9,
                'name' => 'roles.create',
                'group' => 'roles',
            ],
            [
                'id' => 10,
                'name' => 'roles.edit',
                'group' => 'roles',
            ],
            [
                'id' => 11,
                'name' => 'roles.delete',
                'group' => 'roles',
            ],
            [
                'id' => 12,
                'name' => 'roles.assign',
                'group' => 'roles',
            ],
            [
                'id' => 13,
                'name' => 'wards.view',
                'group' => 'wards',
            ],
            [
                'id' => 14,
                'name' => 'wards.create',
                'group' => 'wards',
            ],
            [
                'id' => 15,
                'name' => 'wards.edit',
                'group' => 'wards',
            ],
            [
                'id' => 16,
                'name' => 'wards.delete',
                'group' => 'wards',
            ],
            [
                'id' => 17,
                'name' => 'departments.view',
                'group' => 'departments',
            ],
            [
                'id' => 18,
                'name' => 'departments.create',
                'group' => 'departments',
            ],
            [
                'id' => 19,
                'name' => 'departments.edit',
                'group' => 'departments',
            ],
            [
                'id' => 20,
                'name' => 'departments.delete',
                'group' => 'departments',
            ],
            [
                'id' => 21,
                'name' => 'class.view',
                'group' => 'class',
            ],
            [
                'id' => 22,
                'name' => 'class.create',
                'group' => 'class',
            ],
            [
                'id' => 23,
                'name' => 'class.edit',
                'group' => 'class',
            ],
            [
                'id' => 24,
                'name' => 'class.delete',
                'group' => 'class',
            ],
            [
                'id' => 25,
                'name' => 'bank.view',
                'group' => 'bank',
            ],
            [
                'id' => 26,
                'name' => 'bank.create',
                'group' => 'bank',
            ],
            [
                'id' => 27,
                'name' => 'bank.edit',
                'group' => 'bank',
            ],
            [
                'id' => 28,
                'name' => 'bank.delete',
                'group' => 'bank',
            ],
            [
                'id' => 29,
                'name' => 'financial_year.view',
                'group' => 'financial_year',
            ],
            [
                'id' => 30,
                'name' => 'financial_year.create',
                'group' => 'financial_year',
            ],
            [
                'id' => 31,
                'name' => 'financial_year.edit',
                'group' => 'financial_year',
            ],
            [
                'id' => 32,
                'name' => 'financial_year.delete',
                'group' => 'financial_year',
            ],
            [
                'id' => 33,
                'name' => 'allowance.view',
                'group' => 'allowance',
            ],
            [
                'id' => 34,
                'name' => 'allowance.create',
                'group' => 'allowance',
            ],
            [
                'id' => 35,
                'name' => 'allowance.edit',
                'group' => 'allowance',
            ],
            [
                'id' => 36,
                'name' => 'allowance.delete',
                'group' => 'allowance',
            ],
            [
                'id' => 37,
                'name' => 'deduction.view',
                'group' => 'deduction',
            ],
            [
                'id' => 38,
                'name' => 'deduction.create',
                'group' => 'deduction',
            ],
            [
                'id' => 39,
                'name' => 'deduction.edit',
                'group' => 'deduction',
            ],
            [
                'id' => 40,
                'name' => 'deduction.delete',
                'group' => 'deduction',
            ],
            [
                'id' => 41,
                'name' => 'loan.view',
                'group' => 'loan',
            ],
            [
                'id' => 42,
                'name' => 'loan.create',
                'group' => 'loan',
            ],
            [
                'id' => 43,
                'name' => 'loan.edit',
                'group' => 'loan',
            ],
            [
                'id' => 44,
                'name' => 'loan.delete',
                'group' => 'loan',
            ],
            [
                'id' => 45,
                'name' => 'designation.view',
                'group' => 'designation',
            ],
            [
                'id' => 46,
                'name' => 'designation.create',
                'group' => 'designation',
            ],
            [
                'id' => 47,
                'name' => 'designation.edit',
                'group' => 'designation',
            ],
            [
                'id' => 48,
                'name' => 'designation.delete',
                'group' => 'designation',
            ],
            [
                'id' => 49,
                'name' => 'leaveType.view',
                'group' => 'leaveType',
            ],
            [
                'id' => 50,
                'name' => 'leaveType.create',
                'group' => 'leaveType',
            ],
            [
                'id' => 51,
                'name' => 'leaveType.edit',
                'group' => 'leaveType',
            ],
            [
                'id' => 52,
                'name' => 'leaveType.delete',
                'group' => 'leaveType',
            ],
            [
                'id' => 53,
                'name' => 'employee.view',
                'group' => 'employee',
            ],
            [
                'id' => 54,
                'name' => 'employee.create',
                'group' => 'employee',
            ],
            [
                'id' => 55,
                'name' => 'employee.edit',
                'group' => 'employee',
            ],
            [
                'id' => 56,
                'name' => 'employee.delete',
                'group' => 'employee',
            ],
            [
                'id' => 57,
                'name' => 'pay_scale.view',
                'group' => 'pay_scale',
            ],
            [
                'id' => 58,
                'name' => 'pay_scale.create',
                'group' => 'pay_scale',
            ],
            [
                'id' => 60,
                'name' => 'pay_scale.delete',
                'group' => 'pay_scale',
            ],
            [
                'id' => 61,
                'name' => 'status.view',
                'group' => 'status',
            ],
            [
                'id' => 62,
                'name' => 'status.create',
                'group' => 'status',
            ],
            [
                'id' => 63,
                'name' => 'status.edit',
                'group' => 'status',
            ],
            [
                'id' => 64,
                'name' => 'status.delete',
                'group' => 'status',
            ],
            [
                'id' => 65,
                'name' => 'employee_status.view',
                'group' => 'employee_status',
            ],
            [
                'id' => 66,
                'name' => 'employee_status.create',
                'group' => 'employee_status',
            ],
            [
                'id' => 67,
                'name' => 'employee_status.edit',
                'group' => 'employee_status',
            ],
            [
                'id' => 68,
                'name' => 'employee_status.delete',
                'group' => 'employee_status',
            ],
            [
                'id' => 69,
                'name' => 'employee-salary.view',
                'group' => 'employee-salary',
            ],
            [
                'id' => 70,
                'name' => 'employee-salary.create',
                'group' => 'employee-salary',
            ],
            [
                'id' => 71,
                'name' => 'employee-salary.edit',
                'group' => 'employee-salary',
            ],
            [
                'id' => 72,
                'name' => 'employee-salary.delete',
                'group' => 'employee-salary',
            ],
            [
                'id' => 73,
                'name' => 'basic-salary-increment.view',
                'group' => 'basic-salary-increment',
            ],
            [
                'id' => 74,
                'name' => 'basic-salary-increment.create',
                'group' => 'basic-salary-increment',
            ],
            [
                'id' => 75,
                'name' => 'allowance-increment.view',
                'group' => 'allowance-increment',
            ],
            [
                'id' => 76,
                'name' => 'allowance-increment.create',
                'group' => 'allowance-increment',
            ],
            [
                'id' => 77,
                'name' => 'deduction-increment.view',
                'group' => 'deduction-increment',
            ],
            [
                'id' => 78,
                'name' => 'deduction-increment.create',
                'group' => 'deduction-increment',
            ],
            [
                'id' => 79,
                'name' => 'employee-loans.view',
                'group' => 'employee-loans',
            ],
            [
                'id' => 80,
                'name' => 'employee-loans.create',
                'group' => 'employee-loans',
            ],
            [
                'id' => 81,
                'name' => 'employee-loans.stop',
                'group' => 'employee-loans',
            ],
            [
                'id' => 82,
                'name' => 'employee-monthly-loans.view',
                'group' => 'employee-monthly-loans',
            ],
            [
                'id' => 83,
                'name' => 'employee-monthly-loans.stop',
                'group' => 'employee-monthly-loans',
            ],
            [
                'id' => 84,
                'name' => 'freeze-attendance.view',
                'group' => 'freeze-attendance',
            ],
            [
                'id' => 85,
                'name' => 'freeze-attendance.create',
                'group' => 'freeze-attendance',
            ],
            [
                'id' => 86,
                'name' => 'freeze-attendance.edit',
                'group' => 'freeze-attendance',
            ],
            [
                'id' => 87,
                'name' => 'freeze-attendance.delete',
                'group' => 'freeze-attendance',
            ],
            [
                'id' => 88,
                'name' => 'salary-slips.view',
                'group' => 'salary-slips',
            ],
            [
                'id' => 89,
                'name' => 'bank-statement.view',
                'group' => 'bank-statement',
            ],
            [
                'id' => 90,
                'name' => 'pay-sheet-pdf.view',
                'group' => 'pay-sheet-pdf',
            ],
            [
                'id' => 91,
                'name' => 'bank-deduction-employee-report.view',
                'group' => 'bank-deduction-employee-report',
            ],
            [
                'id' => 92,
                'name' => 'bank-deduction-report.view',
                'group' => 'bank-deduction-report',
            ],
            [
                'id' => 93,
                'name' => 'summary-department-report.view',
                'group' => 'summary-department-report',
            ],
            [
                'id' => 94,
                'name' => 'summary-ward-report.view',
                'group' => 'summary-ward-report',
            ],
            [
                'id' => 95,
                'name' => 'summary-report.view',
                'group' => 'summary-report',
            ],
            [
                'id' => 96,
                'name' => 'pay-sheet-excel.view',
                'group' => 'pay-sheet-excel',
            ],
            [
                'id' => 97,
                'name' => 'allowance-report.view',
                'group' => 'allowance-report',
            ],
            [
                'id' => 98,
                'name' => 'deduction-report.view',
                'group' => 'deduction-report',
            ],
            [
                'id' => 99,
                'name' => 'supplimentary-bill.view',
                'group' => 'supplimentary-bill',
            ],
            [
                'id' => 100,
                'name' => 'supplimentary-bill.create',
                'group' => 'supplimentary-bill',
            ],
            [
                'id' => 101,
                'name' => 'supplimentary-bill.show',
                'group' => 'supplimentary-bill',
            ],
            [
                'id' => 102,
                'name' => 'supplimentary-bill.delete',
                'group' => 'supplimentary-bill',
            ],
            // Pension
            [
                'id' => 103,
                'name' => 'pension.view',
                'group' => 'pension',
            ],
            [
                'id' => 104,
                'name' => 'pension.create',
                'group' => 'pension',
            ],
            [
                'id' => 105,
                'name' => 'pension.edit',
                'group' => 'pension',
            ],
            [
                'id' => 106,
                'name' => 'pension.delete',
                'group' => 'pension',
            ],
            // Freeze Pension
            [
                'id' => 107,
                'name' => 'freeze-pension.view',
                'group' => 'freeze-pension',
            ],
            [
                'id' => 108,
                'name' => 'freeze-pension.create',
                'group' => 'freeze-pension',
            ],
            [
                'id' => 109,
                'name' => 'freeze-pension.show',
                'group' => 'freeze-pension',
            ],
            [
                'id' => 110,
                'name' => 'freeze-pension.delete',
                'group' => 'freeze-pension',
            ],
            [
                'id' => 111,
                'name' => 'pension-bank-statement.view',
                'group' => 'pension-bank-statement',
            ],
            [
                'id' => 112,
                'name' => 'pensioner-bill.view',
                'group' => 'pensioner-bill',
            ],
            [
                'id' => 113,
                'name' => 'grand-total-pdf.view',
                'group' => 'grand-total-pdf',
            ],

            // PF
            [
                'id' => 114,
                'name' => 'pf-opening-balance.view',
                'group' => 'pf-opening-balance',
            ],
            [
                'id' => 115,
                'name' => 'pf-opening-balance.create',
                'group' => 'pf-opening-balance',
            ],
            [
                'id' => 116,
                'name' => 'pf-opening-balance.delete',
                'group' => 'pf-opening-balance',
            ],
            [
                'id' => 117,
                'name' => 'pf-department-loan.create',
                'group' => 'pf-department-loan',
            ],
            [
                'id' => 118,
                'name' => 'generate-pf-report.create',
                'group' => 'generate-pf-report',
            ],
            [
                'id' => 119,
                'name' => 'pf-report.view',
                'group' => 'pf-report',
            ],
            [
                'id' => 120,
                'name' => 'pf-closing.create',
                'group' => 'pf-closing',
            ],
            // Remainig Master
            [
                'id' => 121,
                'name' => 'pension-bank.view',
                'group' => 'pension-bank',
            ],
            [
                'id' => 122,
                'name' => 'pension-bank.create',
                'group' => 'pension-bank',
            ],
            [
                'id' => 123,
                'name' => 'pension-bank.edit',
                'group' => 'pension-bank',
            ],
            [
                'id' => 124,
                'name' => 'pension-bank.delete',
                'group' => 'pension-bank',
            ],
            [
                'id' => 125,
                'name' => 'intrest-rate.view',
                'group' => 'intrest-rate',
            ],
            [
                'id' => 126,
                'name' => 'intrest-rate.create',
                'group' => 'intrest-rate',
            ],
            [
                'id' => 127,
                'name' => 'intrest-rate.edit',
                'group' => 'intrest-rate',
            ],
            [
                'id' => 128,
                'name' => 'intrest-rate.delete',
                'group' => 'intrest-rate',
            ],
            [
                'id' => 129,
                'name' => 'document.view',
                'group' => 'document',
            ],
            [
                'id' => 130,
                'name' => 'document.create',
                'group' => 'document',
            ],
            [
                'id' => 131,
                'name' => 'document.edit',
                'group' => 'document',
            ],
            [
                'id' => 132,
                'name' => 'document.delete',
                'group' => 'document',
            ],
            [
                'id' => 133,
                'name' => 'retirement-report.view',
                'group' => 'retirement-report',
            ],

            // Employee LIC
            [
                'id' => 134,
                'name' => 'lic-dedcution.view',
                'group' => 'lic-dedcution',
            ],
            [
                'id' => 135,
                'name' => 'lic-dedcution.create',
                'group' => 'lic-dedcution',
            ],
            [
                'id' => 136,
                'name' => 'lic-dedcution.stop',
                'group' => 'lic-dedcution',
            ],
            [
                'id' => 137,
                'name' => 'employee-monthly-lic.view',
                'group' => 'employee-monthly-lic',
            ],
            [
                'id' => 138,
                'name' => 'employee-monthly-lic.stop',
                'group' => 'employee-monthly-lic',
            ],
            [
                'id' => 139,
                'name' => 'freeze-attendance.salary_generate',
                'group' => 'freeze-attendance',
            ],
            [
                'id' => 140,
                'name' => 'add-absent.add',
                'group' => 'add-absent',
            ],
            [
                'id' => 141,
                'name' => 'add-leave.add',
                'group' => 'add-leave',
            ],
            [
                'id' => 142,
                'name' => 'leave-approval.view',
                'group' => 'leave-approval',
            ],
            [
                'id' => 143,
                'name' => 'leave-approval.approve',
                'group' => 'leave-approval',
            ],
            [
                'id' => 144,
                'name' => 'leave-approval.reject',
                'group' => 'leave-approval',
            ],

            [
                'id' => 145,
                'name' => 'employee-festival-advance.view',
                'group' => 'employee-festival-advance',
            ],
            [
                'id' => 146,
                'name' => 'employee-festival-advance.create',
                'group' => 'lic-dedcution',
            ],
            [
                'id' => 147,
                'name' => 'monthly-festivalAdvance.view',
                'group' => 'monthly-festivalAdvance',
            ],
            [
                'id' => 148,
                'name' => 'monthly-festivalAdvance.stop',
                'group' => 'monthly-festivalAdvance',
            ],
            [
                'id' => 149,
                'name' => 'employee-transfer.add',
                'group' => 'employee-transfer',
            ],
            [
                'id' => 150,
                'name' => 'working-department.view',
                'group' => 'working-department',
            ],
            [
                'id' => 151,
                'name' => 'working-department.create',
                'group' => 'working-department',
            ],
            [
                'id' => 152,
                'name' => 'working-department.edit',
                'group' => 'working-department',
            ],
            [
                'id' => 153,
                'name' => 'working-department.delete',
                'group' => 'working-department',
            ],

            [
                'id' => 154,
                'name' => 'castes.view',
                'group' => 'castes',
            ],
            [
                'id' => 155,
                'name' => 'castes.create',
                'group' => 'castes',
            ],
            [
                'id' => 156,
                'name' => 'castes.edit',
                'group' => 'castes',
            ],
            [
                'id' => 157,
                'name' => 'castes.delete',
                'group' => 'castes',
            ],
            [
                'id' => 158,
                'name' => 'yearly_pay_sheet.show',
                'group' => 'yearly_pay_sheet',
            ],
            [
                'id' => 159,
                'name' => 'yearly-bonus.view',
                'group' => 'yearly-bonus',
            ],

        ];

        foreach ($permissions as $permission) {
            Permission::updateOrCreate([
                'id' => $permission['id']
            ], [
                'id' => $permission['id'],
                'name' => $permission['name'],
                'group' => $permission['group']
            ]);
        }
    }
}
