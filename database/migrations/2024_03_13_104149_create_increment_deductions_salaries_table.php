<?php

use App\Models\Clas;
use App\Models\Deduction;
use App\Models\Department;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('increment_deductions_salaries', function (Blueprint $table) {
            $table->id();
            // $table->foreignIdFor(Department::class)->nullable()->constrained();
            $table->integer('department_id')->default('0')->nullable()->comment('0) for all department');
            // $table->foreignIdFor(Clas::class)->nullable()->constrained();
            $table->integer('clas_id')->default('0')->nullable()->comment('0) for all class');
            $table->foreignIdFor(Deduction::class)->nullable()->constrained();
            $table->integer('current_amount');
            $table->string('current_paytype');
            $table->integer('new_amount');
            $table->date('applicable_date');
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->foreignId('deleted_by')->nullable()->constrained('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('increment_deductions_salaries');
    }
};
