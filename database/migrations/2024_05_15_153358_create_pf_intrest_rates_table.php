<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pf_intrest_rates', function (Blueprint $table) {
            $table->id();
            $table->string('interest_rate');
            $table->date('from_date')->nullable();
            $table->date('to_date')->nullable();
            $table->integer('active_status')->default(0)->comment('1) Active');
            $table->integer('activity_status_by')->nullable();
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->foreignId('deleted_by')->nullable()->constrained('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pf_intrest_rates');
    }
};
