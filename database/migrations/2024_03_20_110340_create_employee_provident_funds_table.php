<?php

use App\Models\Employee;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('employee_provident_funds', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Employee::class)->nullable()->constrained();
            $table->string('Emp_Code');
            $table->string('pf_account_no');
            $table->date('current_month');
            $table->date('salary_month');
            $table->integer('pf_contribution');
            $table->integer('pf_loan');
            $table->integer('other_amount')->nullable();
            $table->integer('total')->nullable();
            $table->date('loan_date')->nullable();
            $table->integer('loan_amt')->nullable();
            $table->integer('grand_total')->nullable();
            $table->string('intrest_rate')->nullable();
            $table->string('intrest_amt')->nullable();
            $table->string('remark')->nullable();
            $table->integer('previous_year_opening')->nullable();
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->foreignId('deleted_by')->nullable()->constrained('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('employee_provident_funds');
    }
};
