<?php

use App\Models\Clas;
use App\Models\Department;
use App\Models\Designation;
use App\Models\Employee;
use App\Models\FinancialYear;
use App\Models\Ward;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('freeze_attendances', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Employee::class)->nullable()->constrained();
            $table->string('Emp_Code');

            $table->integer('freeze_status')->default(1)->comment('1) freeze 0) unfreeze');
            $table->string('attendance_UId')->nullable();

            $table->foreignIdFor(Ward::class)->nullable()->constrained();
            $table->foreignIdFor(Department::class)->nullable()->constrained();
            $table->foreignIdFor(Designation::class)->nullable()->constrained();
            $table->foreignIdFor(Clas::class)->nullable()->constrained();

            $table->date('from_date');
            $table->date('to_date');
            $table->integer('month');
            $table->foreignIdFor(FinancialYear::class)->nullable()->constrained();

            $table->integer('present_day');
            $table->integer('basic_salary');
            $table->integer('actual_basic')->nullable()->default(0);
            $table->integer('grade_pay');

            $table->string('allowance_Id');
            $table->string('allowance_Amt');
            $table->string('allowance_Type');
            $table->integer('total_allowance');

            $table->string('deduction_Id');
            $table->string('deduction_Amt');
            $table->string('deduction_Type');
            $table->integer('total_deduction');

            $table->integer('stamp_duty');

            $table->string('loan_deduction_id');
            $table->string('loan_deduction_amt');
            $table->string('loan_deduction_bank_id');
            $table->integer('total_loan_deduction');

            $table->integer('net_salary');

            $table->string('emp_name');
            $table->string('pf_account_no');
            $table->string('pay_band_scale');
            $table->string('grade_pay_scale');
            $table->date('date_of_birth')->nullable();
            $table->date('date_of_appointment')->nullable();
            $table->date('date_of_retirement')->nullable();
            $table->string('bank_account_number');
            $table->string('phone_no')->nullable();
            $table->string('corporation_share_da');

            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->foreignId('deleted_by')->nullable()->constrained('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('freeze_attendances');
    }
};
