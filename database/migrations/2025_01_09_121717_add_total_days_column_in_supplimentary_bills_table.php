<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('supplimentary_bills', function (Blueprint $table) {
            $table->decimal('no_of_days', 3, 2)->nullable()->after('financial_year_id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('supplimentary_bills', function (Blueprint $table) {
            $table->dropColumn('no_of_days');
        });
    }
};
