<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\FinancialYear;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('income_tax', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(FinancialYear::class)->constrained();
            $table->string('Emp_Code');
            $table->tinyInteger('tax_regime')->comment('1) Old regime 2) New regime');

            $table->integer('prev_gross')->nullable();
            $table->integer('current_gross');
            $table->integer('estimated_salary')->nullable();
            $table->integer('supplementary_bill')->nullable();
            $table->integer('estimated_supp_bill')->nullable();
            $table->integer('gross_income');
            $table->integer('hre')->nullable();
            $table->integer('prev_ptax')->nullable();
            $table->integer('elec_allowance')->nullable();
            $table->integer('trans_allowance')->nullable();
            $table->integer('standard_deduction')->nullable();
            $table->integer('ptax')->nullable();
            $table->integer('ex_gratia')->nullable();
            $table->integer('total_exemptions');
            $table->integer('investment')->nullable();
            $table->integer('other_investment')->nullable();
            $table->integer('other_income')->nullable();
            $table->integer('hial60d')->nullable();
            $table->integer('hial60p')->nullable();
            $table->integer('hiam60d')->nullable();
            $table->integer('hiam60p')->nullable();
            $table->integer('hcheckup')->nullable();
            $table->integer('hitotal')->nullable();
            $table->integer('sec80d')->nullable();
            $table->integer('don80g')->nullable();
            $table->integer('odon80g')->nullable();
            $table->integer('totaldon')->nullable();
            $table->integer('sec80g')->nullable();
            $table->integer('nsc')->nullable();
            $table->integer('lic')->nullable();
            $table->integer('dcps_emp')->nullable();
            $table->integer('gis')->nullable();
            $table->integer('pli')->nullable();
            $table->integer('hloan')->nullable();
            $table->integer('dcps_empr_contrn')->nullable();
            $table->integer('gpf')->nullable();
            $table->integer('nsc_int')->nullable();
            $table->integer('other_lic')->nullable();
            $table->integer('mutual_fund')->nullable();
            $table->integer('other_gis')->nullable();
            $table->integer('other_pli')->nullable();
            $table->integer('other_hloan')->nullable();
            $table->integer('infra_bond')->nullable();
            $table->integer('ppf')->nullable();
            $table->integer('edu_fees')->nullable();
            $table->integer('total80c')->nullable();
            $table->integer('sec80c')->nullable();
            $table->integer('nps')->nullable();
            $table->integer('dcps_u80ccd')->nullable();
            $table->integer('total80ccd')->nullable();
            $table->integer('sec80ccd')->nullable();
            $table->integer('edu_loan')->nullable();
            $table->integer('total80e')->nullable();
            $table->integer('sec80e')->nullable();
            $table->integer('hlib24')->nullable();
            $table->integer('ohlib24')->nullable();
            $table->integer('hlia24')->nullable();
            $table->integer('ohlia24')->nullable();
            $table->integer('totalsec24')->nullable();
            $table->integer('sec24')->nullable();
            $table->integer('medical_treatment')->nullable();
            $table->integer('total80ddb')->nullable();
            $table->integer('sec80ddb')->nullable();
            $table->integer('sb_int_ded')->nullable();
            $table->integer('total80tta')->nullable();
            $table->integer('sec80tta')->nullable();
            $table->integer('disability')->nullable();
            $table->integer('handicappedalwce')->nullable();
            $table->integer('total80u')->nullable();
            $table->integer('sec80u')->nullable();
            $table->integer('empr_contribution')->nullable();
            $table->integer('total80cc2')->nullable();
            $table->integer('sec80ccd2')->nullable();
            $table->integer('taxable_sal')->default('0');
            $table->integer('tax_payable')->nullable();
            $table->integer('tax_after_cess')->nullable();
            $table->integer('tax_rem_months')->nullable();
            $table->string('reason_requested_ded', 300)->nullable();
            $table->integer('edu_cess')->nullable();
            $table->integer('tax_refunded')->nullable();
            $table->integer('tax_pending')->nullable();
            $table->integer('user_req_ded')->nullable();
            $table->integer('net_tax')->default('0');
            $table->integer('rebated_amount')->default('0');
            $table->integer('tax_paid_cash')->default('0');
            $table->integer('tax_deducted')->default('0');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('income_tax');
    }
};
