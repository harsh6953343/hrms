<?php

use App\Models\Employee;
use App\Models\FinancialYear;
use App\Models\PensionBank;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('freeze_pensions', function (Blueprint $table) {
            $table->id();
            $table->string('pension_id')->nullable();

            $table->date('from_date');
            $table->date('to_date');
            $table->integer('month');
            $table->foreignIdFor(FinancialYear::class)->nullable()->constrained();


            $table->string('emp_name');
            $table->foreignIdFor(Employee::class)->nullable()->constrained();
            $table->string('designation');
            $table->integer('pay_type')->default('0')->comment('1) 6 Pay 2) 7 Pay');

            $table->integer('emp_retire_after_2016')->default('0')->comment('1) Yes 2) No');
            $table->integer('is_emp_dr')->default('2')->comment('1) Yes 2) No');


            $table->date('dob');
            $table->integer('age');
            $table->date('retirement_date');
            $table->date('date_of_death')->nullable();

            $table->string('account_number');
            $table->foreignIdFor(PensionBank::class)->nullable()->constrained();
            $table->string('branch_name');
            $table->string('ifsc_code');

            $table->integer('basic_salary')->default('0');
            $table->integer('calculated_basic')->default('0');
            $table->integer('payable_pension')->default('0');
            $table->integer('da')->default('0');
            $table->integer('da_applicabe')->default('1')->comment('1) yes 2) no');
            $table->integer('sell_computation')->default('2')->comment('1) yes 2) no');
            $table->date('sell_date')->nullable();
            $table->integer('deduct_amt')->default('0');

            $table->integer('differance')->default('0');
            $table->integer('deduction')->default('0');

            $table->integer('business_allowances')->default('0');

            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->foreignId('deleted_by')->nullable()->constrained('users');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('freeze_pensions');
    }
};
