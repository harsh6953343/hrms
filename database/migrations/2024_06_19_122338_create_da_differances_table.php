<?php

use App\Models\FinancialYear;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('da_differances', function (Blueprint $table) {
            $table->id();
            $table->integer('DA_currentRate');
            $table->integer('DA_newRate');
            $table->integer('applicable_month')->comment('previous month when DA Rule applicable');
            $table->integer('given_month');
            $table->integer('no_of_month');
            $table->foreignIdFor(FinancialYear::class)->nullable()->constrained();
            $table->integer('status')->default(0)->comment('1) cron run');
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->foreignId('deleted_by')->nullable()->constrained('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('da_differances');
    }
};
