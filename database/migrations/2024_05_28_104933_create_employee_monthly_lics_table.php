<?php

use App\Models\Employee;
use App\Models\FinancialYear;
use App\Models\LicDeduction;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('employee_monthly_lics', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Employee::class)->nullable()->constrained();
            $table->integer('Emp_Code');
            $table->string('emp_name', 100);
            $table->foreignIdFor(LicDeduction::class)->nullable()->constrained();
            $table->date('from_date');
            $table->date('to_date');
            $table->integer('installment_amt');
            $table->integer('installment_no');
            $table->string('remark', 50)->nullable();
            $table->foreignIdFor(FinancialYear::class)->nullable()->constrained();
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->foreignId('deleted_by')->nullable()->constrained('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('employee_monthly_lics');
    }
};
