<?php

use App\Models\FinancialYear;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('festival_excel_uploads', function (Blueprint $table) {
            $table->id();
            $table->string('Emp_Code');
            $table->string('Name');
            $table->string('given_advance_amt');
            $table->string('total');
            $table->integer('cron_status')->default('0')->comment('0) pending 1) run');
            $table->foreignIdFor(FinancialYear::class)->nullable()->constrained();
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->foreignId('deleted_by')->nullable()->constrained('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('festival_excel_uploads');
    }
};
