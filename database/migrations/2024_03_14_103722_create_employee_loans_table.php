<?php

use App\Models\Employee;
use App\Models\FinancialYear;
use App\Models\Loan;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('employee_loans', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Employee::class)->nullable()->constrained();
            $table->integer('Emp_Code');
            $table->foreignIdFor(Loan::class)->nullable()->constrained();
            $table->string('loan_name', 50);
            $table->integer('loan_amount');
            $table->string('loan_acc_no', 50);
            $table->integer('total_instalment');
            $table->integer('deducted_instalment')->default(0);
            $table->integer('pending_instalment')->default(0);
            $table->integer('instalment_amount');
            $table->date('start_date');
            $table->date('end_date');
            $table->integer('status')->default('1')->nullable()->comment('1) Start 2) Stop');
            $table->integer('status_by')->nullable();
            $table->datetime('status_date')->nullable();
            $table->string('remark', 50)->nullable();
            $table->foreignIdFor(FinancialYear::class)->nullable()->constrained();
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->foreignId('deleted_by')->nullable()->constrained('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('employee_loans');
    }
};
