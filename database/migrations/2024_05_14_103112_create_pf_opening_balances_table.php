<?php

use App\Models\Employee;
use App\Models\FinancialYear;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pf_opening_balances', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Employee::class)->nullable()->constrained();
            $table->integer('Emp_Code');
            $table->integer('pf_no');
            $table->foreignIdFor(FinancialYear::class)->nullable()->constrained();
            $table->string('opening_balance');
            $table->string('closing_balance')->default(0);

            $table->integer('closing_status')->default(0)->comment('1) closed');
            $table->integer('type')->default(0)->comment('1) Retire 2) Expired');
            $table->integer('closing_month')->nullable();
            $table->date('closing_date')->nullable();
            $table->string('remark')->nullable();

            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->foreignId('deleted_by')->nullable()->constrained('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pf_opening_balances');
    }
};
