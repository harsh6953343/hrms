<?php

use App\Models\EmployeeDaDifferance;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('freeze_attendances', function (Blueprint $table) {
            $table->string('da_differance')->after('festival_allowance_id')->default(0);
            $table->foreignIdFor(EmployeeDaDifferance::class)->after('da_differance')->nullable()->constrained();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('freeze_attendances', function (Blueprint $table) {
            $table->dropColumn('da_differance');
            $table->dropForeign(['employee_da_differance_id']);
            $table->dropColumn('employee_da_differance_id');
        });
    }
};
