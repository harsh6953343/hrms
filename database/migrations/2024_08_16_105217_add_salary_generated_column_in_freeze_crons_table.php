<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('freeze_crons', function (Blueprint $table) {
            $table->integer('salary_generated')->after('status')->default('0')->nullable()->comment('1) Run 0) Pending');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('freeze_crons', function (Blueprint $table) {
            $table->dropColumn('salary_generated');
        });
    }
};
