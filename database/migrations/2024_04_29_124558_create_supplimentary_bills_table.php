<?php

use App\Models\Employee;
use App\Models\FinancialYear;
use App\Models\FreezeAttendance;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('supplimentary_bills', function (Blueprint $table) {
            $table->id();
            $table->string('remaining_freeze_id');
            $table->foreignIdFor(Employee::class)->nullable()->constrained();
            $table->integer('Emp_Code');

            $table->string('from_date');
            $table->string('to_date');
            $table->string('month');
            $table->foreignIdFor(FinancialYear::class)->nullable()->constrained();
            $table->integer('type')->nullable()->comment('1) Regular 2) Suspend');
            $table->integer('status')->default(0)->comment('1) Update on Salary');


            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->foreignId('deleted_by')->nullable()->constrained('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('supplimentary_bills');
    }
};
