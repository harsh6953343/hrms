<?php

use App\Models\Clas;
use App\Models\Department;
use App\Models\Designation;
use App\Models\Employee;
use App\Models\FinancialYear;
use App\Models\FreezeAttendance;
use App\Models\Ward;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('remaining_freeze_salaries', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(FreezeAttendance::class)->nullable()->constrained();
            $table->foreignIdFor(Employee::class)->nullable()->constrained();
            $table->string('Emp_Code');

            $table->date('from_date');
            $table->date('to_date');
            $table->integer('month');

            $table->integer('present_day');
            $table->integer('basic_salary');
            $table->integer('actual_basic')->nullable()->default(0);
            $table->integer('grade_pay');

            $table->string('allowance_Id');
            $table->string('allowance_Amt');
            $table->string('allowance_Type');
            $table->integer('total_allowance');

            $table->string('deduction_Id');
            $table->string('deduction_Amt');
            $table->string('deduction_Type');
            $table->integer('total_deduction');

            $table->integer('stamp_duty');

            $table->string('loan_deduction_id');
            $table->string('loan_deduction_amt');
            $table->string('loan_deduction_bank_id');
            $table->integer('total_loan_deduction');

            $table->integer('net_salary');

            $table->string('corporation_share_da');
            $table->integer('salary_percentage')->comment('if status and salary applicable remaining')->nullable();

            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->foreignId('deleted_by')->nullable()->constrained('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('remaining_freeze_salaries');
    }
};
