 <?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('add_employee_leaves', function (Blueprint $table) {
            $table->integer('status')->after('financial_year_id')->default(0)->comment('0) applied 1) approve 2) reject');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('add_employee_leaves', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }
};
