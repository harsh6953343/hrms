<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('freeze_attendances', function (Blueprint $table) {
            $table->integer('supplimentary_status')->after('net_salary')->default(0)->comment('1) Supplimentary Bill Generated');
            $table->string('supplimentary_ids')->after('supplimentary_status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('freeze_attendances', function (Blueprint $table) {
            $table->dropColumn('supplimentary_status');
            $table->dropColumn('supplimentary_ids');
        });
    }
};
