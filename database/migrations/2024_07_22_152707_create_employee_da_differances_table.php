<?php

use App\Models\DaDifferance;
use App\Models\Employee;
use App\Models\FinancialYear;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('employee_da_differances', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(DaDifferance::class)->nullable()->constrained();
            $table->foreignIdFor(Employee::class)->nullable()->constrained();
            $table->integer('Emp_Code');
            $table->integer('DA_currentRate');
            $table->integer('DA_newRate');
            $table->integer('given_month');
            $table->integer('no_of_month');
            $table->integer('differance');
            $table->integer('status')->default(0)->comment('1) applied 0)pending');
            $table->foreignIdFor(FinancialYear::class)->nullable()->constrained();
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->foreignId('deleted_by')->nullable()->constrained('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('employee_da_differances');
    }
};
