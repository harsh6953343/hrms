<?php

use App\Models\Employee;
use App\Models\FinancialYear;
use App\Models\LeaveType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('add_employee_leaves', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Employee::class)->nullable()->constrained();
            $table->integer('Emp_Code');
            $table->foreignIdFor(LeaveType::class)->nullable()->constrained();
            $table->date('from_date')->nullable()->constrained();
            $table->date('to_date')->nullable()->constrained();
            $table->date('rejoin_date')->nullable()->constrained();
            $table->integer('no_of_days')->nullable()->constrained();
            $table->string('remark')->nullable()->constrained();
            $table->integer('month')->nullable()->constrained();
            $table->foreignIdFor(FinancialYear::class)->nullable()->constrained();
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->foreignId('deleted_by')->nullable()->constrained('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('add_employee_leaves');
    }
};
