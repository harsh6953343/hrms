<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('add_employee_leaves', function (Blueprint $table) {
            $table->integer('status_by')->after('status')->nullable();
            $table->dateTime('status_dt')->after('status_by')->nullable();
            $table->string('reject_remark')->after('status_dt')->nullable();


        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('add_employee_leaves', function (Blueprint $table) {
            $table->dropColumn('status_by');
            $table->dropColumn('status_dt');
            $table->dropColumn('reject_remark');
        });
    }
};
