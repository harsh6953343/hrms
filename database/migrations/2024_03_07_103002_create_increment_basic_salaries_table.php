<?php

use App\Models\Department;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('increment_basic_salaries', function (Blueprint $table) {
            $table->id();
            $table->integer('increment_type')->default('0')->nullable()->comment('1) Regular');
            $table->integer('department_id')->default('0')->nullable()->comment('0) for all department');
            // $table->foreignIdFor(Department::class)->nullable()->constrained();
            $table->date('applicable_date');
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->foreignId('deleted_by')->nullable()->constrained('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('increment_basic_salaries');
    }
};
