<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Ward;
use App\Models\Department;
use App\Models\Clas;
use App\Models\Designation;
use App\Models\Bank;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->string('employee_id');
            $table->string('fname', 50);
            $table->string('mname', 50)->nullable();
            $table->string('lname', 50);
            $table->foreignIdFor(Ward::class)->nullable()->constrained()->cascadeOnDelete()->cascadeOnUpdate();
            $table->foreignIdFor(Department::class)->nullable()->constrained()->cascadeOnDelete()->cascadeOnUpdate();
            $table->foreignIdFor(Clas::class)->nullable()->constrained()->cascadeOnDelete()->cascadeOnUpdate();
            $table->foreignIdFor(Designation::class)->nullable()->constrained()->cascadeOnDelete()->cascadeOnUpdate();
            $table->integer('gender')->nullable()->comment('1) Male 2) Female');
            $table->date('dob');
            $table->date('doj');
            $table->string('mobile_number')->nullable(); // This line adds the mobile_number column
            $table->string('email', 50)->nullable();
            $table->string('aadhar', 15);
            $table->string('pan', 15);
            $table->integer('caste');
            $table->string('blood_group', 20);
            $table->string('ccity', 20);
            $table->string('caddress', 100);
            $table->string('cstate', 20);
            $table->integer('cpincode');
            $table->string('pcity', 20);
            $table->string('paddress', 100);
            $table->string('pstate', 20);
            $table->integer('ppincode');
            $table->integer('shift');
            $table->integer('working_type')->nullable()->comment('0) Permanent 1) Temporary');
            $table->date('retirement_date');
            $table->foreignIdFor(Bank::class)->nullable()->constrained()->cascadeOnDelete()->cascadeOnUpdate();
            $table->string('branch', 20);
            $table->string('account_no');
            $table->string('ifsc', 30);
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->foreignId('deleted_by')->nullable()->constrained('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('employees');
    }
};
