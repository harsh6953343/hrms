<?php

use App\Models\Allowance;
use App\Models\Employee;
use App\Models\EmployeeSalary;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;


return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('employee_allowances', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Employee::class)->nullable()->constrained();
            $table->string('Emp_Code');
            $table->foreignIdFor(EmployeeSalary::class)->nullable()->constrained();
            $table->foreignIdFor(Allowance::class)->nullable()->constrained();
            $table->integer('allowance_amt');
            $table->integer('allowance_type');
            $table->integer('is_active')->default('0')->nullable()->comment('1) Active 0) Inactive');
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->foreignId('deleted_by')->nullable()->constrained('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('employee_allowances');
    }
};
