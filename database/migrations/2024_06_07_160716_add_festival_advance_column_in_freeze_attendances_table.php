<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('freeze_attendances', function (Blueprint $table) {
            $table->string('festival_allowance')->default(0)->after('allowance_Type');
            $table->string('festival_allowance_id')->default(0)->after('festival_allowance');
            $table->string('festival_deduction_id')->nullable()->after('total_lic_deduction');
            $table->string('festival_deduction_amt')->nullable()->after('festival_deduction_id');
            $table->integer('total_festival_deduction')->default(0)->after('festival_deduction_amt');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('freeze_attendances', function (Blueprint $table) {
            $table->dropColumn('festival_allowance');
            $table->dropColumn('festival_allowance_id');
            $table->dropColumn('festival_deduction_id');
            $table->dropColumn('festival_deduction_amt');
            $table->dropColumn('total_festival_deduction');
        });
    }
};
