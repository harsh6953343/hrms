<div class="app-menu navbar-menu">
    <!-- LOGO -->
    <div class="navbar-brand-box">
        <!-- Dark Logo-->
        <a href="index.html" class="logo logo-dark">
            <span class="logo-sm">
                <img src="{{ asset('admin/images/logo.png') }}" alt="" height="50" />
            </span>
            <span class="logo-lg">
                <img src="{{ asset('admin/images/logo.png') }}" alt="" height="50" />
            </span>
        </a>
        <!-- Light Logo-->
        <a href="index.html" class="logo logo-light">
            <span class="logo-sm">
                <img src="{{ asset('admin/images/logo.png') }}" alt="" height="50" />
            </span>
            <span class="logo-lg">
                <img src="{{ asset('admin/images/logo.png') }}" alt="" height="50" />
            </span>
        </a>
        <button type="button" class="btn btn-sm p-0 fs-20 header-item float-end btn-vertical-sm-hover" id="vertical-hover">
            <i class="ri-record-circle-line"></i>
        </button>
    </div>

    <div id="scrollbar">
        <div class="container-fluid">
            <div id="two-column-menu"></div>
            <ul class="navbar-nav" id="navbar-nav">
                <li class="menu-title">
                    <span data-key="t-menu">Menu</span>
                </li>

                @canany(['dashboard.view'])
                <li class="nav-item">
                    <a class="nav-link menu-link  {{ request()->routeIs('dashboard') ? 'active' : 'collapsed' }}" href="{{ route('dashboard') }}">
                        <i class="ri-dashboard-2-line"></i>
                        <span data-key="t-dashboards">Dashboard</span>
                    </a>
                </li>
                @endcan

                @canany(['wards.view', 'departments.view', 'class.view', 'bank.view', 'financial_year.view', 'allowance.view', 'deduction.view', 'loan.view', 'designation.view', 'leaveType.view', 'pay_scale.view','users.view', 'roles.view','working-department.view','castes.view'])
                <li class="nav-item">
                    <a class="nav-link menu-link {{ request()->routeIs('wards.index') || request()->routeIs('departments.index') || request()->routeIs('class.index') || request()->routeIs('bank.index') || request()->routeIs('financial_year.index') || request()->routeIs('allowance.index') || request()->routeIs('deduction.index') || request()->routeIs('loan.index') || request()->routeIs('designation.index') || request()->routeIs('leaveType.index') || request()->routeIs('pay_scale.index') || request()->routeIs('document.index') || request()->routeIs('users.index') || request()->routeIs('roles.index') || request()->routeIs('working-department.index') || request()->routeIs('castes.index') ? 'active' : 'collapsed' }}" href="#sidebarAuth1" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarAuth1">
                        <i class="ri-account-circle-line"></i> <span data-key="t-authentication">Masters</span>
                    </a>
                    <div class="collapse menu-dropdown {{ request()->routeIs('wards.index') || request()->routeIs('departments.index') || request()->routeIs('class.index') || request()->routeIs('bank.index') || request()->routeIs('financial_year.index') || request()->routeIs('allowance.index') || request()->routeIs('deduction.index') || request()->routeIs('loan.index') || request()->routeIs('designation.index') || request()->routeIs('leaveType.index') || request()->routeIs('pay_scale.index') || request()->routeIs('document.index') || request()->routeIs('users.index') || request()->routeIs('roles.index') || request()->routeIs('working-department.index') || request()->routeIs('castes.index') ? 'show' : '' }}" id="sidebarAuth1">
                        <ul class="nav nav-sm flex-column">
                            @canany(['users.view', 'roles.view'])
                            <li class="nav-item">
                                <a href="#sidebarSignUp" class="nav-link {{ request()->routeIs('users.index') || request()->routeIs('roles.index') ? 'active' : '' }}" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarSignUp" data-key="t-signup"> User Management
                                </a>
                                <div class="collapse menu-dropdown {{ request()->routeIs('users.index') || request()->routeIs('roles.index') ? 'show' : '' }}" id="sidebarSignUp">
                                    <ul class="nav nav-sm flex-column">
                                        @can('users.view')
                                            <li class="nav-item">
                                                <a href="{{ route('users.index') }}" class="nav-link {{ request()->routeIs('users.index') ? 'active' : '' }}" data-key="t-horizontal">Users</a>
                                            </li>
                                        @endcan
                                        @can('roles.view')
                                            <li class="nav-item">
                                                <a href="{{ route('roles.index') }}" class="nav-link {{ request()->routeIs('roles.index') ? 'active' : '' }}" data-key="t-horizontal">Roles</a>
                                            </li>
                                        @endcan
                                    </ul>
                                </div>
                            </li>
                            @endcanany
                            @can('wards.view')
                                <li class="nav-item">
                                    <a href="{{ route('wards.index') }}" class="nav-link {{ request()->routeIs('wards.index') ? 'active' : '' }}" data-key="t-horizontal">Wards</a>
                                </li>
                            @endcan
                            @can('departments.view')
                                <li class="nav-item">
                                    <a href="{{ route('department.index') }}" class="nav-link {{ request()->routeIs('department.index') ? 'active' : '' }}" data-key="t-horizontal">Department</a>
                                </li>
                            @endcan
                            @can('class.view')
                                <li class="nav-item">
                                    <a href="{{ route('class.index') }}" class="nav-link {{ request()->routeIs('class.index') ? 'active' : '' }}" data-key="t-horizontal">Class</a>
                                </li>
                            @endcan
                            @can('bank.view')
                                <li class="nav-item">
                                    <a href="{{ route('bank.index') }}" class="nav-link {{ request()->routeIs('bank.index') ? 'active' : '' }}" data-key="t-horizontal">Bank</a>
                                </li>
                            @endcan
                            @can('financial_year.view')
                                <li class="nav-item">
                                    <a href="{{ route('financial_year.index') }}" class="nav-link {{ request()->routeIs('financial_year.index') ? 'active' : '' }}" data-key="t-horizontal">Financial Year</a>
                                </li>
                            @endcan
                            @can('allowance.view')
                                <li class="nav-item">
                                    <a href="{{ route('allowance.index') }}" class="nav-link {{ request()->routeIs('allowance.index') ? 'active' : '' }}" data-key="t-horizontal">Allowance</a>
                                </li>
                            @endcan
                            @can('deduction.view')
                                <li class="nav-item">
                                    <a href="{{ route('deduction.index') }}" class="nav-link {{ request()->routeIs('deduction.index') ? 'active' : '' }}" data-key="t-horizontal">Deduction</a>
                                </li>
                            @endcan
                            @can('loan.view')
                                <li class="nav-item">
                                    <a href="{{ route('loan.index') }}" class="nav-link {{ request()->routeIs('loan.index') ? 'active' : '' }}" data-key="t-horizontal">Loan</a>
                                </li>
                            @endcan
                            @can('designation.view')
                                <li class="nav-item">
                                    <a href="{{ route('designation.index') }}" class="nav-link {{ request()->routeIs('designation.index') ? 'active' : '' }}" data-key="t-horizontal">Designation</a>
                                </li>
                            @endcan
                            @can('leaveType.view')
                                <li class="nav-item">
                                    <a href="{{ route('leave-type.index') }}" class="nav-link {{ request()->routeIs('leave-type.index') ? 'active' : '' }}" data-key="t-horizontal">Leave Type</a>
                                </li>
                            @endcan
                            @can('pay_scale.view')
                            <li class="nav-item">
                                <a href="{{ route('pay-scale.index') }}" class="nav-link {{ request()->routeIs('pay-scale.index') ? 'active' : '' }}" data-key="t-horizontal">Pay Scale</a>
                            </li>
                            @endcan
                            @can('status.view')
                            <li class="nav-item">
                                <a href="{{ route('status.index') }}" class="nav-link {{ request()->routeIs('status.index') ? 'active' : '' }}" data-key="t-horizontal">Status</a>
                            </li>
                            @endcan
                            @can('pension-bank.view')
                            <li class="nav-item">
                                <a href="{{ route('pension-bank.index') }}" class="nav-link {{ request()->routeIs('pension-bank.index') ? 'active' : '' }}" data-key="t-horizontal">Pension Banks</a>
                            </li>
                            @endcan
                            @can('intrest-rate.view')
                            <li class="nav-item">
                                <a href="{{ route('intrest-rate.index') }}" class="nav-link {{ request()->routeIs('intrest-rate.index') ? 'active' : '' }}" data-key="t-horizontal">PF Intrest Rate</a>
                            </li>
                            @endcan
                            @can('document.view')
                            <li class="nav-item">
                                <a href="{{ route('document.index') }}" class="nav-link {{ request()->routeIs('document.index') ? 'active' : '' }}" data-key="t-horizontal">Document</a>
                            </li>
                            @endcan
                            @can('working-department.view')
                            <li class="nav-item">
                                <a href="{{ route('working-department.index') }}" class="nav-link {{ request()->routeIs('working-department.index') ? 'active' : '' }}" data-key="t-horizontal">Working Department</a>
                            </li>
                            @endcan
                            {{-- @can('castes.view') --}}
                            <li class="nav-item">
                                <a href="{{ route('castes.index') }}" class="nav-link {{ request()->routeIs('castes.index') ? 'active' : '' }}" data-key="t-horizontal">Caste</a>
                            </li>
                            {{-- @endcan --}}
                        </ul>
                    </div>
                </li>
                @endcanany

                @canany(['employee.view','employee_status.create'])
                <li class="nav-item">
                    <a class="nav-link menu-link {{ request()->routeIs('employee.index') || request()->routeIs('employee-status.index') ? 'active' : 'collapsed' }}" href="#sidebarLayouts1" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarLayouts1">
                        <i class="ri-layout-3-line"></i>
                        <span data-key="t-layouts">Employee</span>
                    </a>
                    <div class="collapse menu-dropdown {{ request()->routeIs('employee.index') || request()->routeIs('employee-status.index') ? 'show' : '' }} " id="sidebarLayouts1">
                        <ul class="nav nav-sm flex-column">
                            @can('employee.view')
                                <li class="nav-item">
                                    <a href="{{ route('employee.index') }}" class="nav-link {{ request()->routeIs('employee.index') ? 'active' : '' }}" data-key="t-horizontal">List</a>
                                </li>
                            @endcan
                            @can('employee_status.create')
                            <li class="nav-item">
                                <a href="{{ route('employee-status.index') }}" class="nav-link {{ request()->routeIs('employee-status.index') ? 'active' : '' }}" data-key="t-horizontal">Status</a>
                            </li>
                            @endcan

                            @can('employee-transfer.add')
                            <li class="nav-item">
                                <a href="{{ route('employee-transfer.index') }}" class="nav-link {{ request()->routeIs('employee-transfer.index') ? 'active' : '' }}" data-key="t-horizontal">Transfer Order</a>
                            </li>
                            @endcan
                        </ul>
                    </div>
                </li>
                @endcanany

                @canany(['employee-salary.view','basic-salary-increment.view','allowance-increment.view','deduction-increment.view'])
                <li class="nav-item">
                    <a class="nav-link menu-link {{ request()->routeIs('employee-salary.index') || request()->routeIs('basic-salary-increment.index') || request()->routeIs('allowance-increment.index') || request()->routeIs('deduction-increment.index') ? 'active' : 'collapsed' }}" href="#sidebarAuth2" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarAuth2">
                        <i class="ri-account-circle-line"></i> <span data-key="t-authentication">Salary</span>
                    </a>
                    <div class="collapse menu-dropdown {{ request()->routeIs('employee-salary.index') || request()->routeIs('basic-salary-increment.index') || request()->routeIs('allowance-increment.index') || request()->routeIs('deduction-increment.index') ? 'show' : '' }}" id="sidebarAuth2">
                        <ul class="nav nav-sm flex-column">
                            @can('employee-salary.view')
                                <li class="nav-item">
                                    <a href="{{ route('employee-salary.index') }}" class="nav-link {{ request()->routeIs('employee-salary.index') ? 'active' : '' }}" data-key="t-horizontal">Salary Structure</a>
                                </li>
                            @endcan

                            {{-- @can('employee-salary.view') --}}
                            <li class="nav-item">
                                <a href="{{ route('da-differance.index') }}" class="nav-link {{ request()->routeIs('da-differance.index') ? 'active' : '' }}" data-key="t-horizontal">DA Differance</a>
                            </li>
                            {{-- @endcan --}}
                            @canany(['basic-salary-increment.view','allowance-increment.view','deduction-increment.view'])
                            <li class="nav-item">
                                <a href="#sidebarSignUp" class="nav-link {{ request()->routeIs('basic-salary-increment.index') || request()->routeIs('allowance-increment.index') || request()->routeIs('deduction-increment.index') ? 'active' : '' }}" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarSignUp" data-key="t-signup"> Salary Increment
                                </a>
                                <div class="collapse menu-dropdown {{ request()->routeIs('basic-salary-increment.index') || request()->routeIs('allowance-increment.index') || request()->routeIs('deduction-increment.index') ? 'show' : '' }}" id="sidebarSignUp">
                                    <ul class="nav nav-sm flex-column">
                                        @can('basic-salary-increment.view')
                                        <li class="nav-item">
                                            <a href="{{ route('basic-salary-increment.index') }}" class="nav-link {{ request()->routeIs('basic-salary-increment.index') ? 'active' : '' }}" data-key="t-basic"> Basic
                                            </a>
                                        </li>
                                        @endcan
                                        @can('allowance-increment.view')
                                        <li class="nav-item">
                                            <a href="{{ route('allowance-increment.index') }}" class="nav-link {{ request()->routeIs('allowance-increment.index') ? 'active' : '' }}" data-key="t-cover"> Allowance
                                            </a>
                                        </li>
                                        @endcan
                                        @can('deduction-increment.view')
                                        <li class="nav-item">
                                            <a href="{{ route('deduction-increment.index') }}" class="nav-link {{ request()->routeIs('deduction-increment.index') ? 'active' : '' }}" data-key="t-cover"> Deduction
                                            </a>
                                        </li>
                                        @endcan
                                    </ul>
                                </div>
                            </li>
                            @endcan
                        </ul>
                    </div>
                </li>
                @endcanany

                @canany(['yearly-bonus.view'])
                <li class="nav-item">
                    <a class="nav-link menu-link {{ request()->routeIs('yearly-bonus.index') ? 'active' : 'collapsed' }}" href="#sidebarAuth3" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarAuth3">
                        <i class="ri-account-circle-line"></i> <span data-key="t-authentication">Other Allowances</span>
                    </a>
                    <div class="collapse menu-dropdown {{ request()->routeIs('yearly-bonus.index') ? 'show' : '' }}" id="sidebarAuth3">
                        <ul class="nav nav-sm flex-column">
                            @canany(['yearly-bonus.view'])
                            <li class="nav-item">
                                <a href="{{ route('yearly-bonus.index') }}" class="nav-link {{ request()->routeIs('yearly-bonus.index') ? 'active' : '' }}" data-key="t-horizontal">Yearly Bonus</a>
                            </li>
                            @endcan
                        </ul>
                    </div>
                </li>
                @endcanany

                @canany(['employee-loans.view', 'employee-monthly-loans.view', 'lic-dedcution.view', 'employee-monthly-lic.view'])
                <li class="nav-item">
                    <a class="nav-link menu-link {{ request()->routeIs('employee-loans.index') || request()->routeIs('employee-monthly-loans.index') || request()->routeIs('lic-dedcution.index') || request()->routeIs('employee-monthly-lic.index') || request()->routeIs('employee-festival-advance.index') || request()->routeIs('monthly-festivalAdvance.index') ? 'active' : 'collapsed' }}" href="#sidebarAuth3" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarAuth3">
                        <i class="ri-account-circle-line"></i> <span data-key="t-authentication">Other Deduction</span>
                    </a>
                    <div class="collapse menu-dropdown {{ request()->routeIs('employee-loans.index') || request()->routeIs('employee-monthly-loans.index') || request()->routeIs('lic-dedcution.index') || request()->routeIs('employee-monthly-lic.index') || request()->routeIs('employee-festival-advance.index') || request()->routeIs('monthly-festivalAdvance.index') ? 'show' : '' }}" id="sidebarAuth3">
                        <ul class="nav nav-sm flex-column">
                            @canany(['employee-loans.view', 'employee-monthly-loans.view'])
                                <li class="nav-item">
                                    <a href="#sidebarSignUp1" class="nav-link {{ request()->routeIs('employee-loans.index') || request()->routeIs('employee-monthly-loans.index') ? 'active' : '' }}" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarSignUp1" data-key="t-signup"> Loan
                                    </a>
                                    <div class="collapse menu-dropdown {{ request()->routeIs('employee-loans.index') || request()->routeIs('employee-monthly-loans.index') ? 'show' : '' }}" id="sidebarSignUp1">
                                        <ul class="nav nav-sm flex-column">
                                            @can('employee-loans.view')
                                                <li class="nav-item">
                                                    <a href="{{ route('employee-loans.index') }}" class="nav-link {{ request()->routeIs('employee-loans.index') ? 'active' : '' }}" data-key="t-horizontal">Loans</a>
                                                </li>
                                            @endcan
                                            @can('employee-monthly-loans.view')
                                                <li class="nav-item">
                                                    <a href="{{ route('employee-monthly-loans.index') }}" class="nav-link {{ request()->routeIs('employee-monthly-loans.index') ? 'active' : '' }}" data-key="t-horizontal">Monthly Loans</a>
                                                </li>
                                            @endcan
                                        </ul>
                                    </div>
                                </li>
                            @endcan

                            @canany(['lic-dedcution.view', 'employee-monthly-lic.view'])
                                <li class="nav-item">
                                    <a href="#sidebarSignUp" class="nav-link {{ request()->routeIs('lic-dedcution.index') || request()->routeIs('employee-monthly-lic.index') ? 'active' : '' }}" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarSignUp" data-key="t-signup"> LIC
                                    </a>
                                    <div class="collapse menu-dropdown {{ request()->routeIs('lic-dedcution.index') || request()->routeIs('employee-monthly-lic.index') ? 'show' : '' }}" id="sidebarSignUp">
                                        <ul class="nav nav-sm flex-column">
                                            @can('lic-dedcution.view')
                                                <li class="nav-item">
                                                    <a href="{{ route('lic-dedcution.index') }}" class="nav-link {{ request()->routeIs('lic-dedcution.index') ? 'active' : '' }}" data-key="t-horizontal">Add LIC Deduction</a>
                                                </li>
                                            @endcan
                                            @can('employee-monthly-lic.view')
                                                <li class="nav-item">
                                                    <a href="{{ route('employee-monthly-lic.index') }}" class="nav-link {{ request()->routeIs('employee-monthly-lic.index') ? 'active' : '' }}" data-key="t-horizontal">Monthly LIC</a>
                                                </li>
                                            @endcan
                                        </ul>
                                    </div>
                                </li>
                            @endcan

                            @canany(['employee-festival-advance.view', 'monthly-festivalAdvance.view'])
                            <li class="nav-item">
                                <a href="#sidebarSignUp2" class="nav-link {{ request()->routeIs('employee-festival-advance.index') || request()->routeIs('monthly-festivalAdvance.index') ? 'active' : '' }}" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarSignUp2" data-key="t-signup"> Festival Deduction
                                </a>
                                <div class="collapse menu-dropdown {{ request()->routeIs('employee-festival-advance.index') || request()->routeIs('monthly-festivalAdvance.index') ? 'show' : '' }}" id="sidebarSignUp2">
                                    <ul class="nav nav-sm flex-column">
                                        @can('employee-festival-advance.view')
                                            <li class="nav-item">
                                                <a href="{{ route('employee-festival-advance.index') }}" class="nav-link {{ request()->routeIs('employee-festival-advance.index') ? 'active' : '' }}" data-key="t-horizontal">Add Festival Deduction</a>
                                            </li>
                                        @endcan
                                        @can('monthly-festivalAdvance.view')
                                            <li class="nav-item">
                                                <a href="{{ route('monthly-festivalAdvance.index') }}" class="nav-link {{ request()->routeIs('monthly-festivalAdvance.index') ? 'active' : '' }}" data-key="t-horizontal">Monthly Festival Deduction</a>
                                            </li>
                                        @endcan
                                    </ul>
                                </div>
                            </li>
                            @endcan

                            {{-- <li class="nav-item">
                                <a href="{{ route('bonus.index') }}" class="nav-link {{ request()->routeIs('bonus.index') ? 'active' : '' }}" data-key="t-horizontal">Bonus</a>
                            </li> --}}

                        </ul>
                    </div>
                </li>
                @endcanany

                {{-- Attendance Management --}}
                @canany(['add-absent.add','add-leave.add', 'leave-approval.view'])
                <li class="nav-item">
                    <a class="nav-link menu-link {{ request()->routeIs('add-absent.index') || request()->routeIs('add-leave.index') ? 'active' : 'collapsed'}}" href="#sidebarAuth4" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarAuth4">
                        <i class="ri-account-circle-line"></i> <span data-key="t-authentication">Attendance Management</span>
                    </a>
                    <div class="collapse menu-dropdown {{ request()->routeIs('add-absent.index') || request()->routeIs('add-leave.index') ? 'show' : ''}} " id="sidebarAuth4">
                        <ul class="nav nav-sm flex-column">
                            @canany(['add-absent.add'])
                                <li class="nav-item">
                                    <a href="#sidebarSignUp3" class="nav-link {{ request()->routeIs('add-absent.index') ? 'active' : 'collapsed'}}" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarSignUp3" data-key="t-signup"> Attendance
                                    </a>
                                    <div class="collapse menu-dropdown {{ request()->routeIs('add-absent.index') ? 'show' : ''}}" id="sidebarSignUp3">
                                        <ul class="nav nav-sm flex-column">
                                            @can('add-absent.add')
                                                <li class="nav-item">
                                                    <a href="{{ route('add-absent.index') }}" class="nav-link {{ request()->routeIs('add-absent.index') ? 'active' : '' }}" data-key="t-horizontal">Add Employee Absent Attendance</a>
                                                </li>
                                            @endcan
                                        </ul>
                                    </div>
                                </li>
                            @endcan

                            @canany(['add-leave.add','leave-approval.view'])
                            <li class="nav-item">
                                <a href="#sidebarSignUp" class="nav-link {{ request()->routeIs('add-leave.index') ? 'active' : 'collapsed'}}" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarSignUp" data-key="t-signup"> Leave Application
                                </a>
                                <div class="collapse menu-dropdown {{ request()->routeIs('add-leave.index') ? 'show' : ''}}" id="sidebarSignUp">
                                    <ul class="nav nav-sm flex-column">
                                        @can('add-leave.add')
                                            <li class="nav-item">
                                                <a href="{{ route('add-leave.index') }}" class="nav-link {{ request()->routeIs('add-leave.index') ? 'active' : '' }}" data-key="t-horizontal">Add</a>
                                            </li>
                                        @endcan
                                        @can('leave-approval.view')
                                        <li class="nav-item">
                                            <a href="{{ route('leave-approval.index') }}" class="nav-link {{ request()->routeIs('leave-approval.index') ? 'active' : '' }}" data-key="t-horizontal">Pending</a>
                                        </li>
                                        @endcan
                                    </ul>
                                </div>
                            </li>
                            @endcanany

                        </ul>
                    </div>
                </li>
                @endcanany

                @canany(['freeze-attendance.view'])
                <li class="nav-item">
                    <a class="nav-link menu-link {{ request()->routeIs('freeze-attendance.index') ? 'active' : 'collapsed' }}" href="#sidebarLayouts2" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarLayouts2">
                        <i class="ri-layout-3-line"></i>
                        <span data-key="t-layouts">Freeze Attendance</span>
                    </a>
                    <div class="collapse menu-dropdown {{ request()->routeIs('freeze-attendance.index') ? 'show' : '' }}" id="sidebarLayouts2">
                        <ul class="nav nav-sm flex-column">
                            @can('freeze-attendance.view')
                                <li class="nav-item">
                                    <a href="{{ route('freeze-attendance.index') }}" class="nav-link {{ request()->routeIs('freeze-attendance.index') ? 'active' : '' }}" data-key="t-horizontal">Freeze</a>
                                </li>
                            @endcan

                        </ul>
                    </div>
                </li>
                @endcanany

                @canany(['supplimentary-bill.view'])
                <li class="nav-item">
                    <a class="nav-link menu-link {{ request()->routeIs('supplimentary-bill.index') || request()->routeIs('suspend-supplimentary-bill.index') ? 'active' : 'collapsed' }}" href="#sidebarLayouts3" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarLayouts3">
                        <i class="ri-layout-3-line"></i>
                        <span data-key="t-layouts">Supplimentory Bill</span>
                    </a>
                    <div class="collapse menu-dropdown {{ request()->routeIs('supplimentary-bill.index') || request()->routeIs('suspend-supplimentary-bill.index') ? 'show' : '' }}" id="sidebarLayouts3">
                        <ul class="nav nav-sm flex-column">

                                <li class="nav-item">
                                    <a href="{{ route('supplimentary-bill.index') }}" class="nav-link {{ request()->routeIs('supplimentary-bill.index') ? 'active' : '' }}" data-key="t-horizontal">Regular</a>
                                </li>

                                <li class="nav-item">
                                    <a href="{{ route('suspend-supplimentary-bill.index') }}" class="nav-link {{ request()->routeIs('suspend-supplimentary-bill.index') ? 'active' : '' }}" data-key="t-horizontal">Suspend</a>
                                </li>
                        </ul>
                    </div>
                </li>
                @endcanany

                {{-- Pension --}}
                @canany(['pension.view', 'freeze-pension.view', 'pensioner-bill.view', 'pension-bank-statement.view'])
                <li class="nav-item">
                    <a class="nav-link menu-link {{ request()->routeIs('pension.index') || request()->routeIs('freeze-pension.index') || request()->routeIs('pensioner-bill.index') || request()->routeIs('pension-bank-statement.index') || request()->routeIs('grand-total-pensioner.index') ? 'active' : 'collapsed' }}" href="#sidebarLayouts4" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarLayouts4">
                        <i class="ri-layout-3-line"></i>
                        <span data-key="t-layouts">Pension</span>
                    </a>
                    <div class="collapse menu-dropdown {{ request()->routeIs('pension.index') || request()->routeIs('freeze-pension.index') || request()->routeIs('pensioner-bill.index') || request()->routeIs('pension-bank-statement.index') || request()->routeIs('grand-total-pensioner.index') ? 'show' : '' }}" id="sidebarLayouts4">
                        <ul class="nav nav-sm flex-column">
                                @can('pension.view')
                                <li class="nav-item">
                                    <a href="{{ route('pension.index') }}" class="nav-link {{ request()->routeIs('pension.index') ? 'active' : '' }}" data-key="t-horizontal">Pension List</a>
                                </li>
                                @endcan

                                @can('freeze-pension.view')
                                <li class="nav-item">
                                    <a href="{{ route('freeze-pension.index') }}" class="nav-link {{ request()->routeIs('freeze-pension.index') ? 'active' : '' }}" data-key="t-horizontal">Freeze Pension</a>
                                </li>
                                @endcan
                                @can('pensioner-bill.view')
                                <li class="nav-item">
                                    <a href="{{ route('pensioner-bill.index') }}" class="nav-link {{ request()->routeIs('pensioner-bill.index') ? 'active' : '' }}" data-key="t-horizontal">Pensioner Bill</a>
                                </li>
                                @endcan
                                @can('pension-bank-statement.view')
                                <li class="nav-item">
                                    <a href="{{ route('pension-bank-statement.index') }}" class="nav-link {{ request()->routeIs('pension-bank-statement.index') ? 'active' : '' }}" data-key="t-horizontal">Bank Statement</a>
                                </li>
                                @endcan
                                @can('grand-total-pdf.view')
                                <li class="nav-item">
                                    <a href="{{ route('grand-total-pensioner.index') }}" class="nav-link {{ request()->routeIs('grand-total-pensioner.index') ? 'active' : '' }}" data-key="t-horizontal">Grand Total Pensioner Bill </a>
                                </li>
                                @endcan
                        </ul>
                    </div>
                </li>
                @endcanany

                {{-- Pension End --}}

                {{-- PF Start --}}
                @canany(['pf-opening-balance.view', 'pf-department-loan.create', 'generate-pf-report.create', 'pf-closing.create'])
                <li class="nav-item">
                    <a class="nav-link menu-link {{ request()->routeIs('pf-opening-balance.index') || request()->routeIs('pf-department-loan.index') || request()->routeIs('pf-report.index') || request()->routeIs('generate-pf-report.index') || request()->routeIs('pf-closing.index') ? 'active' : 'collapsed' }}" href="#sidebarLayouts5" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarLayouts5">
                        <i class="ri-layout-3-line"></i>
                        <span data-key="t-layouts">PF Department</span>
                    </a>
                    <div class="collapse menu-dropdown {{ request()->routeIs('pf-opening-balance.index') || request()->routeIs('pf-department-loan.index') || request()->routeIs('generate-pf-report.index') || request()->routeIs('pf-report.index') || request()->routeIs('pf-closing.index') ? 'show' : '' }}" id="sidebarLayouts5">
                        <ul class="nav nav-sm flex-column">
                                @can('pf-opening-balance.view')
                                <li class="nav-item">
                                    <a href="{{ route('pf-opening-balance.index') }}" class="nav-link {{ request()->routeIs('pf-opening-balance.index') ? 'active' : '' }}" data-key="t-horizontal">PF Opening Balance</a>
                                </li>
                                @endcan
                                @can('pf-department-loan.create')
                                <li class="nav-item">
                                    <a href="{{ route('pf-department-loan.index') }}" class="nav-link {{ request()->routeIs('pf-department-loan.index') ? 'active' : '' }}" data-key="t-horizontal">PF Department Loan</a>
                                </li>
                                @endcan
                                @can('generate-pf-report.create')
                                <li class="nav-item">
                                    <a href="{{ route('generate-pf-report.index') }}" class="nav-link {{ request()->routeIs('generate-pf-report.index') ? 'active' : '' }}" data-key="t-horizontal">Generate PF Report</a>
                                </li>
                                @endcan
                                @can('pf-report.view')
                                <li class="nav-item">
                                    <a href="{{ route('pf-report.index') }}" class="nav-link {{ request()->routeIs('pf-report.index') ? 'active' : '' }}" data-key="t-horizontal">PF Report</a>
                                </li>
                                @endcan
                                @can('pf-closing.create')
                                <li class="nav-item">
                                    <a href="{{ route('pf-closing.index') }}" class="nav-link {{ request()->routeIs('pf-closing.index') ? 'active' : '' }}" data-key="t-horizontal">PF Closing</a>
                                </li>
                                @endcan
                        </ul>
                    </div>
                </li>
                @endcanany
                {{-- PF End --}}


                {{-- Income Tax --}}
                {{-- @canany(['pf-opening-balance.view', 'pf-department-loan.create', 'generate-pf-report.create', 'pf-closing.create']) --}}
                <li class="nav-item">
                    <a class="nav-link menu-link {{ request()->routeIs('tax-calculation') ? 'active' : 'collapsed' }}" href="#sidebarLayouts55" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarLayouts55">
                        <i class="ri-layout-3-line"></i>
                        <span data-key="t-layouts">Income Tax</span>
                    </a>
                    <div class="collapse menu-dropdown {{ request()->routeIs('tax-calculation') ? 'show' : '' }}" id="sidebarLayouts55">
                        <ul class="nav nav-sm flex-column">
                                {{-- @can('pf-opening-balance.view') --}}
                                <li class="nav-item">
                                    <a href="{{ route('tax-calculation') }}" class="nav-link {{ request()->routeIs('tax-calculation') ? 'active' : '' }}" data-key="t-horizontal">Income Tax Calculation</a>
                                </li>
                                {{--<li class="nav-item">
                                    <a href="{{ route('showInvestmentForm') }}" class="nav-link {{ request()->routeIs('showInvestmentForm') ? 'active' : '' }}" data-key="t-horizontal">Investment Form</a>
                                </li>--}}
                                {{-- @endcan --}}
                        </ul>
                    </div>
                </li>
                {{-- @endcanany --}}
                {{-- Income Tax End --}}

                {{-- Service Book --}}
                {{-- @canany(['pf-opening-balance.view', 'pf-department-loan.create', 'generate-pf-report.create', 'pf-closing.create']) --}}
                <li class="nav-item">
                    <a class="nav-link menu-link {{ request()->routeIs('service-book-pdf') ? 'active' : 'collapsed' }}" href="#sidebarLayouts56" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarLayouts56">
                        <i class="ri-layout-3-line"></i>
                        <span data-key="t-layouts">Service Book</span>
                    </a>
                    <div class="collapse menu-dropdown {{ request()->routeIs('service-book-pdf') ? 'show' : '' }}" id="sidebarLayouts56">
                        <ul class="nav nav-sm flex-column">
                                {{-- @can('pf-opening-balance.view') --}}
                                <li class="nav-item">
                                    <a href="{{ route('service-book-pdf') }}" class="nav-link {{ request()->routeIs('service-book-pdf') ? 'active' : '' }}" data-key="t-horizontal">Download Service Book</a>
                                </li>
                                {{-- @endcan --}}
                        </ul>
                    </div>
                </li>
                {{-- @endcanany --}}
                {{-- Service Book End --}}

                @canany(['salary-slips.view','bank-statement.view','pay-sheet-pdf.view','bank-deduction-employee-report.view','pay-sheet-excel.view','allowance-report.view','deduction-report.view','retirement-report.view','yearly-pay-sheet.view'])
                <li class="nav-item">
                    <a class="nav-link menu-link {{ request()->routeIs('salary-slips.index') || request()->routeIs('bank-statement.index') || request()->routeIs('pay-sheet.index') || request()->routeIs('bank-deduction-employee-report.index')
                    || request()->routeIs('bank-deduction-report.index') || request()->routeIs('summary-department-report.index') || request()->routeIs('summary-ward-report.index') || request()->routeIs('summary-report.index') || request()->routeIs('pay-sheet-excel.index') || request()->routeIs('allowance-report.index') || request()->routeIs('deduction-report.index') || request()->routeIs('retirement-report.index') || request()->routeIs('yearly-pay-sheet.index')  ? 'active' : 'collapsed' }}" href="#sidebarLayouts6" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarLayouts6">
                        <i class="ri-layout-3-line"></i>
                        <span data-key="t-layouts">Reports</span>
                    </a>
                    <div class="collapse menu-dropdown {{ request()->routeIs('salary-slips.index') || request()->routeIs('bank-statement.index') || request()->routeIs('pay-sheet.index') || request()->routeIs('bank-deduction-employee-report.index')
                    || request()->routeIs('bank-deduction-report.index') || request()->routeIs('summary-department-report.index') || request()->routeIs('summary-ward-report.index') || request()->routeIs('summary-report.index') || request()->routeIs('pay-sheet-excel.index') || request()->routeIs('allowance-report.index') || request()->routeIs('deduction-report.index') || request()->routeIs('retirement-report.index') || request()->routeIs('yearly-pay-sheet.index') ? 'show' : '' }}" id="sidebarLayouts6">
                        <ul class="nav nav-sm flex-column">
                            @can('salary-slips.view')
                                <li class="nav-item">
                                    <a href="{{ route('salary-slips.index') }}" class="nav-link {{ request()->routeIs('salary-slips.index') ? 'active' : '' }}" data-key="t-horizontal">Salary Slip</a>
                                </li>
                            @endcan
                            @can('bank-statement.view')
                                <li class="nav-item">
                                    <a href="{{ route('bank-statement.index') }}" class="nav-link {{ request()->routeIs('bank-statement.index') ? 'active' : '' }}" data-key="t-horizontal">Bank Statement</a>
                                </li>
                            @endcan
                            @can('pay-sheet-pdf.view')
                                <li class="nav-item">
                                    <a href="{{ route('pay-sheet.index') }}" class="nav-link {{ request()->routeIs('pay-sheet.index') ? 'active' : '' }}" data-key="t-horizontal">Pay Sheet</a>
                                </li>
                            @endcan
                            @can('yearly_pay_sheet.show')
                            <li class="nav-item">
                                <a href="{{ route('yearly-pay-sheet.index') }}" class="nav-link {{ request()->routeIs('yearly-pay-sheet.index') ? 'active' : '' }}" data-key="t-horizontal">Yearly Pay Sheet</a>
                            </li>
                            @endcan
                            @can('bank-deduction-employee-report.view')
                            <li class="nav-item">
                                <a href="{{ route('bank-deduction-employee-report.index') }}" class="nav-link {{ request()->routeIs('bank-deduction-employee-report.index') ? 'active' : '' }}" data-key="t-horizontal">Bank Deduction Employee Wise Report</a>
                            </li>
                            @endcan
                            @can('bank-deduction-report.view')
                            <li class="nav-item">
                                <a href="{{ route('bank-deduction-report.index') }}" class="nav-link {{ request()->routeIs('bank-deduction-report.index') ? 'active' : '' }}" data-key="t-horizontal">Bank Deduction Report</a>
                            </li>
                            @endcan
                            @can('summary-department-report.view')
                            <li class="nav-item">
                                <a href="{{ route('summary-department-report.index') }}" class="nav-link {{ request()->routeIs('summary-department-report.index') ? 'active' : '' }}" data-key="t-horizontal">Grand Summary Department Wise</a>
                            </li>
                            @endcan
                            @can('summary-ward-report.view')
                            <li class="nav-item">
                                <a href="{{ route('summary-ward-report.index') }}" class="nav-link {{ request()->routeIs('summary-ward-report.index') ? 'active' : '' }}" data-key="t-horizontal">Grand Summary Ward Wise</a>
                            </li>
                            @endcan
                            @can('summary-report.view')
                            <li class="nav-item">
                                <a href="{{ route('summary-report.index') }}" class="nav-link {{ request()->routeIs('summary-report.index') ? 'active' : '' }}" data-key="t-horizontal">Grand Summary Report</a>
                            </li>
                            @endcan
                            @can('pay-sheet-excel.view')
                            <li class="nav-item">
                                <a href="{{ route('pay-sheet-excel.index') }}" class="nav-link {{ request()->routeIs('pay-sheet-excel.index') ? 'active' : '' }}" data-key="t-horizontal">Pay Sheet Excel</a>
                            </li>
                            @endcan
                            @can('allowance-report.view')
                            <li class="nav-item">
                                <a href="{{ route('allowance-report.index') }}" class="nav-link {{ request()->routeIs('allowance-report.index') ? 'active' : '' }}" data-key="t-horizontal">Allowance Report</a>
                            </li>
                            @endcan
                            @can('deduction-report.view')
                            <li class="nav-item">
                                <a href="{{ route('deduction-report.index') }}" class="nav-link {{ request()->routeIs('deduction-report.index') ? 'active' : '' }}" data-key="t-horizontal">Deduction Report</a>
                            </li>
                            @endcan
                            @can('retirement-report.view')
                            <li class="nav-item">
                                <a href="{{ route('retirement-report.index') }}" class="nav-link {{ request()->routeIs('retirement-report.index') ? 'active' : '' }}" data-key="t-horizontal">Retirement Report</a>
                            </li>
                            @endcan
                        </ul>
                    </div>
                </li>
                @endcanany
            </ul>
        </div>
    </div>

    <div class="sidebar-background"></div>
</div>


<div class="vertical-overlay"></div>
