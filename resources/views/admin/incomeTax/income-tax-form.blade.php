<x-admin.layout>
    <x-slot name="title">INCOME TAX</x-slot>
    <x-slot name="heading">INCOME TAX</x-slot>

    <style>
        .section-title {
            background-color: #d9cbe7;
            color: black;
            font-weight: bold;
            padding: 5px 10px;
            margin-bottom: 10px;
            text-align: center;
            border-radius: 5px;
        }

        .section-content {
            background-color: #f3f1f8;
            padding: 15px;
            border: 2px solid #bfb1de;
            border-radius: 5px;
        }

        .form-control {
            /* margin: 0; */
            padding: 5px;
            height: 35px;
        }

        .section-footer {
            background-color: #292929;
            color: white;
            padding: 5px;
            border-radius: 5px;
        }

        .section-footer input {
            background-color: #e0e0e0;
            color: #000;
            border: 0;
            font-weight: bold;
            text-align: center;
            width: 100%;
        }

        .claim-button {
            background-color: #ccc;
            color: black;
            border: 1px solid #999;
            padding: 3px 10px;
            border-radius: 3px;
        }

        .claim-button:hover {
            background-color: #bbb;
        }

       /* ---- Deduction  -----*/
       .hidden-section {
            display: none;
            margin-top: 2px;
            background-color: #ffffff;
            padding: 20px;
            border: 2px solid #bfb1de;
            border-radius: 5px;
        }

        .toggle-btn {
            background-color: #d9cbe7;
            cursor: pointer;
            padding: 10px;
            border-radius: 5px;
            text-align: center;
            font-weight: bold;
        }

        .deduction-table th,
        .deduction-table td {
            padding: 8px;
        }

        /* Claculate Tax */
        .calculate-btn {
            width: 50%;
            background-color: #8C68CD;
            color: white;
            padding: 10px;
            font-size: 25px;
            border: none;
            border-radius: 8px;
            cursor: pointer;
            display: block;
            margin: 0px auto;
        }


        .black-section {
            background-color: #333;
            color: white;
            padding: 20px;
            border-radius: 8px;
            margin-top: 20px;
        }

        .btnSubmit{
            width: 10%;
            background-color: #8C68CD;
            color: white;
            font-size: 20px;
            border: none;
            border-radius: 8px;
            cursor: pointer;
            display: block;
            margin: 20px auto;
            padding: 10px;
            text-align: center;
        }

        /* Loader style */
        .loader {
            display: inline-block;
            border: 4px solid #f3f3f3;
            border-radius: 50%;
            border-top: 4px solid #3498db;
            width: 40px;
            height: 40px;
            -webkit-animation: spin 2s linear infinite; /* Safari */
            animation: spin 2s linear infinite;
        }

        /* Safari */
        @-webkit-keyframes spin {
            0% { -webkit-transform: rotate(0deg); }
            100% { -webkit-transform: rotate(360deg); }
        }

        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }

        .taxTable table {
            border-collapse: collapse;
            width: 50%;
            margin: 20px auto;
        }

        .taxTable th, .taxTable td {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: left;
        }

        .taxTable th {
            background-color: #f2f2f2;
        }

    </style>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet">

    <div class="row">
        <div class="col-lg-12">
            <div class="card">

                <div class="card-header">
                    <!--------------------------- Tables  Data -------------------------------->
                        <div class="container">
                            <h2 class="text-center my-3" style="text-transform:uppercase">Income Tax Calculator</h2>

                            <form id="saveIncomeTax" autocomplete="off">
                            @csrf
                            <!-- Section: Financial Year and Employee ID -->
                            <div class="">
                                <div class="section-title"> </div><br>
                                <div class="row mb-5">
                                    <div class="col-md-4">
                                        <label for="financial_year" class="form-label">Financial Year:</label>
                                        <select class="form-select" id="financial_year" name="financial_year_id">
                                            @foreach($finYears as $id => $range)
                                                <option value="{{$id}}">{{$range}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="Emp_Code" class="form-label">Employee ID:</label>
                                        <select class="form-select js-example-basic-single" id="Emp_Code" name="Emp_Code">
                                            <option value="">--Select Employee--</option>
                                            @foreach ($employees as $employee)
                                                <option value="{{ $employee->employee_id }}">{{ $employee->fname." ".$employee->mname." ".$employee->lname." (".$employee->employee_id.")" }}</option>
                                            @endforeach
                                        </select>
                                        <span class="text-danger invalid Emp_Code_err" data-error-for="Emp_Code"></span>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="tax_regime" class="form-label">Tax Regime:</label>
                                        <select class="form-select" id="tax_regime" name="tax_regime">
                                            <option value=1 selected>Old</option>
                                            <option value=2>New</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <!-- Section: Personal Information -->
                            <div class="section-content mb-5" style="">
                                <div class="section-title">Personal Information</div>
                                <table class="table table-borderless table-sm">
                                    <tbody>
                                        <tr>
                                            <td> Employee Name:</td>
                                            <td><input class="form-control" type="text" id="emp_name" name="emp_name" placeholder=""></td>
                                            <td>Gender:</td>
                                            <td>
                                                <select class="form-select" id="gender"  name="gender">
                                                    <option>--Select--</option>
                                                    <option value="1">Male</option>
                                                    <option value="2">Female</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Date of Birth</td>
                                            <td><input class="form-control" type="date" id="dob" name="dob" ></td>
                                            <td>Pan No:</td>
                                            <td><input class="form-control" type="text" id="pan" name="pan" placeholder=""></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>


                            <!-- Section: Salary Income -->
                            <div class="section-content">
                                <div class="section-title">Salary Income</div>
                                <table class="table table-borderless table-sm">
                                    <tbody>
                                        <tr>
                                            <td>Previous Employer Gross:</td>
                                            <td><input class="form-control gross_income" type="number" id="previous_gross" name="prev_gross" placeholder="₹0.00"></td>
                                            <td>Current Gross Salary:</td>
                                            <td><input class="form-control gross_income" type="number" id="current_gross" name="current_gross" placeholder="₹0.00">
                                                <span class="text-danger invalid current_gross_err" data-error-for="current_gross"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>Estimated Salary:</td>
                                            <td><input class="form-control gross_income" type="number" id="estimated_salary" name="estimated_salary" placeholder="₹0.00"></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>Supplementary Bill:</td>
                                            <td><input class="form-control gross_income" type="number" id="supplementary_bill" name="supplementary_bill" placeholder="₹0.00"></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>Estimated Supplementary Bill:</td>
                                            <td><input class="form-control gross_income" type="number" id="estimated_supplementary_bill" name="estimated_supp_bill" placeholder="₹0.00"></td>
                                        </tr>
                                    </tbody>
                                </table>

                                <div class="section-footer">
                                    <label>Gross Income:</label>
                                    <input type="number" id="gross_income" name="gross_income" class="form-control bg-light" >
                                </div>
                            </div>

                            <!-- Section: Exemptions -->
                            <div class="section-content mt-4">
                                <div class="section-title">Exemptions</div>
                                <table class="table table-borderless table-sm">
                                    <tbody>
                                        <tr>
                                            <td>House Rent Exemption:</td>
                                            <td><input class="form-control exemptions" type="number" id="house_rent_exemption" name="hre" placeholder="₹0.00"></td>
                                            <td></td>
                                            <td>Standard Deduction:</td>
                                            <td><input class="form-control exemptions" type="number" id="standard_deduction" name="standard_deduction" value=50000 placeholder="₹0.00"></td>
                                        </tr>
                                        <tr>
                                            <td>Previous Employer Ptax:</td>
                                            <td><input class="form-control exemptions" type="number" id="previous_ptax" name="prev_ptax" placeholder="₹0.00"></td>
                                            <td></td>
                                            <td>Professional Tax:</td>
                                            <td><input class="form-control exemptions" type="number" id="professional_tax" name="ptax" value=2500 placeholder="₹0.00"></td>
                                        </tr>
                                        <tr>
                                            <td>Election Allowance:</td>
                                            <td><input class="form-control exemptions" type="number" id="election_allowance" name="elec_allowance" placeholder="₹0.00"></td>
                                            <td></td>
                                            <td>Ex Gratia:</td>
                                            <td><input class="form-control exemptions" type="number" id="ex_gratia" name="ex_gratia" value=0 placeholder="₹0.00"></td>
                                        </tr>
                                        <tr>
                                            <td>Transport Allowance:</td>
                                            <td><input class="form-control exemptions" type="number" id="transport_allowance" name="trans_allowance" placeholder="₹0.00"></td>
                                        </tr>
                                    </tbody>
                                </table>

                                <div class="section-footer">
                                    <label>Total Exemptions:</label>
                                    <input type="number" id="total_exemptions" name="total_exemptions" value="52500" class="form-control bg-light" >
                                </div>
                            </div>

                            <!-- Section: Net Salary -->
                            <div class="section-content mt-4 old-regime">
                                <div class="section-title">Net Salary</div>
                                <table class="table table-borderless table-sm">
                                    <tbody>
                                        <tr>
                                            <td colspan="4"></td>
                                            <td class="text-center">
                                                <input class="form-control mx-auto" style="max-width: 250px;" type="number" id="net_salary_exemption" placeholder="₹0.00">
                                            </td>
                                            <td colspan="4"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                            <!-- Section: Other Income -->
                            <div class="section-content mt-4 old-regime">
                                <div class="section-title">Other Income</div>
                                <table class="table table-borderless table-sm">
                                    <tbody>
                                        <tr>
                                            <td>Investment:</td>
                                            <td><input class="form-control other_incomes" type="number" id="investment" name="investment" placeholder="₹0.00"></td>
                                            <td></td>
                                            <td>Other Investment:</td>
                                            <td><input class="form-control other_incomes" type="number" id="other_investment" name="other_investment" placeholder="₹0.00"></td>
                                        </tr>
                                    </tbody>
                                </table>

                                <div class="section-footer">
                                    <label>Total Investment:</label>
                                    <input type="number" id="total_other_income" name="other_income" class="form-control bg-light" >
                                </div>
                            </div>


                            <!-- Section: Deduction -->
                            <div class="section-content mt-4 deduction-section old-regime">
                                <div class="section-title">Deduction</div>

                                <!-- 8OD Explanation Toggle Button -->
                                <div class="toggle-btn mb-2" id="toggleBODExplanation" onclick="toggleSection('bodExplanationDetails')">
                                    80D(Explanation) ▼

                                    <span style="margin-left: 300px; font-weight: bold;"> Deducted:</span>
                                    <input type="number" class="form-control d-inline-block deductions" name="sec80d" id="sec80d" style="width: 150px;" placeholder="₹0.00">
                                </div>

                                <div class="hidden-section" id="bodExplanationDetails" >
                                    <table class="deduction-table table-borderless">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th colspan="2">Assessee, Spouse, and dependent Children</th>
                                                <th colspan="2">Assessee's parents</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Health Insurance (Age less Than 60) <span class="ri-information-fill"></span>:</td>
                                                <td><input class="form-control eightyd" type="number" id="hial60d" name="hial60d" placeholder="₹0.00"></td>
                                                <td></td>
                                                <td><input class="form-control eightyd" type="number" id="hial60p" name="hial60p" placeholder="₹0.00"></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>Health Insurance (Age More Than 60) <span class="ri-information-fill"></span>:</td>
                                                <td><input class="form-control eightyd" type="number" id="hiam60d" name="hiam60d" placeholder="₹0.00"></td>
                                                <td></td>
                                                <td><input class="form-control eightyd" type="number" id="hiam60p" name="hiam60p" placeholder="₹0.00"></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>Health-Checkup <span class="ri-information-fill"></span>:</td>
                                                <td colspan="1"><input class="form-control eightyd" type="number" id="hcheckup" name="hcheckup" placeholder="₹0.00"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="section-footer">
                                        <label>Total :</label>
                                        <input type="number" id="hitotal" name="hitotal" class="form-control bg-light" >
                                    </div>
                                </div>


                                <!-- 8OG Explanation Toggle Button -->
                                <div class="toggle-btn mb-2" id="toggleBOGExplanation"  onclick="toggleSection('bogExplanationDetails')">
                                    80G (Explanation) ▼

                                    <span style="margin-left: 300px; font-weight: bold;"> Deducted:</span>
                                    <input type="number" class="form-control d-inline-block deductions" name="sec80g" id="sec80g" style="width: 150px;" placeholder="₹0.00">
                                </div>

                                <div class="hidden-section" id="bogExplanationDetails">
                                    <table class="deduction-table table-borderless">
                                        <tbody>
                                            <tr>
                                                <td>Donation <span class="ri-information-fill"></span>:</td>
                                                <td><input class="form-control eightyg" id="don80g" name="don80g" type="number" placeholder="₹0.00"></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>Other Donation <span class="ri-information-fill"></span>:</td>
                                                <td><input class="form-control eightyg" id="odon80g" name="odon80g" type="number" placeholder="₹0.00"></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="section-footer">
                                        <label>Total :</label>
                                        <input type="number" id="totaldon" name="totaldon" class="form-control bg-light" >
                                    </div>
                                </div>


                                <!-- 8OC Explanation Toggle Button -->
                                <div class="toggle-btn mb-2" id="toggleBOCExplanation"  onclick="toggleSection('bocExplanationDetails')">
                                    80C (Explanation) ▼

                                    <span style="margin-left: 300px; font-weight: bold;"> Deducted:</span>
                                    <input type="number" class="form-control d-inline-block deductions" name="sec80c" id="sec80c" style="width: 150px;" placeholder="₹0.00">
                                </div>

                                <div class="hidden-section" id="bocExplanationDetails">
                                    <table class="deduction-table table-borderless">
                                        <tbody>
                                            <tr>
                                                <td>NSC:</td>
                                                <td><input class="form-control eightyc" type="number" id="nsc" name="nsc" placeholder="₹0.00"></td>
                                                <td></td>
                                                <td>NSC Intrest:</td>
                                                <td><input class="form-control eightyc" type="number" id="nsc_interest" name="nsc_int" placeholder="₹0.00"></td>
                                            </tr>
                                            <tr>
                                                <td>LIC:</td>
                                                <td><input class="form-control eightyc" type="number" id="lic" name="lic" placeholder="₹0.00"></td>
                                                <td></td>
                                                <td>Other LIC:</td>
                                                <td><input class="form-control eightyc" type="number" id="other_lic" name="other_lic" placeholder="₹0.00"></td>
                                            </tr>
                                            <tr>
                                                <td>DCPS Employee Contribution:</td>
                                                <td><input class="form-control eightyc" type="number" id="dcp_emp_contribution" name="dcps_emp" placeholder="₹0.00"></td>
                                                <td></td>
                                                <td>Under 80ccc Mutual Fund:</td>
                                                <td><input class="form-control eightyc" type="number" id="mutual_fund" name="mutual_fund" placeholder="₹0.00"></td>
                                            </tr>
                                            <tr>
                                                <td>GIS:</td>
                                                <td><input class="form-control eightyc" type="number" id="gis" name="gis" placeholder="₹0.00"></td>
                                                <td></td>
                                                <td>Other GIS:</td>
                                                <td><input class="form-control eightyc" type="number" id="other_gis" name="other_gis" placeholder="₹0.00"></td>
                                            </tr>
                                            <tr>
                                                <td>PLI:</td>
                                                <td><input class="form-control eightyc" type="number" id="pli" name="pli" placeholder="₹0.00"></td>
                                                <td></td>
                                                <td>Other PLI:</td>
                                                <td><input class="form-control eightyc" type="number" id="other_pli" name="other_pli" placeholder="₹0.00"></td>
                                            </tr>
                                            <tr>
                                                <td>Housing Loan Repayment:</td>
                                                <td><input class="form-control eightyc" type="number" id="housing_loan_repayment" name="hloan" placeholder="₹0.00"></td>
                                                <td></td>
                                                <td>Other Housing Loan Repayment:</td>
                                                <td><input class="form-control eightyc" type="number" id="other_housing_loan_repayment" name="other_hloan" placeholder="₹0.00"></td>
                                            </tr>
                                            <tr>
                                                <td>DCPS 1 Employer Contribution:</td>
                                                <td><input class="form-control eightyc" type="number" id="dcps_employer_contribution" name="dcps_empr_contrn" placeholder="₹0.00"></td>
                                                <td></td>
                                                <td>Infrastructure Bond:</td>
                                                <td><input class="form-control eightyc" type="number" id="infrastructure_bond" name="infra_bond" placeholder="₹0.00"></td>
                                            </tr>
                                            <tr>
                                                <td>GPF:</td>
                                                <td><input class="form-control eightyc" type="number" id="gpf" name="gpf" placeholder="₹0.00"></td>
                                                <td></td>
                                                <td>PPF:</td>
                                                <td><input class="form-control eightyc" type="number" id="ppf" name="ppf" placeholder="₹0.00"></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td>Education Fees:</td>
                                                <td><input class="form-control eightyc" type="number" id="education_fees" name="edu_fees" placeholder="₹0.00"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="section-footer">
                                        <label>Total :</label>
                                        <input type="number" id="total80c" name="total80c" class="form-control bg-light" >
                                    </div>
                                </div>


                                <!-- 80CCD Explanation Toggle Button -->
                                <div class="toggle-btn mb-2" id="toggleB0CCDExplanation" onclick="toggleSection('BOCCDExplanationDetails')">
                                    80CCD (Explanation) ▼

                                    <span style="margin-left: 280px; font-weight: bold;"> Deducted:</span>
                                    <input type="number" class="form-control d-inline-block deductions" name="sec80ccd" id="sec80ccd" style="width: 150px;" placeholder="₹0.00">
                                </div>

                                <div class="hidden-section" id="BOCCDExplanationDetails">
                                    <table class="deduction-table table-borderless">
                                        <tbody>
                                            <tr>
                                                <td>NPS :</td>
                                                <td><input class="form-control eightyccd" id="npsccd" name="nps" type="number" placeholder="₹0.00"></td>
                                                <td>DCPS + Under 80CCD :</td>
                                                <td><input class="form-control eightyccd" id="dcpsccd" name="dcps_u80ccd" type="number" placeholder="₹0.00"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="section-footer">
                                        <label>Total :</label>
                                        <input type="number" id="totalccd" name="total80ccd" class="form-control bg-light" >
                                    </div>
                                </div>


                                <!-- 80E Explanation Toggle Button -->
                                <div class="toggle-btn mb-2" id="toggleBOEExplanation" onclick="toggleSection('BOEExplanationDetails')">
                                    80E (Explanation) ▼

                                    <span style="margin-left: 300px; font-weight: bold;"> Deducted:</span>
                                    <input type="number" class="form-control d-inline-block deductions" name="sec80e" id="sec80e" style="width: 150px;" placeholder="₹0.00">
                                </div>

                                <div class="hidden-section" id="BOEExplanationDetails">
                                    <table class="deduction-table table-borderless">
                                        <tbody>
                                            <tr>
                                                <td>Education Loan :</td>
                                                <td><input class="form-control" id="eduloan" name="edu_loan" type="number" placeholder="₹0.00"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="section-footer">
                                        <label>Total :</label>
                                        <input type="number" id="total80e" name="total80e" class="form-control bg-light" >
                                    </div>
                                </div>


                                <!-- Section24 Explanation Toggle Button -->
                                <div class="toggle-btn mb-2" id="toggleSection24Explanation" onclick="toggleSection('section24ExplanationDetails')">
                                    Section24 ▼

                                    <span style="margin-left: 360px; font-weight: bold;"> Deducted:</span>
                                    <input type="number" class="form-control d-inline-block deductions" name="sec24" id="sec24" style="width: 150px;" placeholder="₹0.00">
                                </div>

                                <div class="hidden-section" id="section24ExplanationDetails" >
                                    <table class="deduction-table table-borderless">
                                        <tbody>
                                            <tr>
                                                <td>Home Loan Interest Before : </td>
                                                <td><input class="form-control section24" id="hlib" name="hlib24" type="number" placeholder="₹0.00"></td>
                                                <td>Home Loan Interest After:</td>
                                                <td><input class="form-control section24" id="hlia" name="hlia24" type="number" placeholder="₹0.00"></td>
                                            </tr>
                                            <tr>
                                                <td>Oth Home Loan Interest Before:</td>
                                                <td><input class="form-control section24" id="ohlib" name="ohlib24" type="number" placeholder="₹0.00"></td>
                                                <td>Oth Home Loan Interest After:</td>
                                                <td><input class="form-control section24" id="ohlia" name="ohlia24" type="number" placeholder="₹0.00"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="section-footer">
                                        <label>Total :</label>
                                        <input type="number" id="sec24total" name="totalsec24" class="form-control bg-light" >
                                    </div>
                                </div>


                                <!-- 80DDB Explanation Toggle Button -->
                                <div class="toggle-btn mb-2" id="toggleBODDBExplanation" onclick="toggleSection('BODDBExplanationDetails')">
                                    80DDB (Explanation) ▼

                                    <span style="margin-left: 280px; font-weight: bold;"> Deducted:</span>
                                    <input type="number" class="form-control d-inline-block deductions" name="sec80ddb" id="sec80ddb" style="width: 150px;" placeholder="₹0.00">
                                </div>

                                <div class="hidden-section" id="BODDBExplanationDetails">
                                    <table class="deduction-table table-borderless">
                                        <tbody>
                                            <tr>
                                                <td>Medical Treatment  :</td>
                                                <td><input class="form-control eightyddb" type="number" id="medical_treatment" name="medical_treatment" placeholder="₹0.00"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="section-footer">
                                        <label>Total :</label>
                                        <input type="number" id="totalmt" name="total80ddb" class="form-control bg-light" >
                                    </div>
                                </div>


                                <!-- 80TTA Explanation Toggle Button -->
                                <div class="toggle-btn mb-2" id="toggleBOTTAExplanation" onclick="toggleSection('BOTTAExplanationDetails')">
                                    80TTA (Explanation) ▼

                                    <span style="margin-left: 280px; font-weight: bold;"> Deducted:</span>
                                    <input type="number" class="form-control d-inline-block deductions" name="sec80tta" id="sec80tta" style="width: 150px;" placeholder="₹0.00">
                                </div>

                                <div class="hidden-section" id="BOTTAExplanationDetails">
                                    <table class="deduction-table table-borderless">
                                        <tbody>
                                            <tr>
                                                <td>Deduction On SB Interest :</td>
                                                <td><input class="form-control" type="number" id="sb_int_ded" name="sb_int_ded" placeholder="₹0.00"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="section-footer">
                                        <label>Total :</label>
                                        <input type="number" id="ttatotal" name="total80tta" class="form-control bg-light" >
                                    </div>
                                </div>


                                <!-- 8OU Explanation Toggle Button -->
                                <div class="toggle-btn mb-2" id="toggleBOUExplanation"  onclick="toggleSection('BOUExplanationDetails')">
                                    80U (Explanation) ▼

                                    <span style="margin-left: 300px; font-weight: bold;"> Deducted:</span>
                                    <input type="number" class="form-control d-inline-block deductions" name="sec80u" id="sec80u" style="width: 150px;" placeholder="₹0.00">
                                </div>

                                <div class="hidden-section" id="BOUExplanationDetails">
                                    <table class="deduction-table table-borderless">
                                        <tbody>
                                            <tr>
                                                <td>Disability:</td>
                                                <td>40% <input type="radio" name="disability" value="40" placeholder="₹0.00"></td>
                                                <td>60% <input type="radio" name="disability" value="60" placeholder="₹0.00"></td>
                                            </tr>
                                            <tr>
                                                <td>Handicapped Allowance <span class="ri-information-fill"></span>:</td>
                                                <td><input class="form-control" type="number" id="handicappedalwce" name="handicappedalwce" placeholder="₹0.00"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="section-footer">
                                        <label>Total :</label>
                                        <input type="number" id="total80u" name="total80u" class="form-control bg-light" >
                                    </div>
                                </div>


                                <!-- 8OCCD2 Explanation Toggle Button -->
                                <div class="toggle-btn mb-2" id="toggleBOCCD2Explanation"  onclick="toggleSection('BOCCD2ExplanationDetails')">
                                    8OCCD2 (Explanation) ▼

                                    <span style="margin-left: 270px; font-weight: bold;"> Deducted:</span>
                                    <input type="number" class="form-control d-inline-block deductions" name="sec80ccd2" id="sec80ccd2" style="width: 150px;" placeholder="₹0.00">
                                </div>

                                <div class="hidden-section" id="BOCCD2ExplanationDetails">
                                    <table class="deduction-table table-borderless">
                                        <tbody>
                                            <tr>
                                                <td>Employer Contribution:</td>
                                                <td><input type="text" id="empr_contribution" name="empr_contribution" class="form-control" placeholder="₹0.00"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="section-footer">
                                        <label>Total :</label>
                                        <input type="number" id="total80cc2" name="total80cc2" class="form-control bg-light" >
                                    </div>
                                </div>

                            </div>


                            <!-- Section: Total Taxable Salary -->
                            <div class="section-content mt-4">
                                <div class="section-title">Total Taxable Salary</div>
                                <table class="table table-borderless table-sm">
                                    <tbody>
                                        <tr>
                                            <td colspan="4"></td>
                                            <td class="text-center">
                                                <input class="form-control mx-auto" style="max-width: 250px;" type="number" id="total_taxable_salary" name="taxable_sal" placeholder="₹0.00">
                                                <span class="text-danger invalid taxable_sal_err" data-error-for="taxable_sal"></span>
                                            </td>
                                            <td colspan="4"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>


                            <!-- Section: Tax Toggle Button -->
                            <div class="section-content mt-4" >
                                <button class="calculate-btn" type="button" id="calculateTaxBtn" onclick="showTaxTable('TaxExplanationDetails')">Calculate Tax</button>

                                <div class="d-flex justify-content-center d-none" id="TaxExplanationDetails">
                                    <table class="taxTable">
                                        <thead>
                                            <tr>
                                                <th>Income Range</th>
                                                <th>Tax Rate (%)</th>
                                                <th>Taxable Income at Slab</th>
                                                <th>Tax Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>0 - 2,50,000</td>
                                                <td>0</td>
                                                <td id="slab1Income"></td>
                                                <td id="slab1Tax"></td>
                                            </tr>
                                            <tr>
                                                <td>2,50,001 - 5,00,000</td>
                                                <td>5</td>
                                                <td id="slab2Income"></td>
                                                <td id="slab2Tax"></td>
                                            </tr>
                                            <tr>
                                                <td>5,00,001 - 10,00,000</td>
                                                <td>20</td>
                                                <td id="slab3Income"></td>
                                                <td id="slab3Tax"></td>
                                            </tr>
                                            <tr>
                                                <td>10,00,001 and above</td>
                                                <td>30</td>
                                                <td id="slab4Income"></td>
                                                <td id="slab4Tax"></td>
                                            </tr>
                                            <tr>
                                                <td colspan="3"><b>Total Tax</b></td>
                                                <td id="totalTax"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="d-flex justify-content-center d-none" id="TaxExplanationDetailsNewRegime">
                                    <table class="taxTable">
                                        <thead>
                                            <tr>
                                                <th>Income Range</th>
                                                <th>Tax Rate (%)</th>
                                                <th>Taxable Income at Slab</th>
                                                <th>Tax Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>0 - 3,00,000</td>
                                                <td>0</td>
                                                <td id="slab1IncomeN"></td>
                                                <td id="slab1TaxN"></td>
                                            </tr>
                                            <tr>
                                                <td>3,00,001 - 7,00,000</td>
                                                <td>5</td>
                                                <td id="slab2IncomeN"></td>
                                                <td id="slab2TaxN"></td>
                                            </tr>
                                            <tr>
                                                <td>7,00,001 - 10,00,000</td>
                                                <td>10</td>
                                                <td id="slab3IncomeN"></td>
                                                <td id="slab3TaxN"></td>
                                            </tr>
                                            <tr>
                                                <td>10,00,001 - 12,00,000</td>
                                                <td>15</td>
                                                <td id="slab4IncomeN"></td>
                                                <td id="slab4TaxN"></td>
                                            </tr>
                                            <tr>
                                                <td>12,00,001 - 15,00,000</td>
                                                <td>20</td>
                                                <td id="slab5IncomeN"></td>
                                                <td id="slab5TaxN"></td>
                                            </tr>
                                            <tr>
                                                <td>15,00,001 - 50,00,000</td>
                                                <td>30</td>
                                                <td id="slab6IncomeN"></td>
                                                <td id="slab6TaxN"></td>
                                            </tr>
                                            <tr>
                                                <td colspan="3"><b>Total Tax</b></td>
                                                <td id="totalTaxN"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>


                            <!-- Black Data Section -->
                            <div class="black-section">
                                <div class="" id="">
                                    <table class="deduction-table table-borderless">
                                        <tbody>
                                            <tr>
                                                <td>Tax Payable:</td>
                                                <td><input type="text" class="form-control" id="tax_payable" name="tax_payable" placeholder="₹0.00"></td>
                                                <td>Rebate Under 87A: </td>
                                                <td><input type="checkbox" checked></td>
                                                <td>Rebated Amount: </td>
                                                <td><input type="text" class="form-control" id="rebated_amount" name="rebated_amount" placeholder="₹0.00"></td>
                                            </tr>
                                            <tr>
                                                <td>Tax Payable Before Cess:</td>
                                                <td><input type="text" class="form-control" id="tax_before_cess" name="net_tax" placeholder="₹0.00"></td>
                                                <td>Edu Cess(4%): </td>
                                                <td><input type="text" class="form-control" id="edu_cess" name="edu_cess" placeholder="₹0.00"></td>
                                                <td>Tax Paid in Cash: </td>
                                                <td><input type="text" class="form-control" name="tax_paid_cash" value=0 placeholder="₹0.00"></td>
                                            </tr>
                                            <tr>
                                                <td>Tax Payable After Cess:</td>
                                                <td><input type="text" class="form-control" id="tax_after_cess" name="tax_after_cess" placeholder="₹0.00"></td>
                                                <td>Tax Deducted Upto Now:</td>
                                                <td><input type="text" class="form-control" name="tax_deducted" value=0 placeholder="₹0.00"></td>
                                                <td>Tax Pending: </td>
                                                <td><input type="text" class="form-control" name="tax_pending" placeholder="₹0.00"></td>
                                            </tr>
                                            <tr>
                                                <td>Tax for Remaining Months:</td>
                                                <td><input type="text" class="form-control" name="tax_rem_months" placeholder="₹0.00"></td>
                                                <td>Tax Refunded:</td>
                                                <td><input type="text" class="form-control" name="tax_refunded" placeholder="₹0.00"></td>
                                                <td>User Requested Deduction: </td>
                                                <td><input type="text" class="form-control" name="user_req_ded" placeholder="₹0.00"></td>
                                            </tr>
                                            <tr>
                                                <td>Reason For Requestd Deduction:</td>
                                                <td><input type="text" class="form-control" name="reason_requested_ded" placeholder="₹0.00"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <button class="btnSubmit" id="submitIncomeTax" type="submit">Submit</button>

                        </div>
                    <!--------------------------- Tables  Data -------------------------------->
                    </form>
                </div>

            </div>
        </div>
    </div>



    <script>
        function toggleSection(sectionId) {
            var selectedSection = document.getElementById(sectionId);

            var isVisible = selectedSection.style.display === 'block';

            var allSections = document.querySelectorAll('.hidden-section');
            allSections.forEach(function (section) {
                section.style.display = 'none';
            });

            if (!isVisible) {
                selectedSection.style.display = 'block';
            }
        }

        function showTaxTable(sectionId) {
            if($('#tax_regime').val() == 2){
                $("#TaxExplanationDetailsNewRegime").removeClass("d-none");
                $("#TaxExplanationDetails").addClass("d-none");
            }else{
                $("#TaxExplanationDetails").removeClass("d-none");
                $("#TaxExplanationDetailsNewRegime").addClass("d-none");
            }
        }

        </script>


</x-admin.layout>

<script>

$(document).ready(function() {

    $( ".gross_income" ).on( "keyup", function() {

        if($('#Emp_Code').val() == ''){
            swal("Error!", 'Please select Employee ID', "error")
            $('#Emp_Code').focus();
            return false
        }

        let pg = parseFloat($('#previous_gross').val()) || 0
        let cg = parseFloat($('#current_gross').val()) || 0
        let es = parseFloat($('#estimated_salary').val()) || 0
        let sb = parseFloat($('#supplementary_bill').val()) || 0
        let esb = parseFloat($('#estimated_supplementary_bill').val()) || 0

        let total_gross = pg + cg + es + sb + esb

        $('#gross_income').val(total_gross)

        let net_salary = total_gross - $('#total_exemptions').val()
        $('#net_salary_exemption').val(net_salary)
        calculateTaxableSalary()
    });

    $( ".exemptions" ).on( "keyup", function() {

        if($('#Emp_Code').val() == ''){
            swal("Error!", 'Please select Employee ID', "error")
            $('#Emp_Code').focus();
            return false
        }

        let hre = parseFloat($('#house_rent_exemption').val()) || 0
        let ppt = parseFloat($('#previous_ptax').val()) || 0
        let ea  = parseFloat($('#election_allowance').val()) || 0
        let ta  = parseFloat($('#transport_allowance').val()) || 0
        let sd  = parseFloat($('#standard_deduction').val()) || 0
        let pt  = parseFloat($('#professional_tax').val()) || 0
        let eg  = parseFloat($('#ex_gratia').val()) || 0

        let total_exemptions = hre + ppt + ea + ta + sd + pt + eg

        $('#total_exemptions').val(total_exemptions)

        let net_salary = $('#gross_income').val() - total_exemptions
        $('#net_salary_exemption').val(net_salary)
        calculateTaxableSalary()
    });

    $( ".other_incomes" ).on( "keyup", function() {
        let inc = parseFloat($('#investment').val()) || 0
        let oinc = parseFloat($('#other_investment').val()) || 0
        let total_income = inc + oinc

        $('#total_other_income').val(total_income)
        calculateTaxableSalary()
    });

    $( ".eightyd" ).on( "keyup", function() {
        let hi1 = parseFloat($('#hial60d').val()) || 0
        let hi2 = parseFloat($('#hial60p').val()) || 0
        let hi3  = parseFloat($('#hiam60d').val()) || 0
        let hi4  = parseFloat($('#hiam60p').val()) || 0
        let hc  = parseFloat($('#hcheckup').val()) || 0

        let total = hi1 + hi2 + hi3 + hi4 + hc

        $('#hitotal').val(total)
        $('#sec80d').val(total)

        calculateTaxableSalary()
    });

    $( ".eightyg" ).on( "keyup", function() {
        let d1 = parseFloat($('#don80g').val()) || 0
        let d2 = parseFloat($('#odon80g').val()) || 0

        let total = d1 + d2

        $('#totaldon').val(total)
        $('#sec80g').val(total)

        calculateTaxableSalary()
    });

    $( ".eightyc" ).on( "keyup", function() {
        let c1 = parseFloat($('#nsc').val()) || 0
        let c2 = parseFloat($('#lic').val()) || 0
        let c3 = parseFloat($('#dcp_emp_contribution').val()) || 0
        let c4 = parseFloat($('#gis').val()) || 0
        let c5 = parseFloat($('#pli').val()) || 0
        let c6 = parseFloat($('#housing_loan_repayment').val()) || 0
        let c7 = parseFloat($('#dcps_employer_contribution').val()) || 0
        let c8 = parseFloat($('#gpf').val()) || 0
        let c9 = parseFloat($('#nsc_interest').val()) || 0
        let c10 = parseFloat($('#other_lic').val()) || 0
        let c11 = parseFloat($('#mutual_fund').val()) || 0
        let c12 = parseFloat($('#other_gis').val()) || 0
        let c13 = parseFloat($('#other_pli').val()) || 0
        let c14 = parseFloat($('#other_housing_loan_repayment').val()) || 0
        let c15 = parseFloat($('#infrastructure_bond').val()) || 0
        let c16 = parseFloat($('#ppf').val()) || 0
        let c17 = parseFloat($('#education_fees').val()) || 0

        let total = c1 + c2 +c3 + c4 +c5 + c6 +c7 + c8 +c9 + c10 +c11 + c12 +c13 + c14 +c15 + c16 +c17

        $('#total80c').val(total)
        $('#sec80c').val(total)

        calculateTaxableSalary()
    });

    $( ".eightyccd" ).on( "keyup", function() {
        let d1 = parseFloat($('#npsccd').val()) || 0
        let d2 = parseFloat($('#dcpsccd').val()) || 0

        let total = d1 + d2

        $('#totalccd').val(total)
        $('#sec80ccd').val(total)

        calculateTaxableSalary()
    });

    $( "#eduloan" ).on( "keyup", function() {
        let l = parseFloat($('#eduloan').val()) || 0
        $('#total80e').val(l)
        $('#sec80e').val(l)
        calculateTaxableSalary()
    });

    $( ".section24" ).on( "keyup", function() {
        let l1 = parseFloat($('#hlib').val()) || 0
        let l2 = parseFloat($('#hlia').val()) || 0
        let l3 = parseFloat($('#ohlib').val()) || 0
        let l4 = parseFloat($('#ohlia').val()) || 0
        let total = l1 + l2 + l3 + l4

        $('#sec24').val(total)
        $('#sec24total').val(total)
        calculateTaxableSalary()
    });

    $( ".eightyddb" ).on( "keyup", function() {
        let m = parseFloat($('#medical_treatment').val()) || 0
        $('#totalmt').val(m)
        $('#sec80ddb').val(m)
        calculateTaxableSalary()
    });

    $( "#sb_int_ded" ).on( "keyup", function() {
        let sbint = parseFloat($('#sb_int_ded').val()) || 0
        $('#ttatotal').val(sbint)
        $('#sec80tta').val(sbint)
        calculateTaxableSalary()
    });

    $( "#handicappedalwce" ).on( "keyup", function() {
        let hallw = parseFloat($('#handicappedalwce').val()) || 0
        $('#total80u').val(hallw)
        $('#sec80u').val(hallw)
        calculateTaxableSalary()
    });

    $( "#empr_contribution" ).on( "keyup", function() {
        let ec = parseFloat($('#empr_contribution').val()) || 0
        $('#total80cc2').val(ec)
        $('#sec80ccd2').val(ec)
        calculateTaxableSalary()
    });

    function calculateTaxableSalary(){
        let net_salary = parseFloat($('#net_salary_exemption').val()) || 0
        let other_income = parseFloat($('#total_other_income').val()) || 0

        let deductions = [];
        $(".deductions").each(function() {
            deduction = parseFloat($(this).val()) || 0;
            deductions.push(deduction);
        });
		let totalDeductions = deductions.reduce((sum, deduction) => sum + deduction, 0);

        $('#total_taxable_salary').val(net_salary + other_income - totalDeductions)
    }

    // Function to process inputs and calculate taxable income
    $("#calculateTaxBtn").click(function() {
        let grossSalary = parseFloat($("#gross_income").val());
        // let grossSalary = 663430;
        // let grossSalary = 1050000;
        
        //------For new regime-------//
        if($('#tax_regime').val() == 2){
            let exgratia = parseFloat($('#ex_gratia').val())
            let taxableincome = $('#total_taxable_salary').val()
            // $('#total_taxable_salary').val(taxableincome)
            let tax_payable = calculateTaxNewRegime(taxableincome)
            $('#tax_payable').val(tax_payable)

            showSlabwiseCalculationNewRegime(taxableincome)

            return true
        }
        //---------------------------//

        // let deductions = [];
        
        // let deduction = 0;
        // $(".exemptions").each(function() {
        //     deduction = parseFloat($(this).val()) || 0;
        //     deductions.push(deduction);
        // });

        // let totalExemption = deductions.reduce((sum, deduction) => sum + deduction, 0);
        // $('#net_salary_exemption').val(grossSalary - totalExemption)

        // let other_incomes = [];
        // let otherincome = 0
        // $(".other_incomes").each(function() {
        //     otherincome = parseFloat($(this).val()) || 0;
        //     other_incomes.push(otherincome);
        // });

        // $(".deductions").each(function() {
        //     deduction = parseFloat($(this).val()) || 0;
        //     deductions.push(deduction);
        // });

        // let taxableIncome = calculateTaxableIncome(grossSalary, deductions, other_incomes);
        // $('#total_taxable_salary').val(taxableIncome)
        let taxableIncome = $('#total_taxable_salary').val()
        
        let tax_payable = calculateTax(taxableIncome)
        $('#tax_payable').val(tax_payable)

        showSlabwiseCalculation(taxableIncome)
    });

    function calculateTaxableIncome(grossSalary, deductions, other_incomes) {
        let totalDeductions = deductions.reduce((sum, deduction) => sum + deduction, 0);
        let totalOtherIncomes = other_incomes.reduce((sum, oi) => sum + oi, 0);

        let taxable_income = parseFloat(grossSalary + totalOtherIncomes - totalDeductions) || 0
        return taxable_income;
    }

    // Old Tax Regime
    function calculateTax(taxable_income) {
        let tax = 0;
    
        if (taxable_income <= 250000) {
            tax = 0;
        } else if (taxable_income <= 500000) {
            tax = (taxable_income - 250000) * 0.05;
        } else if (taxable_income <= 1000000) {
            tax = 12500 + (taxable_income - 500000) * 0.2;
        } else {
            tax = 112500 + (taxable_income - 1000000) * 0.3;
        }

        let net_tax = tax.toFixed(2)
        let edu_cess = Math.round(net_tax * 0.04)

        //Apply rebate Section 87a
        var rebated_amount = 0
        if(taxable_income <= 500000){
            if(net_tax < 12500){
                rebated_amount = net_tax
            }else{
                rebated_amount = 12500
            }
            net_tax = net_tax - rebated_amount
        }
        $('#rebated_amount').val(rebated_amount);
        if(net_tax == 0){
            edu_cess = 0
        }   
        let total_tax = Math.round(parseFloat(net_tax) + parseFloat(edu_cess))

        $('#tax_before_cess').val(Math.round(net_tax));
        $('#tax_after_cess').val(total_tax);
        $('#edu_cess').val(edu_cess);
        
        return total_tax;
    }

    // New Tax Regime
    function calculateTaxNewRegime(taxable_income) {
        var tax = 0; 
        let initialtaxableincome = taxable_income
        
        if (taxable_income > 1500000) { 
            tax += (taxable_income - 1500000) * 0.30; 
            taxable_income = 1500000; 
        } 
        if (taxable_income > 1200000) { 
            tax += (taxable_income - 1200000) * 0.20; 
            taxable_income = 1200000; 
        } 
        if (taxable_income > 1000000) { 
            tax += (taxable_income - 1000000) * 0.15;
            taxable_income = 1000000; 
        } 
        if (taxable_income > 700000) { 
            tax += (taxable_income - 700000) * 0.10; 
            taxable_income = 700000; 
        }
        if (taxable_income > 300000) { 
            tax += (taxable_income - 300000) * 0.05;
        }
        let net_tax = tax.toFixed(2)
        let edu_cess = Math.round(net_tax * 0.04)

        //Apply rebate Section 87a
        var rebated_amount = 0
        if(initialtaxableincome <= 700000){
            if(net_tax < 25000){
                rebated_amount = net_tax
            }else{
                rebated_amount = 25000
            }
            net_tax = net_tax - rebated_amount
        }
        $('#rebated_amount').val(rebated_amount);
        if(net_tax == 0){
            edu_cess = 0
        }   
        let total_tax = Math.round(parseFloat(net_tax) + parseFloat(edu_cess))

        $('#tax_before_cess').val(Math.round(net_tax));
        $('#tax_after_cess').val(total_tax);
        $('#edu_cess').val(edu_cess);
        
        return total_tax;
    }

    function showSlabwiseCalculation(taxableIncome){
        let tax = 0;

        // Calculate tax for each slab
        let slab1Income = Math.min(taxableIncome, 250000);
        let slab1Tax = 0;

        let slab2Income = Math.max(0, Math.min(taxableIncome - 250000, 250000));
        let slab2Tax = slab2Income * 0.05;

        let slab3Income = Math.max(0, Math.min(taxableIncome - 500000, 500000));
        let slab3Tax = slab3Income * 0.2;

        let slab4Income = Math.max(0, taxableIncome - 1000000);
        let slab4Tax = slab4Income * 0.3;

        tax += slab1Tax + slab2Tax + slab3Tax + slab4Tax;

        // Display results in the table
        document.getElementById("slab1Income").textContent = slab1Income;
        document.getElementById("slab1Tax").textContent = slab1Tax.toFixed(2);
        document.getElementById("slab2Income").textContent = slab2Income;
        document.getElementById("slab2Tax").textContent = slab2Tax.toFixed(2);
        document.getElementById("slab3Income").textContent = slab3Income;
        document.getElementById("slab3Tax").textContent = slab3Tax.toFixed(2);
        document.getElementById("slab4Income").textContent = slab4Income;
        document.getElementById("slab4Tax").textContent = slab4Tax.toFixed(2);
        document.getElementById("totalTax").textContent = tax.toFixed(2);
    }

    function showSlabwiseCalculationNewRegime(taxableIncome){
        let tax = 0;

        // Calculate tax for each slab
        let slab1Income = Math.min(taxableIncome, 300000);
        let slab1Tax = 0;

        let slab2Income = Math.max(0, Math.min(taxableIncome - 300000, 400000));
        let slab2Tax = slab2Income * 0.05;
        
        let slab3Income = Math.max(0, Math.min(taxableIncome - 700000, 300000));
        if(slab3Income != 0){
            var slab3Tax = slab3Income * 0.1;
        }else{
            var slab3Tax = 0
        }

        let slab4Income = Math.max(0, Math.min(taxableIncome - 1000000, 200000));
        if(slab4Income != 0){
            var slab4Tax = slab4Income * 0.15;
        }else{
            var slab4Tax = 0
        }

        let slab5Income = Math.max(0, Math.min(taxableIncome - 1200000, 300000));
        if(slab5Income != 0){
            var slab5Tax = slab5Income * 0.2;
        }else{
            var slab5Tax = 0
        }

        let slab6Income = Math.max(0, Math.min(taxableIncome - 1500000, 3500000));
        if(slab6Income != 0){
            var slab6Tax = slab6Income * 0.3;
        }else{
            var slab6Tax = 0
        }

        // let slab7Income = Math.max(0, taxableIncome - 1500000);
        // let slab7Tax = slab7Income * 0.3;

        // tax += slab1Tax + slab2Tax + slab3Tax + slab4Tax + slab5Tax + slab6Tax + slab7Tax;
        tax += slab1Tax + slab2Tax + slab3Tax + slab4Tax + slab5Tax + slab6Tax;

        // Display results in the table
        document.getElementById("slab1IncomeN").textContent = slab1Income;
        document.getElementById("slab1TaxN").textContent = slab1Tax.toFixed(2);
        document.getElementById("slab2IncomeN").textContent = slab2Income;
        document.getElementById("slab2TaxN").textContent = slab2Tax.toFixed(2);
        document.getElementById("slab3IncomeN").textContent = slab3Income;
        document.getElementById("slab3TaxN").innerHTML = slab3Tax.toFixed(2)
        document.getElementById("slab4IncomeN").textContent = slab4Income;
        document.getElementById("slab4TaxN").innerHTML = slab4Tax.toFixed(2)
        document.getElementById("slab5IncomeN").textContent = slab5Income;
        document.getElementById("slab5TaxN").innerHTML = slab5Tax.toFixed(2)
        document.getElementById("slab6IncomeN").textContent = slab6Income;
        document.getElementById("slab6TaxN").innerHTML = slab6Tax.toFixed(2)
        // document.getElementById("slab7IncomeN").textContent = slab7Income;
        // document.getElementById("slab7TaxN").textContent = slab7Tax.toFixed(2);
        document.getElementById("totalTaxN").textContent = tax.toFixed(2);
    }

    $("#saveIncomeTax").submit(function(e) {
        // $("form").removeClass("was-validated");
        // $("input").removeClass("is-valid");
        e.preventDefault();
        // $("#submitIncomeTax").prop('disabled', true);
        // Replace the submit button with the loader 
        var loaderHtml = '<div class="loader"></div>'; 
        $("#submitIncomeTax").replaceWith(loaderHtml);
        var submitBtnHtml = '<button class="btnSubmit" id="submitIncomeTax" type="submit">Submit</button>';

        var formdata = new FormData(this);
        $.ajax({
            url: '{{ route('saveIncomeTaxDetails') }}',
            type: 'POST',
            data: formdata,
            contentType: false,
            processData: false,
            success: function(data) {
                $("#submitIncomeTax").prop('disabled', false);
                if (!data.error2)
                    swal("Successful!", data.success, "success")
                    .then((action) => {
                        window.location.href = '{{ route('tax-calculation') }}';
                    });
                else
                    swal("Error!", data.error2, "error")
                    .then((action) => {
                        $(".loader").replaceWith(submitBtnHtml);
                    });
            },
            statusCode: {
                422: function(responseObject, textStatus, jqXHR) {
                    resetErrors();
                    printErrMsg(responseObject.responseJSON.errors);
                    swal("Error occured!", "Please fill the required fields", "error")
                    .then((action) => {
                        $(".loader").replaceWith(submitBtnHtml);
                    });
                },
                500: function(responseObject, textStatus, errorThrown) {
                    swal("Error occured!", "Something went wrong please try again", "error")
                    .then((action) => {
                        $(".loader").replaceWith(submitBtnHtml);
                    });
                }
            }
        });

    });

    $("#tax_regime").change(function(){

        if($("#tax_regime").val() == 1){
            $("#house_rent_exemption").prop('disabled', false);
            $("#previous_ptax").prop('disabled', false);
            $("#election_allowance").prop('disabled', false);
            $("#transport_allowance").prop('disabled', false);
            $('#professional_tax').val(2500);
            $("#professional_tax").prop('disabled', false);
            $(".old-regime").show(); 
            $('#standard_deduction').val(50000);
            $('#total_exemptions').val(52500);
        }else{
            $("#house_rent_exemption").prop('disabled', true);
            $("#previous_ptax").prop('disabled', true);
            $("#election_allowance").prop('disabled', true);
            $("#transport_allowance").prop('disabled', true);
            $('#professional_tax').val(0);
            $("#professional_tax").prop('disabled', true);
            $(".old-regime").hide(); 
            $('#standard_deduction').val(75000);
            $('#total_exemptions').val(75000);
        }
    });
});

$("#Emp_Code").change(function(){
            var Emp_Code = $('#Emp_Code').val();

            var url = "{{ route('fetchempdetails', [':Emp_Code', ':fi_year']) }}";
            url = url.replace(':Emp_Code', Emp_Code);

            var fyear = $('#financial_year').val()
            if(fyear != ''){
                url = url.replace(':fi_year', $('#financial_year').val());
            }else{
                swal("Error!", "Please select financial year", "error");
                return false
            }
            
            $.ajax({
                url: url,
                type: 'GET',
                data: {
                    '_method': "GET",
                    '_token': "{{ csrf_token() }}"
                },
                success: function(data) {
                    if (!data.error && !data.error2) {
                        if (data.result === 1) {

                            var full_name = '';
                            if (data.employee_details.fname) {
                                full_name += data.employee_details.fname;
                            }
                            if (data.employee_details.mname) {
                                full_name += (full_name.length ? " " : "") + data.employee_details.mname;
                            }
                            if (data.employee_details.lname) {
                                full_name += (full_name.length ? " " : "") + data.employee_details.lname;
                            }
                            $('#emp_name').val(full_name);
                            $('#gender').val(data.employee_details.gender);
                            $('#dob').val(data.employee_details.dob);
                            $('#pan').val(data.employee_details.pan);
                            $('#current_gross').val(data.current_gross_salary);
                            $('#supplementary_bill').val(data.supplementary_bill);
                            $('#gross_income').val(data.gross_income);

                            let net_salary = data.gross_income - $('#total_exemptions').val()
                            $('#net_salary_exemption').val(net_salary)
                            $('#total_taxable_salary').val(net_salary)
                        } else {
                            alert("Unexpected result from the server");
                        }
                }
                },
                error: function(error, jqXHR, textStatus, errorThrown) {
                    swal("Error!", "Something went wrong", "error");
                },
            });
        });
    </script>
