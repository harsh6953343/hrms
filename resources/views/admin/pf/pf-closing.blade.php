<x-admin.layout>
    <x-slot name="title">PF Closing</x-slot>
    <x-slot name="heading">PF Closing</x-slot>

    <!-- Add Form -->
    <div class="row" id="addContainer">
        <div class="col-sm-12">
            <div class="card">
                <header class="card-header">
                    <h4 class="card-title">PF Closing Form</h4>
                </header>
                <form class="theme-form" name="editForm" id="editForm" enctype="multipart/form-data" method="post">
                    @csrf

                    <input type="hidden" name="edit_model_id" id="edit_model_id">
                    <div class="card-body">
                        <div class="mb-3 row">

                            <div class="col-md-3 mt-2">
                                <label class="col-form-label" for="pf_no">PF No.<span class="text-danger">*</span></label>
                                <input class="form-control" id="pf_no" name="pf_no" type="text" placeholder="Enter PF No.">
                                <span class="text-danger invalid pf_no_err"></span>
                            </div>

                            <div class="col-md-3 mt-5">
                                <button type="button" class="btn btn-primary" id="searchBtn">Search Employee</button>
                            </div>
                        </div>

                        <div class="mb-3 row">

                            <input type="hidden" name="employee_id" id="employee_id">

                            <div class="col-md-4">
                                <label class="col-form-label" for="Emp_Code">Employee Id<span class="text-danger">*</span></label>
                                <input class="form-control" id="Emp_Code" name="Emp_Code" type="text" placeholder="Employee Code" readonly>
                                <span class="text-danger invalid Emp_Code_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="emp_name">Employee Name<span class="text-danger">*</span></label>
                                <input class="form-control" id="emp_name" name="emp_name" type="text" placeholder="Employee Name" readonly>
                                <span class="text-danger invalid emp_name_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="ward">Ward<span class="text-danger">*</span></label>
                                <input class="form-control" id="ward" name="ward" type="text" placeholder="Employee Ward" readonly>
                                <span class="text-danger invalid ward_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="department">Department<span class="text-danger">*</span></label>
                                <input class="form-control" id="department" name="department" type="text" placeholder="Employee Department" readonly>
                                <span class="text-danger invalid department_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="designation">Designation<span class="text-danger">*</span></label>
                                <input class="form-control" id="designation" name="designation" type="text" placeholder="Employee Designation" readonly>
                                <span class="text-danger invalid designation_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="class">Class<span class="text-danger">*</span></label>
                                <input class="form-control" id="class" name="class" type="text" placeholder="Employee Class" readonly>
                                <span class="text-danger invalid class_err"></span>
                            </div>
                        </div>
                        <hr>
                        {{-- style="display: none;" --}}
                        <div class="mb-3 row" id="status_div"  style="display: none">
                            <h2>PF Details</h2>

                            <table id="loan-table" class="table table-bordered nowrap align-middle" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Sr No.</th>
                                        <th>Current Month</th>
                                        <th>Salary Month</th>
                                        <th>PF Contribution</th>
                                        <th>PF Loan</th>
                                        <th>Other Amt.</th>
                                        <th>Total</th>
                                        <th>Loan Taken Date</th>
                                        <th>Loan Amount</th>
                                        <th>Grand Total</th>
                                        <th>Intrest Rate</th>
                                        <th>Intrest Amount</th>
                                        <th>Remark</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>

                        <hr>

                        <h2>PF Closing Form</h2>

                        <div class="col-md-4">
                            <label class="col-form-label" for="type">Select Closing Status<span class="text-danger">*</span></label>
                            <select class="form-control" name="type" id="type">
                                <option value="">Select Status</option>
                                <option value="1">Retire</option>
                                <option value="2">Expired</option>
                            </select>
                            <span class="text-danger invalid type_err"></span>
                        </div>


                        <div class="col-md-3">
                            <label class="col-form-label" for="closing_month">Select Month<span class="text-danger">*</span></label>
                            <select class="form-control" id="closing_month" name="closing_month">
                                <option value="">Select Month</option>
                                <option value="1"  >January</option>
                                <option value="2" >February</option>
                                <option value="3" >March</option>
                                <option value="4" >April</option>
                                <option value="5" >May</option>
                                <option value="6" >June</option>
                                <option value="7" >July</option>
                                <option value="8" >August</option>
                                <option value="9" >September</option>
                                <option value="10">October</option>
                                <option value="11">November</option>
                                <option value="12">December</option>
                            </select>
                            <span class="text-danger invalid closing_month_err"></span>
                        </div>

                        <div class="col-md-4">
                            <label class="col-form-label" for="closing_date">Date<span class="text-danger">*</span></label>
                            <input class="form-control" id="closing_date" name="closing_date" type="date">
                            <span class="text-danger invalid closing_date_err"></span>
                        </div>

                        <div class="col-md-4">
                            <label class="col-form-label" for="remark">Remark<span class="text-danger">*</span></label>
                            <textarea class="form-control" name="remark" id="remark"></textarea>
                            <span class="text-danger invalid remark_err"></span>
                        </div>

                    </div>


                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" id="addSubmit">Generate</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


</x-admin.layout>


{{-- Add --}}
<!-- Update -->
<script>
    $(document).ready(function() {
        $("#editForm").submit(function(e) {
            e.preventDefault();
            $("#editSubmit").prop('disabled', true);
            var formdata = new FormData(this);
            formdata.append('_method', 'PUT');
            var model_id = $('#edit_model_id').val();
            var url = "{{ route('pf-closing.update', ':model_id') }}";
            //
            $.ajax({
                url: url.replace(':model_id', model_id),
                type: 'POST',
                data: formdata,
                contentType: false,
                processData: false,
                success: function(data) {
                    $("#editSubmit").prop('disabled', false);
                    if (!data.error2)
                        swal("Successful!", data.success, "success")
                        .then((action) => {
                            window.location.href = '{{ route('pf-closing.index') }}';
                        });
                    else
                        swal("Error!", data.error2, "error");
                },
                statusCode: {
                    422: function(responseObject, textStatus, jqXHR) {
                        $("#editSubmit").prop('disabled', false);
                        resetErrors();
                        printErrMsg(responseObject.responseJSON.errors);
                    },
                    500: function(responseObject, textStatus, errorThrown) {
                        $("#editSubmit").prop('disabled', false);
                        swal("Error occured!", "Something went wrong please try again", "error");
                    }
                }
            });

        });
    });


    $(document).ready(function() {
        $("#searchBtn").click(function(){
            var pf_no = $('#pf_no').val();
            if(pf_no != '')
            {
                var url = "{{ route('fetch-employee-details-pf', ':pf_no') }}";

                $.ajax({
                    url: url.replace(':pf_no', pf_no),
                    type: 'GET',
                    data: {
                        '_method': "GET",
                        '_token': "{{ csrf_token() }}"
                    },
                    success: function(data) {
                        if (!data.error && !data.error2) {
                            if (data.result === 1) {

                                var full_name = data.employee_details.fname + " " + data.employee_details.mname + " " + data.employee_details.lname;
                                $('#emp_name').val(full_name);
                                $('#ward').val(data.employee_details.ward.name);
                                $('#department').val(data.employee_details.department.name);
                                $('#class').val(data.employee_details.class.name);
                                $('#designation').val(data.employee_details.designation.name);
                                $('#employee_id').val(data.employee_details.id);
                                $('#Emp_Code').val(data.employee_details.employee_id);


                                $('#edit_model_id').val(data.opening_balance.id);

                                var loanData = [];
                                var opening_balance = data.opening_balance.opening_balance;
                                var grand_total = 0;
                                var total_loan_amt = 0;
                                var total_intrest_amt = 0;
                                var closing_bal = 0;

                                for (var i = 0; i < data.pf_data.length; i++) {

                                    grand_total+= data.pf_data[i].total|| 0;
                                    total_loan_amt += data.pf_data[i].loan_amt|| 0;
                                    total_intrest_amt += parseFloat(data.pf_data[i].intrest_amt) || 0;

                                    loanData.push({
                                        financial_year_id:  data.pf_data[i].financial_year_id,
                                        id:                 data.pf_data[i].id,
                                        currentMonth:       data.pf_data[i].current_month,
                                        salaryMonth:        data.pf_data[i].salary_month,
                                        pfContribution:     data.pf_data[i].pf_contribution,
                                        pfLoan:             data.pf_data[i].pf_loan,
                                        otherAmt:           data.pf_data[i].other_amount|| 0,
                                        total:              data.pf_data[i].total|| 0,
                                        loanTakenDate:      data.pf_data[i].loan_date|| '',
                                        loanAmount:         data.pf_data[i].loan_amt|| 0,
                                        grand_total:        data.pf_data[i].grand_total|| 0,
                                        intrest_rate:       data.pf_data[i].intrest_rate|| 0,
                                        intrest_amt:        data.pf_data[i].intrest_amt|| 0,
                                        remark:             data.pf_data[i].remark || ''
                                    });
                                }


                                var $loanTableBody = $('#loan-table tbody');
                                $loanTableBody.empty();

                                $.each(loanData, function(index, loan) {
                                    var row = $('<tr></tr>');

                                    row.append('<td>' + (index + 1) + '</td>');
                                    row.append('<td>' + loan.currentMonth + '</td>');
                                    row.append('<td>' + loan.salaryMonth + '</td>');
                                    row.append('<td>' + loan.pfContribution + '</td>');
                                    row.append('<td>' + loan.pfLoan + '</td>');
                                    row.append('<td>' + loan.otherAmt + '</td>');
                                    row.append('<td>' + loan.total + '</td>');
                                    row.append('<td>' + loan.loanTakenDate + '</td>');
                                    row.append('<td>' + loan.loanAmount + '</td>');
                                    row.append('<td>' + loan.grand_total + '</td>');
                                    row.append('<td>' + loan.intrest_rate + '</td>');
                                    row.append('<td>' + loan.intrest_amt + '</td>');
                                    row.append('<td>' + loan.remark + '</td>');

                                    $loanTableBody.append(row);
                                });

                                var row1 = $('<tr></tr>');
                                row1.append('<td colspan="9"><b>' + 'Previous Year Opening Balance' + '</b></td>');
                                row1.append('<td colspan><b>' + opening_balance + '</b></td>');
                                $loanTableBody.append(row1);

                                var row2 = $('<tr></tr>');
                                row2.append('<td colspan="9"><b>' + 'Current Year PF Conr./PF Loan/Other Amt.' + '</b></td>');
                                row2.append('<td colspan><b>' + grand_total + '</b></td>');
                                $loanTableBody.append(row2);

                                var first_total = parseFloat(opening_balance) + parseFloat(grand_total);
                                var row3 = $('<tr></tr>');
                                row3.append('<td colspan="9"><b>Total</b></td>');
                                row3.append('<td><b>' + first_total + '</b></td>');
                                $loanTableBody.append(row3);

                                var row4 = $('<tr></tr>');
                                row4.append('<td colspan="9"><b>' + 'Loan Amount' + '</b></td>');
                                row4.append('<td colspan><b>' + total_loan_amt + '</b></td>');
                                $loanTableBody.append(row4);

                                var second_total = parseFloat(opening_balance) + parseFloat(grand_total) + parseFloat(total_loan_amt);
                                var row5 = $('<tr></tr>');
                                row5.append('<td colspan="9"><b>Total</b></td>');
                                row5.append('<td><b>' + second_total + '</b></td>');
                                $loanTableBody.append(row5);

                                var row6 = $('<tr></tr>');
                                row6.append('<td colspan="9"><b>' + 'Interest Amt.' + '</b></td>');
                                row6.append('<td colspan><b>' + total_intrest_amt + '</b></td>');
                                $loanTableBody.append(row6);

                                closing_bal = parseFloat(opening_balance) + parseFloat(grand_total) + parseFloat(total_loan_amt) + parseFloat(total_intrest_amt);
                                var row7 = $('<tr></tr>');
                                row7.append('<td colspan="9"><b>Closing Bal.</b></td>');
                                row7.append('<td><b>' + closing_bal + '</b></td>');
                                $loanTableBody.append(row7);

                                $('#status_div').removeAttr('style');


                            } else if (data.result === 0) {
                                $('#emp_name').val('');
                                $('#ward').val('');
                                $('#department').val('');
                                $('#designation').val('');
                                $('#class').val('');
                                $('#employee_id').val('');
                                $('#Emp_Code').val('');

                                $('#status_div').hide();

                                alert("Employee details not found or Employee Status already added!");
                            } else {
                                alert("Unexpected result from the server");
                            }
                    }
                    },
                    error: function(error, jqXHR, textStatus, errorThrown) {
                        swal("Error!", "Something went wrong", "error");
                    },
                });
            }
            else{
                alert('Please Enter PF No.');
            }

        });
    });


</script>

