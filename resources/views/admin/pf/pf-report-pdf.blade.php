@php
  use Carbon\Carbon;
@endphp
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PF Report</title>
    <style>


        body {
                font-family: "Source Sans 3", Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;
                -webkit-font-smoothing: antialiased;
                font-size: 16px;
            }

        @media print {
            body {
                -webkit-print-color-adjust: exact;
            }
        }

        .label {
            display: inline-block;
            width: 150px; /* Adjust width as needed */
        }
        .section {
            width: 100%;
            margin-left: 45px;
            margin-bottom: 20px;
        }
        .section-heading {
            margin: 10px 0 5px;
            font-size: 24px;
        }
        .subsection {
            width: 50%;
            float: left;
        }

        .subsection-details {
            width: 48%;
            float: left;
        }

        .table-container {
            width: 100%;
        }
        table {
            width: 100%;
            border-collapse: collapse; /* Collapse borders */
        }
        th, td {
            padding: 10px;
            font-size: 18px;
            border: 1px solid black; /* Single border */
            text-align: left;
            /* background-color: lightgray; */
        }
        .earnings-header, .deductions-header {
            background-color: #355495; /* Blue background color */
            color: #ffffff; /* White text color */
        }
        .dashed-hr {
            border: 1px dashed black;
            clear: both;
        }
        p {
            font-size: 16px;
        }
        .check
        {
            clear: both;
        }

    </style>
</head>
<body>

    <table style="width: 100%; border:none;">
        <thead>
            <tr>
                <td style="background-color:white; border:none; text-align:right">
                    @if(isset($base64Logo))
                    <img src="data:image/png;base64,{{ $base64Logo }}" alt="Corporation Logo" height="80" width="120">
                    @endif
                </td>
                <td style="background-color:white; border:none;">
                    <h2 class="section-heading" style="margin-left:10%;">{{ $corporation->name }}</h2>
                    <h5 class="section-heading" style="font-size: 18px; margin-left:18%;">Form No 98 <br> As per Rule 134(3) & (5)</h5>
                </td>
            </tr>
        </thead>
    </table>
    <br>
    <div class="section">
        <div class="subsection-details" style=" border-right: 1px dashed black;">
            <p><span class="label">PF Acc. No.</span><strong>: {{ $employee_details?->pf_account_no }}</strong></p>
            <p><span class="label">Employee Name</span><strong>: {{ $employee_details?->fname." ".$employee_details?->mname." ".$employee_details?->lname }}</strong></p>
            <p><span class="label">Department</span><strong>: {{ $employee_details?->department?->name }}</strong></p>
            <p><span class="label">Interst Rate</span><strong>: {{ $get_intrest_rate?->interest_rate }}</strong></p>
        </div>
        <div class="subsection-details" style="margin-left: 2%;">
            <p><span class="label">Emp. No.</span><strong>: {{ $employee_details?->employee_id }}</strong></p>
            <p><span class="label">Basic Salary </span><strong>: {{ $employee_details?->salary?->basic_salary }}</strong></p>
            <p><span class="label">Designation</span><strong>: {{ $employee_details?->designation?->name }}</strong></p>
            <p><span class="label">Ward Operator </span><strong>: {{ $employee_details?->ward?->name }}</strong></p>
        </div>
    </div>
    <hr class="dashed-hr" style="margin-bottom: 3%;">


    <table>
        <thead>
            <tr>
                <td>{{ $financial_year->from_date." To". $financial_year->to_date  }}</td>
                <td>PF Contr.</td>
                <td>PF Loan</td>
                <td>Other Amt.</td>
                <td>Total</td>
                <td>Loan Taken Date</td>
                <td>Loan Taken Amount	</td>
                <td>Grand Total	</td>
                <td>Interest Rate	</td>
                <td>Remark</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Balance of previous year</td>
                <td colspan="7" style="text-align:right;">{{ $pf_opening_balance?->opening_balance }}</td>
                <td colspan="2"></td>
            </tr>
            @php
                $total_pf_contribution = 0;
                $total_pf_loan = 0;
                $total_other_amt = 0;
                $total_amt = 0;
                $total_loan_amt = 0;
                $total_grand_total = 0;
                $total_intrest_amt = 0;

            @endphp

            @foreach ($employee_provident_funds as $employee_provident_fund)

                @if($pf_opening_balance?->closing_status == 1 && $pf_opening_balance?->closing_date < $employee_provident_fund->salary_month)

                   <tr>
                        <td>
                            <b>{{ ($pf_opening_balance?->type == 1)?'Retire':'Expired' }}</b>
                        </td>
                        <td colspan="10">
                            <b>{{ $pf_opening_balance?->remark }}</b>
                        </td>
                    </tr>
                    @php
                        break;
                    @endphp
                @else

                    @php
                    $total_pf_contribution+= $employee_provident_fund->pf_contribution;
                    $total_pf_loan += $employee_provident_fund->pf_loan;
                    $total_other_amt += $employee_provident_fund->other_amount;
                    $total_amt = $employee_provident_fund->total;
                    $total_loan_amt += $employee_provident_fund->loan_amt;
                    $total_grand_total += $employee_provident_fund->grand_total;
                    $total_intrest_amt += $employee_provident_fund->intrest_amt;

                    @endphp

                    <tr  @if(empty($employee_provident_fund->total))
                        style="color: red;"
                    @endif>
                        <td>{{ $employee_provident_fund->current_month }}</td>
                        <td>{{ $employee_provident_fund->pf_contribution }}</td>
                        <td>{{ $employee_provident_fund->pf_loan }}</td>
                        <td>{{ $employee_provident_fund->other_amount }}</td>
                        <td>{{ $employee_provident_fund->total }}</td>
                        <td>{{ $employee_provident_fund->loan_date }}</td>
                        <td>{{ $employee_provident_fund->loan_amt }}</td>
                        <td>{{ $employee_provident_fund->grand_total }}</td>
                        <td>{{ $employee_provident_fund->intrest_rate }}</td>
                        <td>
                            @if(empty($employee_provident_fund->total))
                            {{ 'PF Report Not generated for this month' }}
                            @else
                            {{ $employee_provident_fund->remark }}
                            @endif
                        </td>
                    </tr>

                @endif

            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th>Total</th>
                <th>{{ $total_pf_contribution }}</th>
                <th>{{ $total_pf_loan }}</th>
                <th>{{ $total_other_amt }}</th>
                <th>{{ $total_amt }}</th>
                <th></th>
                <th>{{ $total_loan_amt }}</th>
                <th>{{ $total_grand_total }}</th>
                <th></th>
                <th></th>
            </tr>
        </tfoot>

    </table>


    <hr class="dashed-hr" style="margin-top: 3%;">

    <div class="section">
        <div class="subsection-details" style=" border-right: 1px dashed black;">
            <p><span class="label">Generated & Download by</span><strong>: {{ Auth::user()->name }}</strong></p>
            <p><span class="label">Designation</span><strong>: {{ $employee_details?->designation?->name }}</strong></p>
            <br>
            <p><span class="label">Sign In</span><strong>:  _________________Clerk PF Department</strong></p>
            <br>
            <p><span class="label">Sign In</span><strong>:  _________________Head Of Department </strong></p>


        </div>
        <div class="subsection-details" style="margin-left: 2%;">
            <p><span class="label">Previous Year Opening</span><strong>: {{ $pf_opening_balance?->opening_balance }}</strong></p>
            <p><span class="">Current Year PF Conr./PF Loan/Other Amt. </span><strong>: {{ $total_amt }}</strong></p>
            <p><span class="label">Total</span><strong>: {{ $pf_opening_balance?->opening_balance + $total_amt }}</strong></p>
            <p><span class="label">Loan Amount </span><strong>: {{ $total_loan_amt }}</strong></p>
            <p><span class="label">Total</span><strong>: {{ ($pf_opening_balance?->opening_balance + $total_amt) - $total_loan_amt }}</strong></p>
            <p><span class="label">Interest Amt. </span><strong>: {{ $total_intrest_amt }}</strong></p>
            <p><span class="label">Closing Bal. </span><strong>: {{ ($pf_opening_balance?->opening_balance + $total_amt) - $total_loan_amt + $total_intrest_amt }}</strong></p>

        </div>
    </div>
    <hr class="dashed-hr" style="margin-bottom: 3%;">
</body>
</html>
