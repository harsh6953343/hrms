<x-admin.layout>
    <x-slot name="title">Generate PF Report</x-slot>
    <x-slot name="heading">Generate PF Report</x-slot>

    <!-- Add Form -->
    <div class="row" id="addContainer">
        <div class="col-sm-12">
            <div class="card">
                <header class="card-header">
                    <h4 class="card-title">Generate PF Report</h4>
                </header>
                <form class="theme-form" name="addForm" id="addForm" enctype="multipart/form-data">
                    @csrf

                    <div class="card-body">
                        <div class="mb-3 row">

                            <div class="col-md-3 mt-2">
                                <label class="col-form-label" for="pf_account_no">PF No.<span class="text-danger">*</span></label>
                                <input class="form-control" id="pf_account_no" name="pf_account_no" type="text" placeholder="Enter PF No.">
                                <span class="text-danger invalid pf_account_no_err"></span>
                            </div>

                            <div class="col-md-3 mt-5">
                                <button type="button" class="btn btn-primary" id="searchBtn">Search Employee</button>
                            </div>
                        </div>

                        <div class="mb-3 row">

                            <input type="hidden" name="employee_id" id="employee_id">

                            <div class="col-md-4">
                                <label class="col-form-label" for="Emp_Code">Employee Id<span class="text-danger">*</span></label>
                                <input class="form-control" id="Emp_Code" name="Emp_Code" type="text" placeholder="Employee Code" readonly>
                                <span class="text-danger invalid Emp_Code_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="emp_name">Employee Name<span class="text-danger">*</span></label>
                                <input class="form-control" id="emp_name" name="emp_name" type="text" placeholder="Employee Name" readonly>
                                <span class="text-danger invalid emp_name_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="ward">Ward<span class="text-danger">*</span></label>
                                <input class="form-control" id="ward" name="ward" type="text" placeholder="Employee Ward" readonly>
                                <span class="text-danger invalid ward_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="department">Department<span class="text-danger">*</span></label>
                                <input class="form-control" id="department" name="department" type="text" placeholder="Employee Department" readonly>
                                <span class="text-danger invalid department_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="designation">Designation<span class="text-danger">*</span></label>
                                <input class="form-control" id="designation" name="designation" type="text" placeholder="Employee Designation" readonly>
                                <span class="text-danger invalid designation_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="class">Class<span class="text-danger">*</span></label>
                                <input class="form-control" id="class" name="class" type="text" placeholder="Employee Class" readonly>
                                <span class="text-danger invalid class_err"></span>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" id="addSubmit">Generate</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


</x-admin.layout>


{{-- Add --}}
<script>
    $("#addForm").submit(function(e) {
        e.preventDefault();
        $("#addSubmit").prop('disabled', true);

        var formdata = new FormData(this);
        $.ajax({
            url: '{{ route('generate-pf-report.store') }}',
            type: 'POST',
            data: formdata,
            contentType: false,
            processData: false,
            success: function(data) {
                $("#addSubmit").prop('disabled', false);
                if (!data.error2)
                    swal("Successful!", data.success, "success")
                    .then((action) => {
                        window.location.href = '{{ route('generate-pf-report.index') }}';
                    });
                else
                    swal("Error!", data.error2, "error");
            },
            statusCode: {
                422: function(responseObject, textStatus, jqXHR) {
                    $("#addSubmit").prop('disabled', false);
                    resetErrors();
                    printErrMsg(responseObject.responseJSON.errors);
                },
                500: function(responseObject, textStatus, errorThrown) {
                    $("#addSubmit").prop('disabled', false);
                    swal("Error occured!", "Something went wrong please try again", "error");
                }
            }
        });

    });


    $(document).ready(function() {
        $("#searchBtn").click(function(){
            var pf_account_no = $('#pf_account_no').val();
            if(pf_account_no != '')
            {
                var url = "{{ route('fetch-employee-details-pf', ':pf_account_no') }}";

                $.ajax({
                    url: url.replace(':pf_account_no', pf_account_no),
                    type: 'GET',
                    data: {
                        '_method': "GET",
                        '_token': "{{ csrf_token() }}"
                    },
                    success: function(data) {
                        if (!data.error && !data.error2) {
                            if (data.result === 1) {

                                var full_name = data.employee_details.fname + " " + data.employee_details.mname + " " + data.employee_details.lname;
                                $('#emp_name').val(full_name);
                                $('#ward').val(data.employee_details.ward.name);
                                $('#department').val(data.employee_details.department.name);
                                $('#class').val(data.employee_details.class.name);
                                $('#designation').val(data.employee_details.designation.name);
                                $('#employee_id').val(data.employee_details.id);
                                $('#Emp_Code').val(data.employee_details.employee_id);


                            } else if (data.result === 0) {
                                $('#emp_name').val('');
                                $('#ward').val('');
                                $('#department').val('');
                                $('#designation').val('');
                                $('#class').val('');
                                $('#employee_id').val('');
                                $('#Emp_Code').val('');

                                alert("Employee details not found or Employee Status already added!");
                            } else {
                                alert("Unexpected result from the server");
                            }
                    }
                    },
                    error: function(error, jqXHR, textStatus, errorThrown) {
                        swal("Error!", "Something went wrong", "error");
                    },
                });
            }
            else{
                alert('Please Enter Employee Id');
            }

        });
    });


</script>

