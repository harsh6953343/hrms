<x-admin.layout>
    <x-slot name="title">PF Report</x-slot>
    <x-slot name="heading">PF Report</x-slot>

    <!-- Add Form -->
    <div class="row" id="addContainer">
        <div class="col-sm-12">
            <div class="card">
                <header class="card-header">
                    <h4 class="card-title">PF Report</h4>
                </header>
                <form action="{{ route('pf-report.store') }}" class="theme-form" name="addForm" id="addForm" enctype="multipart/form-data" method="POST">
                    @csrf

                    <div class="card-body">
                        <div class="mb-3 row">
                            <div class="col-md-3 mt-2">
                                <label class="col-form-label" for="pf_account_no">PF No.<span class="text-danger">*</span></label>
                                <input class="form-control" id="pf_account_no" name="pf_account_no" type="text" placeholder="Enter PF No.">
                                @error('pf_account_no')
                                <span class="text-danger invalid pf_account_no_err">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-md-3 mt-5">
                                <button type="submit" class="btn btn-primary" id="addSubmit">View PDF</button>
                                <button type="reset" class="btn btn-warning">Reset</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


</x-admin.layout>

