<x-admin.layout>
    <x-slot name="title">Supplimentary Bill</x-slot>
    <x-slot name="heading">Supplimentary Bill</x-slot>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                @can('supplimentary-bill.create')
                    <div class="card-header">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="">
                                    @if($type == 1)
                                        <a href="{{ route('supplimentary-bill.create') }}" id="addToTable1" class="btn btn-primary">Add <i class="fa fa-plus"></i></a>
                                    @else
                                        <a href="{{ route('suspend-supplimentary-bill.create') }}" id="addToTable1" class="btn btn-primary">Add <i class="fa fa-plus"></i></a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                @endcan
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>Employee Id</th>
                                    <th>Name</th>
                                    <th>From Date</th>
                                    <th>To Date</th>
                                    <th>Month</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($supplimentory_bills as $supplimentory_bill)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $supplimentory_bill->Emp_Code }}</td>
                                        <td>{{ $supplimentory_bill?->employee?->fname." ".$supplimentory_bill?->employee?->mname." ".$supplimentory_bill?->employee?->lname }}</td>
                                        <td>{{ $supplimentory_bill->from_date }}</td>
                                        <td>{{ $supplimentory_bill->to_date }}</td>
                                        <td>
                                            @php
                                                   $monthNames = collect(explode(',', $supplimentory_bill->month))
                                                            ->map(function ($monthNumber) {
                                                                return \Carbon\Carbon::create()->month($monthNumber)->format('F');
                                                            })
                                                            ->sort()
                                                            ->implode(', ');
                                                        echo $monthNames;
                                            @endphp
                                        </td>
                                        <td>
                                            @can('supplimentary-bill.show')
                                                <a href="{{ route('supplimentary-bill.show',$supplimentory_bill->id) }}">
                                                    <button class="edit-element btn btn-secondary px-2 py-1" title="Show Loans">
                                                        <i data-feather="eye"></i>
                                                    </button>
                                                </a>
                                            @endcan
                                            @can('supplimentary-bill.delete')
                                                <button class="btn btn-danger rem-element px-2 py-1" title="Delete Supplimentary Bill" data-id="{{ $supplimentory_bill->id }}"><i data-feather="trash-2"></i> </button>
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




</x-admin.layout>

<!-- Delete -->
<script>
    $("#buttons-datatables").on("click", ".rem-element", function(e) {
        e.preventDefault();
        swal({
                title: "Are you sure to delete this Supplimentary Bill?",
                // text: "Make sure if you have filled Vendor details before proceeding further",
                icon: "info",
                buttons: ["Cancel", "Confirm"]
            })
            .then((justTransfer) => {
                if (justTransfer) {
                    var model_id = $(this).attr("data-id");
                    var url = "{{ route('supplimentary-bill.destroy', ':model_id') }}";

                    $.ajax({
                        url: url.replace(':model_id', model_id),
                        type: 'POST',
                        data: {
                            '_method': "DELETE",
                            '_token': "{{ csrf_token() }}"
                        },
                        success: function(data, textStatus, jqXHR) {
                            if (!data.error && !data.error2) {
                                swal("Success!", data.success, "success")
                                    .then((action) => {
                                        window.location.reload();
                                    });
                            } else {
                                if (data.error) {
                                    swal("Error!", data.error, "error");
                                } else {
                                    swal("Error!", data.error2, "error");
                                }
                            }
                        },
                        error: function(error, jqXHR, textStatus, errorThrown) {
                            swal("Error!", "Something went wrong", "error");
                        },
                    });
                }
            });
    });
</script>

