@php
  use App\Models\EmployeeMonthlyLoan;
  use App\Models\FreezeAttendance;
  use App\Models\SupplimentaryBill;
  use App\Models\RemainingFreezeSalary;

  use Illuminate\Support\Facades\Auth;

@endphp
<x-admin.layout>
    <x-slot name="title">Bank Deduction Report</x-slot>
    <x-slot name="heading">Bank Deduction Report</x-slot>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">

                <form action="{{ route('bank-deduction-report.index') }}" class="theme-form" name="addForm" id="addForm" enctype="multipart/form-data">
                    @csrf

                    <div class="card-body">
                        <div class="mb-3 row">

                            <div class="col-md-3">
                                <label class="col-form-label" for="department">Department<span class="text-danger">*</span></label>
                                <select class="form-control" id="department" name="department">
                                    <option value="">Select Department</option>
                                    <option value="" {{ (empty($department_id))?'Selected':'' }}>All</option>

                                    <?php
                                        if (!Auth::user()->hasRole(['Ward HOD'])) {
                                    ?>
                                    <option value="custom_ward" {{ (!empty($department_id) && $department_id == 'custom_ward')?'Selected':'' }} >Table 1 to 28 & 30</option>
                                    <?php
                                        }
                                    ?>
                                    @foreach ($departments as $department)
                                        <option value="{{ $department->id }}" {{ ($department_id == $department->id ) ? 'Selected':'' }} >{{ $department->name }}</option>
                                    @endforeach
                                </select>
                                <span class="text-danger invalid department_err"></span>
                            </div>

                            <div class="col-md-3">
                                <label class="col-form-label" for="month">Select Month<span class="text-danger">*</span></label>
                                <select class="form-control" id="month" name="month">
                                    {{-- <option value="">Select Month</option> --}}
                                    <option value="1" {{ ($month == 1)?'Selected':'' }} >January</option>
                                    <option value="2" {{ ($month == 2)?'Selected':'' }}>February</option>
                                    <option value="3" {{ ($month == 3)?'Selected':'' }}>March</option>
                                    <option value="4" {{ ($month == 4)?'Selected':'' }}>April</option>
                                    <option value="5" {{ ($month == 5)?'Selected':'' }}>May</option>
                                    <option value="6" {{ ($month == 6)?'Selected':'' }}>June</option>
                                    <option value="7" {{ ($month == 7)?'Selected':'' }}>July</option>
                                    <option value="8" {{ ($month == 8)?'Selected':'' }}>August</option>
                                    <option value="9" {{ ($month == 9)?'Selected':'' }}>September</option>
                                    <option value="10" {{ ($month == 10)?'Selected':'' }}>October</option>
                                    <option value="11" {{ ($month == 11)?'Selected':'' }}>November</option>
                                    <option value="12" {{ ($month == 12)?'Selected':'' }}>December</option>
                                </select>
                                <span class="text-danger invalid month_err"></span>
                            </div>

                            <div class="col-md-3">
                                <label class="col-form-label" for="from_date">From Date<span class="text-danger">*</span></label>
                                <input class="form-control title" id="from_date" name="from_date" type="date" placeholder="From Date" readonly value="{{ $from_date }}">
                                <span class="text-danger invalid from_date_err"></span>
                            </div>

                            <div class="col-md-3">
                                <label class="col-form-label" for="to_date">To Date<span class="text-danger">*</span></label>
                                <input class="form-control title" id="to_date" name="to_date" type="date" placeholder="To Date" readonly value="{{ $to_date }}">
                                <span class="text-danger invalid to_date_err"></span>
                            </div>

                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" id="addSubmit">Submit</button>
                        <a href="{{ route('bank-deduction-report.index') }}" class="btn btn-warning">Refresh</a>
                    </div>
                </form>

                <div class="card-body">
                    <div class="table-responsive">
                        <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>Loan Name</th>
                                    <th>Loan Amount</th>
                                    <th>Total Employees</th>
                                    <th>Month</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (!empty($loans))
                                    @php
                                        $count = 0;
                                        $grand_total_loan_amount = 0;
                                    @endphp
                                    @foreach ($loans as $row => $loan)
                                        @php
                                            $authUser = Auth::user();

                                            $bank_deduction = FreezeAttendance::query();

                                            if ($department) {
                                                if ($department == 'custom_ward') {
                                                    $bank_deduction->whereNotIn('department_id', [145])->whereNotIn('ward_id', [84]);
                                                } else {
                                                    $bank_deduction->where('department_id', $department);
                                                }
                                            }else{
                                                $bank_deduction->whereNotIn('ward_id', [84]);
                                            }


                                            $bank_deduction = $bank_deduction->when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
                                                return $query->where('ward_id', $authUser->ward_id);
                                            })
                                            ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
                                                return $query->where('department_id', $authUser->department_id);
                                            })
                                            ->with('employee')
                                            ->where('freeze_status', 1)
                                            ->where('from_date', $from_date)
                                            ->where('to_date', $to_date)
                                            // ->where('loan_deduction_id', '!=', '')
                                            // ->whereRaw("FIND_IN_SET(?, loan_deduction_bank_id)", [$loan->id])
                                            ->get();

                                            $total_loan_amount = 0;
                                            foreach ($bank_deduction as $key => $value) {

                                                // -- Supplimentary Start --


                                                $supplimentaryloanTotals = [];
                                                $supplimentaryloanTotalsids = [];

                                                if($value->supplimentary_status == 1){

                                                        $supplimentary_record = SupplimentaryBill::
                                                                                                where('employee_id',$value->employee_id)
                                                                                                ->where('Emp_Code',$value->Emp_Code)
                                                                                                ->where('id',$value->supplimentary_ids)->first();

                                                        $remaining_freeze_ids = explode(',', $supplimentary_record->remaining_freeze_id);

                                                        $supplimentaryData = RemainingFreezeSalary::whereIn('id', $remaining_freeze_ids)
                                                                                                    ->where('loan_deduction_id', '!=', '')
                                                                                                    ->whereRaw("FIND_IN_SET(?, loan_deduction_bank_id)", [$loan->id])
                                                                                                    ->get();

                                                    if(!empty($supplimentaryData)){

                                                        foreach ($supplimentaryData as $freeze){


                                                            $supplimentary_explode_loan_ids = explode(',', $freeze->loan_deduction_id);
                                                            $supplimentary_explode_bank_ids = explode(',', $freeze->loan_deduction_bank_id);
                                                            $supplimentary_explode_loan_amt = explode(',', $freeze->loan_deduction_amt);

                                                            if (!empty($freeze->loan_deduction_id) && $freeze->loan_deduction_id != '' ){

                                                                $loan_ids = $supplimentary_explode_loan_ids; // Assuming $supplimentary_explode_loan_ids is an array of loan IDs
                                                                $index1 = array_search($loan->id, $supplimentary_explode_bank_ids);

                                                                if ($index1 !== false) {
                                                                    $total_loan_amount += $supplimentary_explode_loan_amt[$index1];
                                                                }
                                                            }

                                                        }

                                                    }

                                                }

                                                // {{-- Supplimentary End --}}

                                                if($value->loan_deduction_id){
                                                    $explode_loan_ids = explode(',', $value->loan_deduction_id);
                                                    $explode_bank_ids = explode(',', $value->loan_deduction_bank_id);
                                                    $explode_loan_amt = explode(',', $value->loan_deduction_amt);

                                                    $index = array_search($loan->id, $explode_bank_ids);

                                                    if ($index !== false) {
                                                        $total_loan_amount += $explode_loan_amt[$index];
                                                    }
                                                }
                                            }
                                            $grand_total_loan_amount+= $total_loan_amount;
                                        @endphp
                                        <tr>
                                            <td>{{ ++$count }}</td>
                                            <td>{{ $loan->loan }}</td>
                                            <td>{{ $total_loan_amount }}</td>
                                            <td>{{ $bank_deduction->count() }}</td>
                                            <td>{{ $monthName }}</td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>

                            <tr>
                                <td>{{ ++$count }}</td>
                                <td><b>Total</b></td>
                                <td><b>{{ $grand_total_loan_amount }}</b></td>
                                <td></td>
                                <td></td>
                            </tr>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




</x-admin.layout>

<script>
    // On change ward fetch departments
    $("#month").on("change", function (e) {
        var month = this.value;
        var url = "{{ route('fetch-date-range', ':month') }}";

        $.ajax({
            url: url.replace(":month", month),
            type: "GET",
            data: {
                _method: "GET",
                _token: "{{ csrf_token() }}",
            },
            success: function (data, textStatus, jqXHR) {
                if (!data.error && !data.error2) {
                    $("#from_date").val(data.fromDate);
                    $("#to_date").val(data.toDate);
                } else {
                    alert(data.error);
                }
            },
            error: function (error, jqXHR, textStatus, errorThrown) {
                swal("Error!", "Something went wrong", "error");
            },
        });
    });
    </script>
