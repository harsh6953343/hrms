<x-admin.layout>
    <x-slot name="title">Retire Employees Report (current financial year)</x-slot>
    <x-slot name="heading">Retire Employees Report (current financial year)</x-slot>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>Employee Id</th>
                                    <th>Employee Name</th>
                                    <th>Ward</th>
                                    <th>Department</th>
                                    <th>Designation</th>
                                    <th>Class</th>
                                    <th>Retirement Date</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($employees as $employee)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $employee?->employee_id }}</td>
                                        <td>{{ $employee?->fname." ".$employee->mname." ".$employee->lname }}</td>
                                        <td>{{ $employee?->ward?->name }}</td>
                                        <td>{{ $employee?->department?->name }}</td>
                                        <td>{{ $employee?->designation?->name }}</td>
                                        <td>{{ $employee?->class?->name }}</td>
                                        <td>{{ $employee?->retirement_date }}</td>
                                        <td><a href="{{ route('employee-status.index') }}">{{ $employee?->status?->name }}</a></td>
                                    </tr>
                                @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>



</x-admin.layout>

