@php
  use App\Models\EmployeeMonthlyLoan;
  use App\Models\SupplimentaryBill;
  use App\Models\RemainingFreezeSalary;
  use Carbon\Carbon;
@endphp
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pay Sheet Report</title>
    <style>

        body {
                font-family: "Source Sans 3", Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;
                -webkit-font-smoothing: antialiased;
                font-size: 14px;
            }

        @media print {
            body {
                -webkit-print-color-adjust: exact;
            }
        }

        .section-heading {
                text-align: center;
                margin: 10px 0 5px;
                font-size: 24px;
            }



        table, th, td {
            font-size:12px;
        border: 1px solid rgb(177, 173, 173);
        border-collapse: collapse;

        }
        table, th
        {
            font-weight:5px;
            font-size:15px;
            padding: 6px;
            text-align: left;
            background-color: #feebd1;

        }

        table,td
        {
            font-size: 16px;
            background-color: #dde2ee;
            padding: 6px;
        }

        .table-footer{
            background-color: lightgray!important;
        }

        .signature{
            background-color: white;
            border: none;
            padding: 5px; /* Add padding for spacing */
            text-align: center;
            font-weight: bold;
        }

        .page-break {
            page-break-after: always;
        }

        thead { display: table-header-group }
        tfoot { display: table-row-group }
        tr { page-break-inside: avoid }

    </style>
</head>
<body>

    <table style="width: 100%; border:none;">
        <thead>
            <tr>
                <td style="background-color:white; border:none; text-align:right;">
                    @if(isset($base64Logo))
                    <img src="data:image/png;base64,{{ $base64Logo }}" alt="Corporation Logo" height="80" width="90">
                    @endif
                </td>
                <td style="background-color:white; border:none;">
                    <h2 class="section-heading">{{ $corporation->name }}</h2>
                    @if(!empty($freezeAttendances) && isset($freezeAttendances[0]))
                    <h5 class="section-heading">Ward: {{ $freezeAttendances[0]->employee->ward->name }}
                        Salary Bill - All of {{ $department_name }}
                        for the month of {{ Carbon::parse($to_date)->format('F Y') }}</h5>
                    @endif
                </td>
            </tr>
        </thead>
    </table>


    <table style="width: 100%; margin-top:2%;">
        <thead>
            <tr>
                <th class="th">Sr.No/<br/>Emp Code</th>
                <th class="th">Present_Days</th>
                <th class="th">Name/<br/>Pay Scale/<br/>Date Of Appointment</th>
                <th class="th">Ward/ <br>Department/<br/> Class/<br> Desg</th>
                <th class="th">Basic_Pay</th>
                @foreach ($allowances->chunk(5) as $chunk)
                    <th class="th">
                        @foreach ($chunk as $allowance)
                            {{ substr($allowance->allowance, 0, 5) }}/ <br>
                        @endforeach
                    </th>
                @endforeach
                <th class="th">Fest Adv.</th>
                <th class="th">DA Differance</th>
                <th class="th">Total_Earn</th>
                @foreach ($deductions->chunk(5) as $chunk)
                    <th class="th">
                        @foreach ($chunk as $deduction)
                            {{ substr($deduction->deduction, 0, 5) }}/ <br>
                        @endforeach
                    </th>
                @endforeach
                <th class="th">PF Loan/ <br/> Bank_Loan/ <br>LIC/ <br> Festival/ <br> Stamp_Duty</th>
                <th class="th">Employee Share</th>
                <th class="th">Total_Deduct</th>
                <th class="th">Net_Salary</th>
                <th class="th">Corporation_Share</th>
                <th class="th">Remark</th>
            </tr>
        </thead>

        <tbody>

            @php
                $grand_total_basic_salary = 0;
                $grand_total_da_differance = 0;
                $grand_total_earn = 0;
                $grand_total_festival_allowance = 0;

                $allowanceTotals = [];
                $deductionTotals = [];

                $grand_total_pf = 0;
                $grand_total_bank_loan = 0;
                $grand_total_stamp_duty = 0;
                $grand_total_deductions = 0;
                $grand_total_net_salary = 0;
                $grand_total_corporation_share = 0;
                $grand_total_lic = 0;
                $grand_total_festival_deduction = 0;
                $grand_total_employee_share = 0;

                // Supplimentary
                $grand_total_supplimentary_basic_salary = 0;
                $grand_supplimentary_allowanceTotals = [];
                $grand_supplimentary_total_earn = 0;
                $grand_supplimentary_deductionTotals = [];
                $grand_supplimentary_total_stamp_duty = 0;
                $grand_supplimentary_total_deductions = 0;
                $grand_supplimentary_total_net_salary = 0;
                $grand_supplimentary_total_corporation = 0;
                $grand_supplimentary_total_lic = 0;
                $grand_supplimentary_total_festival_advance = 0;
                $grand_supplimentary_total_festival_deduction = 0;
                $grand_supplimentary_total_employee_share = 0;

            @endphp

            @foreach ($freezeAttendances as $index_key => $freezeAttendance)
                @php
                    $explode_allowance_ids = explode(',', $freezeAttendance->allowance_Id);
                    $explode_allowance_type = explode(',', $freezeAttendance->allowance_Type);
                    $explode_allowance_amt = explode(',', $freezeAttendance->allowance_Amt);

                    $explode_deduction_ids = explode(',', $freezeAttendance->deduction_Id);
                    $explode_deduction_amt = explode(',', $freezeAttendance->deduction_Amt);
                    $explode_deduction_type = explode(',', $freezeAttendance->deduction_Type);

                    $explode_loan_ids = explode(',', $freezeAttendance->loan_deduction_id);
                    $explode_bank_ids = explode(',', $freezeAttendance->loan_deduction_bank_id);


                    // Grand Total Calculations
                    $grand_total_basic_salary += $freezeAttendance->basic_salary;
                    $grand_total_da_differance += $freezeAttendance->da_differance;

                    $grand_total_earn+= ($freezeAttendance->basic_salary + $freezeAttendance->total_allowance);
                    $grand_total_festival_allowance+= $freezeAttendance->festival_allowance;

                    $supplimentaryData = '';
                @endphp



                {{-- Supplimentary Calculation --}}


                @if($freezeAttendance->supplimentary_status == 1)

                    @php
                        $supplimentary_record = SupplimentaryBill::
                                                                where('employee_id',$freezeAttendance->employee_id)
                                                                ->where('Emp_Code',$freezeAttendance->Emp_Code)
                                                                ->where('id',$freezeAttendance->supplimentary_ids)->first();

                        $remaining_freeze_ids = explode(',', $supplimentary_record->remaining_freeze_id);

                        $supplimentaryData = RemainingFreezeSalary::whereIn('id', $remaining_freeze_ids)->get();

                    @endphp

                @endif


                    @php
                        $supplimentary_basic_salary = 0;
                        $supplimentary_total_earn = 0;

                        $supplimentaryallowanceTotals = [];
                        $supplimentarydeductionTotals = [];
                        $supplimentaryloanTotals = [];
                        $supplimentaryloanTotalsids = [];

                        $supplimentary_stamp_duty = 0;
                        $supplimentary_total_deductions = 0;
                        $suppliemnatry_net_salary = 0;
                        $suppliemnatry_corporation_share = 0;
                        $suppliemnatry_present_days = 0;
                        $supplimentary_lic = 0;
                        $supplimentary_festival_advance = 0;
                        $supplimentary_festival_deduction = 0;
                        $supplimentary_employee_share = 0;

                    @endphp
                    @if(!empty($supplimentaryData))
                    @foreach ($supplimentaryData as $freeze)

                        @php
                            $supplimentary_explode_allowance_ids = explode(',', $freeze->allowance_Id);
                            $supplimentary_explode_allowance_amt = explode(',', $freeze->allowance_Amt);

                            $supplimentary_explode_deduction_ids = explode(',', $freeze->deduction_Id);
                            $supplimentary_explode_deduction_amt = explode(',', $freeze->deduction_Amt);

                            $supplimentary_explode_loan_ids = explode(',', $freeze->loan_deduction_id);

                            $supplimentary_basic_salary += $freeze->basic_salary;

                            $supplimentary_total_earn   += ($freeze->basic_salary + $freeze->total_allowance);

                            // $grand_total_bank_loan+= $bank_loan;
                            $supplimentary_stamp_duty       += $freeze->stamp_duty;
                            $supplimentary_lic              += $freeze->total_lic_deduction;
                            $supplimentary_festival_advance += $freeze->festival_allowance;
                            $supplimentary_festival_deduction+= $freeze->total_festival_deduction;
                            $supplimentary_employee_share   += $freeze->employee_share_da;



                            $supplimentary_total_deductions+=$freeze->total_deduction;

                            $suppliemnatry_net_salary+= $freeze->net_salary;
                            $suppliemnatry_corporation_share+= $freeze->corporation_share_da;

                            if($freeze->present_day == 0){
                                $startDate = Carbon::createFromFormat('Y-m-d', $freeze->from_date);
                                $endDate = Carbon::createFromFormat('Y-m-d', $freeze->to_date);
                                $numberOfDaysInMonth = $startDate->diffInDays($endDate);
                                $numberOfDaysInMonth += 1;
                                $suppliemnatry_present_days+= $numberOfDaysInMonth;
                            }
                            $suppliemnatry_present_days += $freeze->present_day;

                        @endphp

                        {{-- Allowance --}}
                        @foreach ($allowances->chunk(5, true) as $chunk)
                            @foreach ($chunk as $allowance)
                                @php
                                    $index = array_search($allowance->id, $supplimentary_explode_allowance_ids);
                                @endphp
                                @if($index !== false)
                                    @php
                                    if (array_key_exists($allowance->id, $supplimentaryallowanceTotals)) {
                                        // If it exists, add the deduction amount to the existing total
                                        $supplimentaryallowanceTotals[$allowance->id] += $supplimentary_explode_allowance_amt[$index];
                                    } else {
                                        // If it doesn't exist, initialize the total with the deduction amount
                                        $supplimentaryallowanceTotals[$allowance->id] = $supplimentary_explode_allowance_amt[$index];
                                    }

                                    if (array_key_exists($allowance->id, $grand_supplimentary_allowanceTotals)) {
                                        // If it exists, add the deduction amount to the existing total
                                        $grand_supplimentary_allowanceTotals[$allowance->id] += $supplimentary_explode_allowance_amt[$index];
                                    } else {
                                        // If it doesn't exist, initialize the total with the deduction amount
                                        $grand_supplimentary_allowanceTotals[$allowance->id] = $supplimentary_explode_allowance_amt[$index];
                                    }

                                    @endphp
                                @endif
                            @endforeach
                        @endforeach

                        {{-- Deductions --}}
                        @foreach ($deductions->chunk(5, true) as $chunk)
                            @foreach ($chunk as $deduction)
                                @php
                                    $index = array_search($deduction->id, $supplimentary_explode_deduction_ids);
                                @endphp
                                @if($index !== false)
                                    @php
                                    if (array_key_exists($deduction->id, $supplimentarydeductionTotals)) {
                                        $supplimentarydeductionTotals[$deduction->id] += $supplimentary_explode_deduction_amt[$index];
                                    } else {
                                        $supplimentarydeductionTotals[$deduction->id] = $supplimentary_explode_deduction_amt[$index];
                                    }

                                    if (array_key_exists($deduction->id, $grand_supplimentary_deductionTotals)) {
                                        $grand_supplimentary_deductionTotals[$deduction->id] += $supplimentary_explode_deduction_amt[$index];
                                    } else {
                                        $grand_supplimentary_deductionTotals[$deduction->id] = $supplimentary_explode_deduction_amt[$index];
                                    }
                                    @endphp
                                @endif
                            @endforeach
                        @endforeach

                        {{-- Loan --}}

                        @if (!empty($freeze->loan_deduction_id) && $freeze->loan_deduction_id != '' )

                            @php
                                $loan_ids = $supplimentary_explode_loan_ids; // Assuming $supplimentary_explode_loan_ids is an array of loan IDs
                                $emp_loans = EmployeeMonthlyLoan::with('loan')->whereIn('id', $loan_ids)->get();
                                foreach ($emp_loans as $emp_loan) {
                                    $loan_name = $emp_loan->loan->loan;
                                    $loan_amount = $emp_loan->installment_amount;
                                    $loan_auto_id = $emp_loan->id;

                                    // Store loan name and amount
                                    if (array_key_exists($emp_loan->loan_id, $supplimentaryloanTotals)) {
                                        $supplimentaryloanTotals[$emp_loan->loan_id] += $loan_amount;
                                    } else {
                                        $supplimentaryloanTotals[$emp_loan->loan_id] = $loan_amount;
                                    }

                                    if (array_key_exists($loan_auto_id, $supplimentaryloanTotalsids)) {
                                        $supplimentaryloanTotalsids[$loan_auto_id] += $loan_amount;
                                    } else {
                                        $supplimentaryloanTotalsids[$loan_auto_id] = $loan_amount;
                                    }
                                }
                            @endphp
                        @endif


                    @endforeach
                @endif
                {{-- Supplimentary Calculation End --}}

                @php

                    $grand_total_supplimentary_basic_salary +=  $supplimentary_basic_salary;
                    $grand_supplimentary_total_earn         +=  $supplimentary_total_earn;
                    $grand_supplimentary_total_stamp_duty   +=  $supplimentary_stamp_duty;
                    $grand_supplimentary_total_deductions   +=  $supplimentary_total_deductions;
                    $grand_supplimentary_total_net_salary   +=  $suppliemnatry_net_salary;
                    $grand_supplimentary_total_corporation  +=  $suppliemnatry_corporation_share;
                    $grand_supplimentary_total_lic          +=  $supplimentary_lic;
                    $grand_supplimentary_total_festival_advance +=  $supplimentary_festival_advance;
                    $grand_supplimentary_total_festival_deduction +=  $supplimentary_festival_deduction;
                    $grand_supplimentary_total_employee_share +=  $supplimentary_employee_share;

                @endphp

                <tr>
                    <td>{{ $loop->iteration }} <br> {{ $freezeAttendance->Emp_Code }} </td>
                    <td>{{ $freezeAttendance->present_day }}
                        @if (!empty($supplimentaryData))
                        {{ '/'.$suppliemnatry_present_days }}
                        @endif </td>
                    <td>{{ $freezeAttendance->emp_name }} <br> {{ $freezeAttendance->pay_band_scale." ".$freezeAttendance->grade_pay_scale }} <br> {{ $freezeAttendance->date_of_appointment }}</td>
                    <td>{{ $freezeAttendance?->ward?->name }} <br> {{ $freezeAttendance?->department?->name }} <br> {{ $freezeAttendance?->employee?->class?->name }} <br> {{ $freezeAttendance?->designation?->name }}</td>
                    <td>{{ $freezeAttendance->basic_salary + $supplimentary_basic_salary }}</td>
                    {{-- Allowance --}}
                    @foreach ($allowances->chunk(5, true) as $chunk)
                        <td>
                            @foreach ($chunk as $allowance)
                                @php
                                    // Find the index of the allowance ID in the $explode_allowance_ids array
                                    $index = array_search($allowance->id, $explode_allowance_ids);
                                @endphp

                                @if($index !== false)

                                    @php
                                    if (array_key_exists($allowance->id, $allowanceTotals)) {
                                        // If it exists, add the allowance amount to the existing total
                                        $allowanceTotals[$allowance->id] += $explode_allowance_amt[$index];
                                    } else {
                                        // If it doesn't exist, initialize the total with the allowance amount
                                        $allowanceTotals[$allowance->id] = $explode_allowance_amt[$index];
                                    }
                                    @endphp

                                    @php
                                    $total_amount = $explode_allowance_amt[$index];
                                    if (!empty($supplimentaryData)) {
                                        $supplementary_total = isset($supplimentaryallowanceTotals[$allowance->id]) ? $supplimentaryallowanceTotals[$allowance->id] : 0;
                                        $total_amount += $supplementary_total;
                                    }
                                    @endphp

                                    {{ $total_amount }} <br>

                                @elseif(!in_array($allowance->id, $explode_allowance_ids))

                                    @php
                                    $supplementary_total = isset($supplimentaryallowanceTotals[$allowance->id]) ? $supplimentaryallowanceTotals[$allowance->id] : '-';
                                    @endphp

                                    {{ $supplementary_total }} <br>

                                @else
                                    -<br>
                                @endif
                            @endforeach
                        </td>
                    @endforeach

                    {{-- Festival Allowance --}}
                    <td>
                        {{ ($freezeAttendance->festival_allowance + $supplimentary_festival_advance) }}
                    </td>

                    {{-- DA Differance Allowance --}}
                    <td>
                        {{ $freezeAttendance->da_differance }}
                    </td>

                    {{-- Total Allowance --}}
                    <td>
                        @if (!empty($supplimentaryData))
                            {{ (($freezeAttendance->basic_salary + $freezeAttendance->total_allowance) + $supplimentary_total_earn ) }}
                        @else
                        {{ ($freezeAttendance->basic_salary + $freezeAttendance->total_allowance) }}
                        @endif

                    </td>

                    {{-- Deductions  --}}

                    @foreach ($deductions->chunk(5, true) as $chunk)
                        <td>
                            @foreach ($chunk as $deduction)
                                @php
                                    $index = array_search($deduction->id, $explode_deduction_ids);
                                @endphp

                                @if($index !== false)

                                    @php
                                    if (array_key_exists($deduction->id, $deductionTotals)) {
                                        // If it exists, add the deduction amount to the existing total
                                        $deductionTotals[$deduction->id] += $explode_deduction_amt[$index];
                                    } else {
                                        // If it doesn't exist, initialize the total with the deduction amount
                                        $deductionTotals[$deduction->id] = $explode_deduction_amt[$index];
                                    }
                                    @endphp

                                    @php
                                    $total_amount = $explode_deduction_amt[$index];
                                    if (!empty($supplimentaryData)) {
                                        $supplementary_total = isset($supplimentarydeductionTotals[$deduction->id]) ? $supplimentarydeductionTotals[$deduction->id] : 0;
                                        $total_amount += $supplementary_total;
                                    }
                                    @endphp

                                    {{ $total_amount }} <br>

                                @elseif(!in_array($deduction->id, $explode_deduction_ids))

                                    @php
                                    $supplementary_total = isset($supplimentarydeductionTotals[$deduction->id]) ? $supplimentarydeductionTotals[$deduction->id] : '-';
                                    @endphp

                                    {{ $supplementary_total }} <br>

                                @else
                                    -<br>
                                @endif
                            @endforeach
                        </td>
                    @endforeach


                    {{-- Loan --}}
                    @php
                    $pf_amt = 0;
                    $bank_loan = 0;
                    @endphp


                    @if(!empty($supplimentaryData) && !empty($supplimentaryloanTotals))
                    @foreach($supplimentaryloanTotals as $loan_id => $total_amt)
                        @php $emp_loan = EmployeeMonthlyLoan::with('loan')->where('loan_id', $loan_id)->first(); @endphp
                        @if(!in_array($emp_loan->loan_id, $explode_bank_ids))
                        @php if ($emp_loan && $emp_loan->loan_id == 1) {
                            $pf_amt = $total_amt;
                            $grand_total_pf += $pf_amt;
                        }else{
                            $bank_loan = $total_amt;
                        }
                        @endphp
                        @endif
                    @endforeach
                    @endif

                    @if ($freezeAttendance->loan_deduction_id)
                        @foreach ($explode_loan_ids as $loan_id)
                            @php
                                $emp_loan = EmployeeMonthlyLoan::with('loan')->where('id', $loan_id)->first();

                                if ($emp_loan && $emp_loan->loan_id == 1) {

                                    if (!empty($supplimentaryData) && isset($supplimentaryloanTotals[$emp_loan->loan_id])) {
                                        $pf_amt = $supplimentaryloanTotals[$emp_loan->loan_id] + $emp_loan->installment_amount;;
                                        $grand_total_pf += $pf_amt;
                                    } else {
                                        $pf_amt = $emp_loan->installment_amount;
                                        $grand_total_pf += $pf_amt;
                                    }
                                }else{

                                    $bank_loan = $emp_loan->installment_amount;
                                }

                                if (!empty($supplimentaryData)) {
                                    if (isset($supplimentaryloanTotals[$emp_loan->loan_id]) && $emp_loan->loan_id != 1) {
                                        $bank_loan = ($emp_loan->installment_amount) + $supplimentaryloanTotals[$emp_loan->loan_id];
                                    }
                                } else if($emp_loan->loan_id != 1) {
                                    $bank_loan = $emp_loan->installment_amount;
                                }
                            @endphp
                        @endforeach
                    @endif
                    <td>
                        {{ ($pf_amt != 0) ? $pf_amt : '-' }}<br>
                        {{ ($bank_loan != 0) ? $bank_loan : '-' }}<br>
                        {{ $freezeAttendance->total_lic_deduction + $supplimentary_lic }}
                        <br>
                        {{ $freezeAttendance->total_festival_deduction + $supplimentary_festival_deduction }}
                        <br>
                        {{  $freezeAttendance->stamp_duty + $supplimentary_stamp_duty }}
                    </td>
                    {{-- Loan End  --}}

                    @php
                        $grand_total_bank_loan+= $bank_loan;
                        $grand_total_stamp_duty+= $freezeAttendance->stamp_duty;
                        $grand_total_lic+=$freezeAttendance->total_lic_deduction;

                        $grand_total_deductions+=$freezeAttendance->total_deduction;

                        $grand_total_net_salary+= $freezeAttendance->net_salary;
                        $grand_total_corporation_share+= $freezeAttendance->corporation_share_da;
                        $grand_total_festival_deduction+=$freezeAttendance->total_festival_deduction;
                        $grand_total_employee_share+=$freezeAttendance->employee_share_da;

                    @endphp

                    {{-- employee share --}}
                    <td>{{ $freezeAttendance->employee_share_da + $supplimentary_employee_share }}</td>

                    {{-- Total Deduction --}}
                    <td>{{ $freezeAttendance->total_deduction + $supplimentary_total_deductions }}</td>

                    {{-- Net Salary --}}
                    <td>{{ $freezeAttendance->net_salary + $suppliemnatry_net_salary}}</td>

                    <td>{{ $freezeAttendance->corporation_share_da + $suppliemnatry_corporation_share }}</td>

                    <td>
                        {{ ($freezeAttendance?->employee?->employee_status?->remark) ? $freezeAttendance?->employee?->employee_status?->remark : $freezeAttendance?->remark }}

                    </td>
                </tr>
                {{-- @if (($index_key + 1) % 5 == 0)
                    <tr class="page-break"></tr>
                @endif --}}
            @endforeach
        </tbody>

        <tfoot>
            <tr>
                <th class="table-footer" colspan="4" style="text-align: center; font-size:22px;">
                    <b> Grand Total </b>
                </th>
                <th class="table-footer">
                    <b>{{ $grand_total_basic_salary + $grand_total_supplimentary_basic_salary }}</b>
                </th>

                {{-- Grand Total Allowances --}}
                @foreach ($allowances->chunk(5, true) as $chunk)
                <th class="table-footer">
                    @foreach ($chunk as $allowance)

                        @if(isset($grand_supplimentary_allowanceTotals[$allowance->id]) && isset($allowanceTotals[$allowance->id]))
                            <b>{{ $allowanceTotals[$allowance->id] + $grand_supplimentary_allowanceTotals[$allowance->id] }} </b><br>
                        @elseif(isset($allowanceTotals[$allowance->id]))
                            <b>{{ $allowanceTotals[$allowance->id] }} </b><br>
                        @elseif(isset($grand_supplimentary_allowanceTotals[$allowance->id]))
                            <b>{{ $grand_supplimentary_allowanceTotals[$allowance->id] }} </b><br>
                        @else
                            -<br>
                        @endif

                    @endforeach
                </th>
                @endforeach

                {{-- Grand Total Festival allowance --}}
                <th class="table-footer">
                    {{ $grand_total_festival_allowance + $grand_supplimentary_total_festival_advance }}
                </th>

                {{-- Grand Total DA Differance --}}
                <th class="table-footer">
                    {{ $grand_total_da_differance }}
                </th>

                {{-- Grand Total Earn --}}
                <th class="table-footer">
                    {{ $grand_total_earn + $grand_supplimentary_total_earn }}
                </th>

                {{-- Grand Total Deductions --}}
                @foreach ($deductions->chunk(5, true) as $chunk)
                <th class="table-footer">
                    @foreach ($chunk as $key=>$deduction)

                        @if(isset($grand_supplimentary_deductionTotals[$deduction->id]) && isset($deductionTotals[$deduction->id]))
                            <b>{{ $deductionTotals[$deduction->id] + $grand_supplimentary_deductionTotals[$deduction->id] }} </b><br>
                        @elseif(isset($deductionTotals[$deduction->id]))
                            <b>{{ $deductionTotals[$deduction->id] }} </b><br>
                        @elseif(isset($grand_supplimentary_deductionTotals[$deduction->id]))
                            <b>{{ $grand_supplimentary_deductionTotals[$deduction->id] }} </b><br>
                        @else
                            -<br>
                        @endif

                    @endforeach
                </th>
                @endforeach

                {{-- Loan Calculation --}}
                <th class="table-footer">
                    {{ $grand_total_pf }} <br> {{ $grand_total_bank_loan }} <br> {{ $grand_total_lic + $grand_supplimentary_total_lic }} <br> {{ $grand_total_festival_deduction + $grand_supplimentary_total_festival_deduction }} <br> {{ $grand_total_stamp_duty + $grand_supplimentary_total_stamp_duty }}
                </th>

                {{-- employee share --}}
                <th class="table-footer">
                    {{ $grand_total_employee_share + $grand_supplimentary_total_employee_share }}
                </th>

                {{-- Total Deductions --}}
                <th class="table-footer">
                    {{ $grand_total_deductions + $grand_supplimentary_total_deductions }}
                </th>

                {{-- Total Net Salary  --}}
                <th class="table-footer">
                    {{ $grand_total_net_salary + $grand_supplimentary_total_net_salary }}
                </th>

                {{-- Total Corporation Share --}}
                <th class="table-footer" colspan="2">
                    {{ $grand_total_corporation_share + $grand_supplimentary_total_corporation }}
                </th>

            </tr>
        </tfoot>

    </table>


    {{-- Signature part --}}


    {{-- <table style="width: 100%; margin-top:8%; border:none;">


        <tr>
            <td class="signature">________________</td>
            <td class="signature">____________________</td>
            <td class="signature">____________________________</td>
            <td class="signature">__________________</td>
            <td class="signature">____________________________</td>
            <td class="signature">__________________</td>
        </tr>

        <tr>
            <td class="signature">EST. Cleark</td>
            <td class="signature">Est Department Head</td>
            <td class="signature">ASST. Commissioner(Gen.Admin)</td>
            <td class="signature">Cheif Auditor</td>
            <td class="signature">Cheif Accountant & Finance Officer</td>
            <td class="signature">DY.Commissioner(HQ)</td>
        </tr>

    </table> --}}

    <div class="page-break"></div>

    {{-- All Totals Footer --}}

    <table style="width: 100%; margin-top:8%;">
        <thead>
            <tr>
                <th>Earnings</th>
                <th>Amount</th>
                <th>Deductions</th>
                <th>Amount</th>
                <th>Deductions In Bank</th>
                <th>Amount</th>
            </tr>
        </thead>

        <tbody>
            <tr>
                <td>
                    Basic <br> <br>

                    @foreach ($allowances as $chunk)

                    {{ $chunk->allowance }}          <br> <br>

                    @endforeach

                    Festival Advance <br> <br>

                    DA Differance
                </td>
                <td>
                <b>
                    {{ $grand_total_basic_salary + $grand_total_supplimentary_basic_salary }}  <br> <br>

                    @foreach ($allowances as $allowance)
                            @if(isset($grand_supplimentary_allowanceTotals[$allowance->id]) && isset($allowanceTotals[$allowance->id]))
                                <b>{{ $allowanceTotals[$allowance->id] + $grand_supplimentary_allowanceTotals[$allowance->id] }} </b><br> <br>
                            @elseif(isset($allowanceTotals[$allowance->id]))
                                <b>{{ $allowanceTotals[$allowance->id] }} </b><br> <br>
                            @elseif(isset($grand_supplimentary_allowanceTotals[$allowance->id]))
                                <b>{{ $grand_supplimentary_allowanceTotals[$allowance->id] }} </b><br> <br>
                            @else
                                0<br> <br>
                            @endif
                    @endforeach

                    <b>{{ $grand_total_festival_allowance + $grand_supplimentary_total_festival_advance }}</b> <br> <br>

                    <b>{{ $grand_total_da_differance }}</b>

                </b>
                </td>

                <td>
                    @foreach ($deductions as $chunk)

                        {{ $chunk->deduction }}          <br> <br>

                    @endforeach
                </td>
                <td>
                    @foreach ($deductions as $deduction)
                        @if(isset($grand_supplimentary_deductionTotals[$deduction->id]) && isset($deductionTotals[$deduction->id]))
                            <b>{{ $deductionTotals[$deduction->id] + $grand_supplimentary_deductionTotals[$deduction->id] }} </b><br> <br>
                        @elseif(isset($deductionTotals[$deduction->id]))
                            <b>{{ $deductionTotals[$deduction->id] }} </b><br> <br>
                        @elseif(isset($grand_supplimentary_deductionTotals[$deduction->id]))
                            <b>{{ $grand_supplimentary_deductionTotals[$deduction->id] }} </b><br> <br>
                        @else
                            0<br><br>
                        @endif
                    @endforeach
                </td>

                <td>
                    PF Loan <br> <br>

                    Bank Loan <br> <br>

                    LIC Deduction <br> <br>

                    Festival Deduction <br> <br>

                    Employee Share <br> <br>

                    Stamp Duty <br> <br>
                </td>

                <td>
                    <b>
                        {{ $grand_total_pf }} <br> <br> {{ $grand_total_bank_loan }} <br> <br> {{ $grand_total_lic + $grand_supplimentary_total_lic }} <br> <br> {{ $grand_total_festival_deduction + $grand_supplimentary_total_festival_deduction }} <br> <br> {{ $grand_total_employee_share + $grand_supplimentary_total_employee_share }} <br> <br> {{ $grand_total_stamp_duty + $grand_supplimentary_total_stamp_duty }} <br> <br>
                    </b>
                </td>
            </tr>

        </tbody>

    </table>


</body>
</html>
