@php
    use App\Models\Allowance;
    use App\Models\Deduction;
    use App\Models\EmployeeMonthlyLoan;
    use Carbon\Carbon;


     $explode_loan_ids = explode(',', $freezeAttendance->loan_deduction_id);
     $explode_bank_ids = explode(',', $freezeAttendance->loan_deduction_bank_id);

     $explode_lic_ids = explode(',', $freezeAttendance->lic_deduction_id);

     $explode_festival_ids = explode(',', $freezeAttendance->festival_deduction_id);

@endphp
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Salary Slip - {{ $freezeAttendance->emp_name }}</title>
    <style>


        body {
                font-family: "Source Sans 3", Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;
                -webkit-font-smoothing: antialiased;
                font-size: 16px;
            }

        @media print {
            body {
                -webkit-print-color-adjust: exact;
            }
        }

        /* body {
            font-family: Arial, sans-serif;
            font-size: 16px;
        } */
        .label {
            display: inline-block;
            width: 150px; /* Adjust width as needed */
        }
        .section {
            width: 100%;
            margin-left: 45px;
            margin-bottom: 20px;
        }
        .section-heading {
            margin: 10px 0 5px;
            font-size: 24px;
        }
        .subsection {
            width: 50%;
            float: left;
        }

        .subsection-details {
            width: 48%;
            float: left;
        }

        .table-container {
            width: 100%;
        }
        table {
            width: 100%;
            border-collapse: collapse; /* Collapse borders */
        }
        th, td {
            padding: 10px;
            font-size: 18px;
            border: 1px solid black; /* Single border */
            text-align: left;
            /* background-color: lightgray; */
        }
        .earnings-header, .deductions-header {
            background-color: #355495; /* Blue background color */
            color: #ffffff; /* White text color */
        }
        .dashed-hr {
            border: 1px dashed black;
            clear: both;
        }
        p {
            font-size: 16px;
        }
        .check
        {
            clear: both;
        }

    </style>
</head>
<body>

    <table style="width: 100%; border:none;">
        <thead>
            <tr>
                <td style="background-color:white; border:none; text-align:right">
                    @if(isset($base64Logo))
                    <img src="data:image/png;base64,{{ $base64Logo }}" alt="Corporation Logo" height="80" width="90">
                    @endif
                </td>
                <td style="background-color:white; border:none;">
                    <h2 class="section-heading" style="margin-left:10%;">{{ $corporation->name }}</h2>
                    <h5 class="section-heading" style="font-size: 18px; margin-left:18%;">Salary Slip for the month of {{ Carbon::parse($freezeAttendance->to_date)->format('F Y') }}</h5>

                </td>
            </tr>
        </thead>
    </table>

    <div class="section">
        <div class="subsection-details" style=" border-right: 1px dashed black;">
            <p><span class="label">Actual Department</span><strong>: {{ $employee_details?->ward?->name }}</strong></p>
            <p><span class="label">Employee Name</span><strong>: {{ $freezeAttendance?->emp_name }}</strong></p>
            <p><span class="label">Employee Code</span><strong>: {{ $freezeAttendance?->Emp_Code }}</strong></p>
            <p><span class="label">Address</span><strong>: {{ $freezeAttendance?->caddress }}</strong></p>
            <p><span class="label">PF A/C No</span><strong>: {{ $freezeAttendance?->pf_account_no }}</strong></p>
            <p><span class="label">Payscale</span><strong>: {{ $freezeAttendance?->pay_band_scale ." ".$freezeAttendance?->grade_pay_scale }}</strong></p>
            <p><span class="label">Pan No</span><strong>: {{ $employee_details?->pan }}</strong></p>
            <p><span class="label">Aadhar No.</span><strong>: {{ $employee_details?->aadhar }}</strong></p>
        </div>
        <div class="subsection-details" style="margin-left: 2%;">
            <p><span class="label">Working Department</span><strong>: {{ $employee_details?->working_department?->name }}</strong></p>
            <p><span class="label">Designation</span><strong>: {{ $employee_details?->designation?->name }}</strong></p>
            <p><span class="label">Date of Birth</span><strong>: {{ $freezeAttendance?->date_of_birth }}</strong></p>
            <p><span class="label">Date of Appointment</span><strong>: {{ $freezeAttendance?->date_of_appointment }}</strong></p>
            {{-- <p><span class="label">Date of Increment</span></p> --}}
            <p><span class="label">Date of Retirement</span><strong>: {{ $freezeAttendance?->date_of_retirement }}</strong></p>
            <p><span class="label">Bank A/C No.</span><strong>: {{ $freezeAttendance?->bank_account_number }}</strong></p>
            <p><span class="label">Phone No.</span><strong>: {{ $freezeAttendance?->phone_no }}</strong></p>
            <p><span class="label">Actual Basic</span><strong>: {{ $freezeAttendance?->actual_basic }}</strong></p>
        </div>
    </div>

    {{-- Supplimentary Calculation --}}

    @php
    $total_basic_salary = 0;
    $grand_total_earn = 0;
    $total_da_differance = 0;
    $grand_total_corporation = 0;

    $allowanceTotals = [];
    $deductionTotals = [];
    $loanTotals = [];

    $grand_total_bank_loan = 0;
    $total_stamp_duty = 0;
    $total_deductions = 0;
    $total_net_salary = 0;
    $present_days = 0;
    $total_lic = 0;
    $total_festival_advance = 0;
    $total_festival_deduction = 0;
    $total_employee_contribution = 0
    @endphp
    @if(!empty($supplimentaryData))

        @foreach ($supplimentaryData as $freeze)

            @php
                $supplimentary_explode_allowance_ids = explode(',', $freeze->allowance_Id);
                $supplimentary_explode_allowance_amt = explode(',', $freeze->allowance_Amt);

                $supplimentary_explode_deduction_ids = explode(',', $freeze->deduction_Id);
                $supplimentary_explode_deduction_amt = explode(',', $freeze->deduction_Amt);

                $supplimentary_explode_loan_ids = explode(',', $freeze->loan_deduction_id);

                $total_basic_salary += $freeze->basic_salary;
                $total_da_differance += $freeze->da_differance;
                $grand_total_earn   += ($freeze->basic_salary + $freeze->total_allowance);
                $grand_total_corporation += $freeze->corporation_share_da;

                // $grand_total_bank_loan+= $bank_loan;
                $total_stamp_duty+= $freeze->stamp_duty;

                $total_deductions+=$freeze->total_deduction;

                $total_net_salary+= $freeze->net_salary;

                if($freeze->present_day == 0){
                    $startDate = Carbon::createFromFormat('Y-m-d', $freeze->from_date);
                    $endDate = Carbon::createFromFormat('Y-m-d', $freeze->to_date);
                    $numberOfDaysInMonth = $startDate->diffInDays($endDate);
                    $numberOfDaysInMonth += 1;
                    $present_days+= $numberOfDaysInMonth;
                }
                $present_days += $freeze->present_day;

            @endphp

            {{-- Allowance --}}
            @foreach ($allowances->chunk(5, true) as $chunk)
                @foreach ($chunk as $allowance)
                    @php
                        $index = array_search($allowance->id, $supplimentary_explode_allowance_ids);
                    @endphp
                    @if($index !== false)
                        @php
                        if (array_key_exists($allowance->id, $allowanceTotals)) {
                            // If it exists, add the deduction amount to the existing total
                            $allowanceTotals[$allowance->id] += $supplimentary_explode_allowance_amt[$index];
                        } else {
                            // If it doesn't exist, initialize the total with the deduction amount
                            $allowanceTotals[$allowance->id] = $supplimentary_explode_allowance_amt[$index];
                        }
                        @endphp
                    @endif
                @endforeach
            @endforeach

            {{-- Deductions --}}
            @foreach ($deductions->chunk(5, true) as $chunk)
                @foreach ($chunk as $deduction)
                    @php
                        $index = array_search($deduction->id, $supplimentary_explode_deduction_ids);
                    @endphp
                    @if($index !== false)
                        @php
                        if (array_key_exists($deduction->id, $deductionTotals)) {
                            $deductionTotals[$deduction->id] += $supplimentary_explode_deduction_amt[$index];
                        } else {
                            $deductionTotals[$deduction->id] = $supplimentary_explode_deduction_amt[$index];
                        }
                        @endphp
                    @endif
                @endforeach
            @endforeach

            {{-- Loan --}}

            @if (!empty($freeze->loan_deduction_id) && $freeze->loan_deduction_id != '')
                @php
                    $loan_ids = $supplimentary_explode_loan_ids; // Assuming $supplimentary_explode_loan_ids is an array of loan IDs

                    $emp_loans = EmployeeMonthlyLoan::with('loan')->whereIn('id', $loan_ids)->get();

                    foreach ($emp_loans as $emp_loan) {
                        $loan_name = $emp_loan->loan->loan;
                        $loan_amount = $emp_loan->installment_amount;
                        $loan_auto_id = $emp_loan->id;

                        // Store loan name and amount
                        if (array_key_exists($emp_loan->loan_id, $loanTotals)) {
                            $loanTotals[$emp_loan->loan_id] += $loan_amount;
                        } else {
                            $loanTotals[$emp_loan->loan_id] = $loan_amount;
                        }
                    }
                @endphp
            @endif

            {{-- LIC Start --}}
            @if (!empty($freeze->lic_deduction_id) && $freeze->lic_deduction_id != '')
                @php
                    $total_lic += $freeze->total_lic_deduction;
                @endphp
            @endif
            {{-- LIC End --}}

            {{-- Festival advance Start --}}
            @if($freeze->festival_allowance != 0)
                @php
                    $total_festival_advance += $freeze->festival_allowance;
                @endphp
            @endif
            {{-- festival advance End --}}

            {{-- Festival deduction Start --}}
            @if($freeze->total_festival_deduction != 0)
                @php
                    $total_festival_deduction += $freeze->total_festival_deduction;
                @endphp
            @endif
            {{-- festival deduction End --}}


            {{-- employee share  Start --}}
            @if($freeze->employee_share_da != 0)
                @php
                    $total_employee_contribution += $freeze->employee_share_da;
                @endphp
            @endif
            {{-- employee share End --}}


        @endforeach
    @endif
    {{-- Supplimentary Calculation End --}}

    <hr class="dashed-hr" style="margin-bottom: 3%;">
    @if($freezeAttendance->basic_salary != 0)
        <div class="section" style="margin-left:25px;">
            <div class="subsection">
                <div class="table-container">
                    <table>
                        <thead>
                            <tr>
                                <th colspan="2" class="earnings-header">EARNINGS</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>BASIC SALARY</td>
                                <td><b>{{ $freezeAttendance->basic_salary }}  @if(!empty($supplimentaryData)) / {{ $total_basic_salary }}  @endif </b></td>
                            </tr>

                            {{-- Allowance --}}
                            @if(!empty($supplimentaryData))
                            @foreach($allowanceTotals as $allowance_id => $total_amt)
                                @if(!in_array($allowance_id, $explode_allowance_ids))
                                    @php $allowance = Allowance::find($allowance_id); @endphp
                                    <tr>
                                        <td>{{ $allowance->allowance }}</td>
                                        <td><b>0 / {{ $total_amt }}</b></td>
                                    </tr>
                                @endif
                            @endforeach
                            @endif

                            @foreach($explode_allowance_ids as $key => $allowance_id)
                                @php $allowance = Allowance::find($allowance_id); @endphp
                                <tr>
                                    <td>{{ $allowance->allowance }}</td>
                                    <td><b>{{ $explode_allowance_amt[$key] }} @if(!empty($supplimentaryData))  / @if(isset($allowanceTotals[$allowance->id])) {{ $allowanceTotals[$allowance->id] }} @else 0  @endif @endif </b></td>
                                </tr>
                            @endforeach
                            @if($freezeAttendance->festival_allowance != 0)
                            <tr>
                                <td>Festival Advance</td>
                                <td><b>{{ $freezeAttendance->festival_allowance }}  @if(!empty($supplimentaryData))  / {{ $total_festival_advance }} @endif</b></td>
                            </tr>
                            @endif

                            @if($freezeAttendance->da_differance != 0)
                            <tr>
                                <td>DA Differance</td>
                                <td><b>{{ $freezeAttendance->da_differance }}</b></td>
                            </tr>
                            @endif

                            @php
                                $deduction_rows =  (count($explode_deduction_ids) + 1 ) + count($deductionTotals); //including stamp duty

                                if ($freezeAttendance->lic_deduction_id || $total_lic != 0)
                                    $deduction_rows += 1;
                                if ($loanTotals)
                                    $deduction_rows += count($loanTotals);
                                if ($explode_loan_ids)
                                    $deduction_rows += count($explode_loan_ids);

                            @endphp
                            @if($deduction_rows > ((count($explode_allowance_ids) + 2) + count($allowanceTotals) + 1 ))
                            @php
                                $no_of_rows = $deduction_rows - ((count($explode_allowance_ids) + 2) + count($allowanceTotals) + 2); // including basic
                            @endphp
                            @for($i = 0; $i < $no_of_rows + 1; $i++)
                            <tr>
                                <td><br></td>
                                <td><br></td>
                            </tr>
                            @endfor
                            @endif

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="subsection" style="width: 45%;">
                <div class="table-container">
                    <table>
                        <thead>
                            <tr>
                                <th colspan="2" class="deductions-header">DEDUCTIONS</th>
                            </tr>
                        </thead>
                        <tbody>
                            {{-- Deductions --}}

                            @if(!empty($supplimentaryData))
                            @foreach($deductionTotals  as $deduction_id  => $total_amt)
                            @if(!in_array($deduction_id, $explode_deduction_ids))

                                @php $deduction = Deduction::find($deduction_id); @endphp
                                <tr>
                                    <td>{{ $deduction->deduction }}</td>
                                    <td><b>{{ $total_amt }}</b></td>
                                </tr>
                            @endif
                            @endforeach
                            @endif

                            @foreach($explode_deduction_ids as $key => $deduction_id)
                                @php $deduction = Deduction::find($deduction_id); @endphp
                                <tr>
                                    <td>{{ $deduction?->deduction }}</td>
                                    <td><b>{{ $explode_deduction_amt[$key] }} @if(!empty($supplimentaryData)) / @if(isset($deductionTotals[$deduction->id])) {{ $deductionTotals[$deduction->id] }} @else 0  @endif @endif </b></td>
                                </tr>
                            @endforeach


                            {{-- Employee Loan --}}
                            @if(!empty($supplimentaryData))
                                @foreach($loanTotals as $loan_id => $total_amt)
                                    @php $emp_loan = EmployeeMonthlyLoan::with('loan')->where('loan_id', $loan_id)->first(); @endphp
                                    @if(!in_array($emp_loan->loan_id, $explode_bank_ids))
                                        <tr>
                                            <td>{{ $emp_loan?->loan?->loan . ' (' . $emp_loan?->installment_no.')' }}</td>
                                            <td><b>{{ $total_amt }}</b></td>
                                        </tr>
                                    @endif
                                @endforeach
                            @endif

                            @if ($freezeAttendance->loan_deduction_id)
                                @foreach ($explode_loan_ids as $key=>$loan_id)
                                    @php $emp_loan = EmployeeMonthlyLoan::with('loan')->where('id', $loan_id)->first();
                                        $bank_id = $explode_bank_ids[$key];
                                    @endphp
                                    <tr>
                                        <td>{{ $emp_loan?->loan?->loan . ' (' . $emp_loan?->installment_no.')' }}</td>
                                        <td><b>{{ $emp_loan?->installment_amount }}
                                            @if(!empty($supplimentaryData))
                                            /
                                            @if(isset($loanTotals[$bank_id]))
                                            {{ $loanTotals[$bank_id] }} @else 0  @endif @endif</b></td>
                                    </tr>
                                @endforeach
                            @endif

                            @if ($freezeAttendance->lic_deduction_id || $total_lic != 0)
                            <tr>
                                <td>LIC Deduction</td>
                                <td><b>{{ $freezeAttendance->total_lic_deduction }}  @if(!empty($supplimentaryData))  / {{ $total_lic }} @endif</b></td>
                            </tr>
                            @endif

                            {{-- Festival Deduction --}}
                            @if ($freezeAttendance->total_festival_deduction != 0 || $total_festival_deduction != 0)
                            <tr>
                                <td>Festival Deduction</td>
                                <td><b>{{ $freezeAttendance->total_festival_deduction }}  @if(!empty($supplimentaryData))  / {{ $total_festival_deduction }} @endif</b></td>

                            </tr>
                            @endif
                            {{-- Festival Deduction End --}}

                            {{-- Employee Contribution 10% --}}
                             @if ($freezeAttendance->employee_share_da != 0 || $total_employee_contribution != 0)
                             <tr>
                                 <td>DCPS I EMPLOYEE CONTRIBUTION</td>
                                 <td><b>{{ $freezeAttendance->employee_share_da }}  @if(!empty($supplimentaryData))  / {{ $total_employee_contribution }} @endif</b></td>

                             </tr>
                             @endif
                            {{-- Employee Contribution 10% End --}}

                            <tr>
                                <td>STAMP DUTY</td>
                                <td><b>{{ $freezeAttendance->stamp_duty }}  @if(!empty($supplimentaryData))  / {{ $total_stamp_duty }} @endif</b></td>
                            </tr>

                            @if(count($explode_allowance_ids) + 1 > $deduction_rows)
                            @php
                                $no_of_rows_deduction = count($explode_allowance_ids) + 1 - $deduction_rows;
                            @endphp
                            @for($i = 0; $i < $no_of_rows_deduction; $i++)
                            <tr>
                                <td><br></td>
                                <td><br></td>
                            </tr>
                            @endfor
                            @endif

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <p class="check">

        <div class="section" style="margin-left:25px;">
            <div class="subsection">
                <table>
                    <tr>
                        <td><b>TOTAL EARNINGS</b></td>
                        <td><b>{{ $freezeAttendance->basic_salary + $freezeAttendance->total_allowance }}  @if(!empty($supplimentaryData)) / {{ $grand_total_earn }} @endif</b></td>
                    </tr>
                    <tr>
                        <td><b>Corporation Share</b></td>
                        <td><b>{{ $freezeAttendance->corporation_share_da }}  @if(!empty($supplimentaryData)) / {{ $grand_total_corporation }} @endif</b></td>
                    </tr>
                </table>
            </div>
            <div class="subsection" style="width: 45%;">
                <table>
                    <tr>
                        <td><b>TOTAL DEDUCTIONS</b></td>
                        <td><b>{{ $freezeAttendance->total_deduction }} @if(!empty($supplimentaryData)) / {{ $total_deductions }} @endif</b></td>
                    </tr>
                    <tr>
                        <td><b>NET SALARY</b></td>
                        <td ><b>{{ $freezeAttendance->net_salary }} @if(!empty($supplimentaryData)) / {{ $total_net_salary }} @endif</b></td>
                    </tr>
                </table>
            </div>
        </div>
    @else

    @if(empty($supplimentaryData))
        <h3 style="margin-left:25px;">No Salary Generate For This Month</h3>
    @else
    <h3 style="margin-left:25px;">No salary generate for this month, This is supplimentary Bill  for the month of
        @php
            $monthNames = collect(explode(',', $supplimentary_record->month))
                                            ->map(function ($monthNumber) {
                                                return \Carbon\Carbon::create()->month($monthNumber)->format('F');
                                            })
                                            ->sort()
                                            ->implode(', ');
            echo $monthNames;
        @endphp</h3>

    <div class="section" style="margin-left:25px;">
        <div class="subsection">
            <div class="table-container">
                <table>
                    <thead>
                        <tr>
                            <th colspan="2" class="earnings-header">EARNINGS</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>BASIC SALARY</td>
                            <td><b>{{ $total_basic_salary }}</b></td>
                        </tr>
                        {{-- Allowance --}}
                        @foreach($allowances as $key => $allowance)
                            @if(isset($allowanceTotals[$allowance->id]))
                            <tr>
                                <td>{{ $allowance?->allowance }}</td>
                                <td><b>{{ $allowanceTotals[$allowance->id] }}</b></td>
                            </tr>
                            @endif
                        @endforeach

                        {{-- festival advance --}}
                        @if($total_festival_advance != 0)
                        <tr>
                            <td>Festival Advance</td>
                            <td><b>{{ $total_festival_advance }}</b></td>
                        </tr>
                        @endif
                        {{-- festival advance --}}

                        @if($da_differance != 0)
                        <tr>
                            <td>DA Differance</td>
                            <td><b>{{ $da_differance }}</b></td>
                        </tr>
                        @endif

                        {{-- Count --}}
                        @php

                            $deduction_rows =  count($deductionTotals) + 1; //including stamp duty
                            if ($loanTotals)
                                $deduction_rows += count($loanTotals);
                        @endphp
                        @if($deduction_rows > count($allowanceTotals) + 1)
                        @php
                            $no_of_rows = $deduction_rows - (count($allowanceTotals) + 1); // including basic
                        @endphp
                        @for($i = 0; $i < $no_of_rows; $i++)
                        <tr>
                            <td><br></td>
                            <td><br></td>
                        </tr>
                        @endfor
                        @endif

                    </tbody>
                </table>
            </div>
        </div>

        <div class="subsection" style="width: 45%;">
            <div class="table-container">
                <table>
                    <thead>
                        <tr>
                            <th colspan="2" class="deductions-header">DEDUCTIONS</th>
                        </tr>
                    </thead>
                    <tbody>
                        {{-- Deductions --}}
                        @foreach($deductions as $key => $deduction)
                            @if(isset($deductionTotals[$deduction->id]))
                            <tr>
                                <td>{{ $deduction?->deduction }}</td>
                                <td><b>{{ $deductionTotals[$deduction->id] }}</b></td>
                            </tr>
                            @endif
                        @endforeach

                        {{-- Employee Loan --}}
                        @foreach($loanTotals as $loan_name => $loan_total)
                            <tr>
                                <td>{{ $loan_name }}</td>
                                <td><b>{{ $loan_total }}</b></td>
                            </tr>
                        @endforeach
                        @if ($total_lic != 0)
                        <tr>
                            <td>LIC Deduction</td>
                            <td><b>{{ $total_lic }}</b></td>
                        </tr>
                        @endif
                        @if ($total_festival_deduction != 0)
                        <tr>
                            <td>Festival Deduction</td>
                            <td><b>{{ $total_festival_deduction }}</b></td>
                        </tr>
                        @endif
                        {{-- Employee Contribution 10% --}}
                        @if ($total_employee_contribution != 0)
                        <tr>
                            <td>DCPS I EMPLOYEE CONTRIBUTION</td>
                            <td><b>{{ $total_employee_contribution }}</b></td>
                        </tr>
                        @endif
                        {{-- Employee Contribution 10% End --}}
                        <tr>
                            <td>STAMP DUTY</td>
                            <td><b>{{ $total_stamp_duty }}</b></td>
                        </tr>

                        @if(count($allowanceTotals) + 1 > $deduction_rows)
                        @php
                            $no_of_rows_deduction = count($allowanceTotals) + 1 - $deduction_rows;
                        @endphp
                        @for($i = 0; $i < $no_of_rows_deduction; $i++)
                        <tr>
                            <td><br></td>
                            <td><br></td>
                        </tr>
                        @endfor
                        @endif

                    </tbody>
                </table>
            </div>
        </div>

    </div>

    <p class="check">


    <div class="section" style="margin-left:25px;">
        <div class="subsection">
            <table>
                <tr>
                    <td><b>TOTAL EARNINGS</b></td>
                    <td><b>{{ $grand_total_earn }}</b></td>
                </tr>
                <tr>
                    <td><b>Corporation Share</b></td>
                    <td><b>{{ $grand_total_corporation }}</b></td>
                </tr>
            </table>
        </div>
        <div class="subsection" style="width: 45%;">
            <table>
                <tr>
                    <td><b>TOTAL DEDUCTIONS</b></td>
                    <td><b>{{ $total_deductions }}</b></td>
                </tr>
                <tr>
                    <td><b>NET SALARY</b></td>
                    <td ><b>{{ $total_net_salary }}</b></td>
                </tr>
            </table>
        </div>
    </div>

    @endif
    @endif


    <hr class="dashed-hr" style="margin-top: 12%;">
    <div style="margin-left:25px;">
    <p><b>Present Days: {{ $freezeAttendance->present_day }} @if(!empty($supplimentaryData)) / {{ $present_days }} @endif</b></p>
    <p><b>Remark:</b> </p>
    <p><b>This is a computer-generated payslip. It does not require any authorized sign.</b></p>
    <p><b>Date: {{ date('d-m-Y') }} <span style="margin-left:60%;">Establishment Clerk</span></b></p>
    <p><b>Generated & Download by: {{ Auth::user()?->name }}</b></p>
    </div>

</body>
</html>
