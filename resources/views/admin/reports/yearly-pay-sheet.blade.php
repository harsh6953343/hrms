<x-admin.layout>
    <x-slot name="title">Yearly Pay Sheet Report</x-slot>
    <x-slot name="heading">Yearly Pay Sheet Report</x-slot>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">

                    <div class="card-body">
                        <div class="mb-3 row">
                            <div class="col-md-6">
                                <div class="row d-flex">
                                    <div class="col-6">
                                        <label class="col-form-label" for="employee">Employee<span class="text-danger">*</span></label>
                                        <select class="form-control js-example-basic-single select2-hidden-accessible" id="employee" name="employee">
                                            <option value="">Select Employee</option>
                                            @foreach ($employees as $employee)
                                                <option value="{{ $employee->id }}">{{ $employee->fname." ".$employee->mname." ".$employee->lname." (".$employee->employee_id.")" }}</option>
                                            @endforeach
                                        </select>
                                        @error('employee')
                                        <span class="text-danger invalid employee_err">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="col-6">
                                        <label class="col-form-label" for="finyears">Financial Year<span class="text-danger">*</span></label>
                                        <select class="form-control js-example-basic-single select2-hidden-accessible" id="finyears" name="finyears">
                                            <option value="" disabled>Select Financial Year</option>
                                            @foreach ($finYears as $fy)
                                                <option value="{{ $fy }}">{{ $fy }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer">
                        <a href="#" id="show_yearly_paysheet" class="btn btn-secondary" title="View Yearly Paysheet">Show</a>
                        <a href="{{ route('yearly-pay-sheet.index') }}" class="btn btn-warning">Refresh</a>
                    </div>

            </div>
        </div>
    </div>

</x-admin.layout>

<script>

    $('#employee').on('change', function() {
        var url = "{{ route('yearly-pay-sheet-pdf') }}/:emp_id/:finyear";
        url = url.replace(":emp_id", this.value)
        url = url.replace(":finyear", $('#finyears').val())
        $("#show_yearly_paysheet").attr("href", url)
    });

</script>
