@php
    use App\Models\SupplimentaryBill;
    use App\Models\RemainingFreezeSalary;
@endphp
<x-admin.layout>
    <x-slot name="title">Deduction Report</x-slot>
    <x-slot name="heading">Deduction Report</x-slot>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">

                <form action="{{ route('deduction-report.index') }}" class="theme-form" name="addForm" id="addForm" enctype="multipart/form-data">
                    @csrf

                    <div class="card-body">
                        <div class="mb-3 row">

                            <div class="col-md-3">
                                <label class="col-form-label" for="deduction">Deductions<span class="text-danger">*</span></label>
                                <select class="form-control" id="deduction" name="deduction">
                                    @foreach ($deductions as $deduction)
                                        <option value="{{ $deduction->id }}" {{ ($deduction_id == $deduction->id ) ? 'Selected':'' }} >{{ $deduction->deduction }}</option>
                                    @endforeach
                                </select>
                                <span class="text-danger invalid deduction_err"></span>
                            </div>

                            <div class="col-md-3">
                                <label class="col-form-label" for="department">Department<span class="text-danger">*</span></label>
                                <select class="form-control" id="department" name="department">
                                    <option value="">Select Department</option>
                                    <option value="" {{ (empty($department_id))?'Selected':'' }}>All</option>
                                    @foreach ($departments as $department)
                                        <option value="{{ $department->id }}" {{ ($department_id == $department->id ) ? 'Selected':'' }} >{{ $department->name }}</option>
                                    @endforeach
                                </select>
                                <span class="text-danger invalid department_err"></span>
                            </div>

                            <div class="col-md-3">
                                <label class="col-form-label" for="month">Select Month<span class="text-danger">*</span></label>
                                <select class="form-control" id="month" name="month">
                                    {{-- <option value="">Select Month</option> --}}
                                    <option value="1" {{ ($month == 1)?'Selected':'' }} >January</option>
                                    <option value="2" {{ ($month == 2)?'Selected':'' }}>February</option>
                                    <option value="3" {{ ($month == 3)?'Selected':'' }}>March</option>
                                    <option value="4" {{ ($month == 4)?'Selected':'' }}>April</option>
                                    <option value="5" {{ ($month == 5)?'Selected':'' }}>May</option>
                                    <option value="6" {{ ($month == 6)?'Selected':'' }}>June</option>
                                    <option value="7" {{ ($month == 7)?'Selected':'' }}>July</option>
                                    <option value="8" {{ ($month == 8)?'Selected':'' }}>August</option>
                                    <option value="9" {{ ($month == 9)?'Selected':'' }}>September</option>
                                    <option value="10" {{ ($month == 10)?'Selected':'' }}>October</option>
                                    <option value="11" {{ ($month == 11)?'Selected':'' }}>November</option>
                                    <option value="12" {{ ($month == 12)?'Selected':'' }}>December</option>
                                </select>
                                <span class="text-danger invalid month_err"></span>
                            </div>

                            <div class="col-md-3">
                                <label class="col-form-label" for="from_date">From Date<span class="text-danger">*</span></label>
                                <input class="form-control title" id="from_date" name="from_date" type="date" placeholder="From Date" readonly value="{{ $from_date }}">
                                <span class="text-danger invalid from_date_err"></span>
                            </div>

                            <div class="col-md-3">
                                <label class="col-form-label" for="to_date">To Date<span class="text-danger">*</span></label>
                                <input class="form-control title" id="to_date" name="to_date" type="date" placeholder="To Date" readonly value="{{ $to_date }}">
                                <span class="text-danger invalid to_date_err"></span>
                            </div>

                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" id="addSubmit">Show</button>
                        <a href="{{ route('deduction-report.index') }}" class="btn btn-warning">Refresh</a>
                    </div>
                </form>

                <div class="card-body">
                    <div class="table-responsive">
                        <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>Employee Code </th>
                                    <th>Employee Name</th>
                                    <th>Contact Number</th>
                                    <th>Ward</th>
                                    <th>Department</th>
                                    <th>Designation</th>
                                    <th>Month</th>
                                    <th>Deduction Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (!empty($deduction_reports))
                                    @foreach ($deduction_reports as $row => $deduction_report)


                                        {{-- Supplimentary Start --}}
                                        @php
                                        $deduction_amt = 0;
                                        $monthNames = '';
                                        $supplimentarydeductionTotals = [];

                                        $explode_deduction_ids = explode(',', $deduction_report->deduction_Id);
                                        $explode_deduction_amt = explode(',', $deduction_report->deduction_Amt);

                                        @endphp
                                        @if($deduction_report->supplimentary_status == 1)
                                            @php

                                                $supplimentary_record = SupplimentaryBill::
                                                                                        where('employee_id',$deduction_report->employee_id)
                                                                                        ->where('Emp_Code',$deduction_report->Emp_Code)
                                                                                        ->where('id',$deduction_report->supplimentary_ids)->first();

                                                $remaining_freeze_ids = explode(',', $supplimentary_record->remaining_freeze_id);

                                                $supplimentaryData = RemainingFreezeSalary::whereIn('id', $remaining_freeze_ids)->get();
                                                $monthNames = collect(explode(',', $supplimentary_record->month))
                                                                ->map(function ($monthNumber) {
                                                                    return \Carbon\Carbon::create()->month($monthNumber)->format('F');
                                                                })
                                                                ->sort()
                                                                ->implode(', ');
                                            @endphp

                                            @foreach ($supplimentaryData as $supplimentary)

                                                @php
                                                        $supplimentary_explode_deduction_ids = explode(',', $supplimentary->deduction_Id);
                                                        $supplimentary_explode_deduction_amt = explode(',', $supplimentary->deduction_Amt);
                                                @endphp

                                                {{-- Deductions --}}
                                                @foreach ($deductions as $deduction)
                                                    @if($deduction->id == $deduction_id)
                                                        @php
                                                            $index = array_search($deduction_id, $supplimentary_explode_deduction_ids);
                                                        @endphp
                                                        @if ($index !== false)
                                                            @php
                                                                if (array_key_exists($deduction_id, $supplimentarydeductionTotals)) {
                                                                    $supplimentarydeductionTotals[$deduction_id] += $supplimentary_explode_deduction_amt[$index];
                                                                } else {
                                                                    $supplimentarydeductionTotals[$deduction_id] = $supplimentary_explode_deduction_amt[$index];
                                                                }
                                                            @endphp
                                                        @endif
                                                    @endif
                                                @endforeach


                                            @endforeach

                                            {{-- If Allowance not present in regulary salary but present in supplimentary  --}}
                                            @if(!in_array($deduction_id, $explode_deduction_ids))
                                                    @php
                                                        $supplementary_total = isset($supplimentarydeductionTotals[$deduction_id]) ? $supplimentarydeductionTotals[$deduction_id] : 0;
                                                        $deduction_amt =  $supplementary_total;
                                                    @endphp
                                            @endif

                                        @endif

                                        {{-- Supplimentary End --}}


                                        @php

                                            $index = array_search($deduction_id, $explode_deduction_ids);

                                            if ($index !== false) {
                                                $deduction_amt = $explode_deduction_amt[$index];

                                                if (!empty($supplimentaryData)) {

                                                        $supplementary_total = isset($supplimentarydeductionTotals[$deduction_id]) ? $supplimentarydeductionTotals[$deduction_id] : 0;
                                                        $deduction_amt += $supplementary_total;
                                                }

                                            }

                                        @endphp
                                        @if($deduction_amt != 0)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $deduction_report->Emp_Code }}</td>
                                            <td>{{ $deduction_report->emp_name }}</td>
                                            <td>{{ $deduction_report->phone_no }}</td>
                                            <td>{{ $deduction_report->employee->ward->name }}</td>
                                            <td>{{ $deduction_report->employee->department->name }}</td>
                                            <td>{{ $deduction_report->employee->designation->name }}</td>
                                            <td>{{ $monthName }}</td>
                                            <td>{{ $deduction_amt }}</td>
                                        </tr>
                                        @endif
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




</x-admin.layout>

<script>
    // On change ward fetch departments
    $("#month").on("change", function (e) {
        var month = this.value;
        var url = "{{ route('fetch-date-range', ':month') }}";

        $.ajax({
            url: url.replace(":month", month),
            type: "GET",
            data: {
                _method: "GET",
                _token: "{{ csrf_token() }}",
            },
            success: function (data, textStatus, jqXHR) {
                if (!data.error && !data.error2) {
                    $("#from_date").val(data.fromDate);
                    $("#to_date").val(data.toDate);
                } else {
                    alert(data.error);
                }
            },
            error: function (error, jqXHR, textStatus, errorThrown) {
                swal("Error!", "Something went wrong", "error");
            },
        });
    });
    </script>
