<x-admin.layout>
    <x-slot name="title">Add Employee Absent Attendance</x-slot>
    <x-slot name="heading">Add Employee Absent Attendance</x-slot>

    <!-- Add Form -->
    <div class="row" id="addContainer">
        <div class="col-sm-12">
            <div class="card">
                <header class="card-header">
                    <h4 class="card-title">Add Absent Attendance</h4>
                </header>
                <form class="form-horizontal form-bordered" method="post" id="editForm">
                    @csrf
                    <input type="hidden" id="edit_model_id" name="edit_model_id" value="">
                    <div class="card-body">

                        <div class="mb-3 row">

                            <div class="col-md-3 mt-2">
                                <label class="col-form-label" for="Emp_Code">Employee Id<span class="text-danger">*</span></label>
                                <input class="form-control" id="Emp_Code" name="Emp_Code" type="text" placeholder="Enter Employee Id">
                                <span class="text-danger invalid Emp_Code_err"></span>
                            </div>

                            <div class="col-md-3 mt-2">
                                <label class="col-form-label" for="month">Select Month<span class="text-danger">*</span></label>
                                <select class="form-select" id="month" name="month">
                                    <option value="">Select Month</option>
                                    <option value="1"  >January</option>
                                    <option value="2" >February</option>
                                    <option value="3" >March</option>
                                    <option value="4" >April</option>
                                    <option value="5" >May</option>
                                    <option value="6" >June</option>
                                    <option value="7" >July</option>
                                    <option value="8" >August</option>
                                    <option value="9" >September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                </select>
                                @error('month')
                                <span class="text-danger invalid month_err">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="col-md-3 mt-5">
                                <button type="button" class="btn btn-primary" id="searchBtn">Search Employee</button>
                            </div>
                        </div>

                        <div class="mb-3 row">

                            <input type="hidden" name="employee_id" id="employee_id">
                            <input type="hidden" name="from_date" id="from_date">
                            <input type="hidden" name="to_date" id="to_date">
                            {{-- <input type="hidden" name="month" id="month"> --}}

                            <div class="col-md-4">
                                <label class="col-form-label" for="emp_name">Employee Name<span class="text-danger">*</span></label>
                                <input class="form-control" id="emp_name" name="emp_name" type="text" placeholder="Employee Name" readonly>
                                <span class="text-danger invalid emp_name_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="ward">Ward<span class="text-danger">*</span></label>
                                <input class="form-control" id="ward" name="ward" type="text" placeholder="Employee Ward" readonly>
                                <span class="text-danger invalid ward_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="department">Department<span class="text-danger">*</span></label>
                                <input class="form-control" id="department" name="department" type="text" placeholder="Employee Department" readonly>
                                <span class="text-danger invalid department_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="designation">Designation<span class="text-danger">*</span></label>
                                <input class="form-control" id="designation" name="designation" type="text" placeholder="Employee Designation" readonly>
                                <span class="text-danger invalid designation_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="class">Class<span class="text-danger">*</span></label>
                                <input class="form-control" id="class" name="class" type="text" placeholder="Employee Class" readonly>
                                <span class="text-danger invalid class_err"></span>
                            </div>
                        </div>
                        <hr>

                        <div class="mb-3" id="status_div" style="display: none;">
                            <div class="row">
                                <h2>Add Employee Absent Attendance</h2>
                                <div class="col-md-4">
                                    <label class="col-form-label" for="attendance_type">Attendance Type<span class="text-danger">*</span></label>
                                    <select class="form-control" name="attendance_type" id="attendance_type">
                                        <option value="">Select Type</option>
                                        <option value="1">Present</option>
                                        <option value="2">Absent</option>
                                        <option value="3">Half Day</option>
                                    </select>
                                    <span class="text-danger invalid attendance_type_err"></span>
                                </div>
                                <input type="hidden" name="main_present_days" id="main_present_days">
                                <input type="hidden" id="original_present_days" name="original_present_days">

                                <div class="col-md-4">
                                    <label class="col-form-label" for="total_present_days">Total Present Days<span class="text-danger">*</span></label>
                                    <input class="form-control title" id="total_present_days" name="total_present_days" type="number" placeholder="Total Present Days" readonly>
                                    <span class="text-danger invalid total_present_days_err"></span>
                                </div>

                                <div class="col-md-4">
                                    <label class="col-form-label" for="total_leave">Total Leave<span class="text-danger">*</span></label>
                                    <input class="form-control title" id="total_leave" name="total_leave" type="number" placeholder="Total Leave" readonly>
                                    <span class="text-danger invalid total_leave_err"></span>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="col-form-label" for="present_days">Add Present Days<span class="text-danger">*</span></label>
                                    <input class="form-control title" id="present_days" name="present_days" type="number" placeholder="Enter Present Days" readonly>
                                    <span class="text-danger invalid present_days_err"></span>
                                </div>

                                <div class="col-md-4">
                                    <label class="col-form-label" for="total_absent_days">Add Absent Days<span class="text-danger">*</span></label>
                                    <input class="form-control title" id="total_absent_days" name="total_absent_days" type="number" placeholder="Enter Absent Days" readonly>
                                    <span class="text-danger invalid total_absent_days_err"></span>
                                </div>

                                <div class="col-md-4">
                                    <label class="col-form-label" for="total_half_days">Add Half Days<span class="text-danger">*</span></label>
                                    <input class="form-control title" id="total_half_days" name="total_half_days" type="number" placeholder="Enter Half Days" readonly>
                                    <span class="text-danger invalid total_half_days_err"></span>
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" id="editSubmit">Submit</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</x-admin.layout>


{{-- On change attendance type --}}
<script>
    $("#attendance_type").change(function(){
        var attendance_type = $(this).val();

        if(attendance_type == 1){
            $('#present_days').removeAttr('readonly');
            $('#total_absent_days').attr('readonly', true);
            $('#total_half_days').attr('readonly', true);
        } else if(attendance_type == 2){
            $('#total_absent_days').removeAttr('readonly');
            $('#present_days').attr('readonly', true);
            $('#total_half_days').attr('readonly', true);
        } else if(attendance_type == 3){
            $('#total_half_days').removeAttr('readonly');
            $('#total_absent_days').attr('readonly', true);
            $('#present_days').attr('readonly', true);
        }

    });


    // $("#present_days").change(function(){
    //     var main_present_days = parseFloat($('#main_present_days').val()) || 0;
    //     var present_days = parseFloat($('#present_days').val()) || 0;
    //     var total_present_days = parseFloat($('#total_present_days').val()) || 0;
    //     var total_leave = parseFloat($('#total_leave').val()) || 0;
    //     var calculated_present_days = total_present_days;
    //     calculated_present_days += present_days;

    //     if (calculated_present_days > main_present_days && total_leave == 0) {
    //         alert('The total present days cannot exceed the main present days.');
    //         $('#present_days').val(0);
    //     }else {
    //         if(total_leave != 0){
    //             var calculated_present_days2  = calculated_present_days + total_leave;

    //             if(calculated_present_days2 == main_present_days){
    //                 $('#total_absent_days').val(0);
    //                 $('#total_half_days').val(0);
    //             }

    //             if (calculated_present_days2 > main_present_days)
    //             {
    //                 alert('The total present days cannot exceed the main present days.');
    //                 $('#present_days').val(0);
    //             }else{
    //                 $('#total_present_days').val(calculated_present_days);
    //             }
    //         }else{
    //             $('#total_present_days').val(calculated_present_days);
    //         }
    //     }
    // });

    $("#present_days").change(function() {
        var main_present_days = parseFloat($('#main_present_days').val()) || 0;
        var present_days = parseFloat($('#present_days').val()) || 0;
        var total_present_days = parseFloat($('#total_present_days').val()) || 0;
        var total_leave = parseFloat($('#total_leave').val()) || 0;

        // Restrict negative values for present_days
        if (present_days < 0) {
            alert("Present days cannot be negative.");
            $('#present_days').val(0);
            present_days = 0;
        }

        // Calculate the new total present days
        var calculated_present_days = total_present_days + present_days;

        // Check if calculated present days exceed the main present days with no leave
        if (calculated_present_days > main_present_days && total_leave === 0) {
            alert('The total present days cannot exceed the main present days.');
            $('#present_days').val(0);
        } else {
            if (total_leave !== 0) {
                // Include leave in the calculation
                var calculated_present_days_with_leave = calculated_present_days + total_leave;

                // Reset values if present days and leave match main present days
                if (calculated_present_days_with_leave === main_present_days) {
                    $('#total_absent_days').val(0);
                    $('#total_half_days').val(0);
                }

                // Ensure that calculated present days including leave does not exceed main present days
                if (calculated_present_days_with_leave > main_present_days) {
                    alert('The total present days cannot exceed the main present days.');
                    $('#present_days').val(0);
                } else {
                    $('#total_present_days').val(calculated_present_days);

                    // Calculate absent days based on main present days
                    var absent_days = main_present_days - calculated_present_days_with_leave;
                    $('#total_absent_days').val(absent_days < 0 ? 0 : absent_days);
                }
            } else {
                $('#total_present_days').val(calculated_present_days);

                // Calculate absent days based on main present days
                var absent_days = main_present_days - calculated_present_days;
                $('#total_absent_days').val(absent_days < 0 ? 0 : absent_days);
            }
        }
    });



    // $("#total_absent_days").change(function() {
    //     var main_present_days = parseFloat($('#main_present_days').val()) || 0;
    //     var total_absent_days = parseFloat($('#total_absent_days').val()) || 0;
    //     var total_present_days = parseFloat($('#total_present_days').val()) || 0;
    //     var total_leave = parseFloat($('#total_leave').val()) || 0;
    //     var total_half_days = parseFloat($('#total_half_days').val()) || 0;

    //     // Restrict negative values for total_absent_days
    //     if (total_absent_days < 0) {
    //         alert("Absent days cannot be negative.");
    //         $('#total_absent_days').val(0);
    //         total_absent_days = 0;
    //     }

    //     // Ensure total_absent_days is not less than total_leave
    //     if (total_absent_days < total_leave) {
    //         alert("Absent days cannot be less than total leave.");
    //         $('#total_absent_days').val(total_leave);
    //         total_absent_days = total_leave;
    //     }else   if (total_absent_days > main_present_days) {
    //         alert('The total absent days cannot exceed the main present days.');
    //         $('#total_absent_days').val(main_present_days);
    //         total_absent_days = main_present_days;
    //     }

    //     // Calculate present days after accounting for absent days
    //     calculated_present_days = main_present_days - total_absent_days  - total_leave - (total_half_days * 0.5);;

    //     // Prevent negative present days
    //     if (calculated_present_days < 0) {
    //         alert('The calculated present days cannot be negative.');
    //         $('#total_absent_days').val(0);
    //         calculated_present_days = main_present_days;
    //     }

    //     // Update total present days
    //     $('#total_present_days').val(calculated_present_days);
    //     $('#present_days').val(0);
    // });



    // $("#total_absent_days").change(function(){
    //     var main_present_days = parseFloat($('#main_present_days').val()) || 0;
    //     var total_absent_days = parseFloat($('#total_absent_days').val()) || 0;
    //     var total_present_days = parseFloat($('#total_present_days').val()) || 0;
    //     var total_leave = parseFloat($('#total_leave').val()) || 0;

    //     if(total_present_days != 0)
    //     {
    //         var calculated_present_days = total_present_days;
    //     }else{
    //         var calculated_present_days = main_present_days;
    //     }

    //     if(total_absent_days >= main_present_days)
    //     {
    //         alert('The total absent days cannot exceed the main present days.');
    //         $('#total_absent_days').val(main_present_days);
    //     }else{

    //         calculated_present_days -= total_absent_days;
    //         if (calculated_present_days > main_present_days) {
    //             alert('The total present days cannot exceed the main present days.');
    //             $('#total_absent_days').val(0);
    //         } else {
    //             $('#total_present_days').val(calculated_present_days);
    //         }
    //     }

    // });

    // $("#total_half_days").change(function(){
    //     var main_present_days = parseFloat($('#main_present_days').val()) || 0;
    //     var total_half_days = parseFloat($('#total_half_days').val()) || 0;
    //     var total_present_days = parseFloat($('#total_present_days').val()) || 0;
    //     var total_leave = parseFloat($('#total_leave').val()) || 0;
    //     var calculated_present_days = total_present_days;
    //     calculated_present_days -= total_half_days * 0.5;
    //     if (calculated_present_days > main_present_days) {
    //         alert('The total present days cannot exceed the main present days.');
    //         $('#total_half_days').val(0);
    //     } else {
    //         $('#total_present_days').val(calculated_present_days);
    //     }
    // });


    // $("#total_half_days").change(function() {
    //     var main_present_days = parseFloat($('#main_present_days').val()) || 0;
    //     var total_half_days = parseFloat($('#total_half_days').val()) || 0;
    //     var total_leave = parseFloat($('#total_leave').val()) || 0;
    //     var original_present_days = parseFloat($('#original_present_days').val()) || 0;

    //     // Prevent negative half-day values
    //     if (total_half_days < 0) {
    //         alert("Half days cannot be negative.");
    //         $('#total_half_days').val(0);
    //         total_half_days = 0;
    //     }

    //     // Calculate the new total present days based on the original present days
    //     var calculated_present_days = original_present_days - (total_half_days * 0.5);

    //     // Ensure calculated present days do not fall below zero
    //     if (calculated_present_days < 0) {
    //         alert("The calculated present days cannot be negative.");
    //         $('#total_half_days').val(0);  // Reset half days if it causes negative present days
    //         calculated_present_days = original_present_days;  // Reset to original present days
    //     }

    //     // Update the total present days
    //     $('#total_present_days').val(calculated_present_days);
    // });



    function updateTotalPresentDays() {
    var main_present_days = parseFloat($('#main_present_days').val()) || 0;
    var total_absent_days = parseFloat($('#total_absent_days').val()) || 0;
    var total_half_days = parseFloat($('#total_half_days').val()) || 0;
    var total_leave = parseFloat($('#total_leave').val()) || 0;

    // Calculate the present days, adjusting for absent days and half days
    var calculated_present_days = main_present_days - total_absent_days - (total_half_days * 0.5);

    // Prevent negative present days
    if (calculated_present_days < 0) {
        alert("The calculated present days cannot be negative.");
        calculated_present_days = 0;
    }

    // Update the total present days field
    $('#total_present_days').val(calculated_present_days);
}

$('#total_absent_days').on('focusin', function(){
    $(this).data('total_absent_days', $(this).val());
});

$('#total_half_days').on('focusin', function(){
    $(this).data('total_half_days', $(this).val());
});
// Trigger recalculation whenever absent days or half days change
$("#total_absent_days, #total_half_days").change(function() {
    // Validate total_absent_days does not exceed main_present_days
    var main_present_days = parseFloat($('#main_present_days').val()) || 0;
    var total_absent_days = parseFloat($('#total_absent_days').val()) || 0;
    var total_leave = parseFloat($('#total_leave').val()) || 0;

    if (total_absent_days > main_present_days) {
        alert("The total absent days cannot exceed the main present days.");
        $('#total_absent_days').val(main_present_days);
        total_absent_days = main_present_days;
    }

    // Prevent negative half days
    var total_half_days = parseFloat($('#total_half_days').val()) || 0;
    if (total_half_days < 0) {
        alert("Half days cannot be negative.");
        $('#total_half_days').val(0);
        total_half_days = 0;
    }

    // Ensure half days do not exceed allowable days
    var max_half_days = (main_present_days - total_absent_days) * 2;
    if (total_half_days > max_half_days) {
        alert("The number of half days entered is too high.");
        $('#total_half_days').val(max_half_days);
        total_half_days = max_half_days;
    }

    // Ensure absent days not less than total leave.
    var half_days = total_half_days * 0.5;
    if (total_absent_days + half_days < total_leave) {
        alert("The total absent days should not be less than total leave.");

        if($(this).data('total_absent_days')){
            $('#total_absent_days').val($(this).data('total_absent_days'));
        }
        if($(this).data('total_half_days')){
            $('#total_half_days').val($(this).data('total_half_days'));
        }
        return false;
    }

    // Ensure that total absent days(absent + half day) should not be greater than total present days.
    var total_present_days = $('#total_present_days').val();
    if (total_absent_days + half_days > total_present_days) {
        alert("The total absent days should not be greater than total present days.");

        if($(this).data('total_absent_days')){
            $('#total_absent_days').val($(this).data('total_absent_days'));
        }
        if($(this).data('total_half_days')){
            $('#total_half_days').val($(this).data('total_half_days'));
        }
        return false;
    }

    // Recalculate present days based on current values
    updateTotalPresentDays();
});





</script>


{{-- Add --}}
<script>

$(document).ready(function() {
    $("#searchBtn").click(function(){
            var Emp_Code = $('#Emp_Code').val();
            var month = $('#month').val();
            if(Emp_Code != '' && month != '')
            {
                var url = "{{ route('fetch-attendance-details', [':Emp_Code', ':month']) }}";

                $.ajax({
                    url: url.replace(':Emp_Code', Emp_Code).replace(':month', month),
                    type: 'GET',
                    data: {
                        '_method': "GET",
                        '_token': "{{ csrf_token() }}"
                    },
                    success: function(data) {
                        if (!data.error && !data.error2) {
                            if (data.result === 1) {

                                var full_name = data.employee_details.fname + " " + data.employee_details.mname + " " + data.employee_details.lname;
                                $('#emp_name').val(full_name);
                                $('#ward').val(data.employee_details.ward.name);
                                $('#department').val(data.employee_details.department.name);
                                $('#class').val(data.employee_details.class.name);
                                $('#designation').val(data.employee_details.designation.name);
                                $('#employee_id').val(data.employee_details.id);
                                $('#total_present_days').val(data.attendance_details.total_present_days);
                                $('#original_present_days').val(data.attendance_details.total_present_days);

                                $('#total_leave').val(data.attendance_details.total_leave);
                                $('#total_absent_days').val(data.attendance_details.total_absent_days);
                                $('#total_half_days').val(data.attendance_details.total_half_days);
                                $('#main_present_days').val(data.attendance_details.main_present_days);
                                $('#from_date').val(data.attendance_details.from_date);
                                $('#to_date').val(data.attendance_details.to_date);
                                $('#edit_model_id').val(data.attendance_details.id);


                                $('#status_div').removeAttr('style');

                            } else if (data.result === 0) {
                                $('#status_div').hide();
                                $('#total_present_days').val('');
                                $('#present_days').val(0);
                                $('#total_absent_days').val(0);
                                $('#total_half_days').val(0);
                                $('#total_leave').val(0);

                                alert("Employee details not found or Employee Status already added!");
                            } else {
                                alert("Unexpected result from the server");
                            }
                    }
                    },
                    error: function(error, jqXHR, textStatus, errorThrown) {
                        swal("Error!", "Something went wrong", "error");
                    },
                });
            }
            else{
                alert('Please Enter Employee Id && select Month');
            }

        });
    });


    $(document).ready(function() {
        $("#editForm").submit(function(e) {
            e.preventDefault();
            $("#editSubmit").prop('disabled', true);
            var formdata = new FormData(this);
            formdata.append('_method', 'PUT');
            var model_id = $('#edit_model_id').val();
            var url = "{{ route('add-absent.update', ':model_id') }}";
            //
            $.ajax({
                url: url.replace(':model_id', model_id),
                type: 'POST',
                data: formdata,
                contentType: false,
                processData: false,
                success: function(data) {
                    $("#editSubmit").prop('disabled', false);
                    if (!data.error2)
                        swal("Successful!", data.success, "success")
                        .then((action) => {
                            window.location.href = '{{ route('add-absent.index') }}';
                        });
                    else
                        swal("Error!", data.error2, "error");
                },
                statusCode: {
                    422: function(responseObject, textStatus, jqXHR) {
                        $("#editSubmit").prop('disabled', false);
                        resetErrors();
                        printErrMsg(responseObject.responseJSON.errors);
                    },
                    500: function(responseObject, textStatus, errorThrown) {
                        $("#editSubmit").prop('disabled', false);
                        swal("Error occured!", "Something went wrong please try again", "error");
                    }
                }
            });

        });
    });
</script>
