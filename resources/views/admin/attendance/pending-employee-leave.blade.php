<x-admin.layout>
    <x-slot name="title">Pending Employee Leave</x-slot>
    <x-slot name="heading">Pending Employee Leave</x-slot>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>Employee Id</th>
                                    <th>Leave</th>
                                    <th>From Date</th>
                                    <th>To Date</th>
                                    <th>Rejoin Date</th>
                                    <th>No. of Days</th>
                                    <th>Remark</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($employee_leaves as $employee_leave)
                                        @if ($employee_leave->status == 1)
                                            @php $status = 'Approved'; @endphp
                                        @elseif ($employee_leave->status == 2)
                                            @php $status = 'Rejected'; @endphp
                                        @else
                                            @php $status = 'Pending'; @endphp
                                        @endif

                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $employee_leave?->Emp_Code }}</td>
                                        <td>{{ $employee_leave?->leaveType?->name }}</td>
                                        <td>{{ $employee_leave?->from_date }}</td>
                                        <td>{{ $employee_leave?->to_date }}</td>
                                        <td>{{ $employee_leave?->rejoin_date }}</td>
                                        <td>{{ $employee_leave?->no_of_days }}</td>
                                        <td>{{ $employee_leave?->remark }}</td>
                                        <td>{{ $status }}</td>
                                        <td>
                                            @can('leave-approval.approve')
                                                <button class="edit-element btn btn-secondary px-2 py-1" title="Approve Leave" data-id="{{ $employee_leave->id }}"><i data-feather="check"></i></button>
                                            @endcan
                                            @can('leave-approval.reject')
                                                <button class="btn btn-danger rem-element px-2 py-1" title="Reject Leave" data-id="{{ $employee_leave->id }}"><i data-feather="x"></i> </button>
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="zoomInModal" class="modal fade zoomIn" tabindex="-1" aria-labelledby="zoomInModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="zoomInModalLabel">Reject Remark</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form method="POST" id="editForm">
                        @csrf
                        <div class="mb-3 row">
                            <input type="hidden" name="edit_model_id" id="edit_model_id">
                            <div class="col-md-12 mt-2">
                                <label class="col-form-label" for="remark">Remark<span class="text-danger">*</span></label>
                                <textarea class="form-control" name="remark" id="remark"></textarea>
                                <span class="text-danger invalid Emp_Code_err"></span>
                            </div>

                        </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" data-bs-dismiss="modal">Submit</button>
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

</x-admin.layout>

<!-- Delete -->
<script>

    $("#buttons-datatables").on("click", ".edit-element", function(e) {
        e.preventDefault();
        swal({
                title: "Are you sure to approve this Leave?",
                icon: "info",
                buttons: ["Cancel", "Confirm"]
            })
            .then((justTransfer) => {
                if (justTransfer) {

                    var model_id = $(this).attr("data-id");
                    var url = "{{ route('approve-leave', ':model_id') }}";

                    $.ajax({
                        url: url.replace(':model_id', model_id),
                        type: 'GET',
                        success: function(data, textStatus, jqXHR) {
                            if (!data.error && !data.error2) {
                                swal("Success!", data.success, "success")
                                    .then((action) => {
                                        window.location.reload();
                                    });
                            } else {
                                if (data.error) {
                                    swal("Error!", data.error, "error");
                                } else {
                                    swal("Error!", data.error2, "error");
                                }
                            }
                        },
                        error: function(error, jqXHR, textStatus, errorThrown) {
                            swal("Error!", "Something went wrong", "error");
                        },
                    });
                }
            });
    });

    $("#buttons-datatables").on("click", ".rem-element", function(e) {
        e.preventDefault();
        swal({
                title: "Are you sure to reject this Leave?",
                icon: "info",
                buttons: ["Cancel", "Confirm"]
            })
            .then((justTransfer) => {
                if (justTransfer) {

                    $('#zoomInModal').modal('show');
                    $('#edit_model_id').val($(this).attr("data-id"));
                }
            });
    });

    $(document).ready(function() {
        $("#editForm").submit(function(e) {
            e.preventDefault();
            $("#editSubmit").prop('disabled', true);
            var formdata = new FormData(this);
            formdata.append('_method', 'PUT');
            var model_id = $('#edit_model_id').val();
            var url = "{{ route('leave-approval.update', ':model_id') }}";
            //
            $.ajax({
                url: url.replace(':model_id', model_id),
                type: 'POST',
                data: formdata,
                contentType: false,
                processData: false,
                success: function(data) {
                    $("#editSubmit").prop('disabled', false);
                    if (!data.error2)
                        swal("Successful!", data.success, "success")
                        .then((action) => {
                            window.location.href = '{{ route('leave-approval.index') }}';
                        });
                    else
                        swal("Error!", data.error2, "error");
                },
                statusCode: {
                    422: function(responseObject, textStatus, jqXHR) {
                        $("#editSubmit").prop('disabled', false);
                        resetErrors();
                        printErrMsg(responseObject.responseJSON.errors);
                    },
                    500: function(responseObject, textStatus, errorThrown) {
                        $("#editSubmit").prop('disabled', false);
                        swal("Error occured!", "Something went wrong please try again", "error");
                    }
                }
            });

        });
    });

</script>

