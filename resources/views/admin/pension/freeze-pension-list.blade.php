<x-admin.layout>
    <x-slot name="title">Freeze Pension</x-slot>
    <x-slot name="heading">Freeze Pension</x-slot>

    <!-- Add Form -->
    <div class="row" id="addContainer" style="display:none;">
        <div class="col-sm-12">
            <div class="card">
                <header class="card-header">
                    <h4 class="card-title">Add Freeze Pension</h4>
                </header>
                <form class="theme-form" name="addForm" id="addForm" enctype="multipart/form-data">
                    @csrf

                    <div class="card-body">
                        <div class="mb-3 row">

                            <div class="col-md-4">
                                <label class="col-form-label" for="month">Select Month<span class="text-danger">*</span></label>
                                <select class="form-control" id="month" name="month">
                                    <option value="">Select Month</option>
                                    <option value="1">January</option>
                                    <option value="2">February</option>
                                    <option value="3">March</option>
                                    <option value="4">April</option>
                                    <option value="5">May</option>
                                    <option value="6">June</option>
                                    <option value="7">July</option>
                                    <option value="8">August</option>
                                    <option value="9">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                </select>
                                <span class="text-danger invalid month_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="from_date">From Date<span class="text-danger">*</span></label>
                                <input class="form-control title" id="from_date" name="from_date" type="date" placeholder="From Date" readonly>
                                <span class="text-danger invalid from_date_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="to_date">To Date<span class="text-danger">*</span></label>
                                <input class="form-control title" id="to_date" name="to_date" type="date" placeholder="To Date" readonly>
                                <span class="text-danger invalid to_date_err"></span>
                            </div>

                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" id="addSubmit">Freeze</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                @can('freeze-pension.create')
                    <div class="card-header">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="">
                                    <button id="addToTable" class="btn btn-primary">Add <i class="fa fa-plus"></i></button>
                                    <button id="btnCancel" class="btn btn-danger" style="display:none;">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endcan
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>From Date</th>
                                    <th>To Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($freeze_pensions as $freeze_pension)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $freeze_pension->from_date }}</td>
                                        <td>{{ $freeze_pension->to_date }}</td>
                                        <td>
                                            @can('freeze-pension.show')
                                            <a href="{{ route('pensioner-bill-pdf', ['month' => $freeze_pension->month,'from_date' => $freeze_pension->from_date,'to_date' => $freeze_pension->to_date]) }}">
                                                <button class="btn btn-secondary px-2 py-1" title="Show Salary">
                                                    <i data-feather="eye"></i>
                                                </button>
                                            </a>
                                            @endcan

                                            @can('freeze-pension.delete')
                                            <button class="edit-element btn btn-danger px-2 py-1" title="Delete" data-id="{{ $freeze_pension->from_date }}"><i data-feather="trash-2"></i></button>
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




</x-admin.layout>

<script>
// On change ward fetch departments
$("#month").on("change", function (e) {
    var month = this.value;
    var url = "{{ route('fetch-date-range', ':month') }}";

    $.ajax({
        url: url.replace(":month", month),
        type: "GET",
        data: {
            _method: "GET",
            _token: "{{ csrf_token() }}",
        },
        success: function (data, textStatus, jqXHR) {
            if (!data.error && !data.error2) {
                $("#from_date").val(data.fromDate);
                $("#to_date").val(data.toDate);
            } else {
                alert(data.error);
            }
        },
        error: function (error, jqXHR, textStatus, errorThrown) {
            swal("Error!", "Something went wrong", "error");
        },
    });
});
</script>

{{-- Add --}}
<script>
    $("#addForm").submit(function(e) {
        e.preventDefault();
        $("#addSubmit").prop('disabled', true);

        var formdata = new FormData(this);
        $.ajax({
            url: '{{ route('freeze-pension.store') }}',
            type: 'POST',
            data: formdata,
            contentType: false,
            processData: false,
            success: function(data) {
                $("#addSubmit").prop('disabled', false);
                if (!data.error) {
                    swal("Successful!", data.success, "success")
                    .then((action) => {
                        window.location.href = '{{ route('freeze-pension.index') }}';
                    });
                } else {
                    swal("Error!", data.error, "error");
                }
            },
            error: function(xhr, textStatus, errorThrown) {
                $("#addSubmit").prop('disabled', false);
                if (xhr.status == 422) {
                    var errors = xhr.responseJSON.errors;
                    resetErrors();
                    printErrMsg(errors);
                } else {
                    swal("Error occurred!", "Something went wrong. Please try again later.", "error");
                }
            }
        });

    });
</script>


<!-- Edit -->
<script>
    $("#buttons-datatables").on("click", ".edit-element", function(e) {
        e.preventDefault();
        swal({
                title: "Are you sure to delete this Pension?",
                icon: "info",
                buttons: ["Cancel", "Confirm"]
            })
        .then((justTransfer) => {
        var model_id = $(this).attr("data-id");
        var url = "{{ route('freeze-pension.edit', ':model_id') }}";

        $.ajax({
            url: url.replace(':model_id', model_id),
            type: 'GET',
            data: {
                '_token': "{{ csrf_token() }}"
            },
            success: function(data, textStatus, jqXHR) {
                editFormBehaviour();
                if (!data.error) {

                    if (!data.error2)
                        swal("Successful!", data.success, "success")
                        .then((action) => {
                            window.location.href = '{{ route('freeze-pension.index') }}';
                        });
                    else
                        swal("Error!", data.error2, "error");

                } else {
                    alert(data.error);
                }
            },
            error: function(error, jqXHR, textStatus, errorThrown) {
                alert("Some thing went wrong");
            },
        });
    });
});
</script>

