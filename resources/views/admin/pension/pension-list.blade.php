<x-admin.layout>
    <x-slot name="title">Pension Details List</x-slot>
    <x-slot name="heading">Pension Details List</x-slot>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                @can('pension.create')
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="">
                                <a href="{{ route('pension.create') }}" id="addToTable1" class="btn btn-primary">Add <i class="fa fa-plus"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                @endcan
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>Pension No.</th>
                                    <th>Employee Name</th>
                                    <th>Designation</th>
                                    <th>Basic</th>
                                    <th>Payable Pension</th>
                                    <th>DA</th>
                                    <th>Deduct Amount</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($pensions as $pension)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $pension?->pension_id }}</td>
                                        <td>{{ $pension?->emp_name }}</td>
                                        <td>{{ $pension?->designation }}</td>
                                        <td>{{ $pension?->basic_salary }}</td>
                                        <td>{{ $pension?->payable_pension }}</td>
                                        <td>{{ $pension?->da }}</td>
                                        <td>{{ $pension?->deduct_amt }}</td>
                                        <td>
                                            @can('pension.edit')
                                                <a href="{{ route('pension.edit',$pension->id) }}" class="edit-element btn btn-secondary px-2 py-1" title="Edit Pension"><i data-feather="edit"></i></a>
                                            @endcan
                                            @can('pension.delete')
                                                <button class="btn btn-danger rem-element px-2 py-1" title="Delete Pension" data-id="{{ $pension->id }}"><i data-feather="trash-2"></i> </button>
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>



</x-admin.layout>

<!-- Delete -->
<script>
    $("#buttons-datatables").on("click", ".rem-element", function(e) {
        e.preventDefault();
        swal({
                title: "Are you sure to delete this Pension?",
                icon: "info",
                buttons: ["Cancel", "Confirm"]
            })
            .then((justTransfer) => {
                if (justTransfer) {
                    var model_id = $(this).attr("data-id");
                    var url = "{{ route('pension.destroy', ':model_id') }}";

                    $.ajax({
                        url: url.replace(':model_id', model_id),
                        type: 'POST',
                        data: {
                            '_method': "DELETE",
                            '_token': "{{ csrf_token() }}"
                        },
                        success: function(data, textStatus, jqXHR) {
                            if (!data.error && !data.error2) {
                                swal("Success!", data.success, "success")
                                    .then((action) => {
                                        window.location.reload();
                                    });
                            } else {
                                if (data.error) {
                                    swal("Error!", data.error, "error");
                                } else {
                                    swal("Error!", data.error2, "error");
                                }
                            }
                        },
                        error: function(error, jqXHR, textStatus, errorThrown) {
                            swal("Error!", "Something went wrong", "error");
                        },
                    });
                }
            });
    });

</script>
