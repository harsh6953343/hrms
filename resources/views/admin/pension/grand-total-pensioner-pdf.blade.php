@php
  use Carbon\Carbon;
  use App\Models\FreezePension;
@endphp
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pensioner Bill</title>
    <style>

        body {
                font-family: "Source Sans 3", Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;
                -webkit-font-smoothing: antialiased;
                font-size: 14px;
            }

        @media print {
            body {
                -webkit-print-color-adjust: exact;
            }
        }

        .section-heading {
                text-align: center;
                margin: 10px 0 5px;
                font-size: 24px;
            }



        table, th, td {
            font-size:12px;
        border: 1px solid rgb(177, 173, 173);
        border-collapse: collapse;

        }
        table, th
        {
            font-weight:5px;
            font-size:18px;
            padding: 20px;
            text-align: left;
            background-color: #feebd1;

        }

        table,td
        {
            font-size: 18px;
            background-color: #dde2ee;
            padding: 20px;
        }

        .table-footer{
            background-color: lightgray!important;
        }

        .signature{
            background-color: white;
            border: none;
            padding: 5px; /* Add padding for spacing */
            text-align: center;
            font-weight: bold;
        }

        .page-break {
                page-break-after: always;
            }

    </style>
</head>
<body>


    <table style="width: 100%; border:none;">
        <thead>
            <tr>
                <td style="background-color:white; border:none; text-align:right;">
                    @if(isset($base64Logo))
                    <img src="data:image/png;base64,{{ $base64Logo }}" alt="Corporation Logo" height="80" width="90">
                    @endif
                </td>
                <td style="background-color:white; border:none;">
                    <h2 class="section-heading">{{ $corporation->name }}</h2>
                    @if(!empty($to_date))
                    <h5 class="section-heading">
                        As per the 7th Pay Commission of
                        {{ Carbon::parse($to_date)->format('F Y') }} Grand Total
                    </h5>
                    @endif
                </td>
            </tr>
        </thead>
    </table>

    @php
        $grandTotal = [];
        $final_total = 0;
        $final_differance = 0;
        $final_deduction = 0;
        $final_net_salary = 0;

    @endphp
    @foreach ($banks as $bank)
        @php
            $authUser = Auth::user();

            $freezePensions =    FreezePension::with('employee')
                                        ->when($authUser->hasRole('Ward HOD'), function ($query) use ($authUser) {
                                                $query->whereHas('employee', function ($employeeQuery) use ($authUser) {
                                                    $employeeQuery->where('ward_id', $authUser->ward_id);
                                                });
                                            })
                                        ->when($authUser->id == 1, function ($query) use ($authUser) {
                                            $query->whereHas('employee', function ($employeeQuery) use ($authUser) {
                                                $employeeQuery->whereNot('ward_id', 84);
                                            });
                                        })
                                        ->where('month', $month)
                                        ->where('pension_bank_id', $bank->id)
                                        ->get();

            $grand_total            = 0;
            $grand_total_deduction  = 0;
            $grand_total_differance = 0;
            $grand_total_net_salary = 0;

        @endphp

        @foreach ($freezePensions as $freezePension)

            @php
                $grand_total            += ($freezePension->payable_pension + $freezePension->deduction);
                $grand_total_deduction  += $freezePension->deduction;
                $grand_total_differance += $freezePension->differance;
                $grand_total_net_salary += $freezePension->payable_pension;
            @endphp

        @endforeach
        @php
            $grandTotal[] = [
                                'bank_name'         =>  $bank->name,
                                'total'             =>  $grand_total,
                                'differance'        =>  $grand_total_differance,
                                'audit_deduction'   =>  $grand_total_deduction,
                                'net_salary'        =>  $grand_total_net_salary
            ];
        @endphp
    @endforeach

    <table style="width: 100%; margin-top:2%;">
        <thead>
            <tr>
                <th class="th">Sr.No</th>
                <th class="th">Bank Name</th>
                <th class="th">Total</th>
                <th class="th">Differance	</th>
                <th class="th">Audit / Other Deduction</th>
                <th class="th">Net Salary</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($grandTotal as $val)
                @php
                    $final_total+= $val['total'];
                    $final_differance+= $val['differance'];
                    $final_deduction+= $val['audit_deduction'];
                    $final_net_salary+= $val['net_salary'];
                @endphp
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $val['bank_name'] }}</td>
                    <td>{{ $val['total'] }}</td>
                    <td>{{ $val['differance'] }}</td>
                    <td>{{ $val['audit_deduction'] }}</td>
                    <td>{{ $val['net_salary'] }}</td>
                </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th colspan="2">Grand Total</th>
                <th>{{ $final_total }}</th>
                <th>{{ $final_differance }}</th>
                <th>{{ $final_deduction }}</th>
                <th>{{ $final_net_salary }}</th>
            </tr>
        </tfoot>
    </table>

    {{-- Signature part --}}

    <table style="width: 100%; margin-top:5%; border:none;">


        <tr>
            <td class="signature">________________</td>
            <td class="signature">____________________</td>
            <td class="signature">____________________________</td>
            <td class="signature">__________________</td>
            <td class="signature">____________________________</td>
            <td class="signature">__________________</td>
        </tr>

        <tr>
            <td class="signature">PF. Cleark</td>
            <td class="signature">PF Department Head</td>
            <td class="signature">ASST. Commissioner(Gen.Admin)</td>
            <td class="signature">Cheif Auditor</td>
            <td class="signature">Cheif Accountant & Finance Officer</td>
            <td class="signature">DY.Commissioner(HQ)</td>
        </tr>

    </table>

    <br><br>
    <h3 style="margin: 10px;">Report Generated on {{ Carbon::now() }}</h3>

</body>
</html>
