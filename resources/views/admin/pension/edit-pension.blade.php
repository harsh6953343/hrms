<x-admin.layout>
    <x-slot name="title">Pension Details List</x-slot>
    <x-slot name="heading">Pension Details List</x-slot>

    <!-- Add Form -->
    <div class="row" id="addContainer">
        <div class="col-sm-12">
            <div class="card">
                <header class="card-header">
                    <h4 class="card-title">Edit Pension</h4>
                </header>
                <form class="theme-form" name="editForm" id="editForm" enctype="multipart/form-data">
                    @csrf

                    <input type="hidden" id="edit_model_id" name="edit_model_id" value="{{ $pension->id }}">
                    <input type="hidden" id="DA_rate" value="{{ $da_rate->amount }}">

                    <div class="card-body">
                        <div class="mb-3 row">

                            <div class="col-md-4">
                                <label class="col-form-label" for="emp_retire_after_2016">Is Employee Retire After 2016 ?<span class="text-danger">*</span></label>
                                <select class="form-control" id="emp_retire_after_2016" name="emp_retire_after_2016">
                                    <option value="">Select  Is Employee Retire After 2016 ? </option>
                                    <option value="1" {{ ($pension->emp_retire_after_2016 == '1')? 'Selected':'' }} >Yes</option>
                                    <option value="2" {{ ($pension->emp_retire_after_2016 == '2')? 'Selected':'' }}>No</option>
                                </select>
                                <span class="text-danger invalid emp_retire_after_2016_err"></span>
                            </div>

                            {{-- <div class="col-md-4">
                                <label class="col-form-label" for="is_emp_dr">Is Employee Doctor ?<span class="text-danger">*</span></label>
                                <select class="form-control" id="is_emp_dr" name="is_emp_dr">
                                    <option value="">Select Is Employee Doctor ?</option>
                                    <option value="1" {{ ($pension->is_emp_dr == '1')? 'Selected':'' }} >Yes</option>
                                    <option value="2" {{ ($pension->is_emp_dr == '2')? 'Selected':'' }} >No</option>
                                </select>
                                <span class="text-danger invalid is_emp_dr_err"></span>
                            </div> --}}

                        </div>
                        <div class="mb-3 row">
                            <div class="col-md-4">
                                <label class="col-form-label" for="emp_name">Employee Name<span class="text-danger">*</span></label>
                                <input class="form-control" id="emp_name" name="emp_name" type="text" placeholder="Enter Employee Name" value="{{ $pension->emp_name }}">
                                <span class="text-danger invalid emp_name_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="designation">Designation<span class="text-danger">*</span></label>
                                <input class="form-control" id="designation" name="designation" type="text" placeholder="Enter Designation" value="{{ $pension->designation }}">
                                <span class="text-danger invalid designation_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="pay_type">Select Pay Type<span class="text-danger"></span></label>
                                <select class="form-control pay_type" id="pay_type" name="pay_type">
                                    <option value="">Select Pay Type</option>
                                    <option value="1" {{ ($pension->pay_type == '1')? 'Selected':'' }} >6 Pay</option>
                                    <option value="2" {{ ($pension->pay_type == '2')? 'Selected':'' }} >7 Pay</option>
                                </select>
                                <span class="text-danger invalid pay_type_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="basic_salary">Basic Salary <span class="text-danger">*</span></label>
                                <input class="form-control" id="basic_salary" name="basic_salary" type="number" placeholder="Enter basic_salary" value="{{ $pension->basic_salary }}">
                                <span class="text-danger invalid basic_salary_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="dob">Date of Birth<span class="text-danger">*</span></label>
                                <input class="form-control" id="dob" name="dob" type="date" placeholder="Enter Date of Birth" value="{{ $pension->dob }}">
                                <span class="text-danger invalid dob_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="age">Age<span class="text-danger">*</span></label>
                                <input class="form-control" id="age" name="age" type="text" placeholder="Enter Age" readonly value="{{ $pension->age }}">
                                <span class="text-danger invalid age_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="retirement_date">Retirement Date<span class="text-danger">*</span></label>
                                <input class="form-control" id="retirement_date" name="retirement_date" type="date" placeholder="Enter Retirement Date" value="{{ $pension->retirement_date }}">
                                <span class="text-danger invalid retirement_date_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="date_of_death">Date of Death (If Applicable)<span class="text-danger"></span></label>
                                <input class="form-control" id="date_of_death" name="date_of_death" type="date" placeholder="Enter Date of Death (If Applicable)" value="{{ $pension->date_of_death }}">
                                <span class="text-danger invalid date_of_death_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="account_number">Account Number<span class="text-danger">*</span></label>
                                <input class="form-control" id="account_number" name="account_number" type="text" placeholder="Enter Account Number" value="{{ $pension->account_number }}">
                                <span class="text-danger invalid account_number_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="pension_bank_id">Select Bank<span class="text-danger">*</span></label>
                                <select class="form-control pension_bank_id" id="pension_bank_id" name="pension_bank_id">
                                    <option value="">Select Bank</option>
                                    @foreach ($pension_banks as $pension_bank)
                                        <option value="{{ $pension_bank->id }}" {{ ($pension->pension_bank_id == $pension_bank->id)? 'Selected':'' }} >{{ $pension_bank->name }}</option>
                                    @endforeach
                                </select>
                                <span class="text-danger invalid pension_bank_id_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="branch_name">Branch Name<span class="text-danger">*</span></label>
                                <input class="form-control" id="branch_name" name="branch_name" type="text" placeholder="Enter Branch Name" value="{{ $pension->branch_name }}">
                                <span class="text-danger invalid branch_name_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="ifsc_code">IFSC Code<span class="text-danger">*</span></label>
                                <input class="form-control" id="ifsc_code" name="ifsc_code" type="text" placeholder="Enter IFSC Code" value="{{ $pension->ifsc_code }}">
                                <span class="text-danger invalid ifsc_code_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="differance">Differance<span class="text-danger"></span></label>
                                <input class="form-control" id="differance" name="differance" type="number" placeholder="Enter Differance" value="{{ $pension->differance }}">
                                <span class="text-danger invalid differance_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="deduction">Deduction<span class="text-danger"></span></label>
                                <input class="form-control" id="deduction" name="deduction" type="number" placeholder="Enter Deduction" value="{{ $pension->deduction }}">
                                <span class="text-danger invalid deduction_err"></span>
                            </div>

                            {{-- <div class="col-md-4">
                                <label class="col-form-label" for="business_allowances">Dr. Business Allowances<span class="text-danger">*</span></label>
                                <input class="form-control" id="business_allowances" name="business_allowances" type="number" placeholder="Enter Dr. Business Allowances" value="{{ $pension->business_allowances }}">
                                <span class="text-danger invalid business_allowances_err"></span>
                            </div> --}}

                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-4">
                                <label class="col-form-label" for="calculated_basic">Calculated Basic<span class="text-danger">*</span></label>
                                <input class="form-control" id="calculated_basic" name="calculated_basic" type="number" placeholder="Calculated Basic" readonly value="{{ $pension->calculated_basic }}">
                                <span class="text-danger invalid calculated_basic_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="payable_pension">Payable Pension Amount<span class="text-danger">*</span></label>
                                <input class="form-control" id="payable_pension" name="payable_pension" type="number" placeholder="Payable Pension Amount" readonly value="{{ $pension->payable_pension }}">
                                <span class="text-danger invalid payable_pension_err"></span>
                            </div>

                            <div class="col-md-4">
                                <input type="hidden" id="main_da" name="main_da" value="{{ $pension->da }}">
                                <label class="col-form-label" for="da">DA<span class="text-danger">*</span></label>
                                <input class="form-control" id="da" name="da" type="number" placeholder="Dearness Allowance" readonly value="{{ $pension->da }}">
                                <span class="text-danger invalid da_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="da_applicabe">Is DA Applicable ? (Yes/No)<span class="text-danger">*</span></label>
                                <select class="form-control da_applicabe" id="da_applicabe" name="da_applicabe">
                                    <option value="1" {{ ($pension->da_applicabe == '1')? 'Selected':'' }} >Yes</option>
                                    <option value="2" {{ ($pension->da_applicabe == '2')? 'Selected':'' }} >No</option>
                                </select>
                                <span class="text-danger invalid da_applicabe_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="sell_computation">Sell Computation (Yes/No)<span class="text-danger">*</span></label>
                                <select class="form-control sell_computation" id="sell_computation" name="sell_computation">
                                    <option value="">Select Type</option>
                                    <option value="1" {{ ($pension->sell_computation == '1')? 'Selected':'' }} >Yes</option>
                                    <option value="2" {{ ($pension->sell_computation == '2')? 'Selected':'' }} >No</option>
                                </select>
                                <span class="text-danger invalid sell_computation_err"></span>
                            </div>

                            <div class="col-md-4" id="deduct_amt_div" style="{{ ($pension->sell_computation != 1)? 'display: none;':'' }}">
                                <label class="col-form-label" for="deduct_amt">Enter Amount For Deduct<span class="text-danger">*</span></label>
                                <input class="form-control" id="deduct_amt" name="deduct_amt" type="number" placeholder="Enter Amount For Deduct" value="{{ $pension->deduct_amt }}">
                                <span class="text-danger invalid deduct_amt_err"></span>
                            </div>

                            <div class="col-md-4" id="sell_date_div" style="{{ ($pension->sell_computation != 1)? 'display: none;':'' }}">
                                <label class="col-form-label" for="sell_date">Sell Date<span class="text-danger"></span></label>
                                <input class="form-control" id="sell_date" name="sell_date" type="date" placeholder="Enter Sell Date" value="{{ $pension->sell_date }}">
                                <span class="text-danger invalid sell_date_err"></span>
                            </div>

                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" id="addSubmit">Submit</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                    </div>
                </form>
            </div>
        </div>
    </div>



</x-admin.layout>
<script src="{{ asset('admin/js/pension-calculation.js') }}" defer></script>

<!-- Update -->
<script>
    $(document).ready(function() {
        $("#editForm").submit(function(e) {
            e.preventDefault();
            $("#editSubmit").prop('disabled', true);
            var formdata = new FormData(this);
            formdata.append('_method', 'PUT');
            var model_id = $('#edit_model_id').val();
            var url = "{{ route('pension.update', ':model_id') }}";
            //
            $.ajax({
                url: url.replace(':model_id', model_id),
                type: 'POST',
                data: formdata,
                contentType: false,
                processData: false,
                success: function(data) {
                    $("#editSubmit").prop('disabled', false);
                    if (!data.error2)
                        swal("Successful!", data.success, "success")
                        .then((action) => {
                            window.location.href = '{{ route('pension.index') }}';
                        });
                    else
                        swal("Error!", data.error2, "error");
                },
                statusCode: {
                    422: function(responseObject, textStatus, jqXHR) {
                        $("#editSubmit").prop('disabled', false);
                        resetErrors();
                        printErrMsg(responseObject.responseJSON.errors);
                    },
                    500: function(responseObject, textStatus, errorThrown) {
                        $("#editSubmit").prop('disabled', false);
                        swal("Error occured!", "Something went wrong please try again", "error");
                    }
                }
            });

        });
    });
</script>
