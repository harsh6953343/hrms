@php
  use Carbon\Carbon;
  use App\Models\FreezePension;
@endphp
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pensioner Bill</title>
    <style>

        body {
                font-family: "Source Sans 3", Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;
                -webkit-font-smoothing: antialiased;
                font-size: 14px;
            }

        @media print {
            body {
                -webkit-print-color-adjust: exact;
            }
        }

        .section-heading {
                text-align: center;
                margin: 10px 0 5px;
                font-size: 24px;
            }



        table, th, td {
            font-size:12px;
        border: 1px solid rgb(177, 173, 173);
        border-collapse: collapse;

        }
        table, th
        {
            font-weight:5px;
            font-size:15px;
            padding: 6px;
            text-align: left;
            background-color: #feebd1;

        }

        table,td
        {
            font-size: 16px;
            background-color: #dde2ee;
            padding: 6px;
        }

        .table-footer{
            background-color: lightgray!important;
        }

        .signature{
            background-color: white;
            border: none;
            padding: 5px; /* Add padding for spacing */
            text-align: center;
            font-weight: bold;
        }

        .page-break {
                page-break-after: always;
            }

    </style>
</head>
<body>


    <table style="width: 100%; border:none;">
        <thead>
            <tr>
                <td style="background-color:white; border:none; text-align:right;">
                    @if(isset($base64Logo))
                    <img src="data:image/png;base64,{{ $base64Logo }}" alt="Corporation Logo" height="80" width="90">
                    @endif
                </td>
                <td style="background-color:white; border:none;">
                    <h2 class="section-heading">{{ $corporation->name }}</h2>
                    @if(!empty($to_date))
                    <h5 class="section-heading">
                        Pensioner Bill - All for the month of {{ Carbon::parse($to_date)->format('F Y') }}</h5>
                    @endif
                </td>
            </tr>
        </thead>
    </table>

    @php
        $grandTotal = [];

    @endphp
    @foreach ($banks as $bank)
        <h2 style="text-align: center; margin-top:60px;">As per the 7th Pay Commission of
            {{ Carbon::parse($to_date)->format('F Y') }}
            for Retired and Retired Family Holders({{ $bank->name }})
        </h2>
        <table style="width: 100%; margin-top:2%;">
            <thead>
                <tr>
                    <th class="th">Sr.No</th>
                    <th class="th">Pension Unique No.</th>
                    <th class="th">Name</th>
                    <th class="th">Retirement / Death Date	</th>
                    <th class="th">Date_of_Birth</th>
                    <th class="th">Branch</th>
                    <th class="th">IFSC Code</th>
                    <th class="th">Account Number	</th>
                    <th class="th">Basic	</th>
                    <th class="th">7 pay payable pension amount(A)</th>
                    <th class="th">Sell amount (B)</th>
                    <th class="th">(A-B)</th>
                    <th class="th">DA</th>
                    {{-- <th class="th">Dr. Business Allowances	</th> --}}
                    <th class="th">Total</th>
                    <th class="th">Deduction</th>
                    <th class="th">Differance</th>
                    <th class="th">Net Salary</th>
                </tr>
            </thead>

            <tbody>
                @php

                    $authUser = Auth::user();

                    $freezePensions =    FreezePension::with('employee')
                                                ->when($authUser->hasRole('Ward HOD'), function ($query) use ($authUser) {
                                                        $query->whereHas('employee', function ($employeeQuery) use ($authUser) {
                                                            $employeeQuery->where('ward_id', $authUser->ward_id);
                                                        });
                                                    })
                                                ->when($authUser->id == 1, function ($query) use ($authUser) {
                                                    $query->whereHas('employee', function ($employeeQuery) use ($authUser) {
                                                        $employeeQuery->whereNot('ward_id', 84);
                                                    });
                                                })
                                                ->where('month', $month)
                                                ->where('pension_bank_id', $bank->id)
                                                ->get();
                    $grand_basic            = 0;
                    $grand_payable_pension  = 0;
                    $sell_amount            = 0;
                    $grand_total_calculate  = 0;
                    $grand_total_da         = 0;
                    $grand_total_allowance  = 0;
                    $grand_total            = 0;
                    $grand_total_deduction  = 0;
                    $grand_total_differance = 0;
                    $grand_total_net_salary = 0;

                @endphp

                @foreach ($freezePensions as $freezePension)

                    @php
                        $grand_basic            += $freezePension->basic_salary;
                        $grand_payable_pension  += $freezePension->calculated_basic;
                        $sell_amount            += $freezePension->deduct_amt;
                        $grand_total_calculate  += ($freezePension->calculated_basic - $freezePension->deduct_amt);
                        $grand_total_da         += $freezePension->da;
                        $grand_total_allowance  += $freezePension->business_allowances;
                        $grand_total            += ($freezePension->payable_pension + $freezePension->deduction);
                        $grand_total_deduction  += $freezePension->deduction;
                        $grand_total_differance += $freezePension->differance;
                        $grand_total_net_salary += $freezePension->payable_pension;
                    @endphp

                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $freezePension->pension_id }}</td>
                        <td>{{ $freezePension->emp_name }}</td>
                        <td>{{ $freezePension->retirement_date." ".$freezePension->date_of_death }}</td>
                        <td>{{ $freezePension->dob }}</td>
                        <td>{{ $freezePension->branch_name }}</td>
                        <td>{{ $freezePension->ifsc_code }}</td>
                        <td>{{ $freezePension->account_number }}</td>
                        <td>{{ $freezePension->basic_salary }}</td>
                        <td>{{ $freezePension->calculated_basic }}</td>
                        <td>{{ $freezePension->deduct_amt }}</td>
                        <td>{{ ($freezePension->calculated_basic - $freezePension->deduct_amt) }}</td>
                        <td>{{ $freezePension->da }}</td>
                        {{-- <td>{{ $freezePension->business_allowances }}</td> --}}
                        <td>{{ $freezePension->payable_pension + $freezePension->deduction }}</td>
                        <td>{{ $freezePension->deduction }}</td>
                        <td>{{ $freezePension->differance }}</td>
                        <td>{{ $freezePension->payable_pension }}</td>
                    </tr>

                @endforeach
                @php
                    $grandTotal[] = [
                                        'bank_name'         =>  $bank->name,
                                        'total'             =>  $grand_total,
                                        'differance'        =>  $grand_total_differance,
                                        'audit_deduction'   =>  $grand_total_deduction,
                                        'net_salary'        =>  $grand_total_net_salary
                    ];
                @endphp
                <tfoot>
                    <tr>
                        <th class="table-footer" colspan="8" style="text-align: center; font-size:22px;">
                            <b> Grand Total </b>
                        </th>
                        <th>{{ $grand_basic }}</th>
                        <th>{{ $grand_payable_pension }}</th>
                        <th>{{ $sell_amount }}</th>
                        <th>{{ $grand_total_calculate }}</th>
                        <th>{{ $grand_total_da }}</th>
                        {{-- <th>{{ $grand_total_allowance }}</th> --}}
                        <th>{{ $grand_total }}</th>
                        <th>{{ $grand_total_deduction }}</th>
                        <th>{{ $grand_total_differance }}</th>
                        <th>{{ $grand_total_net_salary }}</th>

                    </tr>
                </tfoot>
            </tbody>
        </table>
    @endforeach

    <br><br>
    <hr>
    <table style="width: 100%; margin-top:2%;">
        <thead>
            <tr>
                <th class="th">Sr.No</th>
                <th class="th">Bank Name</th>
                <th class="th">Total</th>
                <th class="th">Differance	</th>
                <th class="th">Audit / Other Deduction</th>
                <th class="th">Net Salary</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($grandTotal as $val)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $val['bank_name'] }}</td>
                    <td>{{ $val['total'] }}</td>
                    <td>{{ $val['differance'] }}</td>
                    <td>{{ $val['audit_deduction'] }}</td>
                    <td>{{ $val['net_salary'] }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

    {{-- Signature part --}}

    <table style="width: 100%; margin-top:5%; border:none;">


        <tr>
            <td class="signature">________________</td>
            <td class="signature">____________________</td>
            <td class="signature">____________________________</td>
            <td class="signature">__________________</td>
            <td class="signature">____________________________</td>
            <td class="signature">__________________</td>
        </tr>

        <tr>
            <td class="signature">PF. Cleark</td>
            <td class="signature">PF Department Head</td>
            <td class="signature">ASST. Commissioner(Gen.Admin)</td>
            <td class="signature">Cheif Auditor</td>
            <td class="signature">Cheif Accountant & Finance Officer</td>
            <td class="signature">DY.Commissioner(HQ)</td>
        </tr>

    </table>

    <h3 style="margin: 10px;">Report Generated on {{ Carbon::now() }}</h3>

</body>
</html>
