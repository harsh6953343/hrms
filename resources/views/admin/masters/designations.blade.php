<x-admin.layout>
    <x-slot name="title">Designation</x-slot>
    <x-slot name="heading">Designation</x-slot>



    <!-- Add Form -->
    <div class="row" id="addContainer" style="display:none;">
        <div class="col-sm-12">
            <div class="card">
                <header class="card-header">
                    <h4 class="card-title">Add Designation</h4>
                </header>
                <form class="theme-form" name="addForm" id="addForm" enctype="multipart/form-data">
                    @csrf

                    <div class="card-body">
                        <div class="mb-3 row">

                            <div class="col-md-4">
                                <label class="col-form-label" for="name">Select Ward<span class="text-danger">*</span></label>
                                <select class="form-control ward_div" id="ward" name="ward_id">
                                    <option value="">Select Ward</option>
                                    @foreach ($wards as $ward)
                                        <option value="{{ $ward->id }}">{{ $ward->name }}</option>
                                    @endforeach
                                </select>
                                <span class="text-danger invalid ward_id_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="department">Select Department<span class="text-danger">*</span></label>
                                <select class="form-control department_div" id="department" name="department_id">
                                    <option value="">Select Department</option>
                                </select>
                                <span class="text-danger invalid department_id_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="class">Select Class<span class="text-danger">*</span></label>
                                <select class="form-control" id="class" name="clas_id">
                                    <option value="">Select Class</option>
                                    @foreach ($class as $clas)
                                        <option value="{{ $clas->id }}">{{ $clas->name }}</option>
                                    @endforeach
                                </select>
                                <span class="text-danger invalid clas_id_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="designation">Designation <span class="text-danger">*</span></label>
                                <input class="form-control" id="designation" name="name" type="text" placeholder="Enter Designation">
                                <span class="text-danger invalid name_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="initial">Initial <span class="text-danger"></span></label>
                                <input class="form-control" id="initial" name="initial" type="text" placeholder="Enter Ward Initial">
                                <span class="text-danger invalid initial_err"></span>
                            </div>

                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" id="addSubmit">Submit</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                    </div>
                </form>
            </div>
        </div>
    </div>



    {{-- Edit Form --}}
    <div class="row" id="editContainer" style="display:none;">
        <div class="col">
            <form class="form-horizontal form-bordered" method="post" id="editForm">
                @csrf
                <section class="card">
                    <header class="card-header">
                        <h4 class="card-title">Edit Designation</h4>
                    </header>

                    <div class="card-body py-2">

                        <input type="hidden" id="edit_model_id" name="edit_model_id" value="">
                        <div class="mb-3 row">

                            <div class="col-md-4">
                                <label class="col-form-label" for="name">Select Ward<span class="text-danger">*</span></label>
                                <select class="form-control" id="edit_ward" name="ward_id">
                                    <option value="">Select Ward</option>
                                    @foreach ($wards as $ward)
                                        <option value="{{ $ward->id }}">{{ $ward->name }}</option>
                                    @endforeach
                                </select>
                                <span class="text-danger invalid ward_id_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="department">Select Department<span class="text-danger">*</span></label>
                                <select class="form-control department_div" id="department" name="department_id">
                                    <option value="">Select Department</option>
                                </select>
                                <span class="text-danger invalid department_id_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="class">Select Class<span class="text-danger">*</span></label>
                                <select class="form-control" id="edit_class" name="clas_id">
                                    <option value="">Select Class</option>
                                </select>
                                <span class="text-danger invalid clas_id_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="designation">Designation <span class="text-danger">*</span></label>
                                <input class="form-control" id="designation" name="name" type="text" placeholder="Enter Designation">
                                <span class="text-danger invalid name_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="initial">Initial <span class="text-danger"></span></label>
                                <input class="form-control" id="initial" name="initial" type="text" placeholder="Enter Ward Initial">
                                <span class="text-danger invalid initial_err"></span>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer">
                        <button class="btn btn-primary" id="editSubmit">Submit</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                    </div>
                </section>
            </form>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                @can('designation.create')
                    <div class="card-header">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="">
                                    <button id="addToTable" class="btn btn-primary">Add <i class="fa fa-plus"></i></button>
                                    <button id="btnCancel" class="btn btn-danger" style="display:none;">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endcan
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>Ward Name</th>
                                    <th>Department</th>
                                    <th>Class</th>
                                    <th>Designation</th>
                                    <th>Designation Initial</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($designations as $designation)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $designation?->ward?->name }}</td>
                                        <td>{{ $designation?->department?->name }}</td>
                                        <td>{{ $designation?->clas?->name }}</td>
                                        <td>{{ $designation?->name }}</td>
                                        <td>{{ $designation?->initial }}</td>
                                        <td>
                                            @can('designation.edit')
                                                <button class="edit-element btn btn-secondary px-2 py-1" title="Edit ward" data-id="{{ $designation->id }}"><i data-feather="edit"></i></button>
                                            @endcan
                                            @can('designation.delete')
                                                <button class="btn btn-danger rem-element px-2 py-1" title="Delete ward" data-id="{{ $designation->id }}"><i data-feather="trash-2"></i> </button>
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




</x-admin.layout>


{{-- Add --}}
<script>
    $("#addForm").submit(function(e) {
        e.preventDefault();
        $("#addSubmit").prop('disabled', true);

        var formdata = new FormData(this);
        $.ajax({
            url: '{{ route('designation.store') }}',
            type: 'POST',
            data: formdata,
            contentType: false,
            processData: false,
            success: function(data) {
                $("#addSubmit").prop('disabled', false);
                if (!data.error2)
                    swal("Successful!", data.success, "success")
                    .then((action) => {
                        window.location.href = '{{ route('designation.index') }}';
                    });
                else
                    swal("Error!", data.error2, "error");
            },
            statusCode: {
                422: function(responseObject, textStatus, jqXHR) {
                    $("#addSubmit").prop('disabled', false);
                    resetErrors();
                    printErrMsg(responseObject.responseJSON.errors);
                },
                500: function(responseObject, textStatus, errorThrown) {
                    $("#addSubmit").prop('disabled', false);
                    swal("Error occured!", "Something went wrong please try again", "error");
                }
            }
        });

    });
</script>


<!-- Edit -->
<script>
    $("#buttons-datatables").on("click", ".edit-element", function(e) {
        e.preventDefault();
        var model_id = $(this).attr("data-id");
        var url = "{{ route('designation.edit', ':model_id') }}";

        $.ajax({
            url: url.replace(':model_id', model_id),
            type: 'GET',
            data: {
                '_token': "{{ csrf_token() }}"
            },
            success: function(data, textStatus, jqXHR) {
                editFormBehaviour();
                if (!data.error) {
                    $("#editForm input[name='edit_model_id']").val(data.designation.id);
                    $("#editForm input[name='name']").val(data.designation.name);
                    $("#editForm input[name='initial']").val(data.designation.initial);
                    $("#edit_ward").html(data.wardHtml);
                    $(".department_div").html(data.deptHtml);
                    $("#edit_class").html(data.classHtml);
                } else {
                    alert(data.error);
                }
            },
            error: function(error, jqXHR, textStatus, errorThrown) {
                alert("Some thing went wrong");
            },
        });
    });
</script>


<!-- Update -->
<script>
    $(document).ready(function() {
        $("#editForm").submit(function(e) {
            e.preventDefault();
            $("#editSubmit").prop('disabled', true);
            var formdata = new FormData(this);
            formdata.append('_method', 'PUT');
            var model_id = $('#edit_model_id').val();
            var url = "{{ route('designation.update', ':model_id') }}";
            //
            $.ajax({
                url: url.replace(':model_id', model_id),
                type: 'POST',
                data: formdata,
                contentType: false,
                processData: false,
                success: function(data) {
                    $("#editSubmit").prop('disabled', false);
                    if (!data.error2)
                        swal("Successful!", data.success, "success")
                        .then((action) => {
                            window.location.href = '{{ route('designation.index') }}';
                        });
                    else
                        swal("Error!", data.error2, "error");
                },
                statusCode: {
                    422: function(responseObject, textStatus, jqXHR) {
                        $("#editSubmit").prop('disabled', false);
                        resetErrors();
                        printErrMsg(responseObject.responseJSON.errors);
                    },
                    500: function(responseObject, textStatus, errorThrown) {
                        $("#editSubmit").prop('disabled', false);
                        swal("Error occured!", "Something went wrong please try again", "error");
                    }
                }
            });

        });
    });
</script>


<!-- Delete -->
<script>
    $("#buttons-datatables").on("click", ".rem-element", function(e) {
        e.preventDefault();
        swal({
                title: "Are you sure to delete this Designation?",
                icon: "info",
                buttons: ["Cancel", "Confirm"]
            })
            .then((justTransfer) => {
                if (justTransfer) {
                    var model_id = $(this).attr("data-id");
                    var url = "{{ route('designation.destroy', ':model_id') }}";

                    $.ajax({
                        url: url.replace(':model_id', model_id),
                        type: 'POST',
                        data: {
                            '_method': "DELETE",
                            '_token': "{{ csrf_token() }}"
                        },
                        success: function(data, textStatus, jqXHR) {
                            if (!data.error && !data.error2) {
                                swal("Success!", data.success, "success")
                                    .then((action) => {
                                        window.location.reload();
                                    });
                            } else {
                                if (data.error) {
                                    swal("Error!", data.error, "error");
                                } else {
                                    swal("Error!", data.error2, "error");
                                }
                            }
                        },
                        error: function(error, jqXHR, textStatus, errorThrown) {
                            swal("Error!", "Something went wrong", "error");
                        },
                    });
                }
            });
    });


    // On change ward fetch departments
    $(".ward_div,#edit_ward").on("change", function(e) {

        var ward_id = this.value;
        var url = "{{ route('fetch-departments', ':ward_id') }}";

        $.ajax({
                url: url.replace(':ward_id', ward_id),
                type: 'GET',
                data: {
                    '_method': "GET",
                    '_token': "{{ csrf_token() }}"
                },
                success: function(data, textStatus, jqXHR) {
                    if (!data.error && !data.error2) {
                        $(".department_div").html(data.deptHtml);

                    } else {
                        alert(data.error);
                    }
                },
                error: function(error, jqXHR, textStatus, errorThrown) {
                    swal("Error!", "Something went wrong", "error");
                },
        });
    });

</script>
