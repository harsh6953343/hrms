<x-admin.layout>
    <x-slot name="title">Employees</x-slot>
    <x-slot name="heading">Employees</x-slot>

    @push('styles')
        <style>
            .size-checkbox{
                width: 1.7rem;
                height: 1.7rem;
            }
        </style>
    @endpush

    <!-- Add Form -->
    <div class="row" id="addContainer">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title mb-0">Add Employee</h4>
                </div>
                <!-- end card header -->
                <div class="card-body">
                    <form class="form-steps" autocomplete="off" id="addForm">
                        @csrf
                        <div class="step-arrow-nav mb-4">

                            <ul class="nav nav-pills custom-nav nav-justified" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link active" id="gen-info-tab" data-bs-toggle="pill" data-bs-target="#gen-info" type="button" role="tab">Personal</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="leave-info-tab" data-bs-toggle="pill" data-bs-target="#leave-info" type="button" role="tab">Leaves</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="qualification-info-tab" data-bs-toggle="pill" data-bs-target="#qualification-info" type="button" role="tab">Qualification</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="experience-info-tab" data-bs-toggle="pill" data-bs-target="#experience-info" type="button" role="tab">Experience</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="important-documents-info-tab" data-bs-toggle="pill" data-bs-target="#important-documents-info" type="button" role="tab">Important Documents</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="bank-info-tab" data-bs-toggle="pill" data-bs-target="#bank-info" type="button" role="tab">Bank Details</button>
                                </li>
                            </ul>
                        </div>

                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="gen-info" role="tabpanel">
                                <div>
                                    <!-- Employee ID field -->
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="employee_id">Employee Id<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" id="employee_id" name="employee_id" placeholder="Enter Employee Id" value="{{ old('employee_id') }}">
                                                <span class="text-danger invalid employee_id_err" data-error-for="employee_id"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- First Name, Middle Name, Last Name fields -->
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="fname">First Name<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" id="fname" name="fname" placeholder="Enter First Name" value="{{ old('fname') }}">
                                                <span class="text-danger invalid fname_err"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="mname">Middle Name<span class="text-danger"></span></label>
                                                <input type="text" class="form-control" id="mname" name="mname" placeholder="Enter Middle Name" value="{{ old('mname') }}">
                                                <span class="text-danger invalid mname_err"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="lname">Last Name<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" id="lname" name="lname" placeholder="Enter Last Name" value="{{ old('lname') }}">
                                                <span class="text-danger invalid lname_err"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Gender, Date of Birth, Date of Joining fields -->
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="gender">Gender<span class="text-danger">*</span></label>
                                                <select class="form-control" id="gender" name="gender">
                                                    <option value="">Select gender</option>
                                                    <option value="1" {{ old('gender') == 1 ? 'selected' : '' }}>Male</option>
                                                    <option value="2" {{ old('gender') == 2 ? 'selected' : '' }}>Female</option>
                                                </select>
                                                <span class="text-danger invalid gender_err"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="dob">Date of Birth<span class="text-danger">*</span></label>
                                                <input type="date" class="form-control" id="dob" name="dob" value="{{ old('dob') }}">
                                                <span class="text-danger invalid dob_err"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="doj">Date of joining<span class="text-danger">*</span></label>
                                                <input type="date" class="form-control" id="doj" name="doj" value="{{ old('doj') }}">
                                                <span class="text-danger invalid doj_err"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Mobile Number, Email, Aadhar Card Number fields -->
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="mobile_number">Mobile No.<span class="text-danger">*</span></label>
                                                <input type="number" class="form-control" id="mobile_number" name="mobile_number" placeholder="Enter Mobile No." value="{{ old('mobile_number') }}" pattern="[0-9]{1,10}" maxlength="10">
                                                <span class="text-danger invalid mobile_number_err"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="email">Email<span class="text-danger"></span></label>
                                                <input type="text" class="form-control" id="email" name="email" placeholder="Enter Email Address" value="{{ old('email') }}">
                                                <span class="text-danger invalid email_err"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="aadhar">Aadhar Card Number<span class="text-danger">*</span></label>
                                                <input type="number" class="form-control" id="aadhar" name="aadhar" placeholder="Enter Aadhar Card Number" value="{{ old('aadhar') }}">
                                                <span class="text-danger invalid aadhar_err"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Pan Card Number, Caste, Blood Group fields -->
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="pan">Pan Card Number<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" id="pan" name="pan" placeholder="Enter Pan Card Number" value="{{ old('pan') }}" style="text-transform: uppercase;">
                                                <span class="text-danger invalid pan_err"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="pf_account_no">PF Account No.<span class="text-danger">*</span></label>
                                                <input type="number" class="form-control" id="pf_account_no" name="pf_account_no" placeholder="Enter PF Account Number" value="{{ old('pf_account_no') }}">
                                                <span class="text-danger invalid pf_account_no_err"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="caste">Caste<span class="text-danger">*</span></label>
                                                <select class="form-control" id="caste" name="caste">
                                                    <option value="">Select caste</option>
                                                    @foreach ($castes as $caste)
                                                        <option value="{{ $caste->id }}">{{ $caste->name }}</option>
                                                    @endforeach
                                                    {{-- <option value="1" {{ old('caste') == 1 ? 'selected' : '' }}>Open</option>
                                                    <option value="2" {{ old('caste') == 2 ? 'selected' : '' }}>OBC</option>
                                                    <option value="3" {{ old('caste') == 3 ? 'selected' : '' }}>SC</option>
                                                    <option value="4" {{ old('caste') == 4 ? 'selected' : '' }}>ST</option>
                                                    <option value="5" {{ old('caste') == 5 ? 'selected' : '' }}>NT</option> --}}
                                                </select>
                                                <span class="text-danger invalid caste_err"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="caste1">Caste 1<span class="text-danger">*</span></label>
                                                <select class="form-control" id="caste1" name="caste1">
                                                    <option value="">Select caste</option>
                                                    @foreach ($castes as $caste)
                                                        <option value="{{ $caste->id }}">{{ $caste->name }}</option>
                                                    @endforeach
                                                </select>
                                                <span class="text-danger invalid caste1_err"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="blood_group">Blood Group<span class="text-danger">*</span></label>
                                                {{-- <input type="text" class="form-control" id="blood_group" name="blood_group" placeholder="Enter Blood Group" value="{{ old('blood_group') }}"> --}}
                                                <select class="form-control" id="blood_group" name="blood_group">
                                                    <option value="">Select Blood Group</option>
                                                    <option value="O positive" {{ old('blood_group') == 'O positive' ? 'selected' : '' }}>O positive</option>
                                                    <option value="O negative" {{ old('blood_group') == 'O negative' ? 'selected' : '' }}>O negative</option>
                                                    <option value="A positive" {{ old('blood_group') == 'A positive' ? 'selected' : '' }}>A positive</option>
                                                    <option value="A negative" {{ old('blood_group') == 'A negative' ? 'selected' : '' }}>A negative</option>
                                                    <option value="B positive" {{ old('blood_group') == 'B positive' ? 'selected' : '' }}>B positive</option>
                                                    <option value="B negative" {{ old('blood_group') == 'B negative' ? 'selected' : '' }}>B negative</option>
                                                    <option value="AB positive" {{ old('blood_group') == 'AB positive' ? 'selected' : '' }}>AB positive</option>
                                                    <option value="AB negative" {{ old('blood_group') == 'AB negative' ? 'selected' : '' }}>AB negative</option>
                                                </select>

                                                <span class="text-danger invalid blood_group_err"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>

                                    <!-- Present Address Section -->
                                    <div class="row mb-3">
                                        <h3>Present Address <hr></h3>
                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="ccity">City<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" id="ccity" name="ccity" placeholder="Enter Current City" value="{{ old('ccity') }}">
                                                <span class="text-danger invalid ccity_err"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="caddress">Present Address<span class="text-danger">*</span></label>
                                                <textarea class="form-control" name="caddress" id="caddress">{{ old('caddress') }}</textarea>
                                                <span class="text-danger invalid caddress_err"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="cstate">State<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" id="cstate" name="cstate" placeholder="Enter Current State" value="{{ old('cstate') }}">
                                                <span class="text-danger invalid cstate_err"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="cpincode">Pincode<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" id="cpincode" name="cpincode" placeholder="Enter Current Pincode" value="{{ old('cpincode') }}">
                                                <span class="text-danger invalid cpincode_err"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4 mt-4">
                                            <div class="mb-3">
                                                <input class="form-check-input size-checkbox" type="checkbox" id="is_applicable" name="is_applicable" {{ old('is_applicable') ? 'checked' : '' }}>
                                                <label class="col-form-check-label" for="is_applicable">
                                                    Click to use the same address as the present address
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>

                                    <div class="row mb-3">
                                        <h3>Permanent Address <hr></h3>
                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="pcity">City<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" id="pcity" name="pcity" placeholder="Enter Permanent City" value="{{ old('pcity') }}">
                                                <span class="text-danger invalid pcity_err"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="paddress">Present Address<span class="text-danger">*</span></label>
                                                <textarea class="form-control" name="paddress" id="paddress">{{ old('paddress') }}</textarea>
                                                <span class="text-danger invalid paddress_err"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="pstate">State<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" id="pstate" name="pstate" placeholder="Enter Permanent State" value="{{ old('pstate') }}">
                                                <span class="text-danger invalid pstate_err"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="ppincode">Pincode<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" id="ppincode" name="ppincode" placeholder="Enter Permanent Pincode" value="{{ old('ppincode') }}">
                                                <span class="text-danger invalid ppincode_err"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>

                                    <div class="row mb-3">
                                        <h3>Work Details <hr></h3>
                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                            <label class="form-label" for="ward_id">Select Ward<span class="text-danger">*</span></label>
                                            <select class="form-control js-example-basic-single" id="ward_id" name="ward_id">
                                                <option value="">Select Ward</option>
                                                @foreach ($wards as $ward)
                                                    <option value="{{ $ward->id }}" {{ old('ward_id') == $ward->id ? 'selected' : '' }} >{{ $ward->name }}</option>
                                                @endforeach
                                            </select>
                                            <span class="text-danger invalid ward_id_err"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                            <label class="form-label" for="department_id">Select Department<span class="text-danger">*</span></label>
                                            <select class="form-control js-example-basic-single" id="department_id" name="department_id">
                                                <option value="">Select Department</option>
                                            </select>
                                            <span class="text-danger invalid department_id_err"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                            <label class="form-label" for="working_department_id">Select Working Department<span class="text-danger">*</span></label>
                                            <select class="form-control js-example-basic-single" id="working_department_id" name="working_department_id">
                                                <option value="">Select Working Department</option>
                                                @foreach ($working_departments as $working_department)
                                                    <option value="{{ $working_department->id }}">{{ $working_department->name }}</option>
                                                @endforeach
                                            </select>
                                            <span class="text-danger invalid working_department_id_err"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                            <label class="form-label" for="employee_category">Select Employee Category<span class="text-danger">*</span></label>
                                            <select class="form-control" id="employee_category" name="employee_category">
                                                <option value="">Select Employee Category</option>
                                                <option value="1" {{ old('employee_category') == 1 ? 'selected' : '' }} >State Goverment</option>
                                                <option value="2" {{ old('employee_category') == 2 ? 'selected' : '' }} >Central Goverment</option>
                                            </select>
                                            <span class="text-danger invalid employee_category_err"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                            <label class="form-label" for="clas_id">Select Class<span class="text-danger">*</span></label>
                                            <select class="form-control" id="clas_id" name="clas_id">
                                                <option value="">Select Class</option>

                                            </select>
                                            <span class="text-danger invalid clas_id_err"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                            <label class="form-label" for="designation_id">Select Designation<span class="text-danger">*</span></label>
                                            <select class="form-control js-example-basic-single" id="designation_id" name="designation_id">
                                                <option value="">Select Designation</option>
                                            </select>
                                            <span class="text-danger invalid designation_id_err"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                            <label class="form-label" for="shift">Select Shift<span class="text-danger">*</span></label>
                                            <select class="form-control" id="shift" name="shift">
                                                <option value="">Select Shift</option>
                                                <option value="1" {{ old('shift') == 1 ? 'selected' : '' }}>General Shift</option>
                                            </select>
                                            <span class="text-danger invalid shift_err"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                            <label class="form-label" for="working_type">Select Working Type<span class="text-danger">*</span></label>
                                            <select class="form-control" id="working_type" name="working_type">
                                                <option value="">Select Working Type</option>
                                                <option value="0" {{ old('working_type') == 0 ? 'selected' : '' }} >Permanent</option>
                                                <option value="1" {{ old('working_type') == 1 ? 'selected' : '' }} >Temporary</option>
                                            </select>
                                            <span class="text-danger invalid working_type_err"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="retirement_date">Date of Retirement<span class="text-danger">*</span></label>
                                                <input type="date" class="form-control" id="retirement_date" name="retirement_date" readonly value="{{ old('retirement_date') }}">
                                                <span class="text-danger invalid retirement_date_err"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                            <label class="form-label" for="increment_month">Select Increment Month<span class="text-danger">*</span></label>
                                            <select class="form-control" id="increment_month" name="increment_month">
                                                <option value="">Select Increment Month</option>
                                                <option value="1" {{ old('increment_month') == 1 ? 'selected' : '' }} >January</option>
                                                <option value="7" {{ old('increment_month') == 7 ? 'selected' : '' }} >July</option>
                                            </select>
                                            <span class="text-danger invalid increment_month_err"></span>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="d-flex align-items-start gap-3 mt-4">
                                    <button type="button" id="personal_btn" class="btn btn-primary btn-label right ms-auto nexttab"><i class="ri-arrow-right-line label-icon align-middle fs-16 ms-2"></i>Go to Leaves</button>
                                </div>
                            </div>
                            <!-- end tab pane -->

                            <div class="tab-pane fade" id="leave-info" role="tabpanel">
                                <div>
                                    <div class="row">
                                        <h3>Leave Details <hr></h3>

                                        <table class="table table-bordered nowrap align-middle" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <td>Leave Type</td>
                                                    <td>Carry Forward</td>
                                                    <td>No. of leaves</td>
                                                    <td>Cashable</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($leaveType as $type)
                                                <tr>
                                                    <td>
                                                        <label class="form-label" for="des-info-description-input">{{ $type->name }}</label>
                                                        <input type="hidden" name="leave_type_id[]" value="{{ $type->id }}">
                                                    </td>
                                                    <td>
                                                        {{ ($type->carry_forward == 1)?'Yes':'No' }}
                                                        <input type="hidden" name="carry_forward[]" value="{{ $type->carry_forward }}">
                                                    </td>
                                                    <td>
                                                        <input type="number" class="form-control" name="no_of_leaves[]" value="{{ $type->no_of_leaves }}">
                                                    </td>
                                                    <td>
                                                        <select class="form-control" name="encashable[]">
                                                            <option value="1" {{ $type->encashable == 1 ? 'selected' : '' }} >Yes</option>
                                                            <option value="0" {{ $type->encashable == 0 ? 'selected' : '' }} >No</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="d-flex align-items-start gap-3 mt-4">
                                    <button type="button" class="btn btn-light btn-label previestab" data-previous="gen-info-tab"><i class="ri-arrow-left-line label-icon align-middle fs-16 me-2"></i> Back to Personal</button>
                                    <button type="button" class="btn btn-primary btn-label right ms-auto nexttab" data-nexttab="qualification-info-tab"><i class="ri-arrow-right-line label-icon align-middle fs-16 ms-2"></i>Next Qualification</button>
                                </div>
                            </div>
                            <!-- end tab pane -->

                            <div class="tab-pane fade" id="qualification-info" role="tabpanel">
                                <div>
                                    <div class="row">
                                        <h3>Academic Details <hr></h3>

                                        <table class="table table-bordered nowrap align-middle" style="width:100%" id="educationTable">
                                            <thead>
                                                <tr>
                                                    <th>From MM/YY</th>
                                                    <th>To MM/YY</th>
                                                    <th>Certificate Gained	</th>
                                                    <th>Board / University</th>
                                                    <th>Marks / Grade</th>
                                                    <th>Document</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="educationBody">
                                                <tr>
                                                    <td>
                                                        <input type="date" class="form-control" name="qfrom[]" placeholder="From MM/YY">
                                                    </td>
                                                    <td>
                                                        <input type="date" class="form-control" name="qto[]" placeholder="To MM/YY">
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" name="qcertificate[]" placeholder="Certificate Gained">
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" name="qboard[]" placeholder="Board / University">
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" name="qmarks[]" placeholder="Marks / Grade">
                                                    </td>
                                                    <td>
                                                        <input type="file" class="form-control" name="qdocument[]" placeholder="Document">
                                                    </td>
                                                    <td style="width: 10%;">
                                                        <button type="button" class="btn btn-primary" onclick="addEducationRow()">Add More </button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                                <div class="d-flex align-items-start gap-3 mt-4">
                                    <button type="button" class="btn btn-light btn-label previestab" data-previous="leave-info-tab"><i class="ri-arrow-left-line label-icon align-middle fs-16 me-2"></i> Back to Leaves</button>
                                    <button type="button" class="btn btn-primary btn-label right ms-auto nexttab" data-nexttab="experience-info-tab"><i class="ri-arrow-right-line label-icon align-middle fs-16 ms-2"></i>Next Experience</button>
                                </div>
                            </div>
                            <!-- end tab pane -->

                            <div class="tab-pane fade" id="experience-info" role="tabpanel">
                                <div>
                                    <div class="row">
                                        <h3>Work Experience <hr></h3>

                                        <table class="table table-bordered nowrap align-middle" style="width:100%" id="experienceTable">
                                            <thead>
                                                <tr>
                                                    <th>From MM/YY</th>
                                                    <th>To MM/YY</th>
                                                    <th>Name & Address of Organisation	</th>
                                                    <th>Designation</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="experienceBody">
                                                <tr>
                                                    <td>
                                                        <input type="date" class="form-control" name="efrom[]" placeholder="From MM/YY">
                                                    </td>
                                                    <td>
                                                        <input type="date" class="form-control" name="eto[]" placeholder="To MM/YY">
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" name="ename_address[]" placeholder="Name & Address of Organisation">
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" name="edesignation[]" placeholder="Designation">
                                                    </td>
                                                    <td style="width: 10%;">
                                                        <button type="button" class="btn btn-primary" onclick="addExperienceRow()">Add More </button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                                <div class="d-flex align-items-start gap-3 mt-4">
                                    <button type="button" class="btn btn-light btn-label previestab" data-previous="qualification-info-tab"><i class="ri-arrow-left-line label-icon align-middle fs-16 me-2"></i> Back to Qualification</button>
                                    <button type="button" class="btn btn-primary btn-label right ms-auto nexttab" data-nexttab="important-documents-info-tab"><i class="ri-arrow-right-line label-icon align-middle fs-16 ms-2"></i>Next Important Documents</button>
                                </div>
                            </div>
                            <!-- end tab pane -->

                            <div class="tab-pane fade" id="important-documents-info" role="tabpanel">
                                <div>
                                    <div class="row">
                                        <h3>Important Documents <hr></h3>

                                        <table class="table table-bordered nowrap align-middle" style="width:100%" id="importantDocumentTable">
                                            <thead>
                                                <tr>
                                                    <th>Document</th>
                                                    <th>File</th>
                                                    <th>Remark	</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="importantDocumentBody">
                                                <tr>
                                                    <td>
                                                        <select name="document_id[]" id="document_id" class="form-control">
                                                            <option value="">Select Document</option>
                                                            @foreach ($documents as $document)
                                                                <option value="{{ $document->id }}">{{ $document->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <input type="file" class="form-control" name="impdocument[]" placeholder="Important Document">
                                                    </td>
                                                    <td>
                                                        <textarea name="impremark[]" id="impremark" class="form-control" placeholder="Remark"></textarea>
                                                    </td>
                                                    <td style="width: 10%;">
                                                        <button type="button" class="btn btn-primary" onclick="addImportantDocumentRow()">Add More </button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                                <div class="d-flex align-items-start gap-3 mt-4">
                                    <button type="button" class="btn btn-light btn-label previestab" data-previous="experience-info-tab"><i class="ri-arrow-left-line label-icon align-middle fs-16 me-2"></i> Back to Experience</button>
                                    <button type="button" class="btn btn-primary btn-label right ms-auto nexttab" data-nexttab="bank-info-tab"><i class="ri-arrow-right-line label-icon align-middle fs-16 ms-2"></i>Next Bank</button>
                                </div>
                            </div>
                            <!-- end tab pane -->

                            <div class="tab-pane fade" id="bank-info" role="tabpanel">
                                <div>
                                    <!-- First Name, Middle Name, Last Name fields -->
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="mb-3">
                                                <label class="form-label" for="fname">Please Select Bank<span class="text-danger">*</span></label>
                                                <select name="bank_id" id="bank_id" class="form-control">
                                                    <option value="">Select Bank</option>
                                                    @foreach ($banks as $key => $bank)
                                                    <option value="{{ $bank->id }}">{{ $bank->name }}</option>
                                                    @endforeach
                                                </select>
                                                <span class="text-danger invalid bank_id_err"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="mb-3">
                                                <label class="form-label" for="branch">Bank Branch<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" id="branch" name="branch" placeholder="Enter Bank Branch" value="{{ old('branch') }}">
                                                <span class="text-danger invalid branch_err"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="mb-3">
                                                <label class="form-label" for="account_no">Account Number<span class="text-danger">*</span></label>
                                                <input type="number" class="form-control" id="account_no" name="account_no" placeholder="Enter Account Number" value="{{ old('account_no') }}">
                                                <span class="text-danger invalid account_no_err"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="mb-3">
                                                <label class="form-label" for="ifsc">IFSC Code<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" id="ifsc" name="ifsc" placeholder="Enter Account Number" value="{{ old('ifsc') }}">
                                                <span class="text-danger invalid ifsc_err"></span>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="d-flex align-items-start gap-3 mt-4">
                                    <button type="button" class="btn btn-light btn-label previestab" data-previous="important-documents-info-tab"><i class="ri-arrow-left-line label-icon align-middle fs-16 me-2"></i> Back to Important Documents</button>
                                    <button type="submit" class="btn btn-primary btn-label right ms-auto" id="addSubmit"><i class="ri-arrow-right-line label-icon align-middle fs-16 ms-2"></i>Submit</button>
                                </div>
                            </div>
                            <!-- end tab pane -->

                        </div>
                        <!-- end tab content -->
                    </form>
                </div>
                <!-- end card body -->
            </div>
            <!-- end card -->
        </div>
    </div>

</x-admin.layout>
<script src="{{ asset('admin/js/employee-validation.js') }}" defer></script>

<script>

function addImportantDocumentRow() {
    var importantDocumentBody = document.getElementById("importantDocumentBody");
    var newRow = importantDocumentBody.insertRow();

    // Document select dropdown
    var documentCell = newRow.insertCell(0);
    var documentSelect = document.createElement("select");
    documentSelect.name = "document_id[]";
    documentSelect.className = "form-control";
    documentSelect.innerHTML = `
        <option value="">Select Document</option>
        @foreach ($documents as $document)
            <option value="{{ $document->id }}">{{ $document->name }}</option>
        @endforeach
    `;
    documentCell.appendChild(documentSelect);

    // File input
    var fileCell = newRow.insertCell(1);
    var fileInput = document.createElement("input");
    fileInput.type = "file";
    fileInput.className = "form-control";
    fileInput.name = "impdocument[]";
    fileInput.placeholder = "Important Document";
    fileCell.appendChild(fileInput);

    // Remark textarea
    var remarkCell = newRow.insertCell(2);
    var remarkTextarea = document.createElement("textarea");
    remarkTextarea.className = "form-control";
    remarkTextarea.name = "impremark[]";
    remarkTextarea.placeholder = "Remark";
    remarkCell.appendChild(remarkTextarea);

    // Add Remove button
    var actionCell = newRow.insertCell(3);
    var removeButton = document.createElement("button");
    removeButton.type = "button";
    removeButton.className = "btn btn-danger";
    removeButton.innerHTML = "Remove";
    removeButton.onclick = function () {
        removeImportantDocumentRow(this);
    };
    actionCell.appendChild(removeButton);
}

function removeImportantDocumentRow(button) {
    var row = button.closest("tr");
    row.remove();
}
</script>

{{-- Add --}}
<script>
    $("#addForm").submit(function(e) {
        $("form").removeClass("was-validated");
        $("input").removeClass("is-valid");
        e.preventDefault();
        $("#addSubmit").prop('disabled', true);

        var formdata = new FormData(this);
        $.ajax({
            url: '{{ route('employee.store') }}',
            type: 'POST',
            data: formdata,
            contentType: false,
            processData: false,
            success: function(data) {
                $("#addSubmit").prop('disabled', false);
                if (!data.error2)
                    swal("Successful!", data.success, "success")
                    .then((action) => {
                        window.location.href = '{{ route('employee.index') }}';
                    });
                else
                    swal("Error!", data.error2, "error");
            },
            statusCode: {
                422: function(responseObject, textStatus, jqXHR) {
                    $("#addSubmit").prop('disabled', false);
                    resetErrors();
                    printErrMsg(responseObject.responseJSON.errors);

                    // Find the first input field with an error
                    var firstErrorField = Object.keys(responseObject.responseJSON.errors)[0];
                    var tabId = findTabIdForField(firstErrorField);

                    // Hide all tabs and remove active classes
                    $(".tab-content .tab-pane, .nav-pills .nav-link").removeClass("show active");

                    // Show the tab associated with the first error
                    $("#" + tabId + ", #" + tabId + "-tab").addClass("show active");

                },
                500: function(responseObject, textStatus, errorThrown) {
                    $("#addSubmit").prop('disabled', false);
                    swal("Error occured!", "Something went wrong please try again", "error");
                }
            }
        });

    });


    function findTabIdForField(fieldName) {

    switch (fieldName) {
        // Personal tab fields
        case 'fname':
        case 'mname':
        case 'lname':
        case 'gender':
        case 'dob':
        case 'doj':
        case 'mobile_number ':
        case 'email':
        case 'aadhar':
        case 'pan':
        case 'pf_account_no':
        case 'caste':
        case 'blood_group':
        case 'ccity':
        case 'caddress':
        case 'cstate':
        case 'cpincode':
        case 'pcity':
        case 'paddress':
        case 'pstate':
        case 'ppincode':
        case 'ward_id ':
        case 'department_id ':
        case 'clas_id ':
        case 'designation_id ':
        case 'shift':
        case 'working_type':
        case 'retirement_date':
        case 'increment_month':
        case 'employee_category':

            return 'gen-info';

        // Bank Details tab fields
        case 'bank_id':
        case 'branch':
        case 'account_no':
        case 'ifsc':
        // Add more fields corresponding to the Bank Details tab...

            return 'bank-info';

        default:
            return 'gen-info'; // Default to Personal tab if not found
    }
}

</script>

<script>
    // On change ward fetch departments
$("#ward_id").on("change", function (e) {
    var ward_id = this.value;
    if(ward_id != ''){
        var url = "{{ route('fetch-departments', ':ward_id') }}";

        $.ajax({
            url: url.replace(":ward_id", ward_id),
            type: "GET",
            data: {
                _method: "GET",
                _token: "{{ csrf_token() }}",
            },
            success: function (data, textStatus, jqXHR) {
                if (!data.error && !data.error2) {
                    $("#department_id").html(data.deptHtml);
                } else {
                    alert(data.error);
                }
            },
            error: function (error, jqXHR, textStatus, errorThrown) {
                swal("Error!", "Something went wrong", "error");
            },
        });
    }
});

// on change employee category
$("#employee_category").on("change", function (e) {
    var employee_category = this.value;
    var url = "{{ route('fetch-class', ':employee_category') }}";

    $.ajax({
        url: url.replace(":employee_category", employee_category),
        type: "GET",
        data: {
            _method: "GET",
            _token: "{{ csrf_token() }}",
        },
        success: function (data, textStatus, jqXHR) {
            if (!data.error && !data.error2) {
                $("#clas_id").html(data.classHtml);
            } else {
                alert(data.error);
            }
        },
        error: function (error, jqXHR, textStatus, errorThrown) {
            swal("Error!", "Something went wrong", "error");
        },
    });
});

// On change ward/department/class fetch designation
$("#ward_id,#department_id,#clas_id").on("change", function (e) {
    var ward_id = $("#ward_id").val();
    var department_id = $("#department_id").val();
    var clas_id = $("#clas_id").val();

    var url =
        "{{ route('fetch-designations', [':ward_id', ':department_id', ':class_id']) }}";

    // Replace placeholders with actual values
    url = url
        .replace(":ward_id", ward_id)
        .replace(":department_id", department_id)
        .replace(":class_id", clas_id);
    if (ward_id != "" && department_id != "" && clas_id != "") {
        $.ajax({
            url: url,
            type: "GET",
            data: {
                _method: "GET",
                _token: "{{ csrf_token() }}",
            },
            success: function (data, textStatus, jqXHR) {
                if (!data.error && !data.error2) {
                    $("#designation_id").html(data.desHtml);
                } else {
                    alert(data.error);
                }
            },
            error: function (error, jqXHR, textStatus, errorThrown) {
                swal("Error!", "Something went wrong", "error");
            },
        });
    }
});

// Fetch retirement date based on dob doj and class
$("#dob,#doj,#clas_id").on("change", function (e) {
    var dob = $("#dob").val();
    var doj = $("#doj").val();
    var clas_id = $("#clas_id").val();

    var url = "{{ route('fetch-working-years',[':class_id',':dob']) }}";

    // Replace placeholders with actual values
    url = url.replace(":class_id", clas_id).replace(":dob", dob);
    if (dob != "" && doj != "" && clas_id != "") {
        $.ajax({
            url: url,
            type: "GET",
            data: {
                _method: "GET",
                _token: "{{ csrf_token() }}",
            },
            success: function (data, textStatus, jqXHR) {
                if (!data.error) {
                    $("#retirement_date").val(data.date_of_retirement);
                } else {
                    alert(data.error);
                }
            },
            error: function (error, jqXHR, textStatus, errorThrown) {
                swal("Error!", "Something went wrong", "error");
            },
        });
    }
});
</script>
