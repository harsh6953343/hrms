<x-admin.layout>
    <x-slot name="title">DA Differance Employee List</x-slot>
    <x-slot name="heading">DA Differance Employee List</x-slot>


    <div class="row">
        <div class="col-lg-12">
            <div class="card">

                <div class="card-body">
                    <div class="table-responsive">
                        <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>Employee Id</th>
                                    <th>Name</th>
                                    <th>Current DA Rate</th>
                                    <th>New DA Rate</th>
                                    <th>Applicable Month</th>
                                    <th>Applied Month</th>
                                    <th>No. of Months</th>
                                    <th>Differance</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach ($employee_da_differances as $daDifferance)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $daDifferance->Emp_Code }}</td>
                                        <td>{{ $daDifferance->employee->fname." ".$daDifferance->employee->mname." ".$daDifferance->employee->lname }}</td>
                                        <td>{{ $daDifferance->DA_currentRate }}</td>
                                        <td>{{ $daDifferance->DA_newRate }}</td>
                                        <td>{{ $daDifferance->applicable_month }}</td>
                                        <td>{{ $daDifferance->given_month }}</td>
                                        <td>{{ $daDifferance->no_of_month }}</td>
                                        <td>{{ $daDifferance->differance }}</td>
                                        <td>{{ ($daDifferance->status == 0)?'Pending':'Applied'; }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




</x-admin.layout>
