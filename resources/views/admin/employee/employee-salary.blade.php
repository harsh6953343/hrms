<x-admin.layout>
    <x-slot name="title">Salary Structure</x-slot>
    <x-slot name="heading">Salary Structure</x-slot>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                @can('employee-salary.create')
                    <div class="card-header">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="">
                                    <a href="{{ route('employee-salary.create') }}" class="btn btn-primary">Add <i class="fa fa-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endcan
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>Employee Id</th>
                                    <th>Employee Name</th>
                                    <th>Pay Scale</th>
                                    <th>Basic Salary</th>
                                    <th>Grade Pay</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($employee_salarys as $employee_salary)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $employee_salary?->Emp_Code }}</td>
                                        <td>{{ $employee_salary?->employee?->fname." ".$employee_salary?->employee?->mname." ".$employee_salary?->employee?->lname }}</td>
                                        <td>{{ $employee_salary?->pay_scale?->pay_band }}</td>
                                        <td>{{ $employee_salary?->basic_salary }}</td>
                                        <td>{{ $employee_salary?->grade_pay }}</td>
                                        <td>
                                            @can('employee-salary.edit')
                                                <a href="{{ route('employee-salary.edit',$employee_salary->id) }}" class="edit-element btn btn-secondary px-2 py-1" title="Edit Employee Salary" data-id="{{ $employee_salary->id }}"><i data-feather="edit"></i></a>
                                            @endcan
                                            @can('employee-salary.delete')
                                                <a class="btn btn-danger rem-element px-2 py-1" title="Delete Employee Salary" data-id="{{ $employee_salary->id }}"><i data-feather="trash-2"></i> </a>
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




</x-admin.layout>

<!-- Delete -->
<script>
    $("#buttons-datatables").on("click", ".rem-element", function(e) {
        e.preventDefault();
        swal({
                title: "Are you sure to delete this Salary?",
                // text: "Make sure if you have filled Vendor details before proceeding further",
                icon: "info",
                buttons: ["Cancel", "Confirm"]
            })
            .then((justTransfer) => {
                if (justTransfer) {
                    var model_id = $(this).attr("data-id");
                    var url = "{{ route('employee-salary.destroy', ':model_id') }}";

                    $.ajax({
                        url: url.replace(':model_id', model_id),
                        type: 'POST',
                        data: {
                            '_method': "DELETE",
                            '_token': "{{ csrf_token() }}"
                        },
                        success: function(data, textStatus, jqXHR) {
                            if (!data.error && !data.error2) {
                                swal("Success!", data.success, "success")
                                    .then((action) => {
                                        window.location.reload();
                                    });
                            } else {
                                if (data.error) {
                                    swal("Error!", data.error, "error");
                                } else {
                                    swal("Error!", data.error2, "error");
                                }
                            }
                        },
                        error: function(error, jqXHR, textStatus, errorThrown) {
                            swal("Error!", "Something went wrong", "error");
                        },
                    });
                }
            });
    });
</script>

