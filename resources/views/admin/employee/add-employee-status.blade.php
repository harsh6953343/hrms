<x-admin.layout>
    <x-slot name="title">Employee Status</x-slot>
    <x-slot name="heading">Employee Status</x-slot>

    <!-- Add Form -->
    <div class="row" id="addContainer">
        <div class="col-sm-12">
            <div class="card">
                <header class="card-header">
                    <h4 class="card-title">Employee Status</h4>
                </header>
                <form class="theme-form" name="addForm" id="addForm" enctype="multipart/form-data">
                    @csrf

                    <div class="card-body">
                        <div class="mb-3 row">

                            <div class="col-md-3 mt-2">
                                <label class="col-form-label" for="Emp_Code">Employee Id<span class="text-danger">*</span></label>
                                <input class="form-control" id="Emp_Code" name="Emp_Code" type="text" placeholder="Enter Employee Id">
                                <span class="text-danger invalid Emp_Code_err"></span>
                            </div>

                            <div class="col-md-3 mt-5">
                                <button type="button" class="btn btn-primary" id="searchBtn">Search Employee</button>
                            </div>
                        </div>

                        <div class="mb-3 row">

                            <input type="hidden" name="employee_id" id="employee_id">

                            <div class="col-md-4">
                                <label class="col-form-label" for="emp_name">Employee Name<span class="text-danger">*</span></label>
                                <input class="form-control" id="emp_name" name="emp_name" type="text" placeholder="Employee Name" readonly>
                                <span class="text-danger invalid emp_name_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="ward">Ward<span class="text-danger">*</span></label>
                                <input class="form-control" id="ward" name="ward" type="text" placeholder="Employee Ward" readonly>
                                <span class="text-danger invalid ward_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="department">Department<span class="text-danger">*</span></label>
                                <input class="form-control" id="department" name="department" type="text" placeholder="Employee Department" readonly>
                                <span class="text-danger invalid department_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="designation">Designation<span class="text-danger">*</span></label>
                                <input class="form-control" id="designation" name="designation" type="text" placeholder="Employee Designation" readonly>
                                <span class="text-danger invalid designation_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="class">Class<span class="text-danger">*</span></label>
                                <input class="form-control" id="class" name="class" type="text" placeholder="Employee Class" readonly>
                                <span class="text-danger invalid class_err"></span>
                            </div>
                        </div>
                        <hr>

                        <div class="mb-3 row" id="status_div" style="display: none;">
                            <h2>Add Employee Status</h2>
                            <div class="col-md-4 mb-3">
                                <label class="col-form-label" for="status_id">Select Status<span class="text-danger">*</span></label>
                                <select class="form-control" id="status_id" name="status_id">
                                    <option value="">Select Status</option>
                                    @foreach ($statuses as $status)
                                        <option value="{{ $status->id }}">{{ $status->name }}</option>
                                    @endforeach
                                </select>
                                <span class="text-danger invalid status_id_err"></span>
                            </div>

                            <div class="col-md-4 mb-3">
                                <label class="col-form-label" for="remark">Remark<span class="text-danger">*</span></label>
                                <input class="form-control" id="remark" name="remark" type="text" placeholder="Enter Remark">
                                <span class="text-danger invalid remark_err"></span>
                            </div>

                            <div class="col-md-4 mb-3">
                                <label class="col-form-label" for="applicable_date">Applicable Date<span class="text-danger">*</span></label>
                                <input class="form-control" id="applicable_date" name="applicable_date" type="date" placeholder="Enter Applicable Date">
                                <span class="text-danger invalid applicable_date_err"></span>
                            </div>

                            <div class="col-md-4 mb-3">
                                <label label="" class="col-form-label" for="is_salary_applicable">Is Salary Applicable ? <span class="text-danger"></span></label>
                                <div class="form-check mb-2">
                                    <input class="form-check-input is_salary_applicable" type="radio" name="is_salary_applicable" id="is_salary_applicable1" value="1">
                                    <label class="form-check-label" for="is_salary_applicable1">
                                        Yes
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input is_salary_applicable" type="radio" name="is_salary_applicable" id="is_salary_applicable2" value="0" checked="">
                                    <label class="form-check-label" for="is_salary_applicable2">
                                        No
                                    </label>
                                </div>
                                <span class="text-danger invalid is_salary_applicable_err"></span>
                            </div>

                            <div class="col-md-4 mb-3" id="salary_percent_div" style="display: none;">
                                <label class="col-form-label" for="salary_percent">Salary Percent<span class="text-danger">*</span></label>
                                <input class="form-control" type="number" name="salary_percent" id="salary_percent">

                                {{-- <select class="form-control" id="salary_percent" name="salary_percent">
                                    <option value="">Select  Salary Percent</option>
                                    <option value="1">25%</option>
                                    <option value="2">50%</option>
                                    <option value="3">75%</option>
                                    <option value="4">100%</option>
                                </select> --}}
                                <span class="text-danger invalid salary_percent_err"></span>
                            </div>

                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" id="addSubmit">Submit</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


</x-admin.layout>


{{-- Add --}}
<script>
    $("#addForm").submit(function(e) {
        e.preventDefault();
        $("#addSubmit").prop('disabled', true);

        var formdata = new FormData(this);
        $.ajax({
            url: '{{ route('employee-status.store') }}',
            type: 'POST',
            data: formdata,
            contentType: false,
            processData: false,
            success: function(data) {
                $("#addSubmit").prop('disabled', false);
                if (!data.error2)
                    swal("Successful!", data.success, "success")
                    .then((action) => {
                        window.location.href = '{{ route('employee-status.index') }}';
                    });
                else
                    swal("Error!", data.error2, "error");
            },
            statusCode: {
                422: function(responseObject, textStatus, jqXHR) {
                    $("#addSubmit").prop('disabled', false);
                    resetErrors();
                    printErrMsg(responseObject.responseJSON.errors);
                },
                500: function(responseObject, textStatus, errorThrown) {
                    $("#addSubmit").prop('disabled', false);
                    swal("Error occured!", "Something went wrong please try again", "error");
                }
            }
        });

    });


    $(document).ready(function() {
        $("#searchBtn").click(function(){
            var Emp_Code = $('#Emp_Code').val();
            if(Emp_Code != '')
            {
                var url = "{{ route('fetch-employee-details', ':Emp_Code') }}";

                $.ajax({
                    url: url.replace(':Emp_Code', Emp_Code),
                    type: 'GET',
                    data: {
                        '_method': "GET",
                        '_token': "{{ csrf_token() }}"
                    },
                    success: function(data) {
                        if (!data.error && !data.error2) {
                            if (data.result === 1) {

                                var full_name = data.employee_details.fname + " " + data.employee_details.mname + " " + data.employee_details.lname;
                                $('#emp_name').val(full_name);
                                $('#ward').val(data.employee_details.ward.name);
                                $('#department').val(data.employee_details.department.name);
                                $('#class').val(data.employee_details.class.name);
                                $('#designation').val(data.employee_details.designation.name);
                                $('#employee_id').val(data.employee_details.id);

                                $('#status_div').removeAttr('style');

                            } else if (data.result === 0) {
                                $('#status_div').hide();
                                $('#status_id').val('');
                                $('#remark').val('');
                                $('#emp_name').val('');
                                $('#ward').val('');
                                $('#department').val('');
                                $('#designation').val('');
                                $('#class').val('');

                                alert("Employee details not found or Employee Status already added!");
                            } else {
                                alert("Unexpected result from the server");
                            }
                    }
                    },
                    error: function(error, jqXHR, textStatus, errorThrown) {
                        swal("Error!", "Something went wrong", "error");
                    },
                });
            }
            else{
                alert('Please Enter Employee Id');
            }

        });
    });

    $(".is_salary_applicable").change(function(){

        var is_salary_applicable = this.value;

        if(is_salary_applicable == 1)
        {
            $('#salary_percent_div').removeAttr('style');
        }
        else{
            $('#salary_percent_div').hide();
        }

    });


</script>

