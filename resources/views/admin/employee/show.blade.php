<x-admin.layout>
    <x-slot name="title">View Employee Details</x-slot>
    <x-slot name="heading">View Employee Details</x-slot>

    @push('styles')
        <style>
            .size-checkbox {
                width: 1.7rem;
                height: 1.7rem;
            }
        </style>
    @endpush

    <!-- Add Form -->
    <div class="row" id="addContainer">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title mb-0">View Employee Details</h4>
                </div>
                <!-- end card header -->
                <div class="card-body">
                    <form class="form-steps" autocomplete="off" id="editForm">
                        @csrf

                        <div class="step-arrow-nav mb-4">

                            <ul class="nav nav-pills custom-nav nav-justified" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link active" id="gen-info-tab" data-bs-toggle="pill" data-bs-target="#gen-info" type="button" role="tab">Personal</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="leave-info-tab" data-bs-toggle="pill" data-bs-target="#leave-info" type="button" role="tab">Leaves</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="qualification-info-tab" data-bs-toggle="pill" data-bs-target="#qualification-info" type="button" role="tab">Qualification</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="experience-info-tab" data-bs-toggle="pill" data-bs-target="#experience-info" type="button" role="tab">Experience</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="important-documents-info-tab" data-bs-toggle="pill" data-bs-target="#important-documents-info" type="button" role="tab">Important Documents</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="bank-info-tab" data-bs-toggle="pill" data-bs-target="#bank-info" type="button" role="tab">Bank Details</button>
                                </li>
                            </ul>
                        </div>

                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="gen-info" role="tabpanel">
                                <div>
                                    <!-- Employee ID field -->
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="employee_id">Employee Id<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" id="employee_id" name="employee_id" placeholder="Enter Employee Id" value="{{ $employee->employee_id }}" readonly>
                                                <span class="text-danger invalid employee_id_err" data-error-for="employee_id"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- First Name, Middle Name, Last Name fields -->
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="fname">First Name<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" id="fname" name="fname" placeholder="Enter First Name" value="{{ $employee->fname }}" readonly>
                                                <span class="text-danger invalid fname_err"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="mname">Middle Name<span class="text-danger"></span></label>
                                                <input type="text" class="form-control" id="mname" name="mname" placeholder="Enter Middle Name" value="{{ $employee->mname }}" readonly>
                                                <span class="text-danger invalid mname_err"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="lname">Last Name<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" id="lname" name="lname" placeholder="Enter Last Name" value="{{ $employee->lname }}" readonly>
                                                <span class="text-danger invalid lname_err"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Gender, Date of Birth, Date of Joining fields -->
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="gender">Gender<span class="text-danger">*</span></label>
                                                <select class="form-control" id="gender" name="gender" disabled>
                                                    <option value="">Select gender</option>
                                                    <option value="1" {{ $employee->gender == 1 ? 'selected' : '' }}>Male</option>
                                                    <option value="2" {{ $employee->gender == 2 ? 'selected' : '' }}>Female</option>
                                                </select>
                                                <span class="text-danger invalid gender_err"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="dob">Date of Birth<span class="text-danger">*</span></label>
                                                <input type="date" class="form-control" id="dob" name="dob" value="{{ $employee->dob }}" readonly>
                                                <span class="text-danger invalid dob_err"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="doj">Date of joining<span class="text-danger">*</span></label>
                                                <input type="date" class="form-control" id="doj" name="doj" value="{{ $employee->doj }}" readonly>
                                                <span class="text-danger invalid doj_err"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Mobile Number, Email, Aadhar Card Number fields -->
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="mobile_number">Mobile No.<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" id="mobile_number" name="mobile_number" placeholder="Enter Mobile No." value="{{ $employee->mobile_number }}" readonly>
                                                <span class="text-danger invalid mobile_number_err"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="email">Email<span class="text-danger"></span></label>
                                                <input type="text" class="form-control" id="email" name="email" placeholder="Enter Email Address" value="{{ $employee->email }}" readonly>
                                                <span class="text-danger invalid email_err"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="aadhar">Aadhar Card Number<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" id="aadhar" name="aadhar" placeholder="Enter Aadhar Card Number" value="{{ $employee->aadhar }}" readonly>
                                                <span class="text-danger invalid aadhar_err"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Pan Card Number, Caste, Blood Group fields -->
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="pan">Pan Card Number<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" id="pan" name="pan" placeholder="Enter Pan Card Number" value="{{ $employee->pan }}" style="text-transform: uppercase;" readonly>
                                                <span class="text-danger invalid pan_err"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="pf_account_no">PF Account No.<span class="text-danger">*</span></label>
                                                <input type="number" class="form-control" id="pf_account_no" name="pf_account_no" placeholder="Enter Pan Card Number" value="{{ $employee->pf_account_no }}" readonly>
                                                <span class="text-danger invalid pf_account_no_err"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="caste">Caste<span class="text-danger">*</span></label>
                                                <select class="form-control" id="caste" name="caste" disabled>
                                                    <option value="">Select caste</option>
                                                    <option value="1" {{ $employee->caste == 1 ? 'selected' : '' }}>Open</option>
                                                    <option value="2" {{ $employee->caste == 2 ? 'selected' : '' }}>OBC</option>
                                                    <option value="3" {{ $employee->caste == 3 ? 'selected' : '' }}>SC</option>
                                                    <option value="4" {{ $employee->caste == 4 ? 'selected' : '' }}>ST</option>
                                                    <option value="5" {{ $employee->caste == 5 ? 'selected' : '' }}>NT</option>
                                                </select>
                                                <span class="text-danger invalid caste_err"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="blood_group">Blood Group<span class="text-danger">*</span></label>
                                                <select class="form-control" id="blood_group" name="blood_group" disabled>
                                                    <option value="">Select Blood Group</option>
                                                    <option value="O positive" {{ $employee->blood_group == 'O positive' ? 'selected' : '' }}>O positive</option>
                                                    <option value="O negative" {{ $employee->blood_group == 'O negative' ? 'selected' : '' }}>O negative</option>
                                                    <option value="A positive" {{ $employee->blood_group == 'A positive' ? 'selected' : '' }}>A positive</option>
                                                    <option value="A negative" {{ $employee->blood_group == 'A negative' ? 'selected' : '' }}>A negative</option>
                                                    <option value="B positive" {{ $employee->blood_group == 'B positive' ? 'selected' : '' }}>B positive</option>
                                                    <option value="B negative" {{ $employee->blood_group == 'B negative' ? 'selected' : '' }}>B negative</option>
                                                    <option value="AB positive" {{ $employee->blood_group == 'AB positive' ? 'selected' : '' }}>AB positive</option>
                                                    <option value="AB negative" {{ $employee->blood_group == 'AB negative' ? 'selected' : '' }}>AB negative</option>
                                                </select>

                                                <span class="text-danger invalid blood_group_err"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>

                                    <!-- Present Address Section -->
                                    <div class="row mb-3">
                                        <h3>Present Address
                                            <hr>
                                        </h3>
                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="ccity">City<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" id="ccity" name="ccity" placeholder="Enter Current City" value="{{ $employee->ccity }}" readonly>
                                                <span class="text-danger invalid ccity_err"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="caddress">Present Address<span class="text-danger">*</span></label>
                                                <textarea class="form-control" name="caddress" id="caddress" readonly>{{ $employee->caddress }}</textarea>
                                                <span class="text-danger invalid caddress_err"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="cstate">State<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" id="cstate" name="cstate" placeholder="Enter Current State" value="{{ $employee->cstate }}" readonly>
                                                <span class="text-danger invalid cstate_err"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="cpincode">Pincode<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" id="cpincode" name="cpincode" placeholder="Enter Current Pincode" value="{{ $employee->cpincode }}" readonly>
                                                <span class="text-danger invalid cpincode_err"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4 mt-4">
                                            <div class="mb-3">
                                                <input class="form-check-input size-checkbox" type="checkbox" id="is_applicable" name="is_applicable" {{ $employee->is_applicable ? 'checked' : '' }} readonly>
                                                <label class="col-form-check-label" for="is_applicable">
                                                    Click to use the same address as the present address
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>

                                    <div class="row mb-3">
                                        <h3>Permanent Address
                                            <hr>
                                        </h3>
                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="pcity">City<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" id="pcity" name="pcity" placeholder="Enter Permanent City" value="{{ $employee->pcity }}" readonly>
                                                <span class="text-danger invalid pcity_err"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="paddress">Present Address<span class="text-danger">*</span></label>
                                                <textarea class="form-control" name="paddress" id="paddress" readonly>{{ $employee->paddress }}</textarea>
                                                <span class="text-danger invalid paddress_err"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="pstate">State<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" id="pstate" name="pstate" placeholder="Enter Permanent State" value="{{ $employee->pstate }}" readonly>
                                                <span class="text-danger invalid pstate_err"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="ppincode">Pincode<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" id="ppincode" name="ppincode" placeholder="Enter Permanent Pincode" value="{{ $employee->ppincode }}" readonly>
                                                <span class="text-danger invalid ppincode_err"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>

                                    <div class="row mb-3">
                                        <h3>Work Details
                                            <hr>
                                        </h3>
                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="ward_id">Select Ward<span class="text-danger">*</span></label>
                                                <select class="form-control" id="ward_id" name="ward_id" disabled>
                                                    <option value="">Select Ward</option>
                                                    @foreach ($wards as $ward)
                                                        <option value="{{ $ward->id }}" {{ $employee->ward_id == $ward->id ? 'selected' : '' }}>{{ $ward->name }}</option>
                                                    @endforeach
                                                </select>
                                                <span class="text-danger invalid ward_id_err"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="department_id">Select Department<span class="text-danger">*</span></label>
                                                <select class="form-control" id="department_id" name="department_id" disabled>
                                                    <option value="">Select Department</option>
                                                    @foreach ($departments as $department)
                                                        <option value="{{ $department->id }}" {{ $employee->department_id == $department->id ? 'selected' : '' }}>{{ $department->name }}</option>
                                                    @endforeach
                                                </select>
                                                <span class="text-danger invalid department_id_err"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="clas_id">Select Class<span class="text-danger">*</span></label>
                                                <select class="form-control" id="clas_id" name="clas_id" disabled>
                                                    <option value="">Select Class</option>
                                                    @foreach ($class as $clas)
                                                        <option value="{{ $clas->id }}" {{ $employee->clas_id == $clas->id ? 'selected' : '' }}>{{ $clas->name }}</option>
                                                    @endforeach
                                                </select>
                                                <span class="text-danger invalid clas_id_err"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="designation_id">Select Designation<span class="text-danger">*</span></label>
                                                <select class="form-control" id="designation_id" name="designation_id" disabled>
                                                    <option value="">Select Designation</option>
                                                    @foreach ($designations as $designation)
                                                        <option value="{{ $designation->id }}" {{ $employee->designation_id == $designation->id ? 'selected' : '' }}>{{ $designation->name }}</option>
                                                    @endforeach
                                                </select>
                                                <span class="text-danger invalid designation_id_err"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="shift">Select Shift<span class="text-danger">*</span></label>
                                                <select class="form-control" id="shift" name="shift" disabled>
                                                    <option value="">Select Shift</option>
                                                    <option value="1" {{ $employee->shift == 1 ? 'selected' : '' }}>General Shift</option>
                                                </select>
                                                <span class="text-danger invalid shift_err"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="working_type">Select Working Type<span class="text-danger">*</span></label>
                                                <select class="form-control" id="working_type" name="working_type" disabled>
                                                    <option value="">Select Working Type</option>
                                                    <option value="0" {{ $employee->working_type == 0 ? 'selected' : '' }}>Permanent</option>
                                                    <option value="1" {{ $employee->working_type == 1 ? 'selected' : '' }}>Temporary</option>
                                                </select>
                                                <span class="text-danger invalid working_type_err"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                                <label class="form-label" for="retirement_date">Date of Retirement<span class="text-danger">*</span></label>
                                                <input type="date" class="form-control" id="retirement_date" name="retirement_date" readonly value="{{ $employee->retirement_date }}" readonly>
                                                <span class="text-danger invalid retirement_date_err"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="mb-3">
                                            <label class="form-label" for="increment_month">Select Increment Month<span class="text-danger">*</span></label>
                                            <select class="form-control" id="increment_month" name="increment_month" disabled>
                                                <option value="">Select Increment Month</option>
                                                <option value="1" {{ $employee->increment_month == 1 ? 'selected' : '' }} >January</option>
                                                <option value="7" {{ $employee->increment_month == 7 ? 'selected' : '' }} >July</option>
                                            </select>
                                            <span class="text-danger invalid increment_month_err"></span>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="d-flex align-items-start gap-3 mt-4">
                                    <button type="button" id="personal_btn" class="btn btn-primary btn-label right ms-auto nexttab"><i class="ri-arrow-right-line label-icon align-middle fs-16 ms-2"></i>Go to Leaves</button>
                                </div>
                            </div>
                            <!-- end tab pane -->

                            <div class="tab-pane fade" id="leave-info" role="tabpanel">
                                <div>
                                    <div class="row">
                                        <h3>Leave Details
                                            <hr>
                                        </h3>

                                        <table class="table table-bordered nowrap align-middle" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <td>Leave Type</td>
                                                    <td>Carry Forward</td>
                                                    <td>No. of leaves</td>
                                                    <td>Cashable</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($leaveType as $type)
                                                    <tr>
                                                        <td>
                                                            <label class="form-label" for="des-info-description-input">{{ $type['type'] }}</label>
                                                            <input type="hidden" name="leave_type_id[]" value="{{ $type['leave_type_id'] }}">
                                                            <input type="hidden" name="employee_leaves_id[]" value="{{ $type['employee_leaves_id'] }}">
                                                        </td>
                                                        <td>
                                                            {{ $type['carry_forward'] == 1 ? 'Yes' : 'No' }}
                                                            <input type="hidden" name="carry_forward[]" value="{{ $type['carry_forward'] }}">
                                                        </td>
                                                        <td>
                                                            <input type="number" class="form-control" name="no_of_leaves[]" value="{{ $type['no_of_leaves'] }}" readonly>
                                                        </td>
                                                        <td>
                                                            <select class="form-control" name="encashable[]" disabled>
                                                                <option value="1" {{ $type['encashable'] == 1 ? 'selected' : '' }}>Yes</option>
                                                                <option value="0" {{ $type['encashable'] == 0 ? 'selected' : '' }}>No</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="d-flex align-items-start gap-3 mt-4">
                                    <button type="button" class="btn btn-light btn-label previestab" data-previous="gen-info-tab"><i class="ri-arrow-left-line label-icon align-middle fs-16 me-2"></i> Back to Personal</button>
                                    <button type="button" class="btn btn-primary btn-label right ms-auto nexttab" data-nexttab="qualification-info-tab"><i class="ri-arrow-right-line label-icon align-middle fs-16 ms-2"></i>Next
                                        Qualification</button>
                                </div>
                            </div>
                            <!-- end tab pane -->

                            <div class="tab-pane fade" id="qualification-info" role="tabpanel">
                                <div>
                                    <div class="row">
                                        <h3>Academic Details
                                            <hr>
                                        </h3>

                                        <table class="table table-bordered nowrap align-middle" style="width:100%" id="educationTable">
                                            <thead>
                                                <tr>
                                                    <th>From MM/YY</th>
                                                    <th>To MM/YY</th>
                                                    <th>Certificate Gained </th>
                                                    <th>Board / University</th>
                                                    <th>Marks / Grade</th>
                                                    <th>Document</th>
                                                </tr>
                                            </thead>
                                            <tbody id="educationBody">

                                                @foreach ($academicDetails as $key => $value)
                                                    <tr>
                                                        <td>
                                                            <input type="hidden" name="academicDetailsId[]" value="{{ $value->id }}">
                                                            <input type="date" class="form-control" name="qfrom[]" placeholder="From MM/YY" value="{{ $value->qfrom }}">
                                                        </td>
                                                        <td>
                                                            <input type="date" class="form-control" name="qto[]" placeholder="To MM/YY" value="{{ $value->qto }}">
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="qcertificate[]" placeholder="Certificate Gained" value="{{ $value->qcertificate }}">
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="qboard[]" placeholder="Board / University" value="{{ $value->qboard }}">
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="qmarks[]" placeholder="Marks / Grade" value="{{ $value->qmarks }}">
                                                        </td>
                                                        <td>
                                                            <a href="{{ asset('storage/' . $value->qdocument) }}" target="_blank" class="btn btn-primary w-100 mb-4">View Uploaded File</a>
                                                            <input type="hidden" name="existingqdocument[]" value="{{ $value->qdocument }}">
                                                            <input type="file" class="form-control" name="qdocument[]" placeholder="Document">
                                                        </td>

                                                    </tr>
                                                @endforeach

                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                                <div class="d-flex align-items-start gap-3 mt-4">
                                    <button type="button" class="btn btn-light btn-label previestab" data-previous="leave-info-tab"><i class="ri-arrow-left-line label-icon align-middle fs-16 me-2"></i> Back to Leaves</button>
                                    <button type="button" class="btn btn-primary btn-label right ms-auto nexttab" data-nexttab="experience-info-tab"><i class="ri-arrow-right-line label-icon align-middle fs-16 ms-2"></i>Next Experience</button>
                                </div>
                            </div>
                            <!-- end tab pane -->

                            <div class="tab-pane fade" id="experience-info" role="tabpanel">
                                <div>
                                    <div class="row">
                                        <h3>Work Experience
                                            <hr>
                                        </h3>

                                        <table class="table table-bordered nowrap align-middle" style="width:100%" id="experienceTable">
                                            <thead>
                                                <tr>
                                                    <th>From MM/YY</th>
                                                    <th>To MM/YY</th>
                                                    <th>Name & Address of Organisation </th>
                                                    <th>Designation</th>
                                                </tr>
                                            </thead>
                                            <tbody id="experienceBody">

                                                @foreach ($experience as $key => $value)
                                                    <tr>
                                                        <td>
                                                            <input type="hidden" name="experienceId[]" value="{{ $value->id }}">
                                                            <input type="date" class="form-control" name="efrom[]" placeholder="From MM/YY" value="{{ $value->efrom }}" readonly>
                                                        </td>
                                                        <td>
                                                            <input type="date" class="form-control" name="eto[]" placeholder="To MM/YY" value="{{ $value->eto }}" readonly>
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="ename_address[]" placeholder="Name & Address of Organisation" value="{{ $value->ename_address }}" readonly>
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="edesignation[]" placeholder="Designation" value="{{ $value->edesignation }}" readonly>
                                                        </td>
                                                    </tr>
                                                @endforeach

                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                                <div class="d-flex align-items-start gap-3 mt-4">
                                    <button type="button" class="btn btn-light btn-label previestab" data-previous="qualification-info-tab"><i class="ri-arrow-left-line label-icon align-middle fs-16 me-2"></i> Back to Qualification</button>
                                    <button type="button" class="btn btn-primary btn-label right ms-auto nexttab" data-nexttab="important-documents-info-tab"><i class="ri-arrow-right-line label-icon align-middle fs-16 ms-2"></i>Next Important Documents</button>
                                </div>
                            </div>
                            <!-- end tab pane -->

                            <div class="tab-pane fade" id="important-documents-info" role="tabpanel">
                                <div>
                                    <div class="row">
                                        <h3>Important Documents <hr></h3>

                                        <table class="table table-bordered nowrap align-middle" style="width:100%" id="importantDocumentTable">
                                            <thead>
                                                <tr>
                                                    <th>Document</th>
                                                    <th>File</th>
                                                    <th>Remark	</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="importantDocumentBody">

                                                @foreach ($importantDocuments as $key => $value)
                                                    <tr>
                                                        <td>
                                                            <input type="hidden" name="importantDocumentId[]" value="{{ $value->id }}">
                                                            <select name="document_id[]" id="document_id" class="form-control">
                                                                <option value="">Select Document</option>
                                                                @foreach ($documents as $document)
                                                                    <option value="{{ $document->id }}" {{ ($document->id == $value->document_id)?'Selected':'' }}    >{{ $document->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <a href="{{ asset('storage/' . $value->impdocument) }}" target="_blank" class="btn btn-primary w-100 mb-4">View Uploaded File</a>
                                                            <input type="hidden" name="existingimpdocument[]" value="{{ $value->impdocument }}">
                                                            <input type="file" class="form-control" name="impdocument[]" placeholder="Important Document">
                                                        </td>
                                                        <td>
                                                            <textarea name="impremark[]" id="impremark" class="form-control" placeholder="Remark">{{ $value->impremark }}</textarea>
                                                        </td>
                                                    </tr>
                                                @endforeach

                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                                <div class="d-flex align-items-start gap-3 mt-4">
                                    <button type="button" class="btn btn-light btn-label previestab" data-previous="experience-info-tab"><i class="ri-arrow-left-line label-icon align-middle fs-16 me-2"></i> Back to Experience</button>
                                    <button type="button" class="btn btn-primary btn-label right ms-auto nexttab" data-nexttab="bank-info-tab"><i class="ri-arrow-right-line label-icon align-middle fs-16 ms-2"></i>Next Bank</button>
                                </div>
                            </div>
                            <!-- end tab pane -->

                            <div class="tab-pane fade" id="bank-info" role="tabpanel">
                                <div>
                                    <!-- First Name, Middle Name, Last Name fields -->
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="mb-3">
                                                <label class="form-label" for="fname">Please Select Bank<span class="text-danger">*</span></label>
                                                <select name="bank_id" id="bank_id" class="form-control" disabled>
                                                    <option value="">Select Bank</option>
                                                    @foreach ($banks as $key => $bank)
                                                        <option value="{{ $bank->id }}" {{ $employee->bank_id == $bank->id ? 'selected' : '' }}>{{ $bank->name }}</option>
                                                    @endforeach
                                                </select>
                                                <span class="text-danger invalid bank_id_err"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="mb-3">
                                                <label class="form-label" for="branch">Bank Branch<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" id="branch" name="branch" placeholder="Enter Bank Branch" value="{{ $employee->branch }}" readonly>
                                                <span class="text-danger invalid branch_err"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="mb-3">
                                                <label class="form-label" for="account_no">Account Number<span class="text-danger">*</span></label>
                                                <input type="number" class="form-control" id="account_no" name="account_no" placeholder="Enter Account Number" value="{{ $employee->account_no }}" readonly>
                                                <span class="text-danger invalid account_no_err"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="mb-3">
                                                <label class="form-label" for="ifsc">IFSC Code<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" id="ifsc" name="ifsc" placeholder="Enter Account Number" value="{{ $employee->ifsc }}" readonly>
                                                <span class="text-danger invalid ifsc_err"></span>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="d-flex align-items-start gap-3 mt-4">
                                    <button type="button" class="btn btn-light btn-label previestab" data-previous="important-documents-info-tab"><i class="ri-arrow-left-line label-icon align-middle fs-16 me-2"></i> Back to Important Documents</button>
                                    <button type="submit" class="btn btn-primary btn-label right ms-auto" id="addSubmit"><i class="ri-arrow-right-line label-icon align-middle fs-16 ms-2"></i>Submit</button>
                                </div>
                            </div>
                            <!-- end tab pane -->

                        </div>
                        <!-- end tab content -->
                    </form>
                </div>
                <!-- end card body -->
            </div>
            <!-- end card -->
        </div>
    </div>

</x-admin.layout>
{{-- <script src="{{ asset('admin/js/employee-validation.js') }}" defer></script> --}}

