<x-admin.layout>
    <x-slot name="title">Transfer Employee</x-slot>
    <x-slot name="heading">Transfer Employee</x-slot>

    <!-- Add Form -->
    <div class="row" id="addContainer">
        <div class="col-sm-12">
            <div class="card">
                <header class="card-header">
                    <h4 class="card-title">Transfer Employee</h4>
                </header>
                <form class="theme-form" name="addForm" id="addForm" enctype="multipart/form-data">
                    @csrf

                    <div class="card-body">
                        <div class="mb-3 row">

                            <div class="col-md-3 mt-2">
                                <label class="col-form-label" for="Emp_Code">Employee Id<span class="text-danger">*</span></label>
                                <input class="form-control" id="Emp_Code" name="Emp_Code" type="text" placeholder="Enter Employee Id">
                                <span class="text-danger invalid Emp_Code_err"></span>
                            </div>

                            <div class="col-md-3 mt-5">
                                <button type="button" class="btn btn-primary" id="searchBtn">Search Employee</button>
                            </div>
                        </div>

                        <div class="mb-3 row">

                            <input type="hidden" name="employee_id" id="employee_id">

                            <div class="col-md-4">
                                <label class="col-form-label" for="emp_name">Employee Name<span class="text-danger">*</span></label>
                                <input class="form-control" id="emp_name" name="emp_name" type="text" placeholder="Employee Name" readonly>
                                <span class="text-danger invalid emp_name_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="current_ward_value">Ward<span class="text-danger">*</span></label>
                                <input class="form-control" id="current_ward_value" name="current_ward_value" type="text" placeholder="Employee Ward" readonly>
                                <input id="current_ward" name="current_ward" type="hidden">
                                <span class="text-danger invalid current_ward_value_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="current_department_value">Department<span class="text-danger">*</span></label>
                                <input class="form-control" id="current_department_value" name="current_department_value" type="text" placeholder="Employee Department" readonly>
                                <input id="current_department" name="current_department" type="hidden">
                                <span class="text-danger invalid current_department_value_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="designation">Designation<span class="text-danger">*</span></label>
                                <input class="form-control" id="designation" name="designation" type="text" placeholder="Employee Designation" readonly>
                                <span class="text-danger invalid designation_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="class">Class<span class="text-danger">*</span></label>
                                <input class="form-control" id="class" name="class" type="text" placeholder="Employee Class" readonly>
                                <span class="text-danger invalid class_err"></span>
                            </div>
                        </div>
                        <hr>

                        <div class="mb-3 row" id="status_div" style="display: none;">
                            <h2>Add Employee Transfer Details</h2>
                            <div class="col-lg-4">
                                <div class="mb-3">
                                <label class="form-label" for="ward_id">Select Ward<span class="text-danger">*</span></label>
                                <select class="form-control" id="ward_id" name="ward_id">
                                    <option value="">Select Ward</option>
                                    @foreach ($wards as $ward)
                                        <option value="{{ $ward->id }}" {{ old('ward_id') == $ward->id ? 'selected' : '' }} >{{ $ward->name }}</option>
                                    @endforeach
                                </select>
                                <span class="text-danger invalid ward_id_err"></span>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="mb-3">
                                <label class="form-label" for="department_id">Select Department<span class="text-danger">*</span></label>
                                <select class="form-control" id="department_id" name="department_id">
                                    <option value="">Select Department</option>
                                </select>
                                <span class="text-danger invalid department_id_err"></span>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="mb-3">
                                <label class="form-label" for="clas_id">Select Class<span class="text-danger">*</span></label>
                                <select class="form-control" id="clas_id" name="clas_id">
                                    <option value="">Select Class</option>
                                    @foreach ($class as $clas)
                                    <option value="{{ $clas->id }}" {{ old('clas_id') == $clas->id ? 'selected' : '' }} >{{ $clas->name }}</option>
                                    @endforeach
                                </select>
                                <span class="text-danger invalid clas_id_err"></span>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="mb-3">
                                <label class="form-label" for="designation_id">Select Designation<span class="text-danger">*</span></label>
                                <select class="form-control" id="designation_id" name="designation_id">
                                    <option value="">Select Designation</option>
                                </select>
                                <span class="text-danger invalid designation_id_err"></span>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="mb-3">
                                <label class="form-label" for="transfer_order">Transfer Order<span class="text-danger">*</span></label>
                                <input class="form-control" id="transfer_order" name="transfer_order" type="file">
                                <span class="text-danger invalid transfer_order_err"></span>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="mb-3">
                                <label class="form-label" for="transfer_date">Transfer Date<span class="text-danger">*</span></label>
                                <input class="form-control" id="transfer_date" name="transfer_date" type="date">
                                <span class="text-danger invalid transfer_date_err"></span>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="mb-3">
                                <label class="form-label" for="remark">Remark<span class="text-danger">*</span></label>
                                <input class="form-control" id="remark" name="remark" type="text" placeholder="Enter Remark">
                                <span class="text-danger invalid remark_err"></span>
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" id="addSubmit">Submit</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


</x-admin.layout>


{{-- Add --}}
<script>

$("#ward_id").on("change", function (e) {
    var ward_id = this.value;
    if(ward_id != ''){
        var url = "{{ route('fetch-departments', ':ward_id') }}";

        $.ajax({
            url: url.replace(":ward_id", ward_id),
            type: "GET",
            data: {
                _method: "GET",
                _token: "{{ csrf_token() }}",
            },
            success: function (data, textStatus, jqXHR) {
                if (!data.error && !data.error2) {
                    $("#department_id").html(data.deptHtml);
                } else {
                    alert(data.error);
                }
            },
            error: function (error, jqXHR, textStatus, errorThrown) {
                swal("Error!", "Something went wrong", "error");
            },
        });
    }
});

$("#ward_id,#department_id,#clas_id").on("change", function (e) {
    var ward_id = $("#ward_id").val();
    var department_id = $("#department_id").val();
    var clas_id = $("#clas_id").val();

    var url =
        "{{ route('fetch-designations', [':ward_id', ':department_id', ':class_id']) }}";

    // Replace placeholders with actual values
    url = url
        .replace(":ward_id", ward_id)
        .replace(":department_id", department_id)
        .replace(":class_id", clas_id);
    if (ward_id != "" && department_id != "" && clas_id != "") {
        $.ajax({
            url: url,
            type: "GET",
            data: {
                _method: "GET",
                _token: "{{ csrf_token() }}",
            },
            success: function (data, textStatus, jqXHR) {
                if (!data.error && !data.error2) {
                    $("#designation_id").html(data.desHtml);
                } else {
                    alert(data.error);
                }
            },
            error: function (error, jqXHR, textStatus, errorThrown) {
                swal("Error!", "Something went wrong", "error");
            },
        });
    }
});

    $("#addForm").submit(function(e) {
        e.preventDefault();
        $("#addSubmit").prop('disabled', true);

        var formdata = new FormData(this);
        $.ajax({
            url: '{{ route('employee-transfer.store') }}',
            type: 'POST',
            data: formdata,
            contentType: false,
            processData: false,
            success: function(data) {
                $("#addSubmit").prop('disabled', false);
                if (!data.error2)
                    swal("Successful!", data.success, "success")
                    .then((action) => {
                        window.location.href = '{{ route('employee-transfer.index') }}';
                    });
                else
                    swal("Error!", data.error2, "error");
            },
            statusCode: {
                422: function(responseObject, textStatus, jqXHR) {
                    $("#addSubmit").prop('disabled', false);
                    resetErrors();
                    printErrMsg(responseObject.responseJSON.errors);
                },
                500: function(responseObject, textStatus, errorThrown) {
                    $("#addSubmit").prop('disabled', false);
                    swal("Error occured!", "Something went wrong please try again", "error");
                }
            }
        });

    });


    $(document).ready(function() {
        $("#searchBtn").click(function(){
            var Emp_Code = $('#Emp_Code').val();
            if(Emp_Code != '')
            {
                var url = "{{ route('fetch-employee-details', ':Emp_Code') }}";

                $.ajax({
                    url: url.replace(':Emp_Code', Emp_Code),
                    type: 'GET',
                    data: {
                        '_method': "GET",
                        '_token': "{{ csrf_token() }}"
                    },
                    success: function(data) {
                        if (!data.error && !data.error2) {
                            if (data.result === 1) {

                                var full_name = data.employee_details.fname + " " + data.employee_details.mname + " " + data.employee_details.lname;
                                $('#emp_name').val(full_name);
                                $('#current_ward_value').val(data.employee_details.ward.name);
                                $('#current_ward').val(data.employee_details.ward.id);
                                $('#current_department_value').val(data.employee_details.department.name);
                                $('#current_department').val(data.employee_details.department.id);
                                $('#class').val(data.employee_details.class.name);
                                $('#designation').val(data.employee_details.designation.name);
                                $('#employee_id').val(data.employee_details.id);

                                $('#status_div').removeAttr('style');

                            } else if (data.result === 0) {
                                $('#status_div').hide();
                                $('#status_id').val('');
                                $('#remark').val('');
                                $('#emp_name').val('');
                                $('#current_ward_value').val('');
                                $('#current_department_value').val('');
                                $('#current_ward').val('');
                                $('#current_department').val('');
                                $('#designation').val('');
                                $('#class').val('');

                                alert("Employee details not found or Employee Status already added!");
                            } else {
                                alert("Unexpected result from the server");
                            }
                    }
                    },
                    error: function(error, jqXHR, textStatus, errorThrown) {
                        swal("Error!", "Something went wrong", "error");
                    },
                });
            }
            else{
                alert('Please Enter Employee Id');
            }

        });
    });


</script>

