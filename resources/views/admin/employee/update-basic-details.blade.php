    <x-admin.layout>
        {{-- <x-slot name="title">Update Basic Details</x-slot> --}}
        <x-slot name="title">Update Basic Details</x-slot>
        <x-slot name="heading"></x-slot>

        <div class="page-body">
            <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                <div class="col-sm-6">
                    <h3>Update Basic Details</h3>
                </div>
                <div class="col-sm-6">

                </div>
                </div>
            </div>
            </div>
            <!-- Container-fluid starts-->
            <div class="container-fluid">
            <div class="row card">
                <div class="col-sm-12 col-xl-8 mx-auto">
                <div class="row">

                    <div class="col-sm-12">
                    <div class="">
                        <div class="card-header pb-0">
                        </div>
                        <form class="theme-form" id="editForm" method="POST">
                            @csrf
                            <div class="card-body">

                                <input type="hidden" name="edit_model_id" id="edit_model_id" value="{{ $employee_id }}">
                                <div class="mb-3 row">
                                    <label class="col-sm-3 col-form-label" for="mobile_number">Mobile No.</label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <input class="form-control" type="mobile_number" id="mobile_number" name="mobile_number" placeholder="Mobile No.">
                                        </div>
                                        <span class="text-danger invalid mobile_number_err"></span>
                                    </div>
                                </div>

                                <div class="mb-3 row">
                                    <label class="col-sm-3 col-form-label" for="email">Email</label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <input class="form-control" type="email" id="email" name="email" placeholder="Email">
                                        </div>
                                        <span class="text-danger invalid email_err"></span>
                                    </div>
                                </div>

                                <div class="mb-3 row">
                                    <label class="col-sm-3 col-form-label" for="aadhar">Aadhar Card No.</label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <input class="form-control" type="text" id="aadhar" name="aadhar" placeholder="Aadhar Card No.">
                                        </div>
                                        <span class="text-danger invalid aadhar_err"></span>
                                    </div>
                                </div>

                                <div class="mb-3 row">
                                    <label class="col-sm-3 col-form-label" for="pan">Pan Card Number</label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <input class="form-control" type="text" id="pan" name="pan" placeholder="Pan Card Number" style="text-transform: uppercase">
                                        </div>
                                        <span class="text-danger invalid pan_err"></span>
                                    </div>
                                </div>


                            </div>
                            <div class="card-footer">
                                <button class="btn btn-primary" id="updateSubmit">Submit</button>
                                <button class="btn btn-secondary" type="reset">Reset</button>
                            </div>
                        </form>
                    </div>
                    </div>

                </div>
                </div>

            </div>
            </div>
            <!-- Container-fluid Ends-->
        </div>

    </x-admin.layout>


    <!-- Update -->
    <script>
        $(document).ready(function() {
            $("#editForm").submit(function(e) {
                e.preventDefault();
                $("#updateSubmit").prop('disabled', true);
                var formdata = new FormData(this);
                formdata.append('_method', 'PUT');
                var model_id = $('#edit_model_id').val();
                var url = "{{ route('employee-basic-update', ':model_id') }}";
                $.ajax({
                    url: url.replace(':model_id', model_id),
                    type: 'POST',
                    data: formdata,
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        $("#updateSubmit").prop('disabled', false);
                        if (!data.error2)
                            swal("Successful!", data.success, "success")
                            .then((action) => {
                                window.location.href = '{{ route('salary-slips.index') }}';
                            });
                        else
                            swal("Error!", data.error2, "error");
                    },
                    statusCode: {
                        422: function(responseObject, textStatus, jqXHR) {
                            $("#updateSubmit").prop('disabled', false);
                            resetErrors();
                            printErrMsg(responseObject.responseJSON.errors);
                        },
                        500: function(responseObject, textStatus, errorThrown) {
                            $("#updateSubmit").prop('disabled', false);
                            swal("Error occured!", "Something went wrong please try again", "error");
                        }
                    }
                });

            });
        });
    </script>
