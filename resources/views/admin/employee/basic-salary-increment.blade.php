<x-admin.layout>
    <x-slot name="title">Basic Salary Increment</x-slot>
    <x-slot name="heading">Basic Salary Increment</x-slot>
    {{-- <x-slot name="subheading">Test</x-slot> --}}


    <!-- Add Form -->
    <div class="row" id="addContainer">
        <div class="col-sm-12">
            <div class="card">
                <header class="card-header">
                    <h4 class="card-title">Basic Salary Increment</h4>
                </header>
                <form class="theme-form" name="addForm" id="addForm" enctype="multipart/form-data">
                    @csrf

                    <div class="card-body">
                        <div class="mb-3 row">

                            <div class="col-md-3">
                                <label class="col-form-label" for="increment_type">Select Increment Type <span class="text-danger">*</span></label>
                                <select class="form-control js-example-basic-single" name="increment_type" id="increment_type">
                                    <option value="">Select Increment Type</option>
                                    <option value="1">Regular (Next Basic slab of 7th pay chart)</option>
                                </select>
                                <span class="text-danger invalid increment_type_err"></span>
                            </div>

                            <div class="col-md-3">
                                <label class="col-form-label" for="department_id">Select Department <span class="text-danger">*</span></label>
                                <select class="form-control js-example-basic-single" name="department_id" id="department_id">
                                    <option value="">Select Department</option>
                                    <option value="all">All</option>
                                    @foreach ($departments as $department)
                                        <option value="{{ $department->id }}">{{ $department->name }}</option>
                                    @endforeach
                                </select>
                                <span class="text-danger invalid department_id_err"></span>
                            </div>
                            <div class="col-md-3">
                                <label class="col-form-label" for="month">Select Month <span class="text-danger">*</span></label>
                                <select class="form-select js-example-basic-single" name="month" id="month">
                                    <option value="">Select Month</option>
                                    <option value="1">January</option>
                                    <option value="7">July</option>
                                </select>
                                <span class="text-danger invalid month_err"></span>
                            </div>

                            <div class="col-md-3">
                                <label class="col-form-label" for="applicable_date">Applicable Date <span class="text-danger">*</span></label>
                                <input type="date" name="applicable_date" id="applicable_date" class="form-control">
                                <span class="text-danger invalid applicable_date_err"></span>
                            </div>

                            <div class="col-md-3">
                                <button id="showBasicSalary" class="btn btn-primary mt-4">Show</button>
                                {{-- <button class="btn btn-warning mt-4">Cancel</button> --}}
                            </div>

                        </div>
                        @can('basic-salary-increment.create')
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary" id="addSubmit">Submit</button>
                            <button type="reset" class="btn btn-warning">Reset</button>
                        </div>
                        @endcan
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th>Sr No.</th>
                                                        <th>Employee Id</th>
                                                        <th>Employee Name</th>
                                                        <th>Department</th>
                                                        <th>Current Basic Salary</th>
                                                        <th>Incremented Basic Salary</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</x-admin.layout>


{{-- Add --}}
<script>
    $("#showBasicSalary").on("click", function(e) {
        e.preventDefault();

        var incrementType = $('#increment_type').val();
        var department_id = $('#department_id').val();
        var applicable_date = $('#applicable_date').val();
        var month = $('#month').val();

        if(incrementType != '' && department_id != '' && applicable_date != '' && month != '')
        {
            var url = "{{ route('get-incremented-salary') }}";
            url += '?' + $.param({
                '_token': "{{ csrf_token() }}",
                department_id: department_id,
                month: month
            });

            $.ajax({
                url: url,
                type: 'GET',
                success: function(data, textStatus, jqXHR) {
                    if (!data.error) {
                        updateTable(data.incrementArr);
                    } else {
                        alert(data.error);
                    }
                },
                error: function(error, jqXHR, textStatus, errorThrown) {
                    alert("Some thing went wrong");
                },
            });

        }else{
            alert('Please select all feilds');
        }

    });

    function updateTable(data) {
    var tableBody = $('#buttons-datatables tbody');
    tableBody.empty(); // Clear existing data

    if (Array.isArray(data) && data.length > 0) {
        var rowsHtml = '';

        $.each(data, function(index, row) {
            rowsHtml += '<tr>' +
                '<td><input type="hidden" name="salary_unique_id[]" value="' + row.salary_unique_id + '">' + $('<div>').text(row.sr_no).html() + '</td>' +
                '<td><input type="hidden" name="emp_id[]" value="' + row.employee_uniques_id + '">' + $('<div>').text(row.emp_id).html() + '</td>' +
                '<td>' + $('<div>').text(row.emp_name).html() + '</td>' +
                '<td>' + $('<div>').text(row.department).html() + '</td>' +
                '<td>' + $('<div>').text(row.current_basic_salary).html() + '</td>' +
                '<td><input type="hidden" name="incrementted_basic[]" value="' + row.increment_basic_salary + '">' + $('<div>').text(row.increment_basic_salary).html() + '</td>' +
                '</tr>';
        });

        tableBody.append(rowsHtml);

        console.log($('input[name="incrementted_basic[]"]').map(function() { return $(this).val(); }).get());

    } else {
        // Handle case where no data is returned
        var noDataRow = '<tr><td colspan="6">No data available</td></tr>';
        tableBody.append(noDataRow);
    }
}

    $("#addForm").submit(function(e) {
        e.preventDefault();

        swal({
                title: "Are you sure you want to submit?",
                icon: "info",
                buttons: ["Cancel", "Confirm"]
            })
            .then((submitForm) => {
                if(submitForm){
                    $("#addSubmit").prop('disabled', true);

                    var formdata = new FormData(this);
                    $.ajax({
                        url: '{{ route('basic-salary-increment.store') }}',
                        type: 'POST',
                        data: formdata,
                        contentType: false,
                        processData: false,
                        success: function(data) {
                            $("#addSubmit").prop('disabled', false);
                            if (!data.error2)
                                swal("Successful!", data.success, "success")
                                .then((action) => {
                                    window.location.href = '{{ route('basic-salary-increment.index') }}';
                                });
                            else
                                swal("Error!", data.error2, "error");
                        },
                        statusCode: {
                            422: function(responseObject, textStatus, jqXHR) {
                                $("#addSubmit").prop('disabled', false);
                                resetErrors();
                                printErrMsg(responseObject.responseJSON.errors);
                            },
                            500: function(responseObject, textStatus, errorThrown) {
                                $("#addSubmit").prop('disabled', false);
                                swal("Error occured!", "Something went wrong please try again", "error");
                            }
                        }
                    });
                }
            });
    });
</script>

