<x-admin.layout>
    <x-slot name="title">Monthly Festival Advance List</x-slot>
    <x-slot name="heading">Monthly Festival Advance List</x-slot>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>From Date</th>
                                    <th>To Date</th>
                                    <th>Employee Id</th>
                                    <th>Employee Name</th>
                                    <th>Festival Name</th>
                                    <th>Installment Amount</th>
                                    <th>Installment No.</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($employee_monthly_festivalAdvances as $employee_monthly_festivalAdvance)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $employee_monthly_festivalAdvance->from_date }}</td>
                                        <td>{{ $employee_monthly_festivalAdvance->to_date }}</td>
                                        <td>{{ $employee_monthly_festivalAdvance->Emp_Code }}</td>
                                        <td>{{ $employee_monthly_festivalAdvance->emp_name }}</td>
                                        <td>{{ $employee_monthly_festivalAdvance?->festivalAdvance?->festival_name }}</td>
                                        <td>{{ $employee_monthly_festivalAdvance->installment_amount }}</td>
                                        <td>{{ $employee_monthly_festivalAdvance->installment_no }}</td>
                                        <td>
                                            @can('monthly-festivalAdvance.stop')
                                                <button class="edit-element btn btn-secondary px-2 py-1 status-btn" title="Show festival Advance" data-id="{{ $employee_monthly_festivalAdvance->id }}">
                                                    Stop for this month
                                                </button>
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Add Remark --}}
    <div class="modal fade" id="add-remark-modal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form action="" id="changeStatusForm">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Add Remark</h5>
                        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">

                        <input type="hidden" id="unique_id" name="unique_id">

                        <div class="col-8 mx-auto my-2">
                            <div class="form-group">
                                <label>Add Remark</label>
                                <div class="input-group"><span class="input-group-text"><i data-feather="activity"></i></span>
                                    <input class="form-control" type="text" id="remark" name="remark">
                                </div>
                                <span class="text-danger error-text remark_err"></span>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Cancel</button>
                        <button class="btn btn-primary" id="changeStatusSubmit" type="submit">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>


</x-admin.layout>

<!-- Delete -->
<script>

$("#buttons-datatables").on("click", ".status-btn", function(e) {
        e.preventDefault();

        swal({
                title: "Are you sure to Stop this Festival Deduction ?",
                icon: "info",
                buttons: ["Cancel", "Confirm"]
            })
            .then((justTransfer) => {
                if (justTransfer) {

                    var id = $(this).attr("data-id");
                    $('#unique_id').val(id);
                    $('#add-remark-modal').modal('show');
                }
            });
    });


    $("#changeStatusForm").submit(function(e) {
        e.preventDefault();
        $("#changeStatusSubmit").prop('disabled', true);

        var formdata = new FormData(this);
        formdata.append('_method', 'POST');

        var model_id = $('#unique_id').val();
        var url = "{{ route('monthly-festivalAdvance-status', [':model_id']) }}";

        url = url.replace(':model_id', model_id);

        $.ajax({
            url: url,
            type: 'POST',
            data: formdata,
            contentType: false,
            processData: false,
            success: function(data) {
                $("#changeStatusSubmit").prop('disabled', false);
                if (!data.error2)
                    swal("Successful!", data.success, "success")
                    .then((action) => {
                        $("#add-remark-modal").modal('hide');
                        $("#changeStatusSubmit").prop('disabled', false);
                        window.location.reload();
                    });
                else
                    swal("Error!", data.error2, "error");
            },
            statusCode: {
                422: function(responseObject, textStatus, jqXHR) {
                    $("#changeStatusSubmit").prop('disabled', false);
                    resetErrors();
                    printErrMsg(responseObject.responseJSON.errors);
                },
                500: function(responseObject, textStatus, errorThrown) {
                    $("#changeStatusSubmit").prop('disabled', false);
                    swal("Error occured!", "Something went wrong please try again", "error");
                }
            }
        });

        function resetErrors() {
            var form = document.getElementById('changeStatusForm');
            var data = new FormData(form);
            for (var [key, value] of data) {
                $('.' + key + '_err').text('');
                $('#' + key).removeClass('is-invalid');
                $('#' + key).addClass('is-valid');
            }
        }

        function printErrMsg(msg) {
            $.each(msg, function(key, value) {
                $('.' + key + '_err').text(value);
                $('#' + key).addClass('is-invalid');
                $('#' + key).removeClass('is-valid');
            });
        }

    });

</script>
