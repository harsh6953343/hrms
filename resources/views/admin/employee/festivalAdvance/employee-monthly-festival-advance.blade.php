<x-admin.layout>
    <x-slot name="title">Monthly Festival Advance</x-slot>
    <x-slot name="heading">Monthly Festival Advance</x-slot>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>From Date</th>
                                    <th>To Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($employee_monthly_festivalAdvances as $monthly_festivalAdvance)
                                    @php
                                        $from_date = $monthly_festivalAdvance->from_date;
                                        $to_date = $monthly_festivalAdvance->to_date;
                                    @endphp
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $monthly_festivalAdvance->from_date }}</td>
                                        <td>{{ $monthly_festivalAdvance->to_date }}</td>
                                        <td>
                                            {{-- @can('employee-monthly-loans.view') --}}
                                            <a href="{{ route('monthly-festival-advance', ['from_date' => $from_date, 'to_date' => $to_date]) }}">
                                                <button class="edit-element btn btn-secondary px-2 py-1" title="Show Festival Advance" data-id="{{ $from_date }}">
                                                    <i data-feather="eye"></i>
                                                </button>
                                            </a>
                                            {{-- @endcan --}}
                                        </td>
                                    </tr>
                                @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</x-admin.layout>

