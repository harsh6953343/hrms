<x-admin.layout>
    <x-slot name="title">DA Differance</x-slot>
    <x-slot name="heading">DA Differance</x-slot>

    <!-- Add Form -->
    <div class="row" id="addContainer" style="display:none;">
        <div class="col-sm-12">
            <div class="card">
                <header class="card-header">
                    <h4 class="card-title">Add DA Differance</h4>
                </header>
                <form class="theme-form" name="addForm" id="addForm" enctype="multipart/form-data">
                    @csrf

                    <div class="card-body">
                        <div class="mb-3 row">
                            <div class="col-md-4">
                                <label class="col-form-label" for="DA_currentRate">DA Current Rate<span class="text-danger">*</span></label>
                                <input class="form-control title" id="DA_currentRate" name="DA_currentRate" type="text" placeholder="Enter DA Current Rate">
                                <span class="text-danger invalid DA_currentRate_err"></span>
                            </div>
                            <div class="col-md-4">
                                <label class="col-form-label" for="DA_newRate">DA New Rate<span class="text-danger">*</span></label>
                                <input class="form-control title" id="DA_newRate" name="DA_newRate" type="text" placeholder="Enter DA New Rate">
                                <span class="text-danger invalid DA_newRate_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="applicable_month">Applicable Month<span class="text-danger">*</span></label>
                                <select class="form-control" id="applicable_month" name="applicable_month">
                                    <option value="">Select Applicable Month</option>
                                    <option value="1" {{ (old('applicable_month') == 1)?'Selected':'' }} >January</option>
                                    <option value="2" {{ (old('applicable_month') == 2)?'Selected':'' }}>February</option>
                                    <option value="3" {{ (old('applicable_month') == 3)?'Selected':'' }}>March</option>
                                    <option value="4" {{ (old('applicable_month') == 4)?'Selected':'' }}>April</option>
                                    <option value="5" {{ (old('applicable_month') == 5)?'Selected':'' }}>May</option>
                                    <option value="6" {{ (old('applicable_month') == 6)?'Selected':'' }}>June</option>
                                    <option value="7" {{ (old('applicable_month') == 7)?'Selected':'' }}>July</option>
                                    <option value="8" {{ (old('applicable_month') == 8)?'Selected':'' }}>August</option>
                                    <option value="9" {{ (old('applicable_month') == 9)?'Selected':'' }}>September</option>
                                    <option value="10" {{ (old('applicable_month') == 10)?'Selected':'' }}>October</option>
                                    <option value="11" {{ (old('applicable_month') == 11)?'Selected':'' }}>November</option>
                                    <option value="12" {{ (old('applicable_month') == 12)?'Selected':'' }}>December</option>
                                </select>
                                <span class="text-danger invalid applicable_month_err"></span>
                            </div>

                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" id="addSubmit">Submit</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                {{-- @can('allowance.create') --}}
                    <div class="card-header">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="">
                                    <button id="addToTable" class="btn btn-primary">Add <i class="fa fa-plus"></i></button>
                                    <button id="btnCancel" class="btn btn-danger" style="display:none;">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                {{-- @endcan --}}
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>Current DA Rate</th>
                                    <th>New DA Rate</th>
                                    <th>Applicable Month</th>
                                    <th>Applied Month</th>
                                    <th>No. of Months</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($daDifferances as $daDifferance)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $daDifferance->DA_currentRate }}</td>
                                        <td>{{ $daDifferance->DA_newRate }}</td>
                                        <td>{{ $daDifferance->applicable_month }}</td>
                                        <td>{{ $daDifferance->given_month }}</td>
                                        <td>{{ $daDifferance->no_of_month }}</td>
                                        <td>
                                            <a href="{{ route('da-differance.show',$daDifferance->id) }}" class="edit-element btn btn-secondary px-2 py-1" title="Show Employee">
                                                <i data-feather="eye"></i>
                                            </a>

                                            {{-- @can('allowance.delete')  --}}
                                                {{-- <button class="btn btn-danger rem-element px-2 py-1" title="Delete DA Differance" data-id="{{ $daDifferance->id }}"><i data-feather="trash-2"></i> </button> --}}
                                            {{-- @endcan --}}


                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




</x-admin.layout>


{{-- Add --}}
<script>

    $(document).ready(function() {

        var url = "{{ route('fetch-current-da') }}";
        $.ajax({
            url: url,
            type: "GET",
            data: {
                _method: "GET",
                _token: "{{ csrf_token() }}",
            },
            success: function (data, textStatus, jqXHR) {
                if (!data.error) {
                    $("#DA_currentRate").val(data.allowance_arr.amount);
                } else {
                    alert(data.error);
                }
            },
            error: function (error, jqXHR, textStatus, errorThrown) {
                swal("Error!", "Something went wrong", "error");
            },
        });
    });

    $("#DA_newRate").on("change", function (e) {

        var DA_currentRate  = $('#DA_currentRate').val();
        var DA_newRate      = $("#DA_newRate").val();
        if(parseInt(DA_newRate) < parseInt(DA_currentRate)){

            alert('DA New Rate Should be greater than current rate');
            $("#DA_newRate").val('');
        }

    });

    $("#addForm").submit(function(e) {
        e.preventDefault();
        $("#addSubmit").prop('disabled', true);
        var formdata = new FormData(this);
        $.ajax({
            url: '{{ route('da-differance.store') }}',
            type: 'POST',
            data: formdata,
            contentType: false,
            processData: false,
            success: function(data) {
                $("#addSubmit").prop('disabled', false);
                if (!data.error2)
                    swal("Successful!", data.success, "success")
                    .then((action) => {
                        window.location.href = '{{ route('da-differance.index') }}';
                    });
                else
                    swal("Error!", data.error2, "error");
            },
            statusCode: {
                422: function(responseObject, textStatus, jqXHR) {
                    $("#addSubmit").prop('disabled', false);
                    resetErrors();
                    printErrMsg(responseObject.responseJSON.errors);
                },
                500: function(responseObject, textStatus, errorThrown) {
                    $("#addSubmit").prop('disabled', false);
                    swal("Error occured!", "Something went wrong please try again", "error");
                }
            }
        });

    });
</script>


<!-- Delete -->
<script>
    $("#buttons-datatables").on("click", ".rem-element", function(e) {
        e.preventDefault();
        swal({
                title: "Are you sure to delete this DA Differance?",
                icon: "info",
                buttons: ["Cancel", "Confirm"]
            })
            .then((justTransfer) => {
                if (justTransfer) {
                    var model_id = $(this).attr("data-id");
                    var url = "{{ route('da-differance.destroy', ':model_id') }}";

                    $.ajax({
                        url: url.replace(':model_id', model_id),
                        type: 'POST',
                        data: {
                            '_method': "DELETE",
                            '_token': "{{ csrf_token() }}"
                        },
                        success: function(data, textStatus, jqXHR) {
                            if (!data.error && !data.error2) {
                                swal("Success!", data.success, "success")
                                    .then((action) => {
                                        window.location.reload();
                                    });
                            } else {
                                if (data.error) {
                                    swal("Error!", data.error, "error");
                                } else {
                                    swal("Error!", data.error2, "error");
                                }
                            }
                        },
                        error: function(error, jqXHR, textStatus, errorThrown) {
                            swal("Error!", "Something went wrong", "error");
                        },
                    });
                }
            });
    });
</script>
