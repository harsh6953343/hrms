<x-admin.layout>
    <x-slot name="title">Employee Status</x-slot>
    <x-slot name="heading">Employee Status</x-slot>

    <!-- Add Form -->
    <div class="row" id="addContainer">
        <div class="col-sm-12">
            <div class="card">
                <header class="card-header">
                    <h4 class="card-title">Edit Employee Status</h4>
                </header>
                <form class="form-horizontal form-bordered" method="post" id="editForm">
                    @csrf

                    <div class="card-body">
                        <div class="mb-3 row">

                            <div class="col-md-3 mt-2">
                                <label class="col-form-label" for="Emp_Code">Employee Id<span class="text-danger">*</span></label>
                                <input class="form-control" id="Emp_Code" name="Emp_Code" type="number" placeholder="Enter Employee Id" value="{{ $employeeStatus->Emp_Code }}" readonly>
                                <span class="text-danger invalid Emp_Code_err"></span>
                            </div>

                        </div>

                        <div class="mb-3 row">

                            <input type="hidden" name="edit_model_id" id="edit_model_id" value="{{ $employeeStatus->id }}">

                            <div class="col-md-4">
                                <label class="col-form-label" for="emp_name">Employee Name<span class="text-danger">*</span></label>
                                <input class="form-control" id="emp_name" name="emp_name" type="text" placeholder="Employee Name" readonly value="{{ $employeeStatus->employee->fname." ".$employeeStatus->employee->mname." ".$employeeStatus->employee->lname }}">
                                <span class="text-danger invalid emp_name_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="ward">Ward<span class="text-danger">*</span></label>
                                <input class="form-control" id="ward" name="ward" type="text" placeholder="Employee Ward" readonly value="{{ $employeeStatus->employee->ward->name }}">
                                <span class="text-danger invalid ward_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="department">Department<span class="text-danger">*</span></label>
                                <input class="form-control" id="department" name="department" type="text" placeholder="Employee Department" readonly value="{{ $employeeStatus->employee->department->name }}">
                                <span class="text-danger invalid department_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="designation">Designation<span class="text-danger">*</span></label>
                                <input class="form-control" id="designation" name="designation" type="text" placeholder="Employee Designation" readonly value="{{ $employeeStatus->employee->designation->name }}">
                                <span class="text-danger invalid designation_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="class">Class<span class="text-danger">*</span></label>
                                <input class="form-control" id="class" name="class" type="text" placeholder="Employee Class" readonly value="{{ $employeeStatus->employee->class->name }}">
                                <span class="text-danger invalid class_err"></span>
                            </div>
                        </div>
                        <hr>

                        <div class="mb-3 row">
                            <h2>Edit Employee Status</h2>

                            <div class="col-md-4 mb-3">
                                <label class="col-form-label" for="status_id">Status <b>(Cannto change status)</b><span class="text-danger">*</span></label>
                                <input class="form-control" id="status_id" name="status_id" type="text" placeholder="Enter Status" readonly value="{{ $employeeStatus?->status?->name }}">
                                <span class="text-danger invalid status_id_err"></span>
                            </div>

                            <div class="col-md-4 mb-3">
                                <label class="col-form-label" for="applicable_date">Applicable Date<span class="text-danger">*</span></label>
                                <input class="form-control" id="applicable_date" name="applicable_date" type="date" placeholder="Enter Applicable Date" readonly value="{{ $employeeStatus?->applicable_date }}">
                                <span class="text-danger invalid applicable_date_err"></span>
                            </div>
                        </div>
                        <hr>
                        <div class="mb-3 row">

                            <div class="col-md-4 mb-3">
                                <label class="col-form-label" for="remark">Remark<span class="text-danger">*</span></label>
                                <input class="form-control" id="remark" name="remark" type="text" placeholder="Enter Remark" value="{{ $employeeStatus?->remark }}">
                                <span class="text-danger invalid remark_err"></span>
                            </div>

                            <div class="col-md-4 mb-3">
                                <label label="" class="col-form-label" for="is_salary_applicable">Is Salary Applicable ? <span class="text-danger"></span></label>
                                <div class="form-check mb-2">
                                    <input class="form-check-input is_salary_applicable" type="radio" name="is_salary_applicable" id="is_salary_applicable1" value="1" {{ ($employeeStatus?->is_salary_applicable == 1)?'Checked':'' }} >
                                    <label class="form-check-label" for="is_salary_applicable1">
                                        Yes
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input is_salary_applicable" type="radio" name="is_salary_applicable" id="is_salary_applicable2" value="0" {{ ($employeeStatus?->is_salary_applicable == 0)?'Checked':'' }}>
                                    <label class="form-check-label" for="is_salary_applicable2">
                                        No
                                    </label>
                                </div>
                                <span class="text-danger invalid is_salary_applicable_err"></span>
                            </div>

                            <div class="col-md-4 mb-3" id="salary_percent_div"  @if ($employeeStatus && $employeeStatus->is_salary_applicable == 0)
                                    style="display: none;"
                                @endif>
                                <label class="col-form-label" for="salary_percent">Salary Percent<span class="text-danger">*</span></label>
                                <input class="form-control" type="number" name="salary_percent" id="salary_percent" value="{{ $employeeStatus?->salary_percent }}">
                                {{-- <select class="form-control" id="salary_percent" name="salary_percent">
                                    <option value="">Select  Salary Percent</option>
                                    <option value="1" {{ ($employeeStatus?->salary_percent == 1)?'Selected':'' }}>25%</option>
                                    <option value="2" {{ ($employeeStatus?->salary_percent == 2)?'Selected':'' }}>50%</option>
                                    <option value="3" {{ ($employeeStatus?->salary_percent == 3)?'Selected':'' }}>75%</option>
                                    <option value="4" {{ ($employeeStatus?->salary_percent == 4)?'Selected':'' }}>100%</option>
                                </select> --}}
                                <span class="text-danger invalid salary_percent_err"></span>
                            </div>

                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" id="addSubmit">Submit</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


</x-admin.layout>


<!-- Update -->
<script>
    $(document).ready(function() {
        $("#editForm").submit(function(e) {
            e.preventDefault();
            $("#editSubmit").prop('disabled', true);
            var formdata = new FormData(this);
            formdata.append('_method', 'PUT');
            var model_id = $('#edit_model_id').val();
            var url = "{{ route('employee-status.update', ':model_id') }}";
            //
            $.ajax({
                url: url.replace(':model_id', model_id),
                type: 'POST',
                data: formdata,
                contentType: false,
                processData: false,
                success: function(data) {
                    $("#editSubmit").prop('disabled', false);
                    if (!data.error2)
                        swal("Successful!", data.success, "success")
                        .then((action) => {
                            window.location.href = '{{ route('employee-status.index') }}';
                        });
                    else
                        swal("Error!", data.error2, "error");
                },
                statusCode: {
                    422: function(responseObject, textStatus, jqXHR) {
                        $("#editSubmit").prop('disabled', false);
                        resetErrors();
                        printErrMsg(responseObject.responseJSON.errors);
                    },
                    500: function(responseObject, textStatus, errorThrown) {
                        $("#editSubmit").prop('disabled', false);
                        swal("Error occured!", "Something went wrong please try again", "error");
                    }
                }
            });

        });
    });

    $(".is_salary_applicable").change(function(){

        var is_salary_applicable = this.value;

        if(is_salary_applicable == 1)
        {
            $('#salary_percent_div').removeAttr('style');
        }
        else{
            $('#salary_percent').val('');
            $('#salary_percent_div').hide();
        }

    });


</script>

