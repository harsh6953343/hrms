<x-admin.layout>
    <x-slot name="title">Employee LIC Deduction</x-slot>
    <x-slot name="heading">Employee LIC Deduction</x-slot>

    <!-- Add Form -->
    <div class="row" id="addContainer" style="display:none;">
        <div class="col-sm-12">
            <div class="card">
                <header class="card-header">
                    <h4 class="card-title">Add LIC Deduction</h4>
                </header>
                <form class="theme-form" name="addForm" id="addForm" enctype="multipart/form-data">
                    @csrf

                    <div class="card-body">

                        <div class="mb-3 row">

                            <div class="col-md-3 mt-2">
                                <label class="col-form-label" for="Emp_Code">Employee Id<span class="text-danger">*</span></label>
                                <input class="form-control" id="Emp_Code" name="Emp_Code" type="text" placeholder="Enter Employee Id">
                                <span class="text-danger invalid Emp_Code_err"></span>
                            </div>

                            <div class="col-md-3 mt-5">
                                <button type="button" class="btn btn-primary" id="searchBtn">Search Employee</button>
                            </div>
                        </div>

                        <div class="mb-3 row">

                            <input type="hidden" name="employee_id" id="employee_id">

                            <div class="col-md-4">
                                <label class="col-form-label" for="emp_name">Employee Name<span class="text-danger">*</span></label>
                                <input class="form-control" id="emp_name" name="emp_name" type="text" placeholder="Employee Name" readonly>
                                <span class="text-danger invalid emp_name_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="ward">Ward<span class="text-danger">*</span></label>
                                <input class="form-control" id="ward" name="ward" type="text" placeholder="Employee Ward" readonly>
                                <span class="text-danger invalid ward_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="department">Department<span class="text-danger">*</span></label>
                                <input class="form-control" id="department" name="department" type="text" placeholder="Employee Department" readonly>
                                <span class="text-danger invalid department_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="designation">Designation<span class="text-danger">*</span></label>
                                <input class="form-control" id="designation" name="designation" type="text" placeholder="Employee Designation" readonly>
                                <span class="text-danger invalid designation_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="class">Class<span class="text-danger">*</span></label>
                                <input class="form-control" id="class" name="class" type="text" placeholder="Employee Class" readonly>
                                <span class="text-danger invalid class_err"></span>
                            </div>
                        </div>
                        <hr>

                        <div class="mb-3 row" id="status_div" style="display: none;">
                            <h2>Add LIC Details</h2>
                            <div class="col-md-4">
                                <label class="col-form-label" for="lic_name">LIC Policy Name<span class="text-danger">*</span></label>
                                <input class="form-control title" id="lic_name" name="lic_name" type="text" placeholder="Enter LIC Policy Name">
                                <span class="text-danger invalid lic_name_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="lic_number">LIC Policy Number<span class="text-danger">*</span></label>
                                <input class="form-control title" id="lic_number" name="lic_number" type="number" placeholder="Enter LIC Policy Number">
                                <span class="text-danger invalid lic_number_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="total_amt">Total Amount<span class="text-danger">*</span></label>
                                <input class="form-control title" id="total_amt" name="total_amt" type="number" placeholder="Enter Total Amount">
                                <span class="text-danger invalid total_amt_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="total_installment">No. Of Installment<span class="text-danger">*</span></label>
                                <input class="form-control title" id="total_installment" name="total_installment" type="number" placeholder="Enter No. Of Installment">
                                <span class="text-danger invalid total_installment_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="installment_amt">Installment Amount<span class="text-danger">*</span></label>
                                <input class="form-control title" id="installment_amt" name="installment_amt" type="number" placeholder="Enter Installment Amount">
                                <span class="text-danger invalid installment_amt_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="start_date">Start Date<span class="text-danger">*</span></label>
                                <input class="form-control title" id="start_date" name="start_date" type="date" placeholder="Enter Start Date">
                                <span class="text-danger invalid start_date_err"></span>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label" for="end_date">End Date<span class="text-danger">*</span></label>
                                <input class="form-control title" id="end_date" name="end_date" type="date" placeholder="Enter End Date">
                                <span class="text-danger invalid end_date_err"></span>
                            </div>

                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" id="addSubmit">Submit</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                @can('lic-dedcution.create')
                    <div class="card-header">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="">
                                    <button id="addToTable" class="btn btn-primary">Add <i class="fa fa-plus"></i></button>
                                    <button id="btnCancel" class="btn btn-danger" style="display:none;">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endcan
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>Employee Id</th>
                                    <th>Employee Name</th>
                                    <th>LIC Policy Name</th>
                                    <th>LIC Policy Number</th>
                                    <th>Total Amount</th>
                                    <th>Installment Amount</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($lic_deductions as $lic_deduction)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $lic_deduction->Emp_Code }}</td>
                                        <td>{{ $lic_deduction?->employee?->fname." ".$lic_deduction?->employee?->mname." ".$lic_deduction?->employee?->lname }}</td>
                                        <td>{{ $lic_deduction?->lic_name }}</td>
                                        <td>{{ $lic_deduction?->lic_number }}</td>
                                        <td>{{ $lic_deduction->total_installment }}</td>
                                        <td>{{ $lic_deduction->installment_amt }}</td>
                                        <td>{{ $lic_deduction->start_date }}</td>
                                        <td>{{ $lic_deduction->end_date }}</td>
                                        <td>
                                            @can('lic-dedcution.stop')
                                            <div class="form-check form-switch form-switch-lg ml-4" dir="ltr">
                                                &emsp;<input type="checkbox" class="form-check-input status-btn" id="customSwitchsizelg" data-id="{{ $lic_deduction->id }}" value="{{ $lic_deduction->status }}"  {{ ($lic_deduction->status != 2)?'Checked':'disabled' }} >
                                            </div>
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Add Remark --}}
    <div class="modal fade" id="add-remark-modal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form action="" id="changeStatusForm">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Add Remark</h5>
                        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">

                        <input type="hidden" id="unique_id" name="unique_id">

                        <div class="col-8 mx-auto my-2">
                            <div class="form-group">
                                <label>Add Remark</label>
                                <div class="input-group"><span class="input-group-text"><i data-feather="activity"></i></span>
                                    <input class="form-control" type="text" id="remark" name="remark">
                                </div>
                                <span class="text-danger error-text remark_err"></span>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Cancel</button>
                        <button class="btn btn-primary" id="changeStatusSubmit" type="submit">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>


</x-admin.layout>


{{-- Add --}}
<script>

$(document).ready(function() {
    $("#searchBtn").click(function(){
            var Emp_Code = $('#Emp_Code').val();
            if(Emp_Code != '')
            {
                var url = "{{ route('fetch-employee-details', ':Emp_Code') }}";

                $.ajax({
                    url: url.replace(':Emp_Code', Emp_Code),
                    type: 'GET',
                    data: {
                        '_method': "GET",
                        '_token': "{{ csrf_token() }}"
                    },
                    success: function(data) {
                        if (!data.error && !data.error2) {
                            if (data.result === 1) {

                                var full_name = data.employee_details.fname + " " + data.employee_details.mname + " " + data.employee_details.lname;
                                $('#emp_name').val(full_name);
                                $('#ward').val(data.employee_details.ward.name);
                                $('#department').val(data.employee_details.department.name);
                                $('#class').val(data.employee_details.class.name);
                                $('#designation').val(data.employee_details.designation.name);
                                $('#employee_id').val(data.employee_details.id);

                                $('#status_div').removeAttr('style');

                            } else if (data.result === 0) {
                                $('#status_div').hide();
                                $('#lic_name').val('');
                                $('#total_amt').val('');
                                $('#total_installment').val('');
                                $('#installment_amt').val('');
                                $('#start_date').val('');
                                $('#end_date').val('');

                                alert("Employee details not found or Employee Status already added!");
                            } else {
                                alert("Unexpected result from the server");
                            }
                    }
                    },
                    error: function(error, jqXHR, textStatus, errorThrown) {
                        swal("Error!", "Something went wrong", "error");
                    },
                });
            }
            else{
                alert('Please Enter Employee Id');
            }

        });
    });


    $("#addForm").submit(function(e) {
        e.preventDefault();
        $("#addSubmit").prop('disabled', true);

        var formdata = new FormData(this);
        $.ajax({
            url: '{{ route('lic-dedcution.store') }}',
            type: 'POST',
            data: formdata,
            contentType: false,
            processData: false,
            success: function(data) {
                $("#addSubmit").prop('disabled', false);
                if (!data.error2)
                    swal("Successful!", data.success, "success")
                    .then((action) => {
                        window.location.href = '{{ route('lic-dedcution.index') }}';
                    });
                else
                    swal("Error!", data.error2, "error");
            },
            statusCode: {
                422: function(responseObject, textStatus, jqXHR) {
                    $("#addSubmit").prop('disabled', false);
                    resetErrors();
                    printErrMsg(responseObject.responseJSON.errors);
                },
                500: function(responseObject, textStatus, errorThrown) {
                    $("#addSubmit").prop('disabled', false);
                    swal("Error occured!", "Something went wrong please try again", "error");
                }
            }
        });

    });
</script>

<script>
      $(".status-btn").change(function(e){
        e.preventDefault();
        if ($(this).prop('checked')) {
            $('.status-btn').val('1')
        }else{
            $('.status-btn').val('2')
        }

        var btn_status = $('.status-btn').val();

        swal({
                title: "Are you sure to Stop this LIC ? once stop cannot start again!",
                icon: "info",
                buttons: ["Cancel", "Confirm"]
            })
    });


    $("#buttons-datatables").on("click", ".status-btn", function(e) {
        e.preventDefault();

        swal({
                title: "Are you sure to Stop this LIC ? once stop cannot start again!",
                icon: "info",
                buttons: ["Cancel", "Confirm"]
            })
            .then((justTransfer) => {
                if (justTransfer) {

                    var id = $(this).attr("data-id");
                    $('#unique_id').val(id);
                    $('#add-remark-modal').modal('show');
                }
            });
    });
</script>


<!-- Update Status -->
<script>
    $("#changeStatusForm").submit(function(e) {
        e.preventDefault();
        $("#changeStatusSubmit").prop('disabled', true);

        var formdata = new FormData(this);
        formdata.append('_method', 'POST');

        if ($(this).prop('checked')) {
            $('.status-btn').val('1')
        }else{

            $('.status-btn').val('2')
        }

        var btn_status = $('.status-btn').val();

        var model_id = $('#unique_id').val();
        var url = "{{ route('employee-lic-status', [':model_id', ':btn_status']) }}";

        url = url.replace(':model_id', model_id);
        url = url.replace(':btn_status', btn_status);

        $.ajax({
            url: url,
            type: 'POST',
            data: formdata,
            contentType: false,
            processData: false,
            success: function(data) {
                $("#changeStatusSubmit").prop('disabled', false);
                if (!data.error2)
                    swal("Successful!", data.success, "success")
                    .then((action) => {
                        $("#add-remark-modal").modal('hide');
                        $("#changeStatusSubmit").prop('disabled', false);
                        window.location.reload();
                    });
                else
                    swal("Error!", data.error2, "error");
            },
            statusCode: {
                422: function(responseObject, textStatus, jqXHR) {
                    $("#changeStatusSubmit").prop('disabled', false);
                    resetErrors();
                    printErrMsg(responseObject.responseJSON.errors);
                },
                500: function(responseObject, textStatus, errorThrown) {
                    $("#changeStatusSubmit").prop('disabled', false);
                    swal("Error occured!", "Something went wrong please try again", "error");
                }
            }
        });

        function resetErrors() {
            var form = document.getElementById('changeStatusForm');
            var data = new FormData(form);
            for (var [key, value] of data) {
                $('.' + key + '_err').text('');
                $('#' + key).removeClass('is-invalid');
                $('#' + key).addClass('is-valid');
            }
        }

        function printErrMsg(msg) {
            $.each(msg, function(key, value) {
                $('.' + key + '_err').text(value);
                $('#' + key).addClass('is-invalid');
                $('#' + key).removeClass('is-valid');
            });
        }

    });
</script>
