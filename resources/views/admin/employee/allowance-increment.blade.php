<x-admin.layout>
    <x-slot name="title">Allowance Increment</x-slot>
    <x-slot name="heading">Allowance Increment</x-slot>

    <!-- Add Form -->
    <div class="row" id="addContainer">
        <div class="col-sm-12">
            <div class="card">
                <header class="card-header">
                    <h4 class="card-title">Allowance Increment</h4>
                </header>
                <form class="theme-form" name="addForm" id="addForm" enctype="multipart/form-data">
                    @csrf

                    <div class="card-body">
                        <div class="mb-3 row">

                            <div class="col-md-3">
                                <label class="col-form-label" for="department_id">Select Department <span class="text-danger">*</span></label>
                                <select class="form-control js-example-basic-single" name="department_id" id="department_id">
                                    <option value="">Select Department</option>
                                    <option value="all">All</option>
                                    @foreach ($departments as $department)
                                        <option value="{{ $department->id }}">{{ $department->name }}</option>
                                    @endforeach
                                </select>
                                <span class="text-danger invalid department_id_err"></span>
                            </div>

                            <div class="col-md-3">
                                <label class="col-form-label" for="clas_id">Select Class <span class="text-danger">*</span></label>
                                <select class="form-control js-example-basic-single" name="clas_id" id="clas_id">
                                    <option value="">Select Class</option>
                                    <option value="all">All</option>
                                    @foreach ($classes as $class)
                                        <option value="{{ $class->id }}">{{ $class->name }}</option>
                                    @endforeach
                                </select>
                                <span class="text-danger invalid clas_id_err"></span>
                            </div>

                            <div class="col-md-3">
                                <label class="col-form-label" for="allowance_id">Select Allowance <span class="text-danger">*</span></label>
                                <select class="form-control js-example-basic-single" name="allowance_id" id="allowance_id">
                                    <option value="">Select Allowance</option>
                                    @foreach ($allowances as $allowance)
                                        <option value="{{ $allowance->id }}">{{ $allowance->allowance }}</option>
                                    @endforeach
                                </select>
                                <span class="text-danger invalid allowance_id_err"></span>
                            </div>

                            <div class="col-md-3">
                                <label class="col-form-label" for="current_amount">Current Amount <span class="text-danger">*</span></label>
                                <input type="text" name="current_amount" id="current_amount" class="form-control" readonly>
                                <span class="text-danger invalid current_amount_err"></span>
                            </div>

                            <div class="col-md-3">
                                <label class="col-form-label" for="current_paytype">Current Pay Type <span class="text-danger">*</span></label>
                                <input type="text" name="current_paytype" id="current_paytype" class="form-control" readonly>
                                <span class="text-danger invalid current_paytype_err"></span>
                            </div>

                            <div class="col-md-3">
                                <label class="col-form-label" for="new_amount">New Amount <span class="text-danger">*</span></label>
                                <input type="text" name="new_amount" id="new_amount" class="form-control" placeholder="Enter New Amount">
                                <span class="text-danger invalid new_amount_err"></span>
                            </div>

                            <div class="col-md-3">
                                <label class="col-form-label" for="applicable_date">Applicable Date <span class="text-danger">*</span></label>
                                <input type="date" name="applicable_date" id="applicable_date" class="form-control">
                                <span class="text-danger invalid applicable_date_err"></span>
                            </div>

                            <div class="col-md-3" id="showBtn">
                                <button id="showBasicSalary" class="btn btn-primary mt-4">Show</button>
                                {{-- <button class="btn btn-warning mt-4">Cancel</button> --}}
                            </div>

                        </div>
                        @can('allowance-increment.create')
                        <div class="card-footer" id="submitBtn">
                            <button type="submit" class="btn btn-primary" id="addSubmit">Submit</button>
                            <button type="reset" class="btn btn-warning">Reset</button>
                        </div>
                        @endcan
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th>Sr No.</th>
                                                        <th>Employee Id</th>
                                                        <th>Employee Name</th>
                                                        <th>Department</th>
                                                        <th>Allowance</th>
                                                        <th>Current Allowance</th>
                                                        <th>Incremented Allowance</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</x-admin.layout>


{{-- Add --}}
<script>
    $("#allowance_id").on("change", function(e) {

        var allowance_id = this.value;
        var url = "{{ route('fetch-allowance-details', ':allowance_id') }}";

        $.ajax({
            url: url.replace(":allowance_id", allowance_id),
            type: "GET",
            data: {
                _method: "GET",
                _token: "{{ csrf_token() }}",
            },
            success: function (data, textStatus, jqXHR) {
                if (!data.error && !data.error2) {
                    $('#current_amount').val(data.allowanceDetails.amount);
                    if(data.allowanceDetails.type == 1){ var payType = 'Amount'; }else{ var payType = 'Percentage'; }
                    $('#current_paytype').val(payType);

                } else {
                    alert(data.error);
                }
            },
            error: function (error, jqXHR, textStatus, errorThrown) {
                swal("Error!", "Something went wrong", "error");
            },
        });

    });

    $("#showBasicSalary").on("click", function(e) {
        e.preventDefault();

        var department_id = $('#department_id').val();
        var clas_id = $('#clas_id').val();
        var allowance_id = $('#allowance_id').val();
        var current_amount = $('#current_amount').val();
        var current_paytype = $('#current_paytype').val();
        var new_amount = $('#new_amount').val();
        var applicable_date = $('#applicable_date').val();

        if(department_id != '' && clas_id != '' && allowance_id != '' && current_amount != '' && current_paytype != '' && new_amount != '' && applicable_date != '')
        {
            var url = "{{ route('get-incremented-allowance') }}";
            url += '?' + $.param({
                '_token': "{{ csrf_token() }}",
                department_id: department_id,
                clas_id:clas_id,
                allowance_id:allowance_id,
                new_amount:new_amount
            });

            $.ajax({
                url: url,
                type: 'GET',
                success: function(data, textStatus, jqXHR) {
                    if (!data.error) {
                        updateTable(data.incrementArr);
                    } else {
                        alert(data.error);
                    }
                },
                error: function(error, jqXHR, textStatus, errorThrown) {
                    alert("Some thing went wrong");
                },
            });

        }else{
            alert('Please select all feilds');
        }

    });

    function updateTable(data) {
        var tableBody = $('#buttons-datatables tbody');
        tableBody.empty(); // Clear existing data

        if (data.length > 0) {
            $.each(data, function(index, row) {
                var newRow = '<tr>' +
                    '<td><input type="hidden" name="allowance_unique_id[]" value="'+ row.allowance_unique_id +'">' + row.sr_no + '</td>' +
                    '<td><input type="hidden" name="emp_id[]" value="'+ row.employee_uniques_id +'">' + row.emp_id + '</td>' +
                    '<td>' + row.emp_name + '</td>' +
                    '<td>' + row.department + '</td>' +
                    '<td><input type="hidden" name="allowance" value="'+ row.allowance +'">' + row.allowance + '</td>' +
                    '<td>' + row.current_allowances + '</td>' +
                    '<td><input type="hidden" name="new_amount1[]" value="'+ row.increment_allowances +'">' + row.increment_allowances + '</td>' +
                    '</tr>';
                tableBody.append(newRow);
            });
        } else {
            // Handle case where no data is returned
            var noDataRow = '<tr><td colspan="6">No data available</td></tr>';
            tableBody.append(noDataRow);
        }
    }

    $("#addForm").submit(function(e) {
        e.preventDefault();
        $("#addSubmit").prop('disabled', true);

        var formdata = new FormData(this);
        $.ajax({
            url: '{{ route('allowance-increment.store') }}',
            type: 'POST',
            data: formdata,
            contentType: false,
            processData: false,
            success: function(data) {
                $("#addSubmit").prop('disabled', false);
                if (!data.error2)
                    swal("Successful!", data.success, "success")
                    .then((action) => {
                        window.location.href = '{{ route('allowance-increment.index') }}';
                    });
                else
                    swal("Error!", data.error2, "error");
            },
            statusCode: {
                422: function(responseObject, textStatus, jqXHR) {
                    $("#addSubmit").prop('disabled', false);
                    resetErrors();
                    printErrMsg(responseObject.responseJSON.errors);
                },
                500: function(responseObject, textStatus, errorThrown) {
                    $("#addSubmit").prop('disabled', false);
                    swal("Error occured!", "Something went wrong please try again", "error");
                }
            }
        });

    });
</script>

