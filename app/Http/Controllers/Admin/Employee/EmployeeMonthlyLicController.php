<?php

namespace App\Http\Controllers\Admin\Employee;

use App\Http\Controllers\Controller;
use App\Models\EmployeeMonthlyLic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class EmployeeMonthlyLicController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $authUser = Auth::user();

        $employee_monthly_lic = EmployeeMonthlyLic::with('employee')
                                ->when($authUser->hasRole('Ward HOD'), function ($query) use ($authUser) {
                                    $query->whereHas('employee', function ($employeeQuery) use ($authUser) {
                                        $employeeQuery->where('ward_id', $authUser->ward_id);
                                    });
                                })
                                ->when($authUser->id == 1, function ($query) {
                                    $query->whereHas('employee', function ($employeeQuery) {
                                        $employeeQuery->whereNot('ward_id', 84);
                                    });
                                })
                                ->groupBy('from_date', 'to_date')
                                ->select('from_date', 'to_date')
                                ->get();


        return view('admin.employee.lic.employee-monthly-lic')->with(['employee_monthly_lics' => $employee_monthly_lic]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    public function showEmployeeLic($from_date, $to_date)
    {
        $employee_monthly_lics = EmployeeMonthlyLic::with('lic')->where('from_date', $from_date)->where('to_date', $to_date)->latest()->get();
        return view('admin.employee.lic.employee-monthly-lic-list')->with(['employee_monthly_lics' => $employee_monthly_lics]);
    }

    // Change LIC Status
    public function employeeMonthlyLicStatus($model_id, Request $request)
    {
        try {
            $employeeMonthlyLic = EmployeeMonthlyLic::find($model_id);
            if (!$employeeMonthlyLic) {
                return response()->json(['error' => 'Employee monthly lic not found'], 404);
            }
            DB::beginTransaction();
            $employeeMonthlyLic->remark = $request->remark;
            $employeeMonthlyLic->save();
            $employeeMonthlyLic->delete();
            DB::commit();
            return response()->json(['success' => 'LIC deleted successfully!']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['error' => 'An error occurred while deleting the lic'], 500);
        }
    }
}
