<?php

namespace App\Http\Controllers\Admin\Employee;

use App\Http\Controllers\Admin\Controller;
use App\Http\Requests\Admin\Employee\StoreIncrementDeductionRequest;
use App\Models\Clas;
use App\Models\Deduction;
use App\Models\Department;
use App\Models\Employee;
use App\Models\EmployeeDeduction;
use App\Models\IncrementDeductionsSalary;
use App\Models\OldEmployeeDeduction;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DeductionIncrementController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $authUser = Auth::user();

        $departments = Department::
                        when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
                            return $query->where('ward_id', $authUser->ward_id);
                        })
                        ->when($authUser->id == 1, function ($query) {
                            return $query->where('ward_id','!=', 84);
                        })
                        ->latest()->get();;
        $deductions = Deduction::latest()->get();;
        $classes = Clas::latest()->get();

        return view('admin.employee.deduction-increment')->with(['departments' => $departments, 'deductions' => $deductions, 'classes' => $classes]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreIncrementDeductionRequest $request)
    {
        $authUser = Auth::user();

        try {
            DB::beginTransaction();
            $input = $request->validated();

            if ($input['department_id'] == 'all') {
                $input['department_id'] = 0;
            } else {
                $input['department_id'] = $input['department_id'];
            }

            if ($input['clas_id'] == 'all') {
                $input['clas_id'] = 0;
            } else {
                $input['clas_id'] = $input['clas_id'];
            }

            IncrementDeductionsSalary::create(Arr::only($input, IncrementDeductionsSalary::getFillables()));
            $emp_ids = $input['emp_id'];
            $deduction = $input['deduction_id'];

            $deduction_unique_id = $input['deduction_unique_id'];
            foreach ($emp_ids as $key => $emp_id) {

                $employee_deduction = EmployeeDeduction::where('employee_id', $emp_id)->where('id', $deduction_unique_id[$key]);

                $old_employee_deduction = OldEmployeeDeduction::where('employee_id', $emp_id)->where('deduction_id', $deduction)->latest()->first();

                $employee_deduction->update([
                    'deduction_amt' => $input['new_amount'],
                    'updated_by' => $authUser->id,
                    'updated_at' => now(),
                ]);

                $old_employee_deduction->update([
                    'end_date' => date('Y-m-d'),
                    'updated_by' => $authUser->id,
                    'updated_at' => now(),
                    'is_active' => 0,
                ]);

                $old_employee_salary_create = [
                    'employee_id' => $emp_id,
                    'Emp_Code' => $old_employee_deduction->Emp_Code,
                    'old_employee_salary_id' => $old_employee_deduction->old_employee_salary_id,
                    'deduction_id' => $deduction,
                    'deduction_amt' => $input['new_amount'],
                    'deduction_type' => $old_employee_deduction->deduction_type,
                    'is_active' => 1,
                    'applicable_date' => date('Y-m-d')
                ];

                OldEmployeeDeduction::create($old_employee_salary_create);
            }

            DB::commit();

            return response()->json(['success' => 'Deduction Increment successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'creating', 'Deduction');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    public function fetchDeductionDetails($deduction_id)
    {
        $deductionDetails = Deduction::where('id', $deduction_id)->get()->first();

        if ($deductionDetails) {
            $response = [
                'result' => 1,
                'deductionDetails' => $deductionDetails,
            ];
        } else {
            $response = ['result' => 0];
        }
        return $response;
    }

    public function getIncrementedDeduction(Request $request)
    {
        try {
            $departmentId = $request->input('department_id');
            $clas_id = $request->input('clas_id');
            $deduction_id = $request->input('deduction_id');
            $new_amount = $request->input('new_amount');

            $get_employees = Employee::with('department')->when(
                $departmentId == 'all',
                function ($query) {
                    return $query->latest();
                },
                function ($query) use ($departmentId, $clas_id) {
                    $query->when($departmentId != 'all', function ($subquery) use ($departmentId) {
                        $subquery->where('department_id', $departmentId);
                    })->when($clas_id != 'all', function ($subquery) use ($clas_id) {
                        $subquery->where('clas_id', $clas_id);
                    });

                    return $query->latest();
                }
            )->get();

            $incrementArr = [];

            foreach ($get_employees as $key => $employee) {

                $employeeSalary = EmployeeDeduction::with('deduction')->where('employee_id', $employee->id)
                    ->where('deduction_id', $deduction_id)->first();

                if ($employeeSalary) {
                    $incrementArr[] = [
                        'sr_no'                 =>  $key + 1,
                        'employee_uniques_id'   =>  $employee->id,
                        'deduction_unique_id'   =>  $employeeSalary->id,
                        'emp_id'                =>  $employee->employee_id,
                        'emp_name'              =>  $employee->fname . " " . $employee->mname . " " . $employee->lname,
                        'department'            =>  $employee->department->name,
                        'deduction'             =>  $employeeSalary->deduction->deduction,
                        'current_deduction'     =>  $employeeSalary->deduction_amt,
                        'increment_deduction'   =>  $new_amount,
                    ];
                }
            }

            $response = [
                'result' => 1,
                'incrementArr' => $incrementArr,
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            return response()->json(['result' => 0, 'error' => $e->getMessage()]);
        }
    }
}
