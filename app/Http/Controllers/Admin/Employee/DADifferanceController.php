<?php

namespace App\Http\Controllers\Admin\Employee;

use App\Http\Controllers\Admin\Controller;
use App\Http\Requests\Employee\DA\StoreDADifferanceRequest;
use App\Models\Allowance;
use App\Models\DaDifferance;
use App\Models\EmployeeDaDifferance;
use App\Models\FinancialYear;
use App\Models\FreezeAttendance;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class DADifferanceController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $daDifferance = DaDifferance::latest()->get();

        return view('admin.employee.da-differance')->with(['daDifferances' => $daDifferance]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreDADifferanceRequest $request)
    {
        try {
            DB::beginTransaction();
            $input = $request->validated();

            $financial_year = FinancialYear::where('is_active', 1)->first();

            // where('financial_year_id',$financial_year->id)
            $check_applicable_month_has_salary = FreezeAttendance::where('month',$input['applicable_month'])->first();
            if(!$check_applicable_month_has_salary){
                return response()->json(['error2' => 'Salary not present in applicable month!']);
            }

            $check_already_added = DaDifferance::where('status',0)->first();
            if($check_already_added){
                return response()->json(['error2' => 'Already Added']);
            }

            $last_month_salary = FreezeAttendance::latest('month')->first();

            if ($last_month_salary) {
                $currentYear = date('Y');
                $lastMonth = $last_month_salary->month;

                $parsedDate = Carbon::createFromDate($currentYear, $lastMonth, 1);
                $nextMonth = $parsedDate->addMonth();

                $input['given_month'] = $nextMonth->format('n');
            } else {
                $input['given_month'] = Carbon::now()->addMonth()->format('n');
            }

            $input['financial_year_id'] = $financial_year->id;
            $input['no_of_month']       = $input['given_month'] - $input['applicable_month'];

            DaDifferance::create(Arr::only($input, DaDifferance::getFillables()));
            DB::commit();

            return response()->json(['success' => 'DA Differance created successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'creating', 'DA Differance');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(DaDifferance $daDifferance)
    {
        $employee_da_differance = EmployeeDaDifferance::with('employee')->where('da_differance_id',$daDifferance->id)->latest()->get();

        return view('admin.employee.da-differance-list')->with(['employee_da_differances' => $employee_da_differance]);

    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(DaDifferance $daDifferance)
    {
        try {
            DB::beginTransaction();
            $daDifferance->delete();
            DB::commit();
            return response()->json(['success' => 'DA Differance deleted successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'deleting', 'DA Differance');
        }
    }

    public function fetchCurrentDA()
    {
        $allowance_arr = Allowance::where('id',1)->first();

        if ($allowance_arr) {
            $response = [
                'result' => 1,
                'allowance_arr' => $allowance_arr,
            ];
        } else {
            $response = ['result' => 0];
        }
        return $response;
    }
}
