<?php

namespace App\Http\Controllers\Admin\Employee;

use App\Http\Controllers\Controller;
use App\Models\EmployeeFestivalAdvance;
use App\Models\EmployeeMonthlyFestivalAdvance;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


class EmployeeMonthlyFestivalController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $authUser = Auth::user();

        $employee_monthly_festivalAdvances = EmployeeMonthlyFestivalAdvance::with('employee')
                                                ->when($authUser->hasRole('Ward HOD'), function ($query) use ($authUser) {
                                                    $query->whereHas('employee', function ($employeeQuery) use ($authUser) {
                                                        $employeeQuery->where('ward_id', $authUser->ward_id);
                                                    });
                                                })
                                                ->when($authUser->id == 1, function ($query) {
                                                    $query->whereHas('employee', function ($employeeQuery) {
                                                        $employeeQuery->whereNot('ward_id', 84);
                                                    });
                                                })
                                                ->groupBy('from_date', 'to_date')
                                                ->select('from_date', 'to_date')
                                                ->get();

        return view('admin.employee.festivalAdvance.employee-monthly-festival-advance')->with(['employee_monthly_festivalAdvances' => $employee_monthly_festivalAdvances]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    public function showEmployeeFestivalAdvance($from_date, $to_date)
    {
        $employee_monthly_festivalAdvances = EmployeeMonthlyFestivalAdvance::with('festivalAdvance')->where('from_date', $from_date)->where('to_date', $to_date)->latest()->get();
        return view('admin.employee.festivalAdvance.employee-monthly-festival-advance-list')->with(['employee_monthly_festivalAdvances' => $employee_monthly_festivalAdvances]);
    }

    // Change Festival Advance Status
    public function monthlyFestivalAdvanceStatus(Request $request, $model_id)
    {
        try {
            $employeeFestAdv = EmployeeMonthlyFestivalAdvance::find($model_id);
            if (!$employeeFestAdv) {
                return response()->json(['error' => 'Employee monthly festival advance not found'], 404);
            }

            DB::beginTransaction();

            $mainEmployeeFestAdv = EmployeeFestivalAdvance::find($employeeFestAdv->employee_festival_advance_id);
            if (!$mainEmployeeFestAdv) {
                DB::rollback();
                return response()->json(['error' => 'Employee festival advance not found'], 404);
            }

            // Update end_date and save
            $endDate = Carbon::parse($mainEmployeeFestAdv->end_date);
            $mainEmployeeFestAdv->end_date = $endDate->copy()->addMonth()->toDateString();

            // Decrement and increment installments
            $mainEmployeeFestAdv->decrement('deducted_instalment');
            $mainEmployeeFestAdv->increment('pending_instalment');
            $mainEmployeeFestAdv->save();

            // Update remark and delete the record
            $employeeFestAdv->remark = $request->remark;
            $employeeFestAdv->save();
            $employeeFestAdv->delete();

            DB::commit();
            return response()->json(['success' => 'Loan deleted successfully!']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['error' => 'An error occurred while deleting the loan'], 500);
        }
    }

}
