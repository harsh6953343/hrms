<?php

namespace App\Http\Controllers\Admin\Employee;

use App\Http\Controllers\Admin\Controller;
use App\Http\Requests\Admin\Employee\StoreLICDeductionRequest;
use App\Models\LicDeduction;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LICDeductionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $authUser = Auth::user();

        $lic_deductions = LicDeduction::with('employee')
                            ->when($authUser->hasRole('Ward HOD'), function ($query) use ($authUser) {
                                $query->whereHas('employee', function ($employeeQuery) use ($authUser) {
                                    $employeeQuery->where('ward_id', $authUser->ward_id);
                                });
                            })
                            ->when($authUser->id == 1, function ($query) {
                                $query->whereHas('employee', function ($employeeQuery) {
                                    $employeeQuery->whereNot('ward_id', 84);
                                });
                            })
                            ->latest()->get();

        return view('admin.employee.lic.lic-dedcution')->with(['lic_deductions' => $lic_deductions]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreLICDeductionRequest $request)
    {
        try {
            DB::beginTransaction();
            $input = $request->validated();
            $input['pending_installment'] = $input['total_installment'];
            $input['financial_year_id'] = session('financial_year');
            LicDeduction::create(Arr::only($input, LicDeduction::getFillables()));
            DB::commit();
            return response()->json(['success' => 'LIC Deduction created successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'creating', 'LIC Deduction');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    // Change LIC Status
    public function employeeLicStatus($model_id, $btn_status, Request $request)
    {
        try {
            $employeeLic = LicDeduction::find($model_id);
            $employeeLic->status = $btn_status;
            $employeeLic->remark = $request->remark;
            $employeeLic->status_by = Auth::user()->id;
            $employeeLic->status_date = date('Y-m-d H:i:s');
            $employeeLic->save();
            return response()->json(['success' => 'LIC status update successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'updating', 'LIC');
        }
    }
}
