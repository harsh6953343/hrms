<?php

namespace App\Http\Controllers\Admin\Employee;

use App\Http\Controllers\Admin\Controller;
use Illuminate\Http\Request;
use App\Models\Employee;
use App\Models\Ward;
use App\Models\Clas;
use App\Models\Designation;
use App\Models\LeaveType;
use App\Models\Bank;
use App\Http\Requests\Admin\Employee\StoreEmployeeRequest;
use App\Http\Requests\Admin\Employee\UpdateEmployeeBasicDetailsRequest;
use App\Http\Requests\Admin\Employee\UpdateEmployeeRequest;
use App\Models\Department;
use App\Repositories\EmployeeRepository;
use Exception;
use App\Models\AcademicDetails;
use App\Models\Caste;
use App\Models\Document;
use App\Models\Experience;
use App\Models\WorkingDepartment;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class EmployeeController extends Controller
{

    protected $employeeRepository;
    public function __construct()
    {
        $this->employeeRepository = new EmployeeRepository;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $authUser = Auth::user();

        $employees = Employee::with('ward', 'department', 'designation', 'class', 'status')
            ->when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
                return $query->where('ward_id', $authUser->ward_id);
            })
            ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
                return $query->where('department_id', $authUser->department_id);
            })
            ->when($authUser->hasRole(['Employee']), function ($query) use ($authUser) {
                return $query->where('id', $authUser->employee_id);
            })
            ->latest()
            ->get();


        return view('admin.employee.list')->with(['employees' => $employees]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $authUser = Auth::user();
        $wards = Ward::when(
            $authUser->hasRole(['Ward HOD', 'Department HOD']),
            function ($query) use ($authUser) {
                return $query->where('id', $authUser->ward_id);
            }
        )->latest()->get();


        $class = Clas::latest()->get();
        $leaveType = LeaveType::latest()->get();
        $banks = Bank::latest()->get();
        $documents = Document::latest()->get();
        $working_department = WorkingDepartment::latest()->get();
        $castes = Caste::latest()->get();

        return view('admin.employee.add')->with(['wards'                => $wards,
                                                'class'                 => $class,
                                                'leaveType'             => $leaveType,
                                                'banks'                 => $banks,
                                                'documents'             => $documents,
                                                'working_departments'   => $working_department,
                                                'castes'                => $castes,
                                                ]);
    }

    /**
     * Store a newly created resource in storage.
     */

    public function store(StoreEmployeeRequest $request)
    {
        try {
            $this->employeeRepository->store($request->validated());
            return response()->json(['success' => 'Employee created successfully!']);
        } catch (Exception $e) {
            return $this->respondWithAjax($e, 'adding', 'Employee');
        }
    }



    /**
     * Display the specified resource.
     */
    public function show(Employee $employee)
    {
        if ($employee) {
            $data = $this->employeeRepository->editEmployee($employee);

            return view('admin.employee.show', $data);
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Employee $employee)
    {
        if ($employee) {
            $data = $this->employeeRepository->editEmployee($employee);

            return view('admin.employee.edit', $data);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateEmployeeRequest $request, Employee $employee)
    {
        try {
            $this->employeeRepository->updateEmployee($request->validated(), $employee);
            return response()->json(['success' => 'Employee updated successfully!']);
        } catch (Exception $e) {
            return $this->respondWithAjax($e, 'updating', 'Employee');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Employee $employee)
    {
        try {
            DB::beginTransaction();
            $employee->delete();
            DB::commit();
            return response()->json(['success' => 'Employee deleted successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'deleting', 'Employee');
        }
    }

    // Fetch designation based on ward/department/class
    public function fetchDesignation($ward_id, $department_id, $class_id)
    {
        $des_arr = Designation::
        // where('ward_id', $ward_id)
            // ->where('department_id', $department_id)
            // ->where('clas_id', $class_id)
            get();

        if ($des_arr) {
            $desHtml = '<span>
            <option value="">--Select Designation--</option>';
            foreach ($des_arr as $des) :
                $desHtml .= '<option value="' . $des->id . '">' . $des->name . '</option>';
            endforeach;
            $desHtml .= '</span>';

            $response = [
                'result' => 1,
                'desHtml' => $desHtml,
            ];
        } else {
            $response = ['result' => 0];
        }
        return $response;
    }

    protected function fetchWorkingYear($class_id, $dob)
    {
        $class = Clas::where('id', $class_id)->first();

        $date_of_retirement = date('Y-m-d', strtotime($dob . '+' . $class->working_year . 'Years'));

        if ($class) {
            $response = [
                'result' => 1,
                'date_of_retirement' => $date_of_retirement,
            ];
        } else {
            $response = ['result' => 0];
        }
        return $response;
    }


    public function employeeBasicUpdateForm()
    {
        $employee_id = Auth::user()->employee_id;
        return view('admin.employee.update-basic-details')->with(['employee_id' => $employee_id]);
    }

    public function employeeBasicUpdate(UpdateEmployeeBasicDetailsRequest $request, $id)
    {
        $employee = Employee::where('id',$id)->first();
        try {
            DB::beginTransaction();
            $input = $request->validated();
            $employee->update(Arr::only($input, Employee::getFillables()));
            DB::commit();
            return response()->json(['success' => 'Employee updated successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'updating', 'Employee');
        }
    }

    // Change Employee Status
    public function changeStatus($model_id, $btn_status, Request $request)
    {
        try {
            $employeeActivity = Employee::find($model_id);
            $employeeActivity->activity_status = $btn_status;
            $employeeActivity->activity_status_by = Auth::user()->id;
            $employeeActivity->updated_at = date('Y-m-d H:i:s');
            $employeeActivity->save();
            return response()->json(['success' => 'Employee status update successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'updating', 'Employee');
        }
    }
}
