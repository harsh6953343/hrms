<?php

namespace App\Http\Controllers\Admin\Employee;

use App\Http\Controllers\Admin\Controller;
use App\Http\Requests\Admin\Employee\StoreSupplimentaryBillRequest;
use App\Models\AddEmployeeLeave;
use App\Models\Allowance;
use App\Models\Corporation;
use App\Models\Deduction;
use App\Models\Employee;
use App\Models\EmployeeStatus;
use App\Models\FinancialYear;
use App\Models\FreezeAttendance;
use App\Models\RemainingFreezeSalary;
use App\Models\SupplimentaryBill;
use Barryvdh\Snappy\Facades\SnappyPdf;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SuspendSupplimentaryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $authUser = Auth::user();

        $supplimentory_bills = SupplimentaryBill::with('employee')
                                ->when($authUser->hasRole('Ward HOD'), function ($query) use ($authUser) {
                                    $query->whereHas('employee', function ($employeeQuery) use ($authUser) {
                                        $employeeQuery->where('ward_id', $authUser->ward_id);
                                    });
                                })
                                ->when($authUser->id == 1, function ($query) {
                                    $query->whereHas('employee', function ($employeeQuery) {
                                        $employeeQuery->whereNot('ward_id', 84);
                                    });
                                })
                                ->where('type',2)->latest()->get();

        return view('admin.supplimentary.regular-supplimentary-bill')->with(['supplimentory_bills' => $supplimentory_bills, 'type' => 2]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.supplimentary.add-suspend-supplimentary-bill');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreSupplimentaryBillRequest $request)
    {
        try {
            DB::beginTransaction();

            $input = $request->validated();
            $month = $input['month'];

            // Check if the status already exists for the given employee ID
            $existingBill = SupplimentaryBill::where('employee_id', $input['employee_id'])
                ->get()
                ->filter(function ($bill) use ($month) {
                    return collect(explode(',', $bill->month))->intersect($month)->isNotEmpty();
                })
                ->first();

            if ($existingBill) {
                return response()->json(['error2' => 'Employee Supplementary Bill already exists for this month.']);
            }

            $financial_year = FinancialYear::find(session('financial_year'));

            // Check if leave applies and if the employee is present for fewer days than the number of days in the month

            $freezeSalary = RemainingFreezeSalary::where('employee_id', $input['employee_id'])
                ->where('Emp_Code', $input['Emp_Code'])
                ->whereIn('month', $input['month'])
                ->get();

            $employee_sts = EmployeeStatus::where('employee_id',$input['employee_id'])
                                            ->where('Emp_Code',$input['Emp_Code'])
                                            ->first();

            $leaveApply = AddEmployeeLeave::where('employee_id', $input['employee_id'])
            ->where('month', $input['month'])
            ->where('status', 1)
            ->exists();

            if (!empty($financial_year) && $freezeSalary->isNotEmpty() && $leaveApply && !empty($employee_sts)) {

                $remaining_freeze_ids = $freezeSalary->pluck('id')->implode(',');
                $month_ids = $freezeSalary->pluck('month')->implode(',');

                $last_month = $freezeSalary->pluck('month')->last();
                $year = ($last_month <= 3) ? date('Y', strtotime($financial_year->to_date)) : date('Y', strtotime($financial_year->from_date));

                $fromDate = Carbon::createFromDate($year, $last_month, 01)->startOfMonth();
                $toDate = Carbon::createFromDate($year, $last_month, 01)->endOfMonth();

                $input['from_date'] = $fromDate->toDateString();
                $input['to_date'] = $toDate->toDateString();
                $input['financial_year_id'] = session('financial_year');
                $input['remaining_freeze_id'] = $remaining_freeze_ids;
                $input['month'] = $month_ids;
                $input['type'] = 2;

                SupplimentaryBill::create($input);

                 // Fetch the actual RemainingFreezeSalary models
                 $remaining_freeze_salaries = RemainingFreezeSalary::whereIn('id', $freezeSalary->pluck('id'))->get();

                 // Loop through each remaining freeze salary and update
                //  foreach ($remaining_freeze_salaries as $salary) {
                //      $salary->update([
                //          'stamp_duty' => 1,
                //          'total_deduction' => ($salary->total_deduction != 0) ? $salary->total_deduction - 1 : 0,
                //          'net_salary' => $salary->net_salary + 1
                //      ]);
                //  }
                // FreezeAttendance::whereIn('id', $freezeSalary->pluck('freeze_attendance_id')->toArray())
                //     ->update(['supplimentary_status' => 1]);

                DB::commit();

                return response()->json(['success' => 'Supplementary Bill generated successfully!']);

            } else {
                $sts = '';
                if (empty($financial_year)) {
                    $sts = 'Financial Year';
                } elseif ($freezeSalary->isEmpty()) {
                    $sts = 'Salary';
                } elseif (!$leaveApply) {
                    $sts = 'Leave';
                }

                if (empty($employee_sts)) {
                    return response()->json(['error2' => 'Employee Status Not Upload Cant Generate Suspend Bill !']);
                }
                else{
                    return response()->json(['error2' => $sts . ' Not Found']);
                }

            }
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->respondWithAjax($e, 'creating', 'Supplementary Bill');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
