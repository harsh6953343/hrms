<?php

namespace App\Http\Controllers\Admin\Employee;

use App\Http\Controllers\Admin\Controller;
use App\Models\EmployeeMonthlyLoan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class EmployeeMonthlyLoanController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $authUser = Auth::user();

        $employee_monthly_loans = EmployeeMonthlyLoan::with('employee')
                                    ->when($authUser->hasRole('Ward HOD'), function ($query) use ($authUser) {
                                        $query->whereHas('employee', function ($employeeQuery) use ($authUser) {
                                            $employeeQuery->where('ward_id', $authUser->ward_id);
                                        });
                                    })
                                    ->when($authUser->id == 1, function ($query) {
                                        $query->whereHas('employee', function ($employeeQuery) {
                                            $employeeQuery->whereNot('ward_id', 84);
                                        });
                                    })
                                    ->groupBy('from_date', 'to_date')
                                    ->select('from_date', 'to_date')
                                    ->get();


        return view('admin.employee.loans.employee-monthly-loans')->with(['employee_monthly_loans' => $employee_monthly_loans]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id, $test)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(EmployeeMonthlyLoan $employeeMonthlyLoan)
    {
        //
    }

    public function showEmployeeLoans($from_date, $to_date)
    {
        $employee_monthly_loans = EmployeeMonthlyLoan::with('loan')->where('from_date', $from_date)->where('to_date', $to_date)->latest()->get();
        return view('admin.employee.loans.employee-monthly-loans-list')->with(['employee_monthly_loans' => $employee_monthly_loans]);
    }

    // Change Loan Status
    public function employeeMonthlyLoanStatus($model_id, Request $request)
    {
        try {
            $employeeMonthlyLoan = EmployeeMonthlyLoan::find($model_id);
            if (!$employeeMonthlyLoan) {
                return response()->json(['error' => 'Employee monthly loan not found'], 404);
            }
            DB::beginTransaction();
            $employeeMonthlyLoan->remark = $request->remark;
            $employeeMonthlyLoan->save();
            $employeeMonthlyLoan->delete();
            DB::commit();
            return response()->json(['success' => 'Loan deleted successfully!']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['error' => 'An error occurred while deleting the loan'], 500);
        }
    }
}
