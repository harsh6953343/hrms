<?php

namespace App\Http\Controllers\Admin\Employee;

use App\Http\Controllers\Controller;
use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


class YearlyBonusController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $authUser = Auth::user();

        $yearly_bonuses = Employee::when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
            return $query->where('ward_id', $authUser->ward_id);
        })
            ->where('yearly_bonus','!=',0)
            ->latest()->get();

        return view('admin.employee.yearlyBonus.yearly-bonus')->with(['yearly_bonuses' => $yearly_bonuses]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try {

            // dd($request->all());
            DB::beginTransaction();
            $validated = $request->validate([
                'yearly_bonus' => 'required|numeric',
            ]);

            if (!$validated) {
                return response()->json(['error' => 'Please add the yearly bonus.']);
            } else {
                Employee::where('employee_id', $request->Emp_Code)->update([
                    'yearly_bonus'      => $request->yearly_bonus,
                ]);
                DB::commit();
                return response()->json(['success' => 'Yearly Bonus added successfully!']);
            }
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'creating', 'Yearly Bonus');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    public function fetchEmployeeDetails($emp_id)
    {
        $authUser = Auth::user();
        $employee_details = Employee::with('ward','department','designation','class')->where('employee_id', $emp_id)
            ->when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
                return $query->where('ward_id', $authUser->ward_id);
            })
            ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
                return $query->where('department_id', $authUser->department_id);
            })
            ->select('id', 'fname', 'mname', 'lname', 'ward_id', 'department_id', 'clas_id', 'designation_id','pf_account_no','yearly_bonus')
            ->whereNotExists(function ($query) use ($emp_id) {
                $query->select(DB::raw(1))
                    ->from('employee_statuses')
                    ->whereColumn('employee_statuses.employee_id', 'employees.id')
                    ->where('employee_statuses.Emp_Code', $emp_id)
                    ->whereNull('employee_statuses.deleted_at');
            })
            ->first();


        if ($employee_details) {
            $response = [
                'result' => 1,
                'employee_details' => $employee_details,
            ];
        } else {
            $response = ['result' => 0];
        }
        return $response;
    }
}
