<?php

namespace App\Http\Controllers\Admin\Employee;

use App\Http\Controllers\Admin\Controller;
use App\Http\Requests\Admin\Employee\StoreEmployeeLoanRequest;
use App\Models\Employee;
use App\Models\EmployeeLoan;
use App\Models\Loan;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class EmployeeLoanController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $authUser = Auth::user();
        $employee_loans = EmployeeLoan::with('loan', 'employee')
                            ->when($authUser->hasRole('Ward HOD'), function ($query) use ($authUser) {
                                $query->whereHas('employee', function ($employeeQuery) use ($authUser) {
                                    $employeeQuery->where('ward_id', $authUser->ward_id);
                                });
                            })
                            ->when($authUser->id == 1, function ($query) {
                                $query->whereHas('employee', function ($employeeQuery) {
                                    $employeeQuery->whereNot('ward_id', 84);
                                });
                            })
                            ->latest()->get();

        $loans = Loan::where('activity_status', 1)->latest()->get();

        return view('admin.employee.loans.employee-loans')->with(['employee_loans' => $employee_loans, 'loans' => $loans]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreEmployeeLoanRequest $request)
    {
        try {
            DB::beginTransaction();
            $input = $request->validated();

            $check_loan_already_added = EmployeeLoan::where('employee_id', $input['employee_id'])->where('loan_id', $input['loan_id'])->where('instalment_amount', $input['instalment_amount'])->where('status', 1)->first();

            if (empty($check_loan_already_added)) {
                $input['pending_instalment'] = $input['total_instalment'];
                $input['financial_year_id'] = session('financial_year');
                EmployeeLoan::create(Arr::only($input, EmployeeLoan::getFillables()));
                DB::commit();
                return response()->json(['success' => 'Employee Loan created successfully!']);
            } else {
                return response()->json(['error2' => 'Employee Loan Already Added!']);
            }
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'creating', 'Employee Loan');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    // Change Loan Status
    public function employeeLoanStatus($model_id, $btn_status, Request $request)
    {
        try {
            $employeeLoan = EmployeeLoan::find($model_id);
            $employeeLoan->status = $btn_status;
            $employeeLoan->remark = $request->remark;
            $employeeLoan->status_by = Auth::user()->id;
            $employeeLoan->status_date = date('Y-m-d H:i:s');
            $employeeLoan->save();
            return response()->json(['success' => 'Loan status update successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'updating', 'Loan');
        }
    }
}
