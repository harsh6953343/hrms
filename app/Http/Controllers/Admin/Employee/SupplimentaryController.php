<?php

namespace App\Http\Controllers\Admin\Employee;

use App\Http\Controllers\Admin\Controller;
use App\Http\Requests\Admin\Employee\StoreSupplimentaryBillRequest;
use App\Models\AddEmployeeLeave;
use App\Models\Allowance;
use App\Models\Corporation;
use App\Models\Deduction;
use App\Models\Employee;
use App\Models\EmployeeStatus;
use App\Models\FinancialYear;
use App\Models\FreezeAttendance;
use App\Models\RemainingFreezeSalary;
use App\Models\SupplimentaryBill;
use Barryvdh\Snappy\Facades\SnappyPdf;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SupplimentaryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $authUser = Auth::user();

        $supplimentory_bills = SupplimentaryBill::with('employee')
                                    ->when($authUser->hasRole('Ward HOD'), function ($query) use ($authUser) {
                                        $query->whereHas('employee', function ($employeeQuery) use ($authUser) {
                                            $employeeQuery->where('ward_id', $authUser->ward_id);
                                        });
                                    })
                                    ->when($authUser->id == 1, function ($query) {
                                        $query->whereHas('employee', function ($employeeQuery) {
                                            $employeeQuery->whereNot('ward_id', 84);
                                        });
                                    })
                                    ->where('type',1)->latest()->get();


        return view('admin.supplimentary.regular-supplimentary-bill')->with(['supplimentory_bills' => $supplimentory_bills, 'type' => 1]);

    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.supplimentary.add-regular-supplimentary-bill');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreSupplimentaryBillRequest $request)
    {
        try {
            DB::beginTransaction();

            $input = $request->validated();
            $month = $input['month'];

            // Check if the status already exists for the given employee ID
            $existingBill = SupplimentaryBill::where('employee_id', $input['employee_id'])
                ->get()
                ->filter(function ($bill) use ($month) {
                    return collect(explode(',', $bill->month))->intersect($month)->isNotEmpty();
                })
                ->first();

            if ($existingBill) {
                return response()->json(['error2' => 'Employee Supplementary Bill already exists for this month.']);
            }

            $financial_year = FinancialYear::find(session('financial_year'));

            // Check if leave applies and if the employee is present for fewer days than the number of days in the month

            $freezeSalary = RemainingFreezeSalary::where('employee_id', $input['employee_id'])
                ->where('Emp_Code', $input['Emp_Code'])
                ->whereIn('month', $input['month'])
                ->get();

            $employee_sts = EmployeeStatus::where('employee_id',$input['employee_id'])
                                            ->where('Emp_Code',$input['Emp_Code'])
                                            ->first();

            $leaveApply = AddEmployeeLeave::where('employee_id', $input['employee_id'])
                                            ->where('month', $input['month'])
                                            ->where('status', 1)
                                            ->exists();


            if (!empty($financial_year) && $freezeSalary->isNotEmpty() && $leaveApply && empty($employee_sts)) {

                $remaining_freeze_ids = $freezeSalary->pluck('id')->implode(',');
                $month_ids = $freezeSalary->pluck('month')->implode(',');
                $last_month = $freezeSalary->pluck('month')->last();
                $year = ($last_month <= 3) ? date('Y', strtotime($financial_year->to_date)) : date('Y', strtotime($financial_year->from_date));

                $fromDate = Carbon::createFromDate($year, $last_month, 01)->startOfMonth();
                $toDate = Carbon::createFromDate($year, $last_month, 01)->endOfMonth();

                $input['from_date'] = $fromDate->toDateString();
                $input['to_date'] = $toDate->toDateString();
                $input['financial_year_id'] = session('financial_year');
                $input['remaining_freeze_id'] = $remaining_freeze_ids;
                $input['month'] = $month_ids;
                $input['type'] = 1;

                SupplimentaryBill::create($input);

                // Fetch the actual RemainingFreezeSalary models
                $remaining_freeze_salaries = RemainingFreezeSalary::whereIn('id', $freezeSalary->pluck('id'))->get();

                // Loop through each remaining freeze salary and update
                // foreach ($remaining_freeze_salaries as $salary) {
                //     $salary->update([
                //         'stamp_duty' => 1,
                //         'total_deduction' => ($salary->total_deduction != 0) ? $salary->total_deduction - 1 : 0,
                //         'net_salary' => $salary->net_salary + 1
                //     ]);
                // }

                // FreezeAttendance::whereIn('id', $freezeSalary->pluck('freeze_attendance_id')->toArray())
                //     ->update(['supplimentary_status' => 1]);

                DB::commit();

                return response()->json(['success' => 'Supplementary Bill generated successfully!']);

            } else {
                $sts = '';
                if (empty($financial_year)) {
                    $sts = 'Financial Year';
                } elseif ($freezeSalary->isEmpty()) {
                    $sts = 'Salary';
                } elseif (!$leaveApply) {
                    $sts = 'Leave';
                }

                if (!empty($employee_sts)) {
                    return response()->json(['error2' => 'Employee Status Already Upload Cant Generate Regular !']);
                }
                else{
                    return response()->json(['error2' => $sts . ' Not Found']);
                }

            }
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->respondWithAjax($e, 'creating', 'Supplementary Bill');
        }
    }


    /**
     * Display the specified resource.
     */
    public function show(SupplimentaryBill $supplimentaryBill)
    {

        $remaining_freeze_ids = explode(',', $supplimentaryBill->remaining_freeze_id);

        $data['freezeAttendance'] = RemainingFreezeSalary::whereIn('id', $remaining_freeze_ids)->get();
        $data['supplimentory_bill'] = $supplimentaryBill;

        $data['employee_details'] = Employee::with('ward', 'department', 'designation')->where('id', $supplimentaryBill->employee_id)->first();
        $data['allowances'] = Allowance::latest()->get();
        $data['deductions'] = Deduction::latest()->get();

        $data['corporation'] = Corporation::first();

        $logoPath = public_path($data['corporation']->logo);

        if (file_exists($logoPath)) {
            $logoData = file_get_contents($logoPath);
            $base64Logo = base64_encode($logoData);
        } else {
            // Handle if the logo file doesn't exist
            $base64Logo = null;
        }

        $data['base64Logo'] = $base64Logo;

        $filename = "Supplimentary Bill" . " " . $supplimentaryBill->Emp_Code . '.pdf';
        $pdf = SnappyPdf::loadView('admin.supplimentary.pdf.supplimentary-bill', $data)
            ->setPaper('a4')
            ->setOrientation('portrait')
            ->setOption('margin-bottom', 0)
            ->setOption('margin-top', 3)
            ->setOption('margin-left', 0)
            ->setOption('margin-right', 0);

        return $pdf->inline($filename);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(SupplimentaryBill $supplimentaryBill)
    {
        try {
            DB::beginTransaction();

            // Retrieve associated RemainingFreezeSalaries based on the `remaining_freeze_id` stored in SupplimentaryBill
            $remaining_freeze_ids = explode(',', $supplimentaryBill->remaining_freeze_id); // Convert stored IDs back to an array

            // Fetch the RemainingFreezeSalary records
            $remaining_freeze_salaries = RemainingFreezeSalary::whereIn('id', $remaining_freeze_ids)->get();

            // Reverse the deduction by adding 1 Rs back to total_deduction and adjusting net_salary
            foreach ($remaining_freeze_salaries as $salary) {
                $salary->update([
                    'stamp_duty' => 1,
                    'total_deduction' => $salary->total_deduction + 1,
                    'net_salary' => $salary->net_salary - 1, // Adjust net salary logic here
                ]);
            }

            // Delete the SupplimentaryBill after reversing the changes
            $supplimentaryBill->delete();

            DB::commit();

            return response()->json(['success' => 'Supplimentary Bill deleted and changes reversed successfully!']);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->respondWithAjax($e, 'deleting', 'Supplimentary Bill');
        }
    }

    // public function destroy(SupplimentaryBill $supplimentaryBill)
    // {
    //     try {
    //         DB::beginTransaction();
    //         $supplimentaryBill->delete();
    //         DB::commit();
    //         return response()->json(['success' => 'Supplimentary Bill deleted successfully!']);
    //     } catch (\Exception $e) {
    //         return $this->respondWithAjax($e, 'deleting', 'Supplimentary Bill');
    //     }
    // }

    public function fetchEmployeeDetails($emp_id)
    {
        $authUser = Auth::user();
        $employee_details = Employee::with('ward', 'department', 'designation', 'class')
            ->where('employee_id', $emp_id)
            ->when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
                return $query->where('ward_id', $authUser->ward_id);
            })
            ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
                return $query->where('department_id', $authUser->department_id);
            })
            ->select('id', 'fname', 'mname', 'lname', 'ward_id', 'department_id', 'clas_id', 'designation_id')
            ->first();


        if ($employee_details) {
            $response = [
                'result' => 1,
                'employee_details' => $employee_details,
            ];
        } else {
            $response = ['result' => 0];
        }
        return $response;
    }
}
