<?php

namespace App\Http\Controllers\Admin\Employee;

use App\Http\Controllers\Admin\Controller;
use App\Http\Requests\Admin\Employee\StoreIncrementAllowanceRequest;
use App\Models\Allowance;
use App\Models\Department;
use Illuminate\Http\Request;
use App\Models\Clas;
use App\Models\Employee;
use App\Models\EmployeeAllowance;
use App\Models\IncrementAllowanceSalary;
use App\Models\OldEmployeeAllowance;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AllowanceIncrementController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $authUser = Auth::user();

        $departments = Department::
                            when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
                                return $query->where('ward_id', $authUser->ward_id);
                            })
                            ->when($authUser->id == 1, function ($query) {
                                return $query->where('ward_id','!=', 84);
                            })
                            ->latest()->get();
        $allowances = Allowance::latest()->get();;
        $classes = Clas::latest()->get();

        return view('admin.employee.allowance-increment')->with(['departments' => $departments, 'allowances' => $allowances, 'classes' => $classes]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreIncrementAllowanceRequest $request)
    {
        $authUser = Auth::user();

        try {
            DB::beginTransaction();
            $input = $request->validated();

            if ($input['department_id'] == 'all') {
                $input['department_id'] = 0;
            } else {
                $input['department_id'] = $input['department_id'];
            }

            if ($input['clas_id'] == 'all') {
                $input['clas_id'] = 0;
            } else {
                $input['clas_id'] = $input['clas_id'];
            }

            IncrementAllowanceSalary::create(Arr::only($input, IncrementAllowanceSalary::getFillables()));
            $emp_ids = $input['emp_id'];
            $allowance = $input['allowance_id'];

            $allowance_unique_id = $input['allowance_unique_id'];
            foreach ($emp_ids as $key => $emp_id) {

                $employee_allowance = EmployeeAllowance::where('employee_id', $emp_id)->where('id', $allowance_unique_id[$key]);

                $old_employee_allowance = OldEmployeeAllowance::where('employee_id', $emp_id)->where('allowance_id', $allowance)->latest()->first();

                $employee_allowance->update([
                    'allowance_amt' => $input['new_amount'],
                    'updated_by' => $authUser->id,
                    'updated_at' => now(),
                ]);

                $old_employee_allowance->update([
                    'end_date' => date('Y-m-d'),
                    'updated_by' => $authUser->id,
                    'updated_at' => now(),
                    'is_active' => 0,
                ]);

                $old_employee_salary_create = [
                    'employee_id' => $emp_id,
                    'Emp_Code' => $old_employee_allowance->Emp_Code,
                    'old_employee_salary_id' => $old_employee_allowance->old_employee_salary_id,
                    'allowance_id' => $allowance,
                    'allowance_amt' => $input['new_amount'],
                    'allowance_type' => $old_employee_allowance->allowance_type,
                    'is_active' => 1,
                    'applicable_date' => date('Y-m-d')
                ];

                OldEmployeeAllowance::create($old_employee_salary_create);
            }

            DB::commit();

            return response()->json(['success' => 'Basic Salary Increment successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'creating', 'Basic Salary');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    public function fetchAllowanceDetails($allowance_id)
    {
        $allowanceDetails = Allowance::where('id', $allowance_id)->get()->first();

        if ($allowanceDetails) {
            $response = [
                'result' => 1,
                'allowanceDetails' => $allowanceDetails,
            ];
        } else {
            $response = ['result' => 0];
        }
        return $response;
    }

    public function getIncrementedAllowance(Request $request)
    {
        try {
            $departmentId = $request->input('department_id');
            $clas_id = $request->input('clas_id');
            $allowance_id = $request->input('allowance_id');
            $new_amount = $request->input('new_amount');

            $get_employees = Employee::with('department')->when(
                $departmentId == 'all',
                function ($query) {
                    return $query->latest();
                },
                function ($query) use ($departmentId, $clas_id) {
                    $query->when($departmentId != 'all', function ($subquery) use ($departmentId) {
                        $subquery->where('department_id', $departmentId);
                    })->when($clas_id != 'all', function ($subquery) use ($clas_id) {
                        $subquery->where('clas_id', $clas_id);
                    });

                    return $query->latest();
                }
            )->get();

            $incrementArr = [];

            foreach ($get_employees as $key => $employee) {

                $employeeSalary = EmployeeAllowance::with('allowance')->where('employee_id', $employee->id)
                    ->where('allowance_id', $allowance_id)->first();

                if ($employeeSalary) {
                    $incrementArr[] = [
                        'sr_no'                 =>  $key + 1,
                        'employee_uniques_id'   =>  $employee->id,
                        'allowance_unique_id'   =>  $employeeSalary->id,
                        'emp_id'                =>  $employee->employee_id,
                        'emp_name'              =>  $employee->fname . " " . $employee->mname . " " . $employee->lname,
                        'department'            =>  $employee->department->name,
                        'allowance'             =>  $employeeSalary->allowance->allowance,
                        'current_allowances'    =>  $employeeSalary->allowance_amt,
                        'increment_allowances'  =>  $new_amount,
                    ];
                }
            }

            $response = [
                'result' => 1,
                'incrementArr' => $incrementArr,
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            return response()->json(['result' => 0, 'error' => $e->getMessage()]);
        }
    }
}
