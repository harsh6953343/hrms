<?php

namespace App\Http\Controllers\Admin\TaxCalculation;

use App\Http\Controllers\Controller;
use App\Models\FinancialYear;
use App\Models\Employee;
use App\Models\SupplimentaryBill;
use App\Models\RemainingFreezeSalary;
use App\Models\FreezeAttendance;
use App\Models\Allowance;
use App\Models\IncomeTax;
use Carbon\Carbon;
use App\Http\Requests\StoreIncomeTaxRequest;
use Illuminate\Support\Facades\Auth;

class TaxCalculationController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    public function saveIncomeTaxDetails(StoreIncomeTaxRequest $request)
    {
        try {
            $validatedData = $request->validated();
            IncomeTax::create($validatedData);
            return response()->json(['success' => 'Income Tax saved successfully!']);

        } catch (Exception $e) {
            return $this->respondWithAjax($e, 'saving', 'IncomeTax');
        }
    }

    public function showCalculationForm()
    {
        $finYears = FinancialYear::where('is_active', 1)->latest()->pluck('title', 'id')->toArray();

        $employees = Employee::whereNotIn('employee_id', function ($query) {
            $query->select('Emp_Code')->from('employee_statuses')
                ->whereNull('deleted_at');
        })->latest()->get();

        return view('admin.incomeTax.income-tax-form')->with(['finYears' => $finYears, 'employees' => $employees]);
    }

    public function fetchEmployeeDetails($emp_code, $financial_year_id)
    {
        $authUser = Auth::user();

        $employee_details = Employee::where('employee_id', $emp_code)
            ->when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
                return $query->where('ward_id', $authUser->ward_id);
            })
            ->when($authUser->id == 1, function ($query) {
                return $query->where('ward_id','!=', env('EDUCATION_WARD_ID'));
            })
            ->select('employee_id', 'fname', 'mname', 'lname', 'dob', 'gender', 'pan')
            ->first();

        //--Get current gross salary and supplementary bill-------------------//
        $data['allowances'] = Allowance::get();

        // \DB::connection()->enableQueryLog();

        $currentYearRec = FreezeAttendance::with('employee')
            ->where('financial_year_id', $financial_year_id)
                            ->where('freeze_status', 1)
                            ->where('Emp_code', $emp_code)
                            ->get();
        // $queries = \DB::getQueryLog();
// dd($queries);

        $data['freezeAttendances'] = $currentYearRec;//dd($data);
        $income = $this->getGrossSalary($data);
        //------------------------------------------------------------------------------------//

        $response = [
            'result'                => 1,
            'employee_details'      => $employee_details,
            'current_gross_salary'  => $income['gross_salary'],
            'supplementary_bill'    => $income['supplementary_bill'],
            'gross_income'          => $income['gross_salary'] + $income['supplementary_bill'],
        ];

        return response()->json($response);
    }

    //--Get the current gross salary and supplementary bill
    public function getGrossSalary($data)
    {
        $grand_total_basic_salary = 0;
        $grand_total_da_differance = 0;
        $grand_total_earn = 0;
        $grand_total_festival_allowance = 0;

        $allowanceTotals = [];

        // Supplimentary
        $grand_total_supplimentary_basic_salary = 0;
        $grand_supplimentary_allowanceTotals = [];
        $grand_supplimentary_total_earn = 0;
        $grand_supplimentary_total_stamp_duty = 0;
        $grand_supplimentary_total_deductions = 0;
        $grand_supplimentary_total_net_salary = 0;
        $grand_supplimentary_total_corporation = 0;
        $grand_supplimentary_total_lic = 0;
        $grand_supplimentary_total_festival_advance = 0;
        $grand_supplimentary_total_festival_deduction = 0;
        $grand_supplimentary_total_employee_share = 0;


        foreach ($data['freezeAttendances'] as $index_key => $freezeAttendance) {
            $explode_allowance_ids = explode(',', $freezeAttendance->allowance_Id);
            $explode_allowance_type = explode(',', $freezeAttendance->allowance_Type);
            $explode_allowance_amt = explode(',', $freezeAttendance->allowance_Amt);

            // Grand Total Calculations
            $grand_total_basic_salary += $freezeAttendance->basic_salary;
            $grand_total_da_differance += $freezeAttendance->da_differance;

            $grand_total_earn += ($freezeAttendance->basic_salary + $freezeAttendance->total_allowance);
            $grand_total_festival_allowance += $freezeAttendance->festival_allowance;

            $supplimentaryData = '';

            if ($freezeAttendance->supplimentary_status == 1) {

                $supplimentary_record = SupplimentaryBill::where('employee_id', $freezeAttendance->employee_id)
                    ->where('Emp_Code', $freezeAttendance->Emp_Code)
                    ->where('id', $freezeAttendance->supplimentary_ids)->first();

                $remaining_freeze_ids = explode(',', $supplimentary_record->remaining_freeze_id);

                $supplimentaryData = RemainingFreezeSalary::whereIn('id', $remaining_freeze_ids)->get();

                $supplimentary_basic_salary = 0;
                $supplimentary_total_earn = 0;

                $supplimentaryallowanceTotals = [];

                $supplimentary_stamp_duty = 0;
                $supplimentary_total_deductions = 0;
                $suppliemnatry_net_salary = 0;
                $suppliemnatry_corporation_share = 0;
                $suppliemnatry_present_days = 0;
                $supplimentary_lic = 0;
                $supplimentary_festival_advance = 0;
                $supplimentary_festival_deduction = 0;
                $supplimentary_employee_share = 0;

                if (!empty($supplimentaryData)) {
                    foreach ($supplimentaryData as $freeze) {

                        $supplimentary_explode_allowance_ids = explode(',', $freeze->allowance_Id);
                        $supplimentary_explode_allowance_amt = explode(',', $freeze->allowance_Amt);

                        $supplimentary_basic_salary += $freeze->basic_salary;

                        $supplimentary_total_earn   += ($freeze->basic_salary + $freeze->total_allowance);

                        // $grand_total_bank_loan+= $bank_loan;
                        $supplimentary_stamp_duty       += $freeze->stamp_duty;
                        $supplimentary_lic              += $freeze->total_lic_deduction;
                        $supplimentary_festival_advance += $freeze->festival_allowance;
                        $supplimentary_festival_deduction += $freeze->total_festival_deduction;
                        $supplimentary_employee_share   += $freeze->employee_share_da;



                        $supplimentary_total_deductions += $freeze->total_deduction;

                        $suppliemnatry_net_salary += $freeze->net_salary;
                        $suppliemnatry_corporation_share += $freeze->corporation_share_da;

                        if ($freeze->present_day == 0) {
                            $startDate = Carbon::createFromFormat('Y-m-d', $freeze->from_date);
                            $endDate = Carbon::createFromFormat('Y-m-d', $freeze->to_date);
                            $numberOfDaysInMonth = $startDate->diffInDays($endDate);
                            $numberOfDaysInMonth += 1;
                            $suppliemnatry_present_days += $numberOfDaysInMonth;
                        }
                        $suppliemnatry_present_days += $freeze->present_day;

                        foreach ($data['allowances']->chunk(5, true) as $chunk) {
                            foreach ($chunk as $allowance) {

                                $index = array_search($allowance->id, $supplimentary_explode_allowance_ids);

                                if ($index !== false) {

                                    if (array_key_exists($allowance->id, $supplimentaryallowanceTotals)) {
                                        // If it exists, add the deduction amount to the existing total
                                        $supplimentaryallowanceTotals[$allowance->id] += $supplimentary_explode_allowance_amt[$index];
                                    } else {
                                        // If it doesn't exist, initialize the total with the deduction amount
                                        $supplimentaryallowanceTotals[$allowance->id] = $supplimentary_explode_allowance_amt[$index];
                                    }

                                    if (array_key_exists($allowance->id, $grand_supplimentary_allowanceTotals)) {
                                        // If it exists, add the deduction amount to the existing total
                                        $grand_supplimentary_allowanceTotals[$allowance->id] += $supplimentary_explode_allowance_amt[$index];
                                    } else {
                                        // If it doesn't exist, initialize the total with the deduction amount
                                        $grand_supplimentary_allowanceTotals[$allowance->id] = $supplimentary_explode_allowance_amt[$index];
                                    }
                                }
                            }
                        }
                    }
                }

                $grand_total_supplimentary_basic_salary +=  $supplimentary_basic_salary;
                $grand_supplimentary_total_earn         +=  $supplimentary_total_earn;
                $grand_supplimentary_total_stamp_duty   +=  $supplimentary_stamp_duty;
                $grand_supplimentary_total_deductions   +=  $supplimentary_total_deductions;
                $grand_supplimentary_total_net_salary   +=  $suppliemnatry_net_salary;
                $grand_supplimentary_total_corporation  +=  $suppliemnatry_corporation_share;
                $grand_supplimentary_total_lic          +=  $supplimentary_lic;
                $grand_supplimentary_total_festival_advance +=  $supplimentary_festival_advance;
                $grand_supplimentary_total_festival_deduction +=  $supplimentary_festival_deduction;
                $grand_supplimentary_total_employee_share +=  $supplimentary_employee_share;


                foreach ($data['allowances']->chunk(5, true) as $chunk) {
                    foreach ($chunk as $allowance) {
                        // Find the index of the allowance ID in the $explode_allowance_ids array
                        $index = array_search($allowance->id, $explode_allowance_ids);

                        if ($index !== false) {

                            if (array_key_exists($allowance->id, $allowanceTotals)) {
                                // If it exists, add the allowance amount to the existing total
                                $allowanceTotals[$allowance->id] += $explode_allowance_amt[$index];
                            } else {
                                // If it doesn't exist, initialize the total with the allowance amount
                                $allowanceTotals[$allowance->id] = $explode_allowance_amt[$index];
                            }

                            $total_amount = $explode_allowance_amt[$index];
                            if (!empty($supplimentaryData)) {
                                $supplementary_total = isset($supplimentaryallowanceTotals[$allowance->id]) ? $supplimentaryallowanceTotals[$allowance->id] : 0;
                                $total_amount += $supplementary_total;
                            } elseif (!in_array($allowance->id, $explode_allowance_ids)) {

                                $supplementary_total = isset($supplimentaryallowanceTotals[$allowance->id]) ? $supplimentaryallowanceTotals[$allowance->id] : '-';
                            }
                        }
                    }
                }
            }
        }
        $grand_total_earn += $grand_total_da_differance;

        return array('gross_salary' => $grand_total_earn, 'supplementary_bill' => $grand_supplimentary_total_earn);
    }
}
