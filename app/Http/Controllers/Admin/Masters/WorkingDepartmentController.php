<?php

namespace App\Http\Controllers\Admin\Masters;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Masters\StoreWorkingDepartmentRequest;
use App\Models\WorkingDepartment;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class WorkingDepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $working_departments = WorkingDepartment::latest()->get();

        return view('admin.masters.working-department')->with(['working_departments' => $working_departments]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreWorkingDepartmentRequest $request)
    {
        try {
            DB::beginTransaction();
            $input = $request->validated();
            WorkingDepartment::create(Arr::only($input, WorkingDepartment::getFillables()));
            DB::commit();

            return response()->json(['success' => 'Working Department created successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'creating', 'Working Department');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(WorkingDepartment $workingDepartment)
    {
        if ($workingDepartment) {
            $response = [
                'result' => 1,
                'workingDepartment' => $workingDepartment,
            ];
        } else {
            $response = ['result' => 0];
        }
        return $response;
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(StoreWorkingDepartmentRequest $request, WorkingDepartment $workingDepartment)
    {
        try {
            DB::beginTransaction();
            $input = $request->validated();
            $workingDepartment->update(Arr::only($input, WorkingDepartment::getFillables()));
            DB::commit();

            return response()->json(['success' => 'Working Department updated successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'updating', 'Working Department');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(WorkingDepartment $workingDepartment)
    {
        try {
            DB::beginTransaction();
            $workingDepartment->delete();
            DB::commit();
            return response()->json(['success' => 'Working Department deleted successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'deleting', 'Working Department');
        }
    }
}
