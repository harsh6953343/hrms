<?php

namespace App\Http\Controllers\Admin\Masters;

use App\Http\Controllers\Admin\Controller;
use Illuminate\Http\Request;
use App\Models\Loan;
use App\Http\Requests\Admin\Masters\StoreLoanRequest;
use App\Http\Requests\Admin\Masters\UpdateLoanRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

class LoanController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $loans = Loan::latest()->get();

        return view('admin.masters.loans')->with(['loans' => $loans]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreLoanRequest $request)
    {
        try {
            DB::beginTransaction();
            $input = $request->validated();
            Loan::create(Arr::only($input, Loan::getFillables()));
            DB::commit();

            return response()->json(['success' => 'Loan created successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'creating', 'Loan');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Loan $loan)
    {
        if ($loan) {

            $response = [
                'result' => 1,
                'loan' => $loan,
            ];
        } else {
            $response = ['result' => 0];
        }
        return $response;
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateLoanRequest $request, Loan $loan)
    {
        try {
            DB::beginTransaction();
            $input = $request->validated();
            $loan->update(Arr::only($input, Loan::getFillables()));
            DB::commit();

            return response()->json(['success' => 'Loan updated successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'updating', 'Loan');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Loan $loan)
    {
        try {
            DB::beginTransaction();
            $loan->delete();
            DB::commit();
            return response()->json(['success' => 'Loan deleted successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'deleting', 'Loan');
        }
    }

    // Change Loan Status
    public function changeStatus($model_id, $btn_status, Request $request)
    {
        try {
            $loanActivity = Loan::find($model_id);
            $loanActivity->activity_status = $btn_status;
            $loanActivity->activity_status_by = Auth::user()->id;
            $loanActivity->updated_at = date('Y-m-d H:i:s');
            $loanActivity->save();
            return response()->json(['success' => 'Loan status update successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'updating', 'Loan');
        }
    }
}
