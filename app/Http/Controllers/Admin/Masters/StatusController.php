<?php

namespace App\Http\Controllers\Admin\Masters;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Masters\StoreStatusRequest;
use App\Models\Status;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;



class StatusController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $statuses = Status::latest()->get();

        return view('admin.masters.statuses')->with(['statuses' => $statuses]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreStatusRequest $request)
    {
        try {
            DB::beginTransaction();
            $input = $request->validated();
            Status::create(Arr::only($input, Status::getFillables()));
            DB::commit();

            return response()->json(['success' => 'Status created successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'creating', 'Status');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Status $status)
    {
        if ($status) {

            $response = [
                'result' => 1,
                'status' => $status,
            ];
        } else {
            $response = ['result' => 0];
        }
        return $response;
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(StoreStatusRequest $request, Status $status)
    {
        try {
            DB::beginTransaction();
            $input = $request->validated();
            $status->update(Arr::only($input, Status::getFillables()));
            DB::commit();

            return response()->json(['success' => 'Status updated successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'updating', 'Status');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Status $status)
    {
        try {
            DB::beginTransaction();
            $status->delete();
            DB::commit();
            return response()->json(['success' => 'Status deleted successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'deleting', 'Status');
        }
    }
}