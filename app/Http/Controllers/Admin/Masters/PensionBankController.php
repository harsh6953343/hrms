<?php

namespace App\Http\Controllers\Admin\Masters;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Masters\StorePensionBankRequest;
use App\Http\Requests\Admin\Masters\UpdatePensionBankRequest;
use App\Models\PensionBank;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class PensionBankController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $pension_banks = PensionBank::latest()->get();

        return view('admin.masters.pension-bank')->with(['pension_banks' => $pension_banks]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StorePensionBankRequest $request)
    {
        try {
            DB::beginTransaction();
            $input = $request->validated();
            PensionBank::create(Arr::only($input, PensionBank::getFillables()));
            DB::commit();

            return response()->json(['success' => 'Pension Bank created successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'creating', 'Pension Bank');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(PensionBank $pensionBank)
    {

    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(PensionBank $pensionBank)
    {
        if ($pensionBank) {
            $response = [
                'result' => 1,
                'pensionBank' => $pensionBank,
            ];
        } else {
            $response = ['result' => 0];
        }
        return $response;
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdatePensionBankRequest $request, PensionBank $pensionBank)
    {
        try {
            DB::beginTransaction();
            $input = $request->validated();
            $pensionBank->update(Arr::only($input, PensionBank::getFillables()));
            DB::commit();

            return response()->json(['success' => 'Pension Bank updated successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'updating', 'Pension Bank');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(PensionBank $pensionBank)
    {
        try {
            DB::beginTransaction();
            $pensionBank->delete();
            DB::commit();
            return response()->json(['success' => 'Pension Bank deleted successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'deleting', 'Pension Bank');
        }

    }
}
