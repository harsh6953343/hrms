<?php

namespace App\Http\Controllers\Admin\Masters;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\PF\StoreIntrestRateRequest;
use App\Models\PfIntrestRate;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class IntrestRateController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $pf_intrest_rates = PfIntrestRate::latest()->get();

        return view('admin.masters.pf-intrest-rate')->with(['pf_intrest_rates' => $pf_intrest_rates]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreIntrestRateRequest $request)
    {
        try {
            DB::beginTransaction();
            $input = $request->validated();
            $input['active_status'] = 0;
            PfIntrestRate::create(Arr::only($input, PfIntrestRate::getFillables()));
            DB::commit();

            return response()->json(['success' => 'Intrest Rate created successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'creating', 'Intrest Rate');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(PfIntrestRate $intrest_rate)
    {
        if ($intrest_rate) {
            $response = [
                'result' => 1,
                'pfIntrestRate' => $intrest_rate,
            ];
        } else {
            $response = ['result' => 0];
        }
        return $response;
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(StoreIntrestRateRequest $request, PfIntrestRate $intrest_rate)
    {
        try {
            DB::beginTransaction();
            $input = $request->validated();
            $intrest_rate->update(Arr::only($input, PfIntrestRate::getFillables()));
            DB::commit();

            return response()->json(['success' => 'PF Intrest updated successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'updating', 'PF Intrest');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(PfIntrestRate $intrest_rate)
    {
        try {
            DB::beginTransaction();
            $intrest_rate->delete();
            DB::commit();
            return response()->json(['success' => 'Intrest Rate deleted successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'deleting', 'Intrest Rate');
        }
    }

    // Change Activity Status
    public function changeStatus($model_id, $btn_status, Request $request)
    {
        try {
            PfIntrestRate::where('active_status', 1)->update(['active_status' => 0]);
            $intrestRate = PfIntrestRate::find($model_id);
            $intrestRate->active_status = $btn_status;
            $intrestRate->activity_status_by = Auth::user()->id;
            $intrestRate->updated_at = date('Y-m-d H:i:s');
            $intrestRate->save();
            return response()->json(['success' => 'Loan status update successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'updating', 'Loan');
        }
    }
}
