<?php

namespace App\Http\Controllers\Admin\Masters;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Masters\StoreCasteRequest;
use App\Models\Caste;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class CasteController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $castes = Caste::latest()->get();

        return view('admin.masters.castes')->with(['castes' => $castes]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreCasteRequest $request)
    {
        try {
            DB::beginTransaction();
            $input = $request->validated();
            Caste::create(Arr::only($input, Caste::getFillables()));
            DB::commit();

            return response()->json(['success' => 'Caste created successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'creating', 'Caste');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Caste $caste)
    {
        if ($caste) {
            $response = [
                'result' => 1,
                'caste' => $caste,
            ];
        } else {
            $response = ['result' => 0];
        }
        return $response;
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(StoreCasteRequest $request, Caste $caste)
    {
        try {
            DB::beginTransaction();
            $input = $request->validated();
            $caste->update(Arr::only($input, Caste::getFillables()));
            DB::commit();

            return response()->json(['success' => 'Caste updated successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'updating', 'Caste');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Caste $caste)
    {
        try {
            DB::beginTransaction();
            $caste->delete();
            DB::commit();
            return response()->json(['success' => 'Caste deleted successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'deleting', 'Caste');
        }
    }
}
