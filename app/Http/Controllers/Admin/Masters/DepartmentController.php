<?php

namespace App\Http\Controllers\Admin\Masters;

use App\Http\Requests\Admin\Masters\StoreDepartmentRequest;
use App\Http\Requests\Admin\Masters\UpdateDepartmentRequest;
use App\Models\Department;
use App\Models\Ward;
use App\Http\Controllers\Admin\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $authUser = Auth::user();

        $departments = Department::with('ward')
            ->when($authUser->hasRole(['Ward HOD']), fn ($q) => $q->where('ward_id', $authUser->ward_id))
            ->when($authUser->id == 1, function ($query) {
                return $query->whereNot('ward_id', 84);
            })
            ->latest()->get();

        $wards = Ward::when($authUser->hasRole(['Ward HOD']), fn ($q) => $q->where('id', $authUser->ward_id))->latest()->get();

        return view('admin.masters.departments')->with(['departments' => $departments, 'wards' => $wards]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreDepartmentRequest $request)
    {
        try {
            DB::beginTransaction();
            $input = $request->validated();
            Department::create(Arr::only($input, Department::getFillables()));
            DB::commit();

            return response()->json(['success' => 'Department created successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'creating', 'Department');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Department $department)
    {
        $authUser = Auth::user();

        $wards = Ward::when($authUser->hasRole(['Ward HOD']), fn ($q) => $q->where('id', $authUser->ward_id))->latest()->get();

        if ($department) {

            $wardHtml = '<span>
            <option value="">--Select Ward--</option>';
            foreach ($wards as $ward) :
                $is_select = $ward->id == $department->ward_id ? "selected" : "";
                $wardHtml .= '<option value="' . $ward->id . '" ' . $is_select . '>' . $ward->name . '</option>';
            endforeach;
            $wardHtml .= '</span>';

            $response = [
                'result' => 1,
                'department' => $department,
                'wardHtml' => $wardHtml,
            ];
        } else {
            $response = ['result' => 0];
        }
        return $response;
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateDepartmentRequest $request, Department $department)
    {
        try {
            DB::beginTransaction();
            $input = $request->validated();
            $department->update(Arr::only($input, Department::getFillables()));
            DB::commit();

            return response()->json(['success' => 'Department updated successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'updating', 'Department');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Department $department)
    {
        try {
            DB::beginTransaction();
            $department->delete();
            DB::commit();
            return response()->json(['success' => 'Department deleted successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'deleting', 'Department');
        }
    }
}
