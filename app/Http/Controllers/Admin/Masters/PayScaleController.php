<?php

namespace App\Http\Controllers\Admin\Masters;

use App\Http\Controllers\Controller;
use App\Models\PayMst;
use Illuminate\Http\Request;
use App\Models\PayScale;
use App\Http\Requests\Admin\Masters\StorePayScaleRequest;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class PayScaleController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $payScales = PayScale::with('pay')->latest()->get();
        $payNames  = PayMst::latest()->get();

        return view('admin.masters.pay_scale')->with(['payScales' => $payScales, 'payNames' => $payNames]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StorePayScaleRequest $request)
    {
        try {
            DB::beginTransaction();
            $input = $request->validated();
            PayScale::create(Arr::only($input, PayScale::getFillables()));
            DB::commit();

            return response()->json(['success' => 'Pay Scale created successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'creating', 'Pay Scale');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(PayScale $payScale)
    {
        try {
            DB::beginTransaction();
            $payScale->delete();
            DB::commit();
            return response()->json(['success' => 'Pay Scale deleted successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'deleting', 'Pay Scale');
        }
    }
}