<?php

namespace App\Http\Controllers\Admin\Masters;

use App\Models\Clas;
use App\Http\Controllers\Admin\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\Masters\StoreClassRequest;
use App\Http\Requests\Admin\Masters\UpdateClassRequest;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class ClassController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $class = Clas::latest()->get();

        return view('admin.masters.class')->with(['class' => $class]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreClassRequest $request)
    {
        try {
            DB::beginTransaction();
            $input = $request->validated();
            Clas::create(Arr::only($input, Clas::getFillables()));
            DB::commit();

            return response()->json(['success' => 'Class created successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'creating', 'Class');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Clas $class)
    {
        if ($class) {

            $classHtml = '<span>
            <option value="">--Select Working Year--</option>';
            $classHtml .= '<option value="58" ' . ($class->working_year == 58 ? 'selected' : '') . '>58</option>';
            $classHtml .= '<option value="60" ' . ($class->working_year == 60 ? 'selected' : '') . '>60</option>';
            $classHtml .= '</span>';

            $emplyeeCategoryHtml = '<span>
            <option value="">--Select Employee Category--</option>';
            $emplyeeCategoryHtml .= '<option value="1" ' . ($class->employee_category == 1 ? 'selected' : '') . '>State Goverment</option>';
            $emplyeeCategoryHtml .= '<option value="2" ' . ($class->employee_category == 2 ? 'selected' : '') . '>Central Goverment</option>';
            $emplyeeCategoryHtml .= '</span>';

            $response = [
                'result' => 1,
                'class' => $class,
                'classHtml' => $classHtml,
                'emplyeeCategoryHtml' => $emplyeeCategoryHtml,
            ];
        } else {
            $response = ['result' => 0];
        }
        return $response;
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateClassRequest $request, Clas $class)
    {
        try {
            DB::beginTransaction();
            $input = $request->validated();
            $class->update(Arr::only($input, Clas::getFillables()));
            DB::commit();

            return response()->json(['success' => 'Class updated successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'updating', 'Class');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Clas $class)
    {
        try {
            DB::beginTransaction();
            $class->delete();
            DB::commit();
            return response()->json(['success' => 'Class deleted successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'deleting', 'Class');
        }
    }

    // Fetch Class on basis of Employee Category
    public function fetchClass($employee_category)
    {
        $class_arr = Clas::where('employee_category',$employee_category)->get();

        if ($class_arr) {
            $classHtml = '<span>
            <option value="">--Select Class--</option>';
            foreach ($class_arr as $class) :
                $classHtml .= '<option value="' . $class->id . '">' . $class->name . '</option>';
            endforeach;
            $classHtml .= '</span>';

            $response = [
                'result' => 1,
                'classHtml' => $classHtml,
            ];
        } else {
            $response = ['result' => 0];
        }
        return $response;

    }
}
