<?php

namespace App\Http\Controllers\Admin\PF;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\PF\ShowPFReportRequest;
use App\Models\Corporation;
use App\Models\Employee;
use App\Models\EmployeeProvidentFund;
use App\Models\FinancialYear;
use App\Models\PfIntrestRate;
use App\Models\PfOpeningBalance;
use Barryvdh\Snappy\Facades\SnappyPdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PFReport extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('admin.pf.pf-report');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(ShowPFReportRequest $request)
    {

        $input = $request->validated();
        $pf_account_no = $input['pf_account_no'];
        $financial_year_id = session('financial_year');

        $data['employee_provident_funds'] = EmployeeProvidentFund::where('pf_account_no', $pf_account_no)
            ->where('financial_year_id', $financial_year_id)
            ->get();

        $data['pf_opening_balance'] = PfOpeningBalance::where('financial_year_id', $financial_year_id)
            ->where('pf_no', $pf_account_no)
            ->first();

        $data['get_intrest_rate'] = PfIntrestRate::where('active_status', 1)->first();

        $data['employee_details'] = Employee::with('ward', 'department', 'designation', 'class')
                                    ->where('pf_account_no', $pf_account_no)->first();


        $data['financial_year'] = FinancialYear::where('id', $financial_year_id)->first();

        // Logo
        $data['corporation'] = Corporation::first();
        $logoPath = public_path($data['corporation']->logo);
        if (file_exists($logoPath)) {
            $logoData = file_get_contents($logoPath);
            $base64Logo = base64_encode($logoData);
        } else {
            $base64Logo = null;
        }
        $data['base64Logo'] = $base64Logo;
        // Logo End

        $filename = "PF Report " . $pf_account_no . '.pdf';
        $pdf = SnappyPdf::loadView('admin.pf.pf-report-pdf', $data)
            ->setPaper('a4')
            ->setOrientation('portrait')
            ->setOption('margin-bottom', 0)
            ->setOption('margin-top', 3)
            ->setOption('margin-left', 0)
            ->setOption('margin-right', 0);

        return $pdf->inline($filename);

    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
