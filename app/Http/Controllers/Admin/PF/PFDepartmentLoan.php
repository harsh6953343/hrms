<?php

namespace App\Http\Controllers\Admin\PF;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\PF\StorePFDepartmentLoanRequest;
use App\Models\Employee;
use App\Models\EmployeeProvidentFund;
use App\Models\PfOpeningBalance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PFDepartmentLoan extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('admin.pf.add-pf-department-loan');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StorePFDepartmentLoanRequest $request)
    {
        try {

            DB::beginTransaction();
            $input = $request->validated();

            if(isset($input['provident_fund_ids'])){
                $provident_fund_Ids = $input['provident_fund_ids'];
            }
            if (empty($provident_fund_Ids)) {
                return response()->json(['error2' => 'PF Data not exists for this employee.']);
            }else{
                foreach ($provident_fund_Ids as $key => $provident_fund_Id) {
                    $get_old_loan_data = EmployeeProvidentFund::where('id', $provident_fund_Id)->first();

                    if ($get_old_loan_data) {
                        $get_old_loan_data->update([
                            'other_amount' => $input['other_amount'][$key] ?? null,
                            'loan_date' => $input['loan_date'][$key] ?? null,
                            'loan_amt' => $input['loan_amt'][$key] ?? null,
                            'remark' => $input['remark'][$key] ?? null,
                        ]);
                    }
                }
                DB::commit();
                return response()->json(['success' => 'PF Department updated successfully!']);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['error' => 'Failed to update PF Department.']);
        }
    }


    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    public function fetchEmployeeDetails($pf_no)
    {
        $authUser = Auth::user();
        $financial_year = session('financial_year');

        $employee_details = Employee::with('ward', 'department', 'designation', 'class')
            ->where('pf_account_no', $pf_no)
            ->when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
                return $query->where('ward_id', $authUser->ward_id);
            })
            ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
                return $query->where('department_id', $authUser->department_id);
            })
            ->select('id', 'fname', 'mname', 'lname', 'ward_id', 'department_id', 'clas_id', 'designation_id', 'employee_id')
            ->first();


        $pf_data = EmployeeProvidentFund::where('pf_account_no',$pf_no)->where('financial_year_id',$financial_year)->get();
        $opening_balance = PfOpeningBalance::where('pf_no',$pf_no)->where('financial_year_id',$financial_year)->first();

        if ($employee_details && $pf_data && $opening_balance) {
            $response = [
                'result'            => 1,
                'employee_details'  => $employee_details,
                'pf_data'           => $pf_data,
                'opening_balance'   => $opening_balance,
            ];
        } else  {

            // if(empty($employee_details))
            $response = ['result' => 0];
        }
        return $response;
    }
}
