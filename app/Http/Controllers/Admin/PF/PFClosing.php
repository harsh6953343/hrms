<?php

namespace App\Http\Controllers\Admin\PF;

use App\Http\Controllers\Admin\Controller;
use App\Http\Requests\Admin\PF\StorePFClosingRequest;
use App\Models\PfOpeningBalance;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class PFClosing extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('admin.pf.pf-closing');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StorePFClosingRequest $request)
    {

    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(StorePFClosingRequest $request, PfOpeningBalance $pfOpeningBalance)
    {
        try {
            DB::beginTransaction();
            $input = $request->validated();
            $pfOpeningBalance = PfOpeningBalance::where('id',$input['edit_model_id'])->first();
            $input['closing_status'] = 1;
            $pfOpeningBalance->update(Arr::only($input, PfOpeningBalance::getFillables()));
            DB::commit();
            return response()->json(['success' => 'PF Closing Form  updated successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'updating', 'PF Closing Form ');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
