<?php

namespace App\Http\Controllers\Admin\Report;

use App\Exports\PaySheetExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Report\PaySheetExcelRequest;
use App\Models\Department;
use App\Models\FreezeAttendance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class PaySheetExcelController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $authUser = Auth::user();
        $departments = Department::when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
            return $query->where('ward_id', $authUser->ward_id);
        })
            ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
                return $query->where('id', $authUser->department_id);
            })->latest()->get();

        return view('admin.reports.pay-sheet-excel')->with(['departments' => $departments]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(PaySheetExcelRequest $request)
    {
        $input = $request->validated();
        $department = $input['department'];
        $authUser = Auth::user();

        $freezeAttendances = FreezeAttendance::with('employee', 'designation')
            ->when($department !== 'all', function ($query) use ($department) {
                if ($department == 'custom_ward') {
                    return $query->whereNotIn('department_id', [145])->whereNotIn('ward_id', [84]);
                } else {
                    return $query->where('department_id', $department);
                }
            })
            ->when(($department == 'all'), function ($query) use ($authUser) {
                if($authUser->hasRole(['Ward HOD'])){
                    return $query->where('ward_id', $authUser->ward_id);
                }elseif($authUser->id == 1){
                    return $query->whereNot('ward_id', 84);
                }else{
                    $query->whereNotIn('ward_id', [84]);
                }
            })
            ->where('from_date', $input['from_date'])
            ->where('to_date', $input['to_date'])
            ->where('month', $input['month'])
            ->where('freeze_status', 1)
            ->get();

        return Excel::download(new PaySheetExport($freezeAttendances), 'pay-sheet.xlsx');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
