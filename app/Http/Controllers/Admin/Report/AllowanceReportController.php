<?php

namespace App\Http\Controllers\Admin\Report;

use App\Http\Controllers\Controller;
use App\Models\Allowance;
use App\Models\Department;
use App\Models\FinancialYear;
use App\Models\FreezeAttendance;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AllowanceReportController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $authUser = Auth::user();
        $from_date = $request->from_date ?? null;
        $to_date = $request->to_date ?? null;
        $month = $request->month ?? date('m');
        $monthName = Carbon::createFromDate(null, $month, 1)->format('F');
        $department = $request->department ?? null;
        $allowance_id = $request->allowance ?? null;

        $departments = Department::when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
            return $query->where('ward_id', $authUser->ward_id);
        })
            ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
                return $query->where('id', $authUser->department_id);
            })->latest()->get();


        $allowances = Allowance::latest()->get();

        $financial_year = FinancialYear::where('id', session('financial_year'))->first();

        if (!$from_date || !$to_date) {
            if ($financial_year) {
                if ($month <= 3) {
                    $year = date('Y', strtotime($financial_year->to_date));
                } else {
                    $year = date('Y', strtotime($financial_year->from_date));
                }
                $month = $month ?? 1;

                $from_date = Carbon::parse($year . '-' . ($month) . '-' . 01);
                $to_date = clone ($from_date);
                $from_date = (string) $from_date->startOfMonth()->toDateString();
                $to_date = (string) $to_date->endOfMonth()->toDateString();
            }
        }


        $allowance_report = FreezeAttendance::query();

        // Filter by department if selected
        if ($department) {
            $allowance_report->where('department_id', $department);
        }

        // Filter by allowance ID if provided
        if ($allowance_id) {

            // Apply user role-based filters
            $allowance_report = $allowance_report->when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
                return $query->where('ward_id', $authUser->ward_id);
            })
                ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
                    return $query->where('department_id', $authUser->department_id);
                })
                ->where('freeze_status', 1)
                ->where('from_date', $from_date)
                ->where('to_date', $to_date)
                // ->whereRaw("FIND_IN_SET($allowance_id, allowance_Id) > 0")
                ->with('employee')
                ->get();
        }

        return view('admin.reports.allowance-report')->with([
            'departments'   => $departments,
            'from_date'     => $from_date,
            'to_date'       => $to_date,
            'month'         => $month,
            'monthName'     => $monthName,
            'department_id' => $department,
            'allowances'    => $allowances,
            'allowance_id'  => $allowance_id,
            'allowance_reports' => $allowance_report
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
