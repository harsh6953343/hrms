<?php

namespace App\Http\Controllers\Admin\Report;

use App\Http\Controllers\Controller;
use App\Models\Allowance;
use App\Models\Corporation;
use App\Models\Deduction;
use App\Models\Department;
use App\Models\FreezeAttendance;
use App\Models\Ward;
use Barryvdh\Snappy\Facades\SnappyPdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GrandSummaryWardController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $authUser = Auth::user();
        $wards = Ward::when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
            return $query->where('id', $authUser->ward_id);
        })->latest()->get();

        return view('admin.reports.grand-summary-ward')->with(['wards' => $wards]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    public function showGrandSummaryWardPDF(Request $request)
    {
        $authUser = Auth::user();
        $ward = $request->ward;
        $month = $request->month;
        $data['from_date'] = $request->from_date;
        $data['to_date'] = $request->to_date;

        $data['corporation'] = Corporation::first();

        $logoPath = public_path($data['corporation']->logo);

        if (file_exists($logoPath)) {
            $logoData = file_get_contents($logoPath);
            $base64Logo = base64_encode($logoData);
        } else {
            // Handle if the logo file doesn't exist
            $base64Logo = null;
        }

        $data['base64Logo'] = $base64Logo;

        $data['allowances'] = Allowance::get();
        $data['deductions'] = Deduction::get();



        $data['freezeAttendances'] = FreezeAttendance::with('employee', 'designation')
            ->when($ward == 'custom_ward', function ($query) {
                return $query->whereNotIn('ward_id', [72,84]);
            })
            ->when($ward !== 'custom_ward' && !empty($ward), function ($query) use ($ward) {
                return $query->where('ward_id', $ward);
            })
            ->where('from_date', $data['from_date'])
            ->where('to_date', $data['to_date'])
            ->where('month', $month)
            ->where('freeze_status', 1)
            ->get();

        if($ward == 'custom_ward')
        {
            $data['ward_name'] = 'Table 1 to 28 & 30';
        }else{
            $ward_name = Ward::where('id', $ward)->first();
            $data['ward_name'] = $ward_name->name;
        }



        $filename = "Grand Summary " . $data['ward_name'] . " Report" . '.pdf';
        $pdf = SnappyPdf::loadView('admin.reports.pdf.grand-summary-ward-pdf', $data)
            ->setPaper('a4')
            ->setOrientation('landscape')
            ->setOption('margin-bottom', 0)
            ->setOption('margin-top', 3)
            ->setOption('margin-left', 0)
            ->setOption('margin-right', 0);

        return $pdf->inline($filename);
    }
}
