<?php

namespace App\Http\Controllers\Admin\Report;

use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Models\FinancialYear;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RetirementController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $authUser = Auth::user();
        $financial_year_id = session('financial_year');
        $get_financial_year_record = FinancialYear::where('id',$financial_year_id)->first();


        $employees = Employee::with('ward', 'department', 'designation', 'class', 'status')
            ->when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
                return $query->where('ward_id', $authUser->ward_id);
            })
            ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
                return $query->where('department_id', $authUser->department_id);
            })
            ->where('retirement_date','<=',$get_financial_year_record->to_date)
            ->latest()
            ->get();

        return view('admin.reports.retirement-report')->with(['employees' => $employees]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
