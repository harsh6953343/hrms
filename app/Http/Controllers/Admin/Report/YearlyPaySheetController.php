<?php

namespace App\Http\Controllers\Admin\Report;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Report\YearlyPaySheetRequest;
use App\Models\Allowance;
use App\Models\Corporation;
use App\Models\Deduction;
use App\Models\FinancialYear;
use App\Models\Employee;
use App\Models\FreezeAttendance;
use Barryvdh\Snappy\Facades\SnappyPdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class YearlyPaySheetController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $authUser = Auth::user();
        $employees = Employee::when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
            return $query->where('ward_id', $authUser->ward_id);
        })
        ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
            return $query->where('id', $authUser->department_id);
        })
        ->when($authUser->hasRole(['Employee']), function ($query) use ($authUser) {
            return $query->where('id', $authUser->employee_id);
        })->latest()->get();

        $finYears = FinancialYear::all()->pluck('title');

        return view('admin.reports.yearly-pay-sheet')->with(['employees' => $employees,'finYears' => $finYears]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    public function showPaySheetPDF($employee_id, $financial_year_range)
    {
        if($employee_id == ''){
            echo 'Please check URL. Employee ID not present.';
            die;
        }elseif($financial_year_range == ''){
            echo 'Please check URL. Financial Year not present.';
            die;
        }
        
        $data['corporation'] = Corporation::first();

        $logoPath = public_path($data['corporation']->logo);

        if (file_exists($logoPath)) {
            $logoData = file_get_contents($logoPath);
            $base64Logo = base64_encode($logoData);
        } else {
            $base64Logo = null;
        }

        $data['base64Logo'] = $base64Logo;

        $data['allowances'] = Allowance::get();
        $data['deductions'] = Deduction::get();

        // Extract the start year from the current fiscal year string
        $startYear = intval(substr($financial_year_range, 2, 4)); 
        // Calculate the previous fiscal year range 
        $previousEndYear = $startYear;
        $endYear = $startYear + 1;

        $currentYearRec = FreezeAttendance::with('employee', 'designation', 'department')
                            ->where('from_date','>=', "$previousEndYear-03-01")
                            ->where('to_date', '<',"$endYear-02-30")
                            ->where('freeze_status', 1)
                            ->where('employee_id', $employee_id)
                            ->get();

        $dayCountOfMonths = [ 13 => 31, 3 => 31, 4 => 30, 5 => 31, 6 => 30, 7 => 31, 8 => 31, 9 => 30, 10 => 31, 11 => 30, 12 => 31 ];

        $lastRec = $currentYearRec->last();
        if ($lastRec) {
            if($lastRec->month < 3){
                $assumptionRecordCount = 2 - $lastRec->month;
            }else{
                $assumptionRecordCount = 14 - $lastRec->month;
            }
            $lastMonth = $lastRec->month;
            for ($i = 0; $i < $assumptionRecordCount; $i++) { 
                $newRec = $lastRec->replicate(); 
                $lastMonth = $newRec->month = $lastMonth + 1;
                if($lastMonth == 14){
                    if($endYear % 4 == 0){
                        $newRec->present_day = 29;
                    }else{
                        $newRec->present_day = 28;
                    }
                }else{
                    $newRec->present_day = $dayCountOfMonths[$lastMonth];
                }
                $currentYearRec->push($newRec);
            }
        }else{
            echo "<b>Data not present.</b>";
            die;
        }
        
        $data['freezeAttendances'] = $currentYearRec;
            
        $data['yearRange'] = "(Mar $previousEndYear - Feb $endYear)";
        $filename = "Yearly Salary Slip" . " " . $data['freezeAttendances'][0]->Emp_Code . '.pdf';

        $pdf = SnappyPdf::loadView('admin.reports.pdf.yearly-pay-sheet-pdf', $data)
            ->setPaper('a4')
            ->setOrientation('landscape')
            ->setOption('margin-bottom', 0)
            ->setOption('margin-top', 3)
            ->setOption('margin-left', 0)
            ->setOption('margin-right', 0);

        return $pdf->inline($filename);
    }

}
