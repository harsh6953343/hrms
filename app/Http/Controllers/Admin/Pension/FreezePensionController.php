<?php

namespace App\Http\Controllers\Admin\Pension;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Pension\StoreFreezePensionRequest;
use App\Models\Allowance;
use App\Models\Corporation;
use App\Models\FreezePension;
use App\Models\Pension;
use App\Models\PensionBank;
use Barryvdh\Snappy\Facades\SnappyPdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class FreezePensionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $authUser = Auth::user();

        $freeze_pensions =  FreezePension::with('employee')
                                    ->when($authUser->hasRole('Ward HOD'), function ($query) use ($authUser) {
                                        $query->whereHas('employee', function ($employeeQuery) use ($authUser) {
                                            $employeeQuery->where('ward_id', $authUser->ward_id);
                                        });
                                    })
                                    ->groupBy('from_date', 'to_date', 'month')
                                    ->select('from_date', 'to_date', 'month')
                                    ->get();

        return view('admin.pension.freeze-pension-list')->with(['freeze_pensions' => $freeze_pensions]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreFreezePensionRequest $request)
    {
        DB::beginTransaction();

        try {
            $input = $request->validated();
            $financial_year_id = session('financial_year');
            $from_date = $input['from_date'];
            $to_date = $input['to_date'];
            $month = $input['month'];

            $check_already_added = FreezePension::where('from_date', $from_date)
            ->where('to_date', $to_date)
            ->where('month', $month)
            ->first();

            if ($check_already_added) {
                return response()->json(['error' => 'Already Generated!']);
            } else {

            $da_rate = Allowance::where('id', 1)->value('amount');
            $pension_banks = PensionBank::latest()->get();

            $authUser = Auth::user();

            foreach ($pension_banks as $bank) {
                $all_pension_freeze = Pension::with('employee')->where('pension_bank_id', $bank->id)
                                            ->when($authUser->hasRole('Ward HOD'), function ($query) use ($authUser) {
                                                $query->whereHas('employee', function ($employeeQuery) use ($authUser) {
                                                    $employeeQuery->where('ward_id', $authUser->ward_id);
                                                });
                                            })
                                            ->when($authUser->id == 1, function ($query) {
                                                $query->whereHas('employee', function ($employeeQuery) {
                                                    $employeeQuery->whereNot('ward_id', 84);
                                                });
                                            })
                                            ->latest()->get();

                if ($all_pension_freeze->isEmpty()) {
                    continue; // Skip processing if no records found
                }

                foreach ($all_pension_freeze as $row) {
                    $payable_pension_amt = $row->emp_retire_after_2016 == 1 ? ($row->basic_salary < 7500 && $row->pay_type == 1 ? 7500 : $row->basic_salary) : $row->basic_salary * 2.57;
                    $da_amt = $row->da_applicabe == 1 ? ($payable_pension_amt * $da_rate / 100) : 0;
                    $total = ($payable_pension_amt - $row->deduct_amt) + $da_amt + $row->business_allowances;
                    $total = $total - $row->deduction;

                    FreezePension::create([
                        'pension_id' => $row->pension_id,
                        'from_date' => $from_date,
                        'to_date' => $to_date,
                        'month' => $month,
                        'financial_year_id' => $financial_year_id,
                        'emp_name' => $row->emp_name,
                        'employee_id' => $row->employee_id,
                        'designation' => $row->designation,
                        'pay_type' => $row->pay_type,
                        'emp_retire_after_2016' => $row->emp_retire_after_2016,
                        'is_emp_dr' => $row->is_emp_dr,
                        'dob' => $row->dob,
                        'age' => $row->age,
                        'retirement_date' => $row->retirement_date,
                        'date_of_death' => $row->date_of_death,
                        'account_number' => $row->account_number,
                        'pension_bank_id' => $bank->id,
                        'branch_name' => $row->branch_name,
                        'ifsc_code' => $row->ifsc_code,
                        'basic_salary' => $row->basic_salary,
                        'calculated_basic' => $payable_pension_amt,
                        'payable_pension' => $total,
                        'da' => $da_amt,
                        'da_applicabe' => $row->da_applicabe,
                        'sell_computation' => $row->sell_computation,
                        'sell_date' => $row->sell_date,
                        'deduct_amt' => $row->deduct_amt,
                        'differance' => $row->differance,
                        'deduction' => $row->deduction,
                        'business_allowances' => $row->business_allowances,
                    ]);
                }
            }
                DB::commit();
            }
            return response()->json(['success' => 'Freeze Pension successfully.']);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['error' => 'Failed to freeze pension.']);
        }
    }


    /**
     * Display the specified resource.
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($from_date)
    {
        // Retrieve the records based on the provided criteria
        $recordsToDelete = FreezePension::where('from_date', $from_date)->get();
        $deleteCount = $recordsToDelete->count();
        $recordsToDelete->each->delete();
        return response()->json([
            'success' => "$deleteCount records deleted successfully.",
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    public function showPensionerBillPDF($month,$from_date,$to_date){

        $authUser = Auth::user();

        $data['corporation'] = Corporation::first();

        $logoPath = public_path($data['corporation']->logo);

        if (file_exists($logoPath)) {
            $logoData = file_get_contents($logoPath);
            $base64Logo = base64_encode($logoData);
        } else {
            $base64Logo = null;
        }

        $data['base64Logo'] = $base64Logo;

        $data['month'] = $month;
        $data['from_date'] = $from_date;
        $data['to_date'] = $to_date;


        $data['banks'] = PensionBank::latest()->get();

        $filename = "Pensioner Bill Report" . '.pdf';
        $pdf = SnappyPdf::loadView('admin.pension.pensioner-bill-pdf', $data)
            ->setPaper('a4')
            ->setOrientation('landscape')
            ->setOption('margin-bottom', 0)
            ->setOption('margin-top', 3)
            ->setOption('margin-left', 0)
            ->setOption('margin-right', 0);

        return $pdf->inline($filename);

    }
}
