<?php

namespace App\Http\Controllers\Admin\Pension;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Pension\PensionerBillRequest;
use App\Models\Corporation;
use App\Models\PensionBank;
use Barryvdh\Snappy\Facades\SnappyPdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PensionerBillController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $banks = PensionBank::latest()->get();

        return view('admin.pension.pensioner-bill')->with(['banks' => $banks]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    public function showPensionerBillPDF(PensionerBillRequest $request){

        $data['corporation'] = Corporation::first();

        $logoPath = public_path($data['corporation']->logo);

        if (file_exists($logoPath)) {
            $logoData = file_get_contents($logoPath);
            $base64Logo = base64_encode($logoData);
        } else {
            $base64Logo = null;
        }

        $data['base64Logo'] = $base64Logo;

        $data['month'] = $request->month;
        $data['from_date'] = $request->from_date;
        $data['to_date'] = $request->to_date;

        if($request->bank && $request->bank != 'all'){
            $data['banks'] = PensionBank::where('id',$request->bank)->latest()->get();
        }else{
            $data['banks'] = PensionBank::latest()->get();
        }


        $filename = "Pensioner Bill Report" . '.pdf';
        $pdf = SnappyPdf::loadView('admin.pension.pensioner-bill-pdf', $data)
            ->setPaper('a4')
            ->setOrientation('landscape')
            ->setOption('margin-bottom', 0)
            ->setOption('margin-top', 3)
            ->setOption('margin-left', 0)
            ->setOption('margin-right', 0);

        return $pdf->inline($filename);

    }
}
