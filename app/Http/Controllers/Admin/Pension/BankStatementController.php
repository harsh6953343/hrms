<?php

namespace App\Http\Controllers\Admin\Pension;

use App\Http\Controllers\Controller;
use App\Models\FinancialYear;
use App\Models\FreezePension;
use App\Models\PensionBank;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BankStatementController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        try {
            $authUser = Auth::user();
            $from_date = $request->from_date ?? null;
            $to_date = $request->to_date ?? null;
            $month = $request->month ?? date('m');
            $monthName = Carbon::createFromDate(null, $month, 1)->format('F');
            $bank_id = $request->bank ?? null; //bank

            $banks = PensionBank::latest()->get();
            // If from date and to date are not provided, calculate current month's from date and to date
            $financial_year = FinancialYear::where('id', session('financial_year'))->first();

            if (!$from_date || !$to_date) {
                if ($financial_year) {
                    if ($month <= 3) {
                        $year = date('Y', strtotime($financial_year->to_date));
                    } else {
                        $year = date('Y', strtotime($financial_year->from_date));
                    }
                    $month = $month ?? 1;

                    $from_date = Carbon::parse($year . '-' . ($month) . '-' . 16);
                    $to_date = clone ($from_date);
                    $from_date = (string) $from_date->subMonth()->toDateString();
                    $to_date = (string) $to_date->subDay()->toDateString();
                }
            }

            $bank_statements = FreezePension::query();

            if (Auth::user()->hasRole(['Ward HOD'])) {
                $authUser = Auth::user();
                $bank_statements->with('employee')->whereHas('employee', function ($employeeQuery) use ($authUser) {
                    $employeeQuery->where('ward_id', $authUser->ward_id);
                });
            }elseif(Auth::user()->id == 1){
                $bank_statements->with('employee')->whereHas('employee', function ($employeeQuery) {
                    $employeeQuery->whereNot('ward_id', 84);
                });
            }

            // Filter by ward if selected
            if ($bank_id) {

                $bank_statements->where('pension_bank_id', $bank_id);
            }

            $bank_statements = $bank_statements->where('from_date', $from_date)
                                                ->where('to_date', $to_date)
                                                ->get();

            return view('admin.pension.bank-statement')->with(['bank_statements' => $bank_statements, 'from_date' => $from_date, 'to_date' => $to_date, 'month' => $month, 'monthName' => $monthName, 'banks' => $banks, 'bank_id' => $bank_id]);
        } catch (\Exception $e) {
            return response()->json([
                'error2' => 'An error occurred while processing the request.'
            ], 500);
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
