<?php

namespace App\Http\Controllers\Admin\Pension;

use App\Http\Controllers\Admin\Controller;
use App\Http\Requests\Admin\Pension\StorePensionRequest;
use App\Models\Allowance;
use App\Models\Corporation;
use App\Models\Pension;
use App\Models\PensionBank;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


class PensionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $authUser = Auth::user();

        $pensions = Pension::with('employee')
                        ->when($authUser->hasRole('Ward HOD'), function ($query) use ($authUser) {
                                $query->whereHas('employee', function ($employeeQuery) use ($authUser) {
                                    $employeeQuery->where('ward_id', $authUser->ward_id);
                                });
                            })
                        ->when($authUser->id == 1, function ($query) {
                            $query->whereHas('employee', function ($employeeQuery) {
                                $employeeQuery->whereNot('ward_id', 84);
                            });
                        })
                        ->latest()->get();

        return view('admin.pension.pension-list')->with(['pensions' => $pensions]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $pension_banks  = PensionBank::latest()->get();
        $da_rate         = Allowance::where('id',1)->first();
        return view('admin.pension.add-pension')->with(['pension_banks' => $pension_banks,  'da_rate' => $da_rate]);

    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StorePensionRequest $request)
    {
        try {
            $corporation_name = Corporation::first();
            DB::beginTransaction();
            $input = $request->validated();
            $pension = Pension::create(Arr::only($input, Pension::getFillables()));
            $pension->save();
            $num = rand(10,10000);
            $pension_id = $corporation_name->initial.'PEN'.$num.$pension->id;
            $pension->pension_id = $pension_id;
            $pension->save();
            DB::commit();
            return response()->json(['success' => 'Pension created successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'creating', 'Pension');
        }

    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Pension $pension)
    {
        if ($pension) {
            $pension_banks  = PensionBank::latest()->get();
            $da_rate         = Allowance::where('id',1)->first();

            return view('admin.pension.edit-pension')->with(['pension' => $pension, 'pension_banks'=> $pension_banks, 'da_rate' => $da_rate]);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(StorePensionRequest $request, Pension $pension)
    {
        try {
            DB::beginTransaction();
            $input = $request->validated();
            $pension->update(Arr::only($input, Pension::getFillables()));
            DB::commit();
            return response()->json(['success' => 'Pension updated successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'updating', 'Pension');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Pension $pension)
    {
        try {
            DB::beginTransaction();
            $pension->delete();
            DB::commit();
            return response()->json(['success' => 'Pension deleted successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'deleting', 'Pension');
        }
    }
}
