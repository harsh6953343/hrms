<?php

namespace App\Http\Controllers\Admin\Pension;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Pension\GrandTotalPensionerRequest;
use App\Models\Corporation;
use App\Models\PensionBank;
use Barryvdh\Snappy\Facades\SnappyPdf;
use Illuminate\Http\Request;

class GrandTotalPensionerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('admin.pension.grand-total-pensioner');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    public function showGrandTotalPensionerPDF(GrandTotalPensionerRequest $request)
    {
        $data['corporation'] = Corporation::first();

        $logoPath = public_path($data['corporation']->logo);

        if (file_exists($logoPath)) {
            $logoData = file_get_contents($logoPath);
            $base64Logo = base64_encode($logoData);
        } else {
            $base64Logo = null;
        }

        $data['base64Logo'] = $base64Logo;

        $data['month'] = $request->month;
        $data['from_date'] = $request->from_date;
        $data['to_date'] = $request->to_date;

        $data['banks'] = PensionBank::latest()->get();

        $filename = "Grand Total Pensioner Bill Report" . '.pdf';
        $pdf = SnappyPdf::loadView('admin.pension.grand-total-pensioner-pdf', $data)
            ->setPaper('a4')
            ->setOrientation('landscape')
            ->setOption('margin-bottom', 0)
            ->setOption('margin-top', 3)
            ->setOption('margin-left', 0)
            ->setOption('margin-right', 0);

        return $pdf->inline($filename);
    }
}
