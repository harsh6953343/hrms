<?php

namespace App\Http\Controllers\Admin\ServiceBook;

use App\Http\Controllers\Controller;
use App\Models\AddEmployeeLeave;
use App\Models\Corporation;
use App\Models\Employee;
use App\Models\EmployeeLeaves;
use App\Models\EmployeeTransfer;
use Barryvdh\Snappy\Facades\SnappyPdf;
use Illuminate\Http\Request;

class ServiceBookController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    public function showServiceBookPDF()
    {
        $data['corporation'] = Corporation::first();

        $logoPath = public_path($data['corporation']->logo);

        if (file_exists($logoPath)) {
            $logoData = file_get_contents($logoPath);
            $data['base64Logo'] = base64_encode($logoData);

        } else {
            $data['base64Logo'] = null;
        }


        $data['employee'] = Employee::with('designation')->where('id','=','907')->first();
        $data['employee_leaves'] = EmployeeLeaves::with('leaveType')->where('employee_id',907)->get();
        $data['leave_taken_details'] = AddEmployeeLeave::with('leaveType')->where('employee_id',907)->get();
        $data['transfer_details'] = EmployeeTransfer::where('employee_id',907)->get();



        $filename = "Service Book" . '.pdf';
        $pdf = SnappyPdf::loadView('admin.service_book.service-book-pdf', $data)
            ->setPaper('a4')
            ->setOrientation('landscape')
            ->setOption('margin-bottom', 0)
            ->setOption('margin-top', 3)
            ->setOption('margin-left', 0)
            ->setOption('margin-right', 0);

        return $pdf->inline($filename);
    }
}
