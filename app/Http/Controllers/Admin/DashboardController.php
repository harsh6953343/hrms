<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Bank;
use App\Models\Department;
use App\Models\Employee;
use App\Models\EmployeeStatus;
use App\Models\FreezeAttendance;
use App\Models\Ward;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

class DashboardController extends Controller
{

    public function index()
    {
        $authUser = Auth::user();

        $employees = Employee::when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
                return $query->where('ward_id', $authUser->ward_id);
            })
            ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
                return $query->where('department_id', $authUser->department_id);
            })
            ->when($authUser->id == 1, function ($query) use ($authUser) {
                return $query->where('ward_id','!=', 84);
            })
            ->latest()
            ->count();


        $emp_statuses = EmployeeStatus::when($authUser->hasRole('Ward HOD'), function ($query) use ($authUser) {
                $query->whereHas('employee', function ($employeeQuery) use ($authUser) {
                    $employeeQuery->where('ward_id', $authUser->ward_id);
                });
            })
            ->when($authUser->hasRole('Department HOD'), function ($query) use ($authUser) {
                $query->whereHas('employee', function ($employeeQuery) use ($authUser) {
                    $employeeQuery->where('department_id', $authUser->department_id);
                });
            })
            ->when($authUser->id == 1, function ($query) use ($authUser) {
                $query->whereHas('employee', function ($employeeQuery) use ($authUser) {
                    $employeeQuery->where('ward_id', '!=', 84);
                });
            })
            ->latest()
            ->count();

            if(Auth::user()->hasRole(['Ward HOD'])){
                $wards = 1;
            }else{
                $wards = Ward::latest()->count() - 1;
            }

        $departments = Department::when($authUser->hasRole(['Ward HOD']), fn ($q) => $q->where('ward_id', $authUser->ward_id))
                        ->when($authUser->id == 1, function ($query) use ($authUser) {
                            return $query->where('ward_id','!=', 84);
                        })
                        ->latest()->count();

        $banks = Bank::latest()->count();

        $retire_employees = Employee::with('ward','department')
            ->when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
                return $query->where('ward_id', $authUser->ward_id);
            })
            ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
                return $query->where('department_id', $authUser->department_id);
            })
            ->where('retirement_date', '>=', Carbon::now())
            ->where('retirement_date', '<', Carbon::now()->addMonths(6)->startOfDay())
            ->latest()
            ->get();


        $employee_salary = FreezeAttendance::with('employee')->when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
            return $query->where('ward_id', $authUser->ward_id);
        })
        ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
            return $query->where('department_id', $authUser->department_id);
        })
        ->where('net_salary','<',0)
        ->where('basic_salary','!=',0)
        ->get();

        return view('admin.dashboard')->with(['employees' => $employees,
                                                'emp_statuses' => $emp_statuses,
                                                'wards' => $wards,
                                                'departments' => $departments,
                                                'banks' => $banks,
                                                'retire_employees'=> $retire_employees,
                                                'employee_salarys'=> $employee_salary]);
    }

    public function changeThemeMode()
    {
        $mode = request()->cookie('theme-mode');

        if($mode == 'dark')
            Cookie::queue('theme-mode', 'light', 43800);
        else
            Cookie::queue('theme-mode', 'dark', 43800);

        return true;
    }
}
