<?php

namespace App\Http\Controllers\Admin\Attendance;

use App\Http\Controllers\Admin\Controller;
use App\Http\Requests\Admin\Attendance\StoreAbsentAttendanceRequest;
use App\Models\Attendance;
use App\Models\Employee;
use App\Models\FinancialYear;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class EmployeeAbsent extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('admin.attendance.add-absent-attendance');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreAbsentAttendanceRequest $request)
    {

    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(StoreAbsentAttendanceRequest $request, $id)
    {
        $attendance = Attendance::where('id',$id)->first();

        try {
            DB::beginTransaction();
            $input = $request->validated();
            $attendance->update(Arr::only($input, Attendance::getFillables()));
            DB::commit();
            return response()->json(['success' => 'Attendnce updated successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'updating', 'Attendnce');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    public function fetchAttendanceDetails($emp_id, $month)
    {
        $authUser = Auth::user();

        // Fetch employee details with necessary conditions
        $employee_details = Employee::with('ward', 'department', 'designation', 'class')
            ->where('employee_id', $emp_id)
            ->when($authUser->hasRole('Ward HOD'), function ($query) use ($authUser) {
                return $query->where('ward_id', $authUser->ward_id);
            })
            ->when($authUser->hasRole('Department HOD'), function ($query) use ($authUser) {
                return $query->where('department_id', $authUser->department_id);
            })
            ->whereNotExists(function ($query) use ($emp_id) {
                $query->select(DB::raw(1))
                    ->from('employee_statuses')
                    ->whereColumn('employee_statuses.employee_id', 'employees.id')
                    ->where('employee_statuses.Emp_Code', $emp_id)
                    ->whereNull('employee_statuses.deleted_at');
            })
            ->select('id', 'fname', 'mname', 'lname', 'ward_id', 'department_id', 'clas_id', 'designation_id', 'pf_account_no')
            ->first();

        // Fetch active financial year
        $fy_id = session('financial_year');

        $financial_year = FinancialYear::where('id', $fy_id)->first();
        if ($financial_year) {


            $year = ($month <= 3) ? date('Y', strtotime($financial_year->to_date)) : date('Y', strtotime($financial_year->from_date));
            $fromDate = Carbon::parse("$year-$month-16")->startOfMonth()->toDateString();
            $toDate = Carbon::parse("$year-$month-16")->endOfMonth()->toDateString();

            // Fetch attendance details
            $attendance_details = Attendance::where('Emp_Code', $emp_id)
                ->where('from_date', $fromDate)
                ->where('to_date', $toDate)
                ->first();


        } else {
            $attendance_details = null;
        }

        if ($employee_details && $attendance_details) {

            $response = [
                'result'                => 1,
                'employee_details'      => $employee_details,
                'attendance_details'    => $attendance_details,
            ];
        } else {
            $response = [
                'result' => 0,
                'message' => 'Employee details or attendance details not found.'
            ];
        }

        return response()->json($response);
    }

}
