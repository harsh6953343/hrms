<?php

namespace App\Http\Controllers\Admin\Attendance;

use App\Http\Controllers\Admin\Controller;
use App\Http\Requests\Admin\Attendance\StoreEmployeeLeaveRequest;
use App\Models\AddEmployeeLeave;
use App\Models\Attendance;
use App\Models\Employee;
use App\Models\EmployeeLeaves;
use App\Models\FinancialYear;
use App\Models\FreezeCron;
use App\Models\LeaveType;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AddEmployeeLeaveController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $authUser = Auth::user();

        $employee_leave = AddEmployeeLeave::with('employee')
            ->whereHas('employee', function ($query) use ($authUser) {
                $query->when($authUser->hasRole('Ward HOD'), function ($query) use ($authUser) {
                    return $query->where('ward_id', $authUser->ward_id);
                })
                ->when($authUser->hasRole('Department HOD'), function ($query) use ($authUser) {
                    return $query->where('department_id', $authUser->department_id);
                });
            })
            ->latest()
            ->get();

        $leaveType      = LeaveType::latest()->get();

        return view('admin.attendance.employee_leave')->
                                                    with(['employee_leaves' => $employee_leave,
                                                     'leaveTypes'=> $leaveType]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreEmployeeLeaveRequest $request)
    {
        try {
            DB::beginTransaction();
            $input = $request->validated();

            $check_leave_available = EmployeeLeaves::where('employee_id', $input['employee_id'])
                        ->where('leave_type_id', $input['leave_type_id'])
                        ->where('no_of_leaves', '>=', $input['no_of_days'])->first();

            if (empty($check_leave_available)) {
                return response()->json(['error2' => 'No Leaves Available!']);
            }

            // Check if leave already exists for the given dates
            $checkAlreadyExist = AddEmployeeLeave::where('employee_id', $input['employee_id'])
                ->where('from_date', '>=', $input['from_date'])
                ->where('to_date', '<=', $input['to_date'])
                ->whereIn('status',['0','1'])
                ->exists();

            if ($checkAlreadyExist) {
                return response()->json(['error2' => 'Leave already exists for the given dates!']);
            }

            // Get the active financial year
            $fy_id = session('financial_year');

            $financial_year = FinancialYear::where('id', $fy_id)->first();
            $input['financial_year_id'] = $financial_year->id;


            // Create the employee leave record
            AddEmployeeLeave::create(Arr::only($input, AddEmployeeLeave::getFillables()));

            DB::commit();

            return response()->json(['success' => 'Employee Leave created successfully!']);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->respondWithAjax($e, 'creating', 'Employee Leave');
        }
    }


    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(AddEmployeeLeave $add_leave)
    {
        try {
            DB::beginTransaction();

            // Delete the leave record
            // dd($add_leave->status);


            // Find the freeze record for the specified date range with salary_generated = 0
            $get_freeze_record = FreezeCron::where('to_date', '<=', $add_leave->from_date)
                // ->where('to_date', '<=', $add_leave->to_date)
                ->where('salary_generated', 0)
                ->first();

            if ($get_freeze_record && $add_leave->status == 1) {


                // Find the attendance record for the employee in the specified month and financial year
                $check_attendance = Attendance::where('employee_id', $add_leave->employee_id)
                    ->where('month', $add_leave->month)
                    ->where('financial_year_id', $add_leave->financial_year_id)
                    ->first();

                $employeeLeaves = EmployeeLeaves::where('employee_id',$add_leave->employee_id)->where('leave_type_id',$add_leave->leave_type_id)->first();

                if ($check_attendance) {
                    // Update attendance with adjusted leave and present days
                    $new_total_leave = $check_attendance->total_leave - $add_leave->no_of_days;
                    $new_total_present_days = min($check_attendance->main_present_days, $check_attendance->total_present_days + $new_total_leave);

                    $check_attendance->update([
                        'total_leave' => $new_total_leave,
                        'total_present_days' => $new_total_present_days,
                    ]);

                    $employeeLeaves->update([
                            'no_of_leaves' => $employeeLeaves->no_of_leaves +  $add_leave->no_of_days,
                    ]);

                }
            }

            $add_leave->delete();
            DB::commit();
            return response()->json(['success' => 'Employee Leave deleted successfully!']);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['error' => 'Error deleting Employee Leave: ' . $e->getMessage()], 500);
        }
    }


    public function fetchEmployeeDetails($emp_id, $month)
    {
        $authUser = Auth::user();

        // Fetch employee details with necessary conditions
        $employee_details = Employee::with('ward', 'department', 'designation', 'class')
            ->where('employee_id', $emp_id)
            ->when($authUser->hasRole('Ward HOD'), function ($query) use ($authUser) {
                return $query->where('ward_id', $authUser->ward_id);
            })
            ->when($authUser->hasRole('Department HOD'), function ($query) use ($authUser) {
                return $query->where('department_id', $authUser->department_id);
            })
            ->whereNotExists(function ($query) use ($emp_id) {
                $query->select(DB::raw(1))
                    ->from('employee_statuses')
                    ->whereColumn('employee_statuses.employee_id', 'employees.id')
                    ->where('employee_statuses.Emp_Code', $emp_id)
                    ->whereNull('employee_statuses.deleted_at');
            })
            ->select('id', 'fname', 'mname', 'lname', 'ward_id', 'department_id', 'clas_id', 'designation_id', 'pf_account_no')
            ->first();

        // Fetch active financial year
        $fy_id = session('financial_year');

        $financial_year = FinancialYear::where('id', $fy_id)->first();

        if ($financial_year) {
            $year = ($month <= 3) ? date('Y', strtotime($financial_year->to_date)) : date('Y', strtotime($financial_year->from_date));
            $fromDate = Carbon::parse("$year-$month-01")->startOfMonth()->toDateString();
            $toDate = Carbon::parse("$year-$month-01")->endOfMonth()->toDateString();

        }
        if ($employee_details) {
            $response = [
                'result'                => 1,
                'employee_details'      => $employee_details,
                'from_date'             => $fromDate,
                'to_date'               => $toDate,
            ];
        } else {
            $response = [
                'result' => 0,
                'message' => 'Employee details or attendance details not found.'
            ];
        }

        return response()->json($response);
    }
}
