<?php

namespace App\Http\Controllers\Admin\Attendance;

use App\Http\Controllers\Controller;
use App\Models\AddEmployeeLeave;
use App\Models\Attendance;
use App\Models\EmployeeLeaves;
use App\Models\LeaveType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LeaveApprovalController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $authUser = Auth::user();

        $employee_leave = AddEmployeeLeave::whereHas('employee', function ($query) use ($authUser) {
            $query->when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
                return $query->where('ward_id', $authUser->ward_id);
            })
            ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
                return $query->where('department_id', $authUser->department_id);
            });
        })
        ->where('status',0)
        ->latest()
        ->get();

        $leaveType = LeaveType::latest()->get();
        return view('admin.attendance.pending-employee-leave')->
                                                    with(['employee_leaves' => $employee_leave,
                                                     'leaveTypes'=> $leaveType]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {

        $check_applied_leave = AddEmployeeLeave::where('id',$id)->first();

        if($check_applied_leave)
        {
            $check_applied_leave->status = 2;
            $check_applied_leave->status_by = Auth::user()->id;
            $check_applied_leave->status_dt = now();
            $check_applied_leave->reject_remark = $request->remark;
            $check_applied_leave->save();

            return response()->json(['success' => 'Employee Leave rejected successfully!']);
        }else{
            return response()->json(['error' => 'Something went wrong!']);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    public function ApproveLeave($id)
    {

        $check_applied_leave = AddEmployeeLeave::where('id',$id)->first();
        if($check_applied_leave)
        {

            // Get the attendance record for the employee for the specific month and financial year
            $get_attendance = Attendance::where('employee_id', $check_applied_leave->employee_id)
            ->where('month', $check_applied_leave->month)
            ->where('financial_year_id', $check_applied_leave->financial_year_id)
            ->first();

            if(!empty($get_attendance) && $get_attendance->total_absent_days - $get_attendance->total_leave < $check_applied_leave->no_of_days){
                return response()->json(['error' => 'No Absent Days Found']);
            }

            $check_leave_available = EmployeeLeaves::where('employee_id', $check_applied_leave->employee_id)
            ->where('leave_type_id', $check_applied_leave->leave_type_id)
            ->where('no_of_leaves', '>=', $check_applied_leave->no_of_days)->first();

            if(empty($check_leave_available)){
                return response()->json(['error' => 'No Leaves Found']);
            }

            if (!empty($get_attendance) && !empty($check_leave_available)) {
                // Update the attendance record
                $get_attendance->total_leave += $check_applied_leave->no_of_days;
                // $get_attendance->total_present_days -= $check_applied_leave->no_of_days;
                $get_attendance->save(); // Make sure to save the updated attendance record

                $check_leave_available->no_of_leaves -= $check_applied_leave->no_of_days;
                $check_leave_available->save();

                $check_applied_leave->status = 1;
                $check_applied_leave->status_by = Auth::user()->id;
                $check_applied_leave->status_dt = now();
                $check_applied_leave->save();

                return response()->json(['success' => 'Employee Leave approved successfully!']);
            }
        }else{
            return response()->json(['error' => 'Something went wrong!']);
        }
    }

}
