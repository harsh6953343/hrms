<?php

namespace App\Http\Requests\Admin\Masters;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateDeductionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $deductionId = $this->edit_model_id ?? null;
        return [
            // 'deduction' => 'required',
            'deduction'             => ['required', Rule::unique('deductions')->ignore($deductionId)->whereNull('deleted_at')],
            'deduction_in_marathi'  => ['required', Rule::unique('deductions')->ignore($deductionId)->whereNull('deleted_at')],
            'type' => 'required',
            'amount' => [
                'required',
                'integer',
                function ($attribute, $value, $fail) {
                    if ($this->input('type') == 2 && $value > 100) {
                        $fail('The percentage must be less than 100.');
                    }
                },
            ],
            'is_applicable' => 'nullable',
            'calculation'   => 'required',
        ];
    }
}
