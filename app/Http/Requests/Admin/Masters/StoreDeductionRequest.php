<?php

namespace App\Http\Requests\Admin\Masters;

use Illuminate\Foundation\Http\FormRequest;

class StoreDeductionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'deduction'             => 'required|unique:deductions,deduction,NULL,id,deleted_at,NULL',
            'deduction_in_marathi'  => 'required|unique:deductions,deduction_in_marathi,NULL,id,deleted_at,NULL',
            'type' => 'required',
            'amount' => [
                'required',
                'integer',
                function ($attribute, $value, $fail) {
                    if ($this->input('type') == 2 && $value > 100) {
                        $fail('The percentage must be less than 100.');
                    }
                },
            ],
            'is_applicable' => 'nullable',
            'calculation'   => 'required',
        ];
    }
}
