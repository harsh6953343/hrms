<?php

namespace App\Http\Requests\Admin\Masters;

use Illuminate\Foundation\Http\FormRequest;

class StoreLoanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'loan'              => 'required|unique:loans,loan,NULL,id,deleted_at,NULL',
            'loan_in_marathi'   => 'required|unique:loans,loan_in_marathi,NULL,id,deleted_at,NULL',
            'initial' => 'nullable',
        ];
    }
}
