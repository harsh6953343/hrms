<?php

namespace App\Http\Requests\Admin\Masters;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateClassRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $classId = $this->class->id ?? null;
        return [
            'employee_category' => 'required',
            'name' => [
                'required',
                Rule::unique('clas')
                    ->ignore($classId)
                    ->where(function ($query) {
                        return $query->where('employee_category', $this->employee_category)->whereNull('deleted_at');
                    }),
            ],
            'working_year' => 'required',
        ];
    }
}
