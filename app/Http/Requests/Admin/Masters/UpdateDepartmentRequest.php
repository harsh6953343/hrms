<?php

namespace App\Http\Requests\Admin\Masters;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateDepartmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $departmentId = $this->department->id ?? null;

        return [
            'ward_id' => 'required',
            'name' => 'required',
            // 'name' => ['required', Rule::unique('departments')->ignore($departmentId)->whereNull('deleted_at')],
            'initial' => 'nullable',
        ];
    }
}