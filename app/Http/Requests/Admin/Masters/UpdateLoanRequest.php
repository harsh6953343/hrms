<?php

namespace App\Http\Requests\Admin\Masters;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;


class UpdateLoanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $loanId = $this->edit_model_id ?? null;
        return [
            // 'loan' => 'required',
            'loan'              => ['required', Rule::unique('loans')->ignore($loanId)->whereNull('deleted_at')],
            'loan_in_marathi'   => ['required', Rule::unique('loans')->ignore($loanId)->whereNull('deleted_at')],
            'initial'           => 'nullable',
        ];
    }
}
