<?php

namespace App\Http\Requests\Admin\Employee;

use Illuminate\Foundation\Http\FormRequest;

class StoreIncrementAllowanceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'department_id'         => 'required',
            'clas_id'               => 'required',
            'allowance_id'          => 'required',
            'current_amount'        => 'required',
            'current_paytype'       => 'required',
            'new_amount'            => 'required',
            'applicable_date'       => 'required',
            'emp_id'                => 'nullable',
            'allowance'             => 'nullable',
            'allowance_unique_id'   => 'nullable',
        ];
    }
}