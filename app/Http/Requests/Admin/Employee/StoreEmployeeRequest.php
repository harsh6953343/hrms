<?php

namespace App\Http\Requests\Admin\Employee;

use Illuminate\Foundation\Http\FormRequest;

class StoreEmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'employee_id'   => 'required|unique:employees,employee_id,NULL,id,deleted_at,NULL',
            'fname'         => 'required',
            'mname'         => 'nullable',
            'lname'         => 'required',
            'gender'        => 'required',
            'dob'           => 'required',
            'doj'           => 'required',
            'mobile_number' => 'required|digits:10',
            'email'         => 'nullable',
            // 'email'         =>'required|email|unique:employees,email,NULL,id,deleted_at,NULL',
            'aadhar'        => 'required|numeric|digits:12',
            'pan'           => 'required|regex:/^[A-Z]{5}[0-9]{4}[A-Z]$/',
            // 'pf_account_no' => 'required|unique|numeric',
            'pf_account_no' => 'required|numeric',
            'caste'         => 'required',
            'caste1'        => 'required',
            'blood_group'   => 'required',
            'ccity'         => 'required',
            'caddress'      => 'required',
            'cstate'        => 'required',
            'cpincode'      => 'required',
            // 'is_applicable' =>'required',
            'pcity'         => 'required',
            'paddress'      => 'required',
            'pstate'        => 'required',
            'ppincode'      => 'required|numeric|digits:6',
            'ward_id'       => 'required',
            'department_id' => 'required',
            'working_department_id' => 'required',
            'clas_id'       => 'required',
            'designation_id' => 'required',
            'shift'         => 'required',
            'working_type'  => 'required',
            'retirement_date' => 'required|date',
            'increment_month' => 'required',

            // Bank Details
            'bank_id'       => 'required',
            'branch'        => 'required',
            'account_no'    => 'required|numeric',
            'ifsc'          => 'required',

            //Leave Details
            'leave_type_id' => 'nullable',
            'carry_forward' => 'nullable',
            'no_of_leaves'  => 'nullable',
            'encashable'    => 'nullable',

            // Qualification Details
            'qfrom'        => 'nullable',
            'qto'          => 'nullable',
            'qcertificate' => 'nullable',
            'qboard'       => 'nullable',
            'qmarks'       => 'nullable',
            'qdocument'    => 'nullable',

            // Experience Details
            'efrom'           => 'nullable',
            'eto'             => 'nullable',
            'ename_address'   => 'nullable',
            'edesignation'    => 'nullable',

            // Important Document Details
            'document_id'     => 'nullable',
            'impdocument'     => 'nullable',
            'impremark'       => 'nullable',
        ];
    }

    public function messages(): array
    {
        return [
            'employee_id.required' => 'The Employee ID field is required.',
            'employee_id.unique' => 'The Employee ID has already been taken.',
            'fname.required' => 'The First Name field is required.',
            'lname.required' => 'The Last Name field is required.',
            'gender.required' => 'The Gender field is required.',
            'dob.required' => 'The Date of Birth field is required.',
            'doj.required' => 'The Date of Joining field is required.',
            'mobile_number.required' => 'The Mobile Number field is required.',
            'mobile_number.digits' => 'The Mobile Number must be 10 digits.',
            // 'email.required' => 'The Email field is required.',
            // 'email.email' => 'The Email must be a valid email address.',
            // 'email.unique' => 'The Email has already been taken.',
            'aadhar.required' => 'The Aadhar field is required.',
            'aadhar.numeric' => 'The Aadhar must be a numeric value.',
            'aadhar.digits' => 'The Aadhar must be 12 digits.',
            'pan.required' => 'The PAN Card field is required.',
            'pan.regex' => 'The PAN Card must follow the specified format.',
            'caste.required' => 'The Caste field is required.',
            'blood_group.required' => 'The Blood Group field is required.',
            'ccity.required' => 'The City (Present Address) field is required.',
            'caddress.required' => 'The Present Address field is required.',
            'cstate.required' => 'The State (Present Address) field is required.',
            'cpincode.required' => 'The Pincode (Present Address) field is required.',
            // 'is_applicable.required' => 'The Is Applicable field is required.',
            'pcity.required' => 'The City (Permanent Address) field is required.',
            'paddress.required' => 'The Present Address field is required.',
            'pstate.required' => 'The State (Permanent Address) field is required.',
            'ppincode.required' => 'The Pincode (Permanent Address) field is required.',
            'ppincode.numeric' => 'The Pincode (Permanent Address) must be a numeric value.',
            'ppincode.digits' => 'The Pincode (Permanent Address) must be 6 digits.',
            'ward_id.required' => 'The Ward field is required.',
            'department_id.required' => 'The Department field is required.',
            'clas_id.required' => 'The Class field is required.',
            'designation_id.required' => 'The Designation field is required.',
            'shift.required' => 'The Shift field is required.',
            'working_type.required' => 'The Working Type field is required.',
            'retirement_date.required' => 'The Date of Retirement field is required.',
            'retirement_date.date' => 'The Date of Retirement must be a valid date.',
            'bank_id.required' => 'The Bank field is required.',
            'branch.required' => 'The Branch field is required.',
            'account_no.required' => 'The Account Number field is required.',
            'account_no.numeric' => 'The Account Number must be a numeric value.',
            'ifsc.required' => 'The IFSC Code field is required.',

        ];
    }
}
