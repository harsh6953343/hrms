<?php

namespace App\Http\Requests\Admin\Employee;

use Illuminate\Foundation\Http\FormRequest;

class StoreEmployeeFestivalAdvanceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'employee_id'       => 'required',
            'Emp_Code'          => 'required',
            'festival_name'     => 'required',
            'total_amount'      => 'required',
            'total_instalment'  => 'required',
            'instalment_amount' => 'required',
            'applicable_month'  => 'required',
            'start_date'        => 'required',
            'end_date'          => 'required',
            'pending_instalment'=> 'nullable',
            'financial_year_id' => 'nullable',
        ];
    }
}
