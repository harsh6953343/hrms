<?php

namespace App\Http\Requests\Admin\Employee;

use Illuminate\Foundation\Http\FormRequest;

class StoreEmployeeTransferDetails extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'employee_id'        => 'required|exists:employees,id',
            'Emp_Code'           => 'required|string',
            'current_ward'       => 'required|string',
            'current_department' => 'required|string',
            'ward_id'            => 'required|exists:wards,id',
            'department_id'      => 'required|exists:departments,id',
            'clas_id'            => 'required|exists:clas,id',
            'designation_id'     => 'nullable|exists:designations,id',
            'transfer_order'     => 'nullable|file|mimes:pdf,jpg,jpeg,png|max:2048', // Example: allowing only PDF or images up to 2MB
            'transfer_date'      => 'nullable|date',
            'remark'             => 'nullable|string|max:500', // Optional max length for remarks
        ];

    }
}
