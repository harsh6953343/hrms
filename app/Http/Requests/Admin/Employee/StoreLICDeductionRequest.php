<?php

namespace App\Http\Requests\Admin\Employee;

use Illuminate\Foundation\Http\FormRequest;

class StoreLICDeductionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'employee_id'       =>  'required',
            'Emp_Code'          =>  'required',
            'lic_name'          =>  'required',
            'lic_number'        =>  'required',
            'start_date'        =>  'required',
            'end_date'          =>  'required',
            'total_amt'         =>  'required',
            'installment_amt'   =>  'required',
            'total_installment' =>  'required',
            'deducted_installment'  =>  'nullable',
            'pending_installment'   =>  'nullable',
        ];
    }
}
