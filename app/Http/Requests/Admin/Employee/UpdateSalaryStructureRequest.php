<?php

namespace App\Http\Requests\Admin\Employee;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSalaryStructureRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'employee_id' => 'required',
            'Emp_Code' => 'required',
            'pay_scale_id' => 'required',
            'basic_salary' => 'required|integer',
            'grade_pay' => 'required',

            'employee_salary_id' => 'nullable',
            'allowance_id' => 'required',
            'allowance_amt' => 'required',
            'allowance_type' => 'required',
            'allowance_is_active' => 'required',
            'active_allowance_ids' => 'nullable',

            'deduction_id' => 'required',
            'deduction_amt' => 'required',
            'deduction_type' => 'required',
            'deduction_is_active' => 'required',
        ];
    }

    public function messages(): array
    {
        return [
            'Emp_Code' => 'The Employee Id is required',
            'Emp_Code.unique' => 'Salary already exist for this employee',
            'pay_scale_id' => 'The select Pay Scale is required.',
            'basic_salary' => 'The Basic Salary required',
            'grade_pay' => 'The Grade Pay is required',

            'allowance_is_active' => 'Please select at least one allowance',
            'deduction_is_active' => 'Please select at least one deduction',
        ];
    }
}
