<?php

namespace App\Http\Requests\Admin\Freeze;

use Illuminate\Foundation\Http\FormRequest;

class StoreFreezeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'ward_id'       => 'required',
            'month'         => 'required',
            'from_date'     => 'required',
            'to_date'       => 'required',
            'financial_year_id' => 'nullable',
        ];
    }
}
