<?php

namespace App\Http\Requests\Admin\PF;

use Illuminate\Foundation\Http\FormRequest;

class StoreOpeningBalanceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'employee_id'       =>  'required',
            'Emp_Code'          =>  'required',
            'pf_no'             =>  'required',
            'financial_year_id' =>  'required',
            'opening_balance'   =>  'required',
        ];
    }
}
