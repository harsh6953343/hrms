<?php

namespace App\Http\Requests\Admin\PF;

use Illuminate\Foundation\Http\FormRequest;

class StoreIntrestRateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'interest_rate' => 'required|numeric',
            'from_date'     => 'nullable',
            'to_date'       => 'nullable',
            'active_status' => 'nullable',
        ];
    }
}
