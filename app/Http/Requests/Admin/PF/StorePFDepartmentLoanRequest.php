<?php

namespace App\Http\Requests\Admin\PF;

use Illuminate\Foundation\Http\FormRequest;

class StorePFDepartmentLoanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'pf_account_no'     =>  'required',
            'Emp_Code'          =>  'required',
            'emp_name'          =>  'required',
            'ward'              =>  'required',
            'department'        =>  'required',
            'designation'       =>  'required',
            'class'             =>  'required',
            'employee_id'       =>  'required',


            'provident_fund_ids'    =>  'nullable',
            'other_amount'          =>  'nullable',
            'loan_date'             =>  'nullable',
            'loan_amt'              =>  'nullable',
            'remark'                =>  'nullable',

            // 'employee_id'           =>  'required',
            // 'Emp_Code'              =>  'required',
            // 'pf_account_no'         =>  'required',
            // 'current_month'         =>  'required',
            // 'salary_month'          =>  'required',
            // 'financial_year_id'     =>  'required',
        ];
    }
}
