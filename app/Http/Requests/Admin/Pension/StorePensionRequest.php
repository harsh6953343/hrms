<?php

namespace App\Http\Requests\Admin\Pension;

use Illuminate\Foundation\Http\FormRequest;

class StorePensionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'pension_id'            => 'nullable',
            'emp_name'              => 'required',
            'designation'           => 'required',
            'pay_type'              => 'nullable',
            'emp_retire_after_2016' => 'required',
            'is_emp_dr'             => 'nullable',
            'dob'                   => 'required',
            'age'                   => 'required',
            'retirement_date'       => 'required',
            'date_of_death'         => 'nullable',
            'account_number'        => 'required',
            'pension_bank_id'       => 'required',
            'branch_name'           => 'required',
            'ifsc_code'             => 'required',
            'basic_salary'          => 'required',
            'calculated_basic'      => 'required',
            'payable_pension'       => 'required',
            'da'                    => 'nullable',
            'da_applicabe'          => 'required',
            'sell_computation'      => 'required',
            'sell_date' => [
                'required_if:sell_computation,1'
            ],
            'deduct_amt' => [
                'required_if:sell_computation,1',
                'integer',
            ],
            'differance'            => 'nullable',
            'deduction'             => 'required',
            'business_allowances'   => 'nullable',
        ];
    }
}
