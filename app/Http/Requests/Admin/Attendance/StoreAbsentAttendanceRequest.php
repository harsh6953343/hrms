<?php

namespace App\Http\Requests\Admin\Attendance;

use Illuminate\Foundation\Http\FormRequest;

class StoreAbsentAttendanceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'employee_id'       =>  'required',
            'Emp_Code'          =>  'required',
            'emp_name'          =>  'required',
            'from_date'         =>  'required',
            'to_date'            =>  'required',
            'total_present_days' => 'required',
            'total_absent_days'  =>  'nullable',
            'total_half_days'    =>  'nullable',
            'month'             =>  'required',
            'financial_year_id' =>  'nullable',
            'attendance_type'   =>  'required',
        ];
    }
}
