<?php

namespace App\Http\Requests\Employee\DA;

use Illuminate\Foundation\Http\FormRequest;

class StoreDADifferanceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'DA_currentRate'    => 'required',
            'DA_newRate'        => 'required',
            'applicable_month'  => 'required'
        ];
    }
}
