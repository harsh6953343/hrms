<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreIncomeTaxRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'financial_year_id'      => 'required',
            'Emp_Code'               => 'required|unique:income_tax',
            'tax_regime'             => 'required',
            'prev_gross'             => 'nullable',
            'current_gross'          => 'required',
            'estimated_salary'       => 'nullable',
            'supplementary_bill'     => 'nullable',
            'estimated_supp_bill'    => 'nullable',
            'gross_income'           => 'required',
            'hre'                    => 'nullable',
            'prev_ptax'              => 'nullable',
            'elec_allowance'         => 'nullable',
            'trans_allowance'        => 'nullable',
            'standard_deduction'     => 'nullable',
            'ptax'                   => 'nullable',
            'ex_gratia'              => 'nullable',
            'total_exemptions'       => 'required',
            'investment'             => 'nullable',
            'other_investment'       => 'nullable',
            'other_income'           => 'nullable',
            'hial60d'                => 'nullable',
            'hial60p'                => 'nullable',
            'hiam60d'                => 'nullable',
            'hiam60p'                => 'nullable',
            'hcheckup'               => 'nullable',
            'hitotal'                => 'nullable',
            'sec80d'                 => 'nullable',
            'don80g'                 => 'nullable',
            'odon80g'                => 'nullable',
            'totaldon'               => 'nullable',
            'sec80g'                 => 'nullable',
            'nsc'                    => 'nullable',
            'lic'                    => 'nullable',
            'dcps_emp'               => 'nullable',
            'gis'                    => 'nullable',
            'pli'                    => 'nullable',
            'hloan'                  => 'nullable',
            'dcps_empr_contrn'       => 'nullable',
            'gpf'                    => 'nullable',
            'nsc_int'                => 'nullable',
            'other_lic'              => 'nullable',
            'mutual_fund'            => 'nullable',
            'other_gis'              => 'nullable',
            'other_pli'              => 'nullable',
            'other_hloan'            => 'nullable',
            'infra_bond'             => 'nullable',
            'ppf'                    => 'nullable',
            'edu_fees'               => 'nullable',
            'total80c'               => 'nullable',
            'sec80c'                 => 'nullable',
            'nps'                    => 'nullable',
            'dcps_u80ccd'            => 'nullable',
            'total80ccd'             => 'nullable',
            'sec80ccd'               => 'nullable',
            'edu_loan'               => 'nullable',
            'total80e'               => 'nullable',
            'sec80e'                 => 'nullable',
            'hlib24'                 => 'nullable',
            'ohlib24'                => 'nullable',
            'hlia24'                 => 'nullable',
            'ohlia24'                => 'nullable',
            'totalsec24'             => 'nullable',
            'sec24'                  => 'nullable',
            'medical_treatment'      => 'nullable',
            'total80ddb'             => 'nullable',
            'sec80ddb'               => 'nullable',
            'sb_int_ded'             => 'nullable',
            'total80tta'             => 'nullable',
            'sec80tta'               => 'nullable',
            'disability'             => 'nullable',
            'handicappedalwce'       => 'nullable',
            'total80u'               => 'nullable',
            'sec80u'                 => 'nullable',
            'empr_contribution'      => 'nullable',
            'total80cc2'             => 'nullable',
            'sec80ccd2'              => 'nullable',
            'taxable_sal'            => 'required',
            'tax_payable'            => 'nullable',
            'tax_after_cess'         => 'nullable',
            'tax_rem_months'         => 'nullable',
            'reason_requested_ded'   => 'nullable',
            'edu_cess'               => 'nullable',
            'tax_refunded'           => 'nullable',
            'tax_pending'            => 'nullable',
            'user_req_ded'           => 'nullable',
            'net_tax'                => 'required',
            'rebated_amount'         => 'nullable',
            'tax_paid_cash'          => 'nullable',
            'tax_deducted'           => 'nullable'
        ];
    }

    public function messages(): array
    {
        return [
            'financial_year_id.required' => 'Financial field is required.',
            'Emp_Code.unique' => 'The Employee ID data already present.',
            'Emp_Code.required' => 'The Employee ID field is required.',
            'tax_regime.required' => 'The tax regime field is required.',
            'current_gross.required' => 'The current gross field is required.',
            'taxable_sal.required' => 'The taxable salary field is required.',
            'net_tax.required' => 'The net tax field is required.'
        ];
    }
}
