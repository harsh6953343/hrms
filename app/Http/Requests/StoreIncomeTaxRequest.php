<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreIncomeTaxRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'financial_year_id'      => 'required',
            'Emp_Code'               => 'required|unique:income_tax',
            'tax_regime'             => 'required',
            'prev_gross'             => 'nullable',
            'current_gross'          => 'required',
            'hre'                    => 'nullable',
            'prev_ptax'              => 'nullable',
            'elec_allowance'         => 'nullable',
            'trans_allowance'        => 'nullable',
            'standard_deduction'     => 'nullable',
            'ptax'                   => 'nullable',
            'ex_gratia'              => 'nullable',
            'other_income'           => 'nullable',
            'sec80d'                 => 'nullable',
            'sec80g'                 => 'nullable',
            'nsc'                    => 'nullable',
            'lic'                    => 'nullable',
            'dcps_emp'               => 'nullable',
            'gis'                    => 'nullable',
            'pli'                    => 'nullable',
            'hloan'                  => 'nullable',
            'gpf'                    => 'nullable',
            'nsc_int'                => 'nullable',
            'other_lic'              => 'nullable',
            'mutual_fund'            => 'nullable',
            'other_gis'              => 'nullable',
            'other_pli'              => 'nullable',
            'other_hloan'            => 'nullable',
            'infra_bond'             => 'nullable',
            'ppf'                    => 'nullable',
            'edu_fees'               => 'nullable',
            'sec80c'                 => 'nullable',
            'sec80ccd'               => 'nullable',
            'sec80e'                 => 'nullable',
            'sec24'                  => 'nullable',
            'sec80ddb'               => 'nullable',
            'sec80tta'               => 'nullable',
            'sec80u'                 => 'nullable',
            'sec80ccd2'              => 'nullable',
            'taxable_sal'            => 'required',
            'net_tax'                => 'required',
            'rebated_amount'         => 'nullable',
            'tax_paid_cash'          => 'nullable',
            'tax_deducted'           => 'nullable'
        ];
    }

    public function messages(): array
    {
        return [
            'financial_year_id.required' => 'Financial field is required.',
            'Emp_Code.unique' => 'The Employee ID data already present.',
            'Emp_Code.required' => 'The Employee ID field is required.',
            'tax_regime.required' => 'The tax regime field is required.',
            'current_gross.required' => 'The current gross field is required.',
            'taxable_sal.required' => 'The taxable salary field is required.',
            'net_tax.required' => 'The net tax field is required.'
        ];
    }
}
