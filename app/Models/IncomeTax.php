<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IncomeTax extends Model
{
    protected $table = 'income_tax';

    protected $fillable = ['financial_year_id','Emp_Code','tax_regime','prev_gross','current_gross','hre','prev_ptax','elec_allowance','trans_allowance','standard_deduction','ptax','ex_gratia','other_income','sec80d','sec80g','nsc','lic','dcps_emp','gis','pli','hloan','gpf','nsc_int','other_lic','mutual_fund','other_gis','other_pli','other_hloan','infra_bond','ppf','edu_fees','sec80c','sec80ccd','sec80e','sec24','sec80ddb','sec80tta','sec80u','sec80ccd2','taxable_sal','net_tax','rebated_amount','tax_paid_cash','tax_deducted'];
}
