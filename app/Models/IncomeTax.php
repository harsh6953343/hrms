<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IncomeTax extends Model
{
    protected $table = 'income_tax';

    protected $fillable = ['financial_year_id','Emp_Code','tax_regime','prev_gross','current_gross','hre','prev_ptax','elec_allowance','trans_allowance','standard_deduction','ptax','ex_gratia','other_income','sec80d','sec80g','nsc','lic','dcps_emp','gis','pli','hloan','gpf','nsc_int','other_lic','mutual_fund','other_gis','other_pli','other_hloan','infra_bond','ppf','edu_fees','sec80c','sec80ccd','sec80e','sec24','sec80ddb','sec80tta','sec80u','sec80ccd2','taxable_sal','net_tax','rebated_amount','tax_paid_cash','tax_deducted','estimated_salary','supplementary_bill','estimated_supp_bill','gross_income','total_exemptions','investment','other_investment','hial60d','hial60p','hiam60d','hiam60p','hcheckup','hitotal','don80g','odon80g','totaldon','dcps_empr_contrn','total80c','nps','dcps_u80ccd','total80ccd','edu_loan','total80e','hlib24','ohlib24','hlia24','ohlia24','totalsec24','medical_treatment','total80ddb','sb_int_ded','total80tta','disability','handicappedalwce','total80u','empr_contribution','total80cc2','tax_payable','tax_after_cess','tax_rem_months','reason_requested_ded','edu_cess','tax_refunded','tax_pending','user_req_ded'];
}
