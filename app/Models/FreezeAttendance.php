<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class FreezeAttendance extends BaseModel
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['employee_id', 'Emp_Code', 'freeze_status', 'attendance_UId', 'ward_id', 'department_id', 'designation_id', 'clas_id', 'from_date', 'to_date', 'month', 'financial_year_id', 'present_day', 'basic_salary', 'actual_basic', 'grade_pay', 'allowance_Id', 'allowance_Amt', 'allowance_Type', 'festival_allowance', 'festival_allowance_id' ,'total_allowance', 'deduction_Id', 'deduction_Amt', 'deduction_Type', 'total_deduction', 'stamp_duty', 'loan_deduction_id', 'loan_deduction_amt', 'loan_deduction_bank_id', 'total_loan_deduction','lic_deduction_id', 'lic_deduction_amt', 'total_lic_deduction', 'festival_deduction_id', 'festival_deduction_amt', 'total_festival_deduction', 'net_salary', 'emp_name', 'pf_account_no', 'pay_band_scale', 'grade_pay_scale', 'date_of_birth', 'date_of_appointment', 'date_of_retirement', 'bank_account_number', 'phone_no', 'corporation_share_da', 'employee_share_da', 'salary_percentage','supplimentary_status','supplimentary_ids', 'da_differance', 'employee_da_differance_id'];

    public function employee()
    {
        return $this->hasOne(Employee::class, 'id', 'employee_id');
    }

    public function employee_status()
    {
        return $this->hasOne(EmployeeStatus::class, 'employee_id', 'employee_id');
    }

    public function designation()
    {
        return $this->hasOne(Designation::class, 'id', 'designation_id');
    }

    public function ward()
    {
        return $this->hasOne(Ward::class, 'id', 'ward_id');
    }

    public function department()
    {
        return $this->hasOne(Department::class, 'id', 'department_id');
    }

    public function freezeCron()
    {
        return $this->hasMany(FreezeCron::class, 'from_date', 'from_date');
    }



    public static function booted()
    {
        static::created(function (self $user) {
            if (Auth::check()) {
                self::where('id', $user->id)->update([
                    'created_by' => Auth::user()->id,
                ]);
            }
        });
        static::updated(function (self $user) {
            if (Auth::check()) {
                self::where('id', $user->id)->update([
                    'updated_by' => Auth::user()->id,
                ]);
            }
        });
        static::deleting(function (self $user) {
            if (Auth::check()) {
                self::where('id', $user->id)->update([
                    'deleted_by' => Auth::user()->id,
                ]);
            }
        });
    }
}
