<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class OldEmployeeSalary extends BaseModel
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['employee_id', 'Emp_Code', 'pay_scale_id', 'basic_salary', 'grade_pay', 'applicable_date', 'end_date'];

    public function employee()
    {
        return $this->hasOne(Employee::class, 'id', 'employee_id');
    }

    public function pay_scale()
    {
        return $this->hasOne(PayScale::class, 'id', 'pay_scale_id');
    }

    public function employee_allowances()
    {
        return $this->hasMany(OldEmployeeAllowance::class, 'old_employee_salary_id', 'id');
    }

    public function employee_deductions()
    {
        return $this->hasMany(OldEmployeeDeduction::class, 'old_employee_salary_id', 'id');
    }

    public static function booted()
    {
        static::created(function (self $user) {
            if (Auth::check()) {
                self::where('id', $user->id)->update([
                    'created_by' => Auth::user()->id,
                ]);
            }
        });
        static::updated(function (self $user) {
            if (Auth::check()) {
                self::where('id', $user->id)->update([
                    'updated_by' => Auth::user()->id,
                ]);
            }
        });

        static::deleting(function (EmployeeSalary $user) {

            EmployeeAllowance::where('employee_id', $user->id)->get()->each(function ($allowanceDetails) {
                $allowanceDetails->delete();
            });

            EmployeeDeduction::where('employee_id', $user->id)->get()->each(function ($deductionDetails) {
                $deductionDetails->delete();
            });

            if (Auth::check()) {
                EmployeeSalary::where('id', $user->id)->update([
                    'deleted_by' => Auth::user()->id,
                ]);
            }
        });
    }
}
