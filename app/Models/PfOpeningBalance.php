<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class PfOpeningBalance extends BaseModel
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['employee_id', 'Emp_Code', 'pf_no', 'financial_year_id', 'opening_balance', 'status', 'closing_status', 'type', 'closing_month', 'closing_date', 'remark'];

    public function financial_year()
    {
        return $this->hasOne(FinancialYear::class, 'id', 'financial_year_id');
    }

    public function employee()
    {
        return $this->hasOne(Employee::class, 'id', 'employee_id');
    }

    public static function booted()
    {
        static::created(function (self $user) {
            if (Auth::check()) {
                self::where('id', $user->id)->update([
                    'created_by' => Auth::user()->id,
                ]);
            }
        });
        static::updated(function (self $user) {
            if (Auth::check()) {
                self::where('id', $user->id)->update([
                    'updated_by' => Auth::user()->id,
                ]);
            }
        });
        static::deleting(function (self $user) {
            if (Auth::check()) {
                self::where('id', $user->id)->update([
                    'deleted_by' => Auth::user()->id,
                ]);
            }
        });
    }
}
