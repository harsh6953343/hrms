<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Relations\HasMany;


class Employee extends BaseModel
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['employee_id', 'fname', 'mname', 'lname', 'gender', 'dob', 'doj', 'mobile_number', 'email', 'aadhar', 'pan', 'caste', 'caste1', 'blood_group', 'ccity', 'caddress', 'cstate', 'cpincode', 'pcity', 'paddress', 'pstate', 'ppincode', 'ward_id', 'department_id', 'clas_id', 'designation_id', 'working_department_id', 'shift', 'working_type', 'retirement_date', 'bank_id', 'branch', 'account_no', 'ifsc', 'pf_account_no', 'increment_month', 'login_status', 'yearly_bonus', 'deleted_by'];

    public function ward()
    {
        return $this->hasOne(Ward::class, 'id', 'ward_id');
    }

    public function department()
    {
        return $this->hasOne(Department::class, 'id', 'department_id');
    }

    public function working_department()
    {
        return $this->hasOne(WorkingDepartment::class, 'id', 'working_department_id');
    }

    public function designation()
    {
        return $this->hasOne(Designation::class, 'id', 'designation_id');
    }

    public function class()
    {
        return $this->hasOne(Clas::class, 'id', 'clas_id');
    }

    public function status()
    {
        return $this->hasOne(Status::class, 'id', 'status_id');
    }

    public function employee_status()
    {
        return $this->hasOne(EmployeeStatus::class);
    }

    public function salary()
    {
        return $this->hasOne(EmployeeSalary::class);
    }

    public function old_salary()
    {
        return $this->hasOne(OldEmployeeSalary::class);
    }

    public function employee_allowances()
    {
        return $this->hasMany(EmployeeAllowance::class);
    }

    public function employee_deductions()
    {
        return $this->hasMany(EmployeeDeduction::class);
    }

    public function freeze_attendance()
    {
        return $this->hasMany(FreezeAttendance::class);
    }

    public static function booted()
    {
        static::created(function (self $user) {
            if (Auth::check()) {
                self::where('id', $user->id)->update([
                    'created_by' => Auth::user()->id,
                ]);
            }
        });
        static::updated(function (self $user) {
            if (Auth::check()) {
                self::where('id', $user->id)->update([
                    'updated_by' => Auth::user()->id,
                ]);
            }
        });
        static::deleting(function (Employee $user) {

            EmployeeLeaves::where('employee_id', $user->id)->get()->each(function ($leaveDetails) {
                $leaveDetails->delete();
            });

            AcademicDetails::where('employee_id', $user->id)->get()->each(function ($academicDetails) {
                $academicDetails->delete();
            });

            Experience::where('employee_id', $user->id)->get()->each(function ($experienceDetails) {
                $experienceDetails->delete();
            });


            if (Auth::check()) {
                Employee::where('id', $user->id)->update([
                    'deleted_by' => Auth::user()->id,
                ]);
            }
        });
    }
}
